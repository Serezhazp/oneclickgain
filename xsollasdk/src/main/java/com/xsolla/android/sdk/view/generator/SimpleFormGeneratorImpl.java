package com.xsolla.android.sdk.view.generator;

import android.content.Context;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;
import com.xsolla.android.sdk.api.model.сheckout.XForm;
import com.xsolla.android.sdk.util.validator.BaseValidator;
import com.xsolla.android.sdk.util.validator.EmptyValidator;
import com.xsolla.android.sdk.util.validator.NoValidator;
import com.xsolla.android.sdk.view.adapter.FormElementsAdapter;
import com.xsolla.android.sdk.view.widget.text.BaseWatcher;

import java.util.ArrayList;
import java.util.HashMap;

public class SimpleFormGeneratorImpl implements ICheckoutGenerator {

    private XDirectpayment directpayment;
    private ArrayList<BaseWatcher> watchers;


    public SimpleFormGeneratorImpl() {
        watchers = new ArrayList<>();
    }

    @Override
    public View generate(Context context, XDirectpayment form, HashMap<String, String> translations) {
        directpayment = form;
        FormElementsAdapter adapter = new FormElementsAdapter(context, form.getForm());
        return initLinearLayout(context, adapter);
    }

    private static View initListView(Context context, FormElementsAdapter adapter) {
        ListView listView = new ListView(context);
        listView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        listView.setAdapter(adapter);
        return listView;
    }

    private View initLinearLayout(Context context, FormElementsAdapter adapter) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        int padding = (int) context.getResources().getDimension(R.dimen.xsolla_base_16);
        linearLayout.setPadding(0, padding, 0, padding);
        for (int i = 0; i < adapter.getCount(); i++) {
            View newField = adapter.getView(i, null, null);
//            newField.findViewById();
            EditText ed = (EditText) newField.findViewById(R.id.editText);
            if(ed != null) {
                BaseValidator validator;
                if(!"couponCode".equals(adapter.getItem(i).getName()))
                    validator = new EmptyValidator();
                else
                    validator = new NoValidator();
                validator.setErrorMsg("Can not be empty");
                addWatcher(directpayment.getForm(), ed, validator);
            }
            linearLayout.addView(newField);
        }
        return linearLayout;
    }

    private BaseWatcher addWatcher(final XForm xsollaForm, EditText editText, BaseValidator validator) {
        BaseWatcher watcher = new BaseWatcher(editText, validator) {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                xsollaForm.updateElement(mEditText.getTag().toString(), s.toString());
            }
        };
        editText.addTextChangedListener(watcher);
        watchers.add(watcher);
        return watcher;
    }

    @Override
    public String validate() {
        for (BaseWatcher bw : watchers) {
            if (!bw.validate())
                return bw.getErrorMessage();
        }
        return null;
    }
}
