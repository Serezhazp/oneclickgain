package com.xsolla.android.sdk.api.model.shop;

/**
 *
 */
public class XBonusItem {
    private String name;    //"name":"Egg",
    private String quantity;//"quantity":"1",

    public XBonusItem(String name, String quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public String getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "XBonusItem{" +
                "name='" + name + '\'' +
                ", quantity='" + quantity + '\'' +
                '}';
    }
}
