package com.xsolla.android.sdk.api.model;

import com.xsolla.android.sdk.XsollaObject;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 */
public class XError extends XsollaObject implements IParseble {

    String  supportCode;
    String  message;

    public XError() {
    }

    public XError(String supportCode, String message) {
        this.supportCode = supportCode;
        this.message = message;
    }

    public void parseMany(JSONObject jobj){
        JSONArray jarr = jobj.optJSONArray("errors");
        for(int i = 0; i < jarr.length(); i++) {
            parse(jarr.optJSONObject(i));
        }
    }

    public String getSupportCode() {
        return supportCode;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public void parse(JSONObject jobj) {
        supportCode = jobj.optString("support_code");
        message     = jobj.optString("message");
    }

    @Override
    public String toString() {
        return supportCode + " : " + message;
    }
}
