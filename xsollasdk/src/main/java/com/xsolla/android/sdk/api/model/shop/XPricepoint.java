package com.xsolla.android.sdk.api.model.shop;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XPricepoint implements IParseble {
    private int     out;// "out":10000,
    private int     outWithoutDiscount;// "outWithoutDiscount":10000,
    private int     bonusOut;// "bonusOut":0,
    private float   sum;// "sum":49.99,
    private float   sumWithoutDiscount;// "sumWithoutDiscount":49.99,
    private String  currency;// "currency":"USD",
    private String  image;// "image":"\/\/cdn.xsolla.com\/img\/misc\/images\/ea69c4b2085f7203f1e947807e2f86f0.png",
    private String  description;// "description":"+50% extra",
    private String  longDescription;// "longDescription":"",
    private String  label;// "label":"Best Value",  RECOMMENDED = option_recommended_default ||  BEST DEAL = option_best_deal_default
    private String  advertisementType;// "advertisementType":"best_deal",
    private String  offerLabel;// "offerLabel":""

    private ArrayList<XBonusItem> listBonusItems;// "bonusItems":[{"name":"Name","quantity":"Action",},],

    public XPricepoint() {
        listBonusItems = new ArrayList<>();
    }

    public int getOut() {
        return out;
    }

    public String getDescription() {
        return description;
    }

    public int getOutWithoutDiscount() {
        return outWithoutDiscount;
    }

    public String getImage() {
        return image.startsWith("http") ? image : "https:" + image;
    }

    public boolean isPriceChanged() {
        return sum != sumWithoutDiscount;
    }

    public String getPrice() {
        return PriceFormatter.formatPrice(currency, Float.toString(sum));
    }

    public String getPriceWithoutDiscount() {
        return PriceFormatter.formatPrice(currency, Float.toString(sumWithoutDiscount));
    }

    public int getLabelType() {
        if(isBonus() || isPriceChanged())
            return 2;
        switch (advertisementType) {
            case "recommended":
                return 0;
            case "best_deal":
                return 1;
            case "special_offer":
                return 2;
            default:
                return -1;
        }
    }

    public boolean isBonus(){
        return bonusOut > 0 || listBonusItems.size() > 0;
    }

    public String getBonusString(String projectVirtualCurrency) {
        StringBuilder builder = new StringBuilder("+");
        if(bonusOut != 0) {
            builder.append(getBonusOut(projectVirtualCurrency))
                    .append(", ");
            if(listBonusItems.size() > 0)
                builder.append(getBonusItems());
        } else {
            builder.append(getBonusItems());
        }
        return builder.toString();
    }

    public String getBonusOut(String projectVirtualCurrency) {
        return bonusOut + " " + projectVirtualCurrency;
    }

    public String getBonusItems() {
        StringBuilder builder = new StringBuilder();
        for(XBonusItem bonusItem : listBonusItems) {
            builder.append(bonusItem.getQuantity()).append(" ").append(bonusItem.getName()).append(", ");
        }
        int size = builder.length();
        builder.delete(size - 2, size - 1);
        return builder.toString();
    }

    @Override
    public void parse(JSONObject jobj) {
        out = jobj.optInt("out");
        outWithoutDiscount = jobj.optInt("outWithoutDiscount");
        bonusOut = jobj.optInt("bonusOut");

        sum = (float) jobj.optDouble("sum");
        sumWithoutDiscount = (float) jobj.optDouble("sumWithoutDiscount");

        currency = jobj.optString("currency");
        image = jobj.optString("image");
        description = jobj.optString("description");
        longDescription = jobj.optString("longDescription");
        label = jobj.optString("label");
        advertisementType = jobj.optString("advertisementType");
        offerLabel = jobj.optString("offerLabel");

        JSONArray jarr = jobj.optJSONArray("bonusItems");
        if(jarr != null)
            for(int i = 0; i < jarr.length(); i++) {
                JSONObject jobjNewItem = jarr.optJSONObject(i);
                XBonusItem newBonusItem = new XBonusItem(jobjNewItem.optString("name"), jobjNewItem.optString("quantity"));
                listBonusItems.add(newBonusItem);
            }
    }

    @Override
    public String toString() {
        return "XPricepoint{" +
                "out=" + out +
                ", outWithoutDiscount=" + outWithoutDiscount +
                ", bonusOut=" + bonusOut +
                ", sum=" + sum +
                ", sumWithoutDiscount=" + sumWithoutDiscount +
                ", currency='" + currency + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", longDescription='" + longDescription + '\'' +
                ", label='" + label + '\'' +
                ", advertisementType='" + advertisementType + '\'' +
                ", offerLabel='" + offerLabel + '\'' +
                ", listBonusItems=" + listBonusItems +
                '}';
    }
}
