package com.xsolla.android.sdk.api.model.сheckout;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 */
public class XSummary implements IParseble {

    XPurchase purchase;// "purchase" : {},
    XFinance finance;// "finance" : {}

    public XSummary() {
        purchase    = new XPurchase();
        finance     = new XFinance();
    }

    public XPurchase getPurchase() {
        return purchase;
    }

    public XFinance getFinance() {
        return finance;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONObject jobjPurchase = jobj.optJSONObject("purchase");
        if(jobjPurchase != null)
            purchase.parse(jobjPurchase);

        JSONObject jobjFinance = jobj.optJSONObject("finance");
        if(jobjFinance != null)
            finance.parse(jobjFinance);
    }

    @Override
    public String toString() {
        return "\nXSummary{" +
                "purchase=" + purchase +
                ", finance=" + finance +
                '}';
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     *
     * */
    public class XPurchase implements IParseble {
        private ArrayList<XPurchaseItemBase>    virtualItems;
        private ArrayList<XPurchaseItemBase>    pricepoints;
        private ArrayList<XSubscription>        subscriptions;
        private XPurchaseItemBase               checkout;

        public XPurchase() {
            this.virtualItems   = new ArrayList<>();
            this.pricepoints    = new ArrayList<>();
            this.subscriptions  = new ArrayList<>();
        }

        public ArrayList<XPurchaseItem> getList(String subscriptionsDescText){
            ArrayList<XPurchaseItem> list = new ArrayList<>();
            if(checkout != null)
                list.add(new XPurchaseItem(checkout));
            for(XPurchaseItemBase item : virtualItems){
                list.add(new XPurchaseItem(item));
            }
            for(XPurchaseItemBase item : pricepoints){
                list.add(new XPurchaseItem(item));
            }
            for(XSubscription item : subscriptions){
                list.add(new XPurchaseItem(item, subscriptionsDescText));
            }
            return list;
        }

        @Override
        public void parse(JSONObject jobj) {


            JSONObject jobjCheckout = jobj.optJSONObject("checkout");
            if(jobjCheckout != null) {
                checkout = new XPurchaseItemBase();
                checkout.parse(jobjCheckout);
            }

            JSONArray jarrVItems = jobj.optJSONArray("virtual_items");
            if(jarrVItems != null)
                for (int i = 0; i < jarrVItems.length(); i++) {
                    JSONObject jobjVItem = jarrVItems.optJSONObject(i);
                    XPurchaseItemBase newItem = new XPurchaseItemBase();
                    newItem.parse(jobjVItem);
                    virtualItems.add(newItem);
                }

            JSONArray jarrPp = jobj.optJSONArray("virtual_currency");
            if(jarrPp != null)
                for (int i = 0; i < jarrPp.length(); i++) {
                    JSONObject jobjPp = jarrPp.optJSONObject(i);
                    XPurchaseItemBase newItem = new XPurchaseItemBase();
                    newItem.parse(jobjPp);
                    pricepoints.add(newItem);
                }

            JSONArray jarrSubs = jobj.optJSONArray("subscriptions");
            if(jarrSubs != null)
                for (int i = 0; i < jarrSubs.length(); i++) {
                    JSONObject jobjVItem = jarrSubs.optJSONObject(i);
                    XSubscription newItem = new XSubscription();
                    newItem.parse(jobjVItem);
                    subscriptions.add(newItem);
                }

        }

        @Override
        public String toString() {
            return "\n\tXPurchase{" +
                    "virtualItems=" + virtualItems +
                    ", pricepoints=" + pricepoints +
                    ", subscriptions=" + subscriptions +
                    '}';
        }


         /* * * * * * * * * * * * * * * * * * * * * * * * * * *
        * INNER CLASSES
        * * * * * * * * * * * * * * * * * * * * * * * * * * */
        /**
         *
         * */
        class XPurchaseItemBase implements IParseble {
            private double  amount;// "amount" : 1,
            private int     quantity;// "quantity":1,
            private String  currency;// "currency" : "USD",
            private String  name;// "name" : "Sheaf",
            private String  imageUrl;//"image_url" : "\/\/cdn3.xsolla.com\/img\/misc\/merchant-digital-goods\/93d7db52618d577f0d5a957959c442c3.png",
            private String  description;// "description" : "Sheaf, a large bundle in which cereal plants are bound after reaping.",
            private String  longDescription;// "longDescription" : "Sheaf (agriculture), a large bundle in which cereal plants are bound after reaping.",
            private boolean isBonus;// "is_bonus" : false

            public String getName() {
                return name;
            }

            public String getImageUrl() {
                if("".equals(imageUrl))
                    return "";
                else
                    return imageUrl.startsWith("http") ? imageUrl : "https:" + imageUrl;
            }

            public String getDescription() {
                return "null".equals(description) ? "" : description;
            }

            public String getPrice() {
                if(!isBonus)
                    return PriceFormatter.formatPrice(currency, Double.toString(amount));
                else
                    return PriceFormatter.formatPrice(currency, "0");

            }

            public boolean isBonus() {
                return isBonus;
            }

            @Override
            public void parse(JSONObject jobj) {
                this.amount             = jobj.optDouble("amount");
                this.quantity           = jobj.optInt("quantity");

                this.currency           = jobj.optString("currency");
                this.name               = jobj.optString("name");
                this.imageUrl           = jobj.optString("image_url");
                this.description        = jobj.optString("description");
                this.longDescription    = jobj.optString("longDescription");

                this.isBonus            = jobj.optBoolean("is_bonus");
            }

            @Override
            public String toString() {
                return "\n\t\tXPurchaseItemBase{" +
                        "amount=" + amount +
                        ", quantity=" + quantity +
                        ", currency='" + currency + '\'' +
                        ", name='" + name + '\'' +
                        ", imageUrl='" + imageUrl + '\'' +
                        ", description='" + description + '\'' +
                        ", longDescription='" + longDescription + '\'' +
                        ", isBonus=" + isBonus +
                        '}';
            }
        }

        /**
         *
         * */
        class XSubscription implements IParseble {
            private double  amount;// "amount":0.99,
            private int     period;// "period" : 1,
            private String  currency;// "currency" : "USD",
            private String  description;// "description" : "Silver Status",
            private String  packageInfo;// "package_info" : "2x more experience!",
            private String  periodType;// "period_type" : "month",
            private String  expirationPeriodType;// "expiration_period_type" : "day",
            private String  recurrentType;// "recurrent_type" : "charge",
            private String  dateNextCharge;// "date_next_charge" : "2016-01-23",
            private String  amountNextCharge;// "amount_next_charge" : "0.99",
            private String  currencyNextCharge;// "currency_next_charge" : "USD"

            public String getName() {
                return description;
            }

            public String getImageUrl() {
                return "";
            }

            public String getDescription() {
                return dateNextCharge;
            }

            public String getDateNextCharge() {
                return dateNextCharge;
            }

            public String getPrice() {
                return PriceFormatter.formatPrice(currency, Double.toString(amount));
            }

            @Override
            public void parse(JSONObject jobj) {
                this.amount                 = jobj.optDouble("amount");
                this.period                 = jobj.optInt("period");

                this.currency               = jobj.optString("currency");
                this.description            = jobj.optString("description");
                this.packageInfo            = jobj.optString("package_info");
                this.periodType             = jobj.optString("period_type");
                this.expirationPeriodType   = jobj.optString("expiration_period_type");
                this.recurrentType          = jobj.optString("recurrent_type");
                this.dateNextCharge         = jobj.optString("date_next_charge");
                this.amountNextCharge       = jobj.optString("amount_next_charge");
                this.currencyNextCharge     = jobj.optString("currency_next_charge");
            }

            @Override
            public String toString() {
                return "\n\t\tXSubscription{" +
                        "amount=" + amount +
                        ", period=" + period +
                        ", currency='" + currency + '\'' +
                        ", description='" + description + '\'' +
                        ", packageInfo='" + packageInfo + '\'' +
                        ", periodType='" + periodType + '\'' +
                        ", expirationPeriodType='" + expirationPeriodType + '\'' +
                        ", recurrentType='" + recurrentType + '\'' +
                        ", dateNextCharge='" + dateNextCharge + '\'' +
                        ", amountNextCharge='" + amountNextCharge + '\'' +
                        ", currencyNextCharge='" + currencyNextCharge + '\'' +
                        '}';
            }
        };

        public class XPurchaseItem {
            private String imgUrl;
            private String title;
            private String description;
            private String price;
            private boolean isBonus;

            public XPurchaseItem(XPurchaseItemBase item) {
                imgUrl      = item.getImageUrl();
                title       = item.getName();
                description = item.getDescription();
                price       = item.getPrice();
                isBonus     = item.isBonus();
            }

            public XPurchaseItem(XSubscription subscription, String descText) {
                imgUrl      = subscription.getImageUrl();
                title       = subscription.getName();
                description = descText.replace("{{date}}", subscription.getDescription());
                price       = subscription.getPrice();
                isBonus     = false;
            }

            public String getImgUrl() {
                return imgUrl;
            }

            public String getTitle() {
                return title;
            }

            public String getDescription() {
                return description;
            }

            public String getPrice() {
                return price;
            }

            public boolean isBonus() {
                return isBonus;
            }
        }
        
    };

    /**
     *
     * */
    public class XFinance implements IParseble {
        final static String DISCOUNT        = "discount";
        final static String FEE             = "fee";
        final static String SALES_TAX       = "sales_tax";
        final static String TOTAL           = "total";
        final static String VAT             = "vat";
        final static String USER_BALANCE    = "user_balance";

        private XFinanceItem                        subTotal;// "sub_total" :{},
        private HashMap<String, XFinanceItemBase>   mapBase;//discount, fee, salesTax, total, vat, userBalance; //"discount" : {}, //"fee" : {}, "sales_tax" : {},  "total" : {}, "vat" : {}, "user_balance" : {}

        public XFinance() {
            subTotal    = new XFinanceItem();
            mapBase     = new HashMap<>(6);
//            this.discount       = new XFinanceItemBase();
//            this.fee            = new XFinanceItemBase();
//            this.salesTax       = new XFinanceItemBase();
//            this.total          = new XFinanceItemBase();
//            this.vat            = new XFinanceItemBase();
//            this.userBalance    = new XFinanceItemBase();
        }

        public XFinanceItemBase getSubTotal() {
            return subTotal;
        }

        public XFinanceItemBase getTotal() {
            return mapBase.get(TOTAL);
        }

        public XFinanceItemBase getDiscount() {
            return mapBase.get(DISCOUNT);
        }

        public XFinanceItemBase getFee() {
            return mapBase.get(FEE);
        }

        public XFinanceItemBase getTax() {
            return mapBase.get(SALES_TAX);
        }

        public XFinanceItemBase getVat() {
            return mapBase.get(VAT);
        }

        public XFinanceItemBase getUserBalance() {
            return mapBase.get(USER_BALANCE);
        }


        public HashMap<String, XFinanceItemBase> getMapBase() {
            return mapBase;
        }

        @Override
        public void parse(JSONObject jobj) {
            for(Iterator<String> iter = jobj.keys(); iter.hasNext();) {
                String key = iter.next();
                JSONObject jobjElem = jobj.optJSONObject(key);
                if(jobjElem != null)
                    if(!"sub_total".equals(key)) {
                        XFinanceItemBase newItem = new XFinanceItemBase();
                        newItem.parse(jobjElem);
                        mapBase.put(key, newItem);
                    } else {
                        subTotal.parse(jobjElem);
                    }
            }
        }

        @Override
        public String toString() {
            return "\n\tXFinance{" +
                    "subTotal=" + subTotal +
                    ", mapBase=" + mapBase +
                    '}';
        }

         /* * * * * * * * * * * * * * * * * * * * * * * * * * *
        * INNER CLASSES
        * * * * * * * * * * * * * * * * * * * * * * * * * * */
        /**
         *
         * */
        public class XFinanceItemBase implements IParseble {
            protected double amount;// "amount":0,
            protected String currency;// "currency" : "USD"

            public double getAmount() {
                return amount;
            }

            public String getPrice(){
                return PriceFormatter.formatPrice(currency, Double.toString(amount));
            }

            @Override
            public void parse(JSONObject jobj) {
                this.amount     = jobj.optDouble("amount");
                this.currency   = jobj.optString("currency");
            }

            @Override
            public String toString() {
                return "\n\t\tXFinanceItemBase{" +
                        "amount=" + amount +
                        ", currency='" + currency + '\'' +
                        '}';
            }
        }

        /**
         *
         * */
        public class XFinanceItem extends XFinanceItemBase {
            private double paymentAmount;// "payment_amount" : 1,
            private String paymentCurrency;// "payment_currency" : "USD"

            public String getPrice(){
                if(currency != null) {
                    if (currency.equals(paymentCurrency)) {
                        return super.getPrice();
                    } else {
                        return PriceFormatter.formatPrice(currency, Double.toString(amount))
                                + "="
                                + PriceFormatter.formatPrice(paymentCurrency, Double.toString(paymentAmount));
                    }
                } else {
                    return super.getPrice();
                }
            }

            @Override
            public void parse(JSONObject jobj) {
                super.parse(jobj);
                this.paymentAmount     = jobj.optDouble("payment_amount");
                this.paymentCurrency   = jobj.optString("payment_currency");
            }

            @Override
            public String toString() {
                return "\n\t\tXFinanceItem{" +
                        "amount=" + amount +
                        ", currency='" + currency + '\'' +
                        ", paymentAmount=" + paymentAmount +
                        ", paymentCurrency='" + paymentCurrency + '\'' +
                        '}';
            }
        }

    };
}
