package com.xsolla.android.sdk.view.adapter;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItem;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemGroup;
import com.xsolla.android.sdk.util.ImageLoader;
import com.xsolla.android.sdk.view.fragment.ItemFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class VirtualItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_GROUP = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<Object> mListItems;
    private String[]          mLocalTranslations;//virtual_item_option_button//option_description_expand//option_description_collapse//
    private String[]          mOfferStrings;//option_recommended_default//option_best_deal_default//option_offer
    private String            mVcImageUrl;
    private OnListFragmentInteractionListener mListener;

    public VirtualItemsAdapter(ArrayList<Object> mListItems, OnListFragmentInteractionListener listener) {
        this.mListItems = mListItems;
        this.mListener = listener;
        mLocalTranslations = new String[]{"Buy", "Learn more", "Collapse", "Coins"};
        mOfferStrings       = new String[]{"ADVICE", "BEST", "OFFER"};
    }

    public VirtualItemsAdapter(ArrayList<Object> mListItems, OnListFragmentInteractionListener listener, String[] localTranslations, String[] offerStrings, String vcImageUrl) {
        this.mListItems = mListItems;
        this.mListener = listener;
        this.mLocalTranslations = localTranslations;
        this.mOfferStrings = offerStrings;
        this.mVcImageUrl = vcImageUrl;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.xsolla_shop_vitem, parent, false);
            //inflate your layout and pass it to view holder
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.xsolla_shop_vigroup, parent, false);
            //inflate your layout and pass it to view holder
            return new ViewHolderGroup(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            renderVItem((ViewHolder)holder, position);
        } else if (holder instanceof ViewHolderGroup) {
            renderVGroup((ViewHolderGroup)holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(getItem(position) instanceof XVirtualItemGroup)
            return TYPE_GROUP;
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    private Object getItem(int position) {
        return mListItems.get(position);
    }

    public void renderVItem(final ViewHolder holder, int position) {
        XVirtualItem item = (XVirtualItem)getItem(position);
        holder.mVirtualItem = item;

        holder.showOfferLabel(item.getLabelType());
        ImageLoader.getInstance().loadImage(holder.mImage, item.getImgUrl());
        holder.mTvTitle.setText(item.getName());

        if(!"".equals(item.getDescription()) || !"".equals(item.getLongDescription()))
            holder.mTvDescription.setText(item.getDescription());
        else
            holder.mTvDescription.setVisibility(View.GONE);

        if(item.isBonus()) {
            holder.mTvBonus.setVisibility(View.VISIBLE);
            holder.mTvBonus.setText(item.getBonusString("Coins"));
        } else {
            holder.mTvBonus.setVisibility(View.GONE);
        }


        if(!item.isVirtualPayment()) {
            holder.mTvBuy.setVisibility(View.VISIBLE);
            holder.mVcImage.setVisibility(View.GONE);
            holder.mTvBuy.setText(mLocalTranslations[0]);
            if (item.isPriceChanged()) {
                holder.mTvPriceOld.setVisibility(View.VISIBLE);
                holder.mTvPriceOld.setText(item.getPriceWithoutDiscount());
                holder.mTvPriceOld.setPaintFlags(holder.mTvPriceOld.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.mTvPrice.setText(item.getPrice());
            } else {
                holder.mTvPriceOld.setVisibility(View.GONE);
                holder.mTvPrice.setText(item.getPriceWithoutDiscount());
            }
        } else {
            holder.mTvPrice.setText(item.getVcAmount());
            if(mVcImageUrl != null && !"".equals(mVcImageUrl)) {
                holder.mVcImage.setVisibility(View.VISIBLE);
                holder.mTvBuy.setVisibility(View.GONE);
                ImageLoader.getInstance().loadImage(holder.mVcImage, mVcImageUrl);
            } else {
                holder.mVcImage.setVisibility(View.GONE);
                holder.mTvBuy.setVisibility(View.VISIBLE);
                holder.mTvBuy.setText(mLocalTranslations[3]);
            }
        }


        String longDescription = item.getLongDescription().trim();
        if(longDescription != null && !"".equals(longDescription)) {
            holder.mTvLearnMore.setVisibility(View.VISIBLE);
            holder.mTvLearnMore.setText(mLocalTranslations[1]);
            holder.mTvLearnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.mTvLearnMore.getText().equals(mLocalTranslations[1])) {
                        holder.mTvLearnMore.setText(mLocalTranslations[2]);
                        holder.mTvDescription.setMaxLines(999);
                        holder.mTvDescription.setText(holder.mVirtualItem.getLongDescription());
                    } else {
                        holder.mTvLearnMore.setText(mLocalTranslations[1]);
                        holder.mTvDescription.setMaxLines(2);
                        holder.mTvDescription.setText(holder.mVirtualItem.getDescription());
                    }
                }
            });
        } else {
            holder.mTvLearnMore.setVisibility(View.GONE);
        }

        ((View)holder.mTvBuy.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> map = new HashMap<String, Object>(1);
                XVirtualItem item = holder.mVirtualItem;
                String key = "sku[" + item.getSku() + "]";
                map.put(key, 1);
                mListener.onOnProductSelected(map, item.isVirtualPayment());
            }
        });

    }

    public void renderVGroup(final ViewHolderGroup holder, int position) {
        XVirtualItemGroup item = (XVirtualItemGroup)getItem(position);
        holder.mGroup = item;
        holder.mTvTitle.setText(item.getName());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View       mView, mImageBorder;
        public final ImageView  mImage, mVcImage;
        public final TextView   mOfferLable, mTvTitle, mTvDescription, mTvLearnMore, mTvBonus, mTvPrice, mTvPriceOld,  mTvBuy;
        public XVirtualItem     mVirtualItem;

        public ViewHolder(View itemView) {
            super(itemView);
            mView               = itemView;

            mImageBorder        = itemView.findViewById(R.id.xsolla_offer_label_border);
            mOfferLable         = (TextView) itemView.findViewById(R.id.xsolla_tv_offer_label);

            mImage              = (ImageView) itemView.findViewById(R.id.xsolla_iv);
            mVcImage            = (ImageView) itemView.findViewById(R.id.xsolla_vc_image);
            mTvTitle            = (TextView) itemView.findViewById(R.id.xsolla_tv_title);
            mTvDescription      = (TextView) itemView.findViewById(R.id.xsolla_tv_description);
            mTvLearnMore        = (TextView) itemView.findViewById(R.id.xsolla_tv_learn_more);
            mTvBonus            = (TextView) itemView.findViewById(R.id.xsolla_tv_bonus);
            mTvPrice            = (TextView) itemView.findViewById(R.id.xsolla_tv_price);
            mTvPriceOld         = (TextView) itemView.findViewById(R.id.xsolla_tv_price_old);
            mTvBuy              = (TextView) itemView.findViewById(R.id.xsolla_tv_buy);
        }

        public void showOfferLabel(int offerType) {
            int offerColor      = Color.LTGRAY;
            String offerText    = "";
            int visibility      = View.VISIBLE;
            switch (offerType) {
                case 0:
                    offerColor = Color.parseColor("#ff809efc");//R.color.xsolla_advice
                    offerText = mOfferStrings[0];
                    break;
                case 1:
                    offerColor = Color.parseColor("#ff9896de");//R.color.xsolla_best
                    offerText = mOfferStrings[1];
                    break;
                case 2:
                    offerColor = Color.parseColor("#fffcb07f");//R.color.xsolla_special
                    offerText = mOfferStrings[2];
                    break;
                default:
                    visibility = View.GONE;
            }
            mImageBorder.setBackgroundColor(offerColor);
            mOfferLable.setBackgroundColor(offerColor);
            mOfferLable.setText(offerText);
            mOfferLable.setVisibility(visibility);
        }

    }

    class ViewHolderGroup extends RecyclerView.ViewHolder {
        public View                mView;
        public TextView            mTvTitle;
        public XVirtualItemGroup   mGroup;

        public ViewHolderGroup(View itemView) {
            super(itemView);
            mView = itemView;
            mTvTitle = (TextView)itemView.findViewById(R.id.xsolla_tv_title);
        }
    }
}
