package com.xsolla.android.sdk.api.payment;

import android.content.Context;
import android.os.Handler;
import android.util.Pair;

import com.xsolla.android.sdk.api.XPurchase;
import com.xsolla.android.sdk.api.XRestAdapter;
import com.xsolla.android.sdk.api.model.XError;
import com.xsolla.android.sdk.api.model.additional.XSufficiency;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.psystems.XPSAllManager;
import com.xsolla.android.sdk.api.model.psystems.XPSQuickManager;
import com.xsolla.android.sdk.api.model.psystems.XPaymentSystemsManager;
import com.xsolla.android.sdk.api.model.shop.XPricepointsManager;
import com.xsolla.android.sdk.api.model.shop.XSubscriptionsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItem;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemGroup;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemGroupsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemsManagerMobile;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XProceed;
import com.xsolla.android.sdk.api.model.vpayment.XVPStatus;
import com.xsolla.android.sdk.api.model.vpayment.XVPSummary;
import com.xsolla.android.sdk.api.model.сheckout.CurrentCommand;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.functions.Func3;
import rx.schedulers.Schedulers;


public class XPaymentController implements XPayment {

    private static final String TAG = "XPaymentController";

    private Context                     mContext;
    private XPayment.XPaymentListener   mListener;
    private XPayment.Params             mParams;
    private Subscriber                  subscriberToCancel;
    private boolean                     isSandbox = false;
    private PaymentStep                 currentStep = PaymentStep.UTILS;
    private PaymentStep[]               flow;
    private Stack<PaymentStep>          flowStack;
    private HashMap<Class, String>      mapResponses;
    private boolean                     isCanceled = false;

    public XPaymentController() {

    }

    protected XPaymentController(Context context, XPayment.Params params) {
        this.mContext = context;
        this.mParams = params;
        flowStack = new Stack<>();
    }

    protected XPaymentController(Context context, XPayment.Params params, boolean isSandboxModeEnabled) {
        this(context, params);
        this.isSandbox = isSandboxModeEnabled;
    }

    public XPaymentController(Context context, XPayment.Params params, XPaymentListener xsollaPaymentListener) {
        this(context, params);
        setListener(xsollaPaymentListener);
    }

    public XPaymentController(Context context, XPayment.Params params, XPaymentListener xsollaPaymentListener, boolean isSandboxModeEnabled) {
        this(context, params, isSandboxModeEnabled);
        setListener(xsollaPaymentListener);
    }

    @Override
    public void startPayment() {
        setModeSandbox(mParams.getPurchase().isSandbox());
//        init();
        initWithCountries();
    }

    @Override
    public void nextStep(String key, Object value) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(key, value);
        nextStep(map);
    }

    @Override
    public void nextStep(HashMap<String, Object> fields) {
        PaymentStep nextStep = PaymentStep.CHECKOUT;
        switch (getCurrentStep()) {
            case UTILS:
            case SHOP:
            case SHOP_VI:
            case SHOP_P:
            case SHOP_S:
                mParams.getPurchase().add(XPurchase.Part.PURCHASE, fields);
                if(flow.length == 3)
                    nextStep = PaymentStep.PAYMENT;
                else
                    nextStep = PaymentStep.CHECKOUT;
                break;
            case PAYMENT:
                mParams.getPurchase().add(XPurchase.Part.PAYMENT_SYSTEM, fields);
                nextStep = PaymentStep.CHECKOUT;
                break;
            case CHECKOUT:
                mParams.getPurchase().add(XPurchase.Part.XPS, fields);
                nextStep = PaymentStep.CHECKOUT;
                break;
        }

        switch (nextStep){
            case SHOP:
//                loadShop();
                break;
            case PAYMENT:
                sufficiency();
                break;
            case CHECKOUT:
                next();
                break;
            default:
        }
    }



    public void prevStep() {
        if(!flowStack.isEmpty()) {
            flowStack.pop();
            if(!flowStack.isEmpty()) {
                currentStep = flowStack.peek();
                mParams.getPurchase().removeLastExceptToken();
                if (currentStep == PaymentStep.CHECKOUT)
                    prevStep();
            }
        }
    }

    public void repeatWithNewCountry(String countryIso) {
        mParams.getPurchase().add(XPurchase.Part.TOKEN, "country", countryIso);
        switch (currentStep) {
            case UTILS:
            case SHOP:
                initWithCountries();
                break;
            case SHOP_VI:
                //loadVirtualItemsGroups();
                loadVirtualItems();
                break;
            case SHOP_P:
                loadPricepoints();
                break;
            case SHOP_S:
                loadSubscriptions();
                break;
            case PAYMENT:
                sufficiency();
                break;
            case CHECKOUT:
                next();
                break;
        }

    }

    private void setCurrentStep(PaymentStep currentStep) {
        if(!flowStack.contains(currentStep) || currentStep == PaymentStep.CHECKOUT) {
            flowStack.push(currentStep);
        }
        this.currentStep = currentStep;
    }

    private PaymentStep getCurrentStep() {
        return currentStep;
    }

    @Override
    public void status(String token, String invoice) {

    }

    @Override
    public void setModeSandbox(boolean isSandbox) {
        this.isSandbox = isSandbox;
    }

    protected void init() {
        XRestAdapter.setSandbox(isSandbox);
        executeObserver(utilsSubscriber(),  XRestAdapter.getUtilsObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XUtils> utilsSubscriber() {
        return new XSubscriber<XUtils>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XUtils utils) {
                mListener.onUtilsReceived(utils);
                defineNextStep(utils);
            }
        };
    }

    protected void initWithCountries() {
        setCurrentStep(PaymentStep.UTILS);
        XRestAdapter.setSandbox(isSandbox);
        Observable observable = Observable.combineLatest(
                XRestAdapter.getUtilsObserver(mParams.getPurchase().getMergedMap()),
                XRestAdapter.getCountriesObserver(mParams.getPurchase().getMergedMap()),
                new Func2<XUtils, XCountryManager, Pair<XUtils, XCountryManager>>() {
                    @Override
                    public Pair<XUtils, XCountryManager> call(XUtils utils, XCountryManager countryManager) {
                        return  new Pair<>(utils, countryManager);
                    }
                });
        executeObserver(utilsCountriesSubscriber(), observable);

    }

    Subscriber<Pair<XUtils, XCountryManager>> utilsCountriesSubscriber() {
        return new XSubscriber<Pair<XUtils, XCountryManager>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(Pair<XUtils, XCountryManager> pair) {
                mListener.onUtilsReceivedAlt(pair);
                defineNextStep(pair.first);
            }
        };
    }

    private void defineNextStep(XUtils utils) {
        if(utils.getPurchase().isPurchase() && utils.getPurchase().isPaymentSystem()) {
            next();
            flow = new PaymentStep[]{PaymentStep.CHECKOUT};
        } else if(utils.getPurchase().isPurchase()) {
            sufficiency();
            flow = new PaymentStep[]{PaymentStep.PAYMENT, PaymentStep.CHECKOUT};
        } else {
            if(utils.getActiveComponents().size() > 1) {
                loadShop(utils);
            } else {
                XUtils.XActiveComponent component = utils.getActiveComponents().get(0);
                switch (component.getAction()) {
                    case 0:
                        //loadVirtualItemsGroups();
                        loadVirtualItems();
                        break;
                    case 1:
                        loadSubscriptions();
                        break;
                    case 2:
                        loadPricepoints();
                        break;
                    default:
                }
            }
            if(!utils.getPurchase().isPaymentSystem())
                flow = new PaymentStep[]{PaymentStep.SHOP, PaymentStep.PAYMENT, PaymentStep.CHECKOUT};
            else
                flow = new PaymentStep[]{PaymentStep.SHOP, PaymentStep.CHECKOUT};
        }
    }

    /*
    *                       SHOP
    * */

    private void loadShop(XUtils utils) {
        setCurrentStep(PaymentStep.SHOP);
        mListener.onShopRequired(utils);
    }

    public void loadVirtualItemsGroups() {
        setCurrentStep(PaymentStep.SHOP_VI);
        executeObserver(viGroupSubscriber(),
                XRestAdapter.getGroupsObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XVirtualItemGroupsManager> viGroupSubscriber(){
        return new XSubscriber<XVirtualItemGroupsManager>() {

            public ArrayList<Object> objects;


            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XVirtualItemGroupsManager groupsManager) {
                objects = new ArrayList<>();
                final int size = groupsManager.getListGroups().size();
                for(int i = 0; i < size; i++) {
                    final int counter = i;
                    final XVirtualItemGroup group = groupsManager.getListGroups().get(i);
                    String groupId = group.getId();
                    HashMap<String, Object> map = mParams.getPurchase().getMergedMap();
                    map.put("group_id", groupId);
                    XRestAdapter.getItemsObserver(map)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<XVirtualItemsManager>() {
                                @Override
                                public void onCompleted() {
                                    if(counter + 1 == size)
                                        mListener.onVirtualItemsReceived(objects);
                                }

                                @Override
                                public void onError(Throwable throwable) {

                                }

                                @Override
                                public void onNext(XVirtualItemsManager xVirtualItemsManager) {
                                    objects.add(group);
                                    for(XVirtualItem item : xVirtualItemsManager.getListItems()) {
                                        objects.add(item);
                                    }
                                }
                            });
                }
            }
        };
    }

    public void loadVirtualItems() {
        setCurrentStep(PaymentStep.SHOP_VI);
        executeObserver(viItemsSubscriber(),
                XRestAdapter.getMobileItemsObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XVirtualItemsManagerMobile> viItemsSubscriber(){
        return new XSubscriber<XVirtualItemsManagerMobile>() {

            public ArrayList<Object> objects;


            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XVirtualItemsManagerMobile groupsManager) {
                mListener.onVirtualItemsReceived(groupsManager.getSimplifiedList());
            }
        };
    }

    public void loadPricepoints() {
        setCurrentStep(PaymentStep.SHOP_P);
        executeObserver(pricepointSubscriber(),
                XRestAdapter.getPricepointsObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XPricepointsManager> pricepointSubscriber() {
        return new XSubscriber<XPricepointsManager>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XPricepointsManager pricepointsManager) {
                mListener.onPricepointsReceived(pricepointsManager);
            }
        };
    }

    public void loadSubscriptions() {
        setCurrentStep(PaymentStep.SHOP_S);
        executeObserver(subscriptionsSubscriber(),
                XRestAdapter.getSubscriptionsObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XSubscriptionsManager> subscriptionsSubscriber() {
        return new XSubscriber<XSubscriptionsManager>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XSubscriptionsManager subscriptionsManager) {
                mListener.onSubscriptionsReceived(subscriptionsManager);
            }
        };
    }

    /*
    *                    SHOP END
    * */

    /*
    *                 VIRTUAL PAYMENTS
    * */

    public void loadVpSummary(HashMap<String, Object> fields) {
//        setCurrentStep(PaymentStep.PAYMENT);
        mParams.getPurchase().add(XPurchase.Part.PURCHASE, fields);
//        mParams.getPurchase().add(XPurchase.Part.PURCHASE, );
        executeObserver(getVpSummarySubscriber(),
                XRestAdapter.getVPObserver(mParams.getPurchase().getMergedMap()));
    }
    private XVPSummary mVpSummary;
    Subscriber<XVPSummary> getVpSummarySubscriber() {
        return new XSubscriber<XVPSummary>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XVPSummary xvpSummary) {
                mVpSummary = xvpSummary;
                if(xvpSummary.isSkipConfirmation()) {
                    mParams.getPurchase().add(XPurchase.Part.PAYMENT_SYSTEM, "dont_ask_again", 1);
                    proceedVP(null);
                } else {
                    mListener.onVpSummaryRecieved(xvpSummary);
                }
            }
        };
    }

    public void proceedVP(HashMap<String, Object> fields) {
        if(fields != null)
            mParams.getPurchase().add(XPurchase.Part.PAYMENT_SYSTEM, fields);
        executeObserver(getProceedSubscriber(),
                XRestAdapter.getVPProceed(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XProceed> getProceedSubscriber() {
        return new XSubscriber<XProceed>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XProceed xProceed) {
                if(xProceed.isInvoiceCreated()) {
                    vpStatus(xProceed.getOperationId());
                } else {
                    if(mVpSummary != null) {
                        mVpSummary.setProceedError(xProceed.getError());
                        mListener.onVpSummaryRecieved(mVpSummary);
                    }
                }
            }
        };
    }

    public void vpStatus(long operationId) {
        mParams.getPurchase().add(XPurchase.Part.XPS, "operation_id", operationId);
        executeObserver(getVPStatusSubscriber(),
                XRestAdapter.getVPStatus(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XVPStatus> getVPStatusSubscriber() {
        return new XSubscriber<XVPStatus>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XVPStatus xVPstatus) {
                mListener.onVPStatusRecieved(xVPstatus);
            }
        };
    }

    /*
    *                 VIRTUAL PAYMENTS END
    * */


    private void loadCountries() {
        executeObserver(countriesSubscriber(),
                XRestAdapter.getCountriesObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XCountryManager> countriesSubscriber() {
        return new Subscriber<XCountryManager>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(XCountryManager xCountryManager) {

            }
        };
    }

    public void loadPaymentSystems() {
        Observable observable =   Observable.combineLatest(
                XRestAdapter.getQuickPSObserver(mParams.getPurchase().getMergedMap()),
                XRestAdapter.getAllPSObserver(mParams.getPurchase().getMergedMap()),
                XRestAdapter.getCountriesObserver(mParams.getPurchase().getMergedMap()),
                new Func3<XPSQuickManager, XPSAllManager, XCountryManager, Object>() {
                    @Override
                    public Object call(XPSQuickManager xpsQuickManager, XPSAllManager xpsAllManager, XCountryManager xCountryManager) {
                        return new Object[]{xpsQuickManager, xpsAllManager, xCountryManager};
                    }
                });
        executeObserver(paymentSystemsSubscriber(), observable);
    }

    Subscriber<Object> paymentSystemsSubscriber() {
        return new XSubscriber<Object>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(Object o) {
                Object[] in = (Object[]) o;
                if(in != null) {
                    XPaymentSystemsManager manager = new XPaymentSystemsManager();
                    manager.setQuickManger((XPSQuickManager) in[0]);
                    manager.setAllManager((XPSAllManager) in[1]);
                    manager.setCountryManager((XCountryManager) in[2]);
                    mListener.onPaymentSystemsReceived(manager);
                }
                setCurrentStep(PaymentStep.PAYMENT);
            }
        };
    }

    private void sufficiency() {
        executeObserver(sufficiencySubscriber(),
                XRestAdapter.getSufficiencyObserver(mParams.getPurchase().getMergedMap()));
    }

    Subscriber<XSufficiency> sufficiencySubscriber() {
        return new XSubscriber<XSufficiency>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XSufficiency sufficiency) {
                if(sufficiency.isEnoughUserBalance()){
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("pid", 2760);//1370
                    mParams.getPurchase().add(XPurchase.Part.PAYMENT_SYSTEM, map);
                    next();
                } else {
                    loadPaymentSystems();
                }
            }
        };
    }

    protected void next(){
        if(!isCanceled) {
            executeObserver(directpaymentSubscriber(),
                    XRestAdapter.getDirectpaymentObserver(mParams.getPurchase().getMergedMap()));
        }
    }

    Subscriber<XDirectpayment> directpaymentSubscriber() {
        return new XSubscriber<XDirectpayment>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onNext(XDirectpayment xDirectpayment) {
                setCurrentStep(PaymentStep.CHECKOUT);
                if(xDirectpayment.isSkipForm()) {
                    nextStep(xDirectpayment.getForm().getMapXps());
                } else if(xDirectpayment.getCurrentCommand() == CurrentCommand.STATUS){
                    mListener.onDirectpaymentReceived(xDirectpayment);
                    boolean isStatusInProgress = "invoice".equals(xDirectpayment.getStatus().getGroup()) || "delivering".equals(xDirectpayment.getStatus().getGroup());
                    if(isStatusInProgress) {
                        final Handler handler = new Handler();
                        final HashMap<String, Object> map = xDirectpayment.getForm().getMapXps();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                nextStep(map);
                            }
                        }, 5000);
                    }
                } else {
                    mListener.onDirectpaymentReceived(xDirectpayment);
                }
            }
        };
    }


    private void executeObserver(Subscriber subscriber, Observable observable) {
        subscriberToCancel = subscriber;
        observable.subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(subscriber);
    }

    public XPayment.Params getParams() {
        return mParams;
    }

    public Context getContext() {
        return mContext;
    }

    public void cancel() {
        isCanceled = true;
        if(subscriberToCancel != null)
                    subscriberToCancel.unsubscribe();
    }

    private void setListener(XPaymentListener newListener) {
        if(newListener != null)
            mListener = newListener;
        else
            mListener = new XPaymentListener() {
                @Override
                public void onUtilsReceived(XUtils utils) {

                }

                @Override
                public void onUtilsReceivedAlt(Pair<XUtils, XCountryManager> utilsCountry) {

                }

                @Override
                public void onShopRequired(XUtils utils) {

                }

                @Override
                public void onVirtualItemsReceived(ArrayList<Object> groupsItemsList) {

                }

                @Override
                public void onPricepointsReceived(XPricepointsManager manager) {

                }

                @Override
                public void onSubscriptionsReceived(XSubscriptionsManager manager) {

                }

                @Override
                public void onPaymentSystemsReceived(XPaymentSystemsManager manager) {

                }

                @Override
                public void onVpSummaryRecieved(XVPSummary vpSummary) {

                }

                @Override
                public void onVPStatusRecieved(XVPStatus xVPstatus) {

                }

                @Override
                public void onDirectpaymentReceived(XDirectpayment checkout) {

                }

                @Override
                public void onErrorReceived(XError error) {

                }
            };
    }

    private abstract class XSubscriber<T> extends Subscriber<T> {
        @Override
        public void onError(Throwable throwable) {
            if(throwable instanceof HttpException) {
                Response response = ((HttpException)throwable).response();//;// = ((Response) throwable).getResponse();
                int status = response.code();
                XError error = new XError();
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));//
                    StringBuilder out = new StringBuilder();
                    String newLine = System.getProperty("line.separator");
                    String line;
                    while ((line = reader.readLine()) != null) {
                        out.append(line);
                        out.append(newLine);
                    }
                    String s = out.toString();
                    error.parseMany(new JSONObject(s));
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                mListener.onErrorReceived(error);
            } else {
                XError error = new XError("1000-0003", "Something is going wrong");
                mListener.onErrorReceived(error);
            }
        }
    }

    enum PaymentStep{
        UTILS, SHOP, SHOP_VI, SHOP_P, SHOP_S, PAYMENT, CHECKOUT
    }

}
