package com.xsolla.android.sdk.api.model.shop;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XSubscription implements IParseble {


    boolean isTrial;// "is_trial":false,

    float chargeAmount;// "charge_amount":0.99,
    float chargeAmountWithoutDiscount;// "charge_amount_without_discount":0.99,

    int period;// "period":1,
    int periodTrial;// "period_trial":0
    int promotionChargeCount;// "promotion_charges_count":null,

    String id;// "id":"c9000261",
    String chargeCurrency;// "charge_currency":"USD",
    String periodUnit;// "period_unit":"month",
    String name;// "name":"Silver Status",
    String description;// "description":"2x more experience!",
    String bonusVirtualCurrency;// "bonus_virtual_currency":null,// {vc_amount:100}
    String offerLabel;// "offer_label":"",


    ArrayList<XBonusItem> listBonusItems;// "bonus_virtual_items":[{"name":"Name","action":"Action",},],

    public XSubscription() {
        listBonusItems = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isPriceChanged() {
        return chargeAmount != chargeAmountWithoutDiscount;
    }

    public String getPrice() {
        return PriceFormatter.formatPrice(chargeCurrency, Float.toString(chargeAmount));
    }

    public String getPriceWithoutDiscount() {
        return PriceFormatter.formatPrice(chargeCurrency, Float.toString(chargeAmountWithoutDiscount));
    }

    public String getPeriodUnit() {
        return periodUnit;
    }

    public String getPeriod(){
        return "every " + period + " " + periodUnit;
    }

    public String getPeriod(String every, String periodUnit){
        return every + " " + period + " " + periodUnit;
    }

    public boolean isBonus(){
        return !"".equals(bonusVirtualCurrency) || listBonusItems.size() > 0;
    }

    public String getBonusString(String projectVirtualCurrency) {
        StringBuilder builder = new StringBuilder("+");
        if(!"".equals(bonusVirtualCurrency)) {
            builder.append(getBonusOut(projectVirtualCurrency))
            .append(", ");
            if(listBonusItems.size() > 0)
                builder.append(getBonusItems());
        } else {
            builder.append(getBonusItems());
        }
        return builder.toString();
    }

    public String getBonusOut(String projectVirtualCurrency) {
        return bonusVirtualCurrency + " " + projectVirtualCurrency;
    }

    public String getBonusItems() {
        StringBuilder builder = new StringBuilder();
        for(XBonusItem bonusItem : listBonusItems) {
            builder.append(bonusItem.getQuantity()).append(" ").append(bonusItem.getName()).append(", ");
        }
        int size = builder.length();
        builder.delete(size - 2, size - 1);
        return builder.toString();
    }

    @Override
    public void parse(JSONObject jobj) {

        isTrial = jobj.optBoolean("is_trial");

        chargeAmount                = (float)jobj.optDouble("charge_amount");
        chargeAmountWithoutDiscount = (float)jobj.optDouble("charge_amount_without_discount");

        period                  = jobj.optInt("period");
        periodTrial             = jobj.optInt("period_trial");
        promotionChargeCount    = jobj.optInt("promotion_charges_count");

        id                      = jobj.optString("id");
        name                    = jobj.optString("name");
        description             = jobj.optString("description");
        chargeCurrency          = jobj.optString("charge_currency");
        periodUnit              = jobj.optString("period_unit");
        offerLabel              = jobj.optString("offer_label");
        JSONObject jobjBonusVC  = jobj.optJSONObject("bonus_virtual_currency");
        if(jobjBonusVC != null) {
            int outAmount = jobjBonusVC.optInt("vc_amount");
            bonusVirtualCurrency = outAmount == 0 ? "" : Integer.toString(outAmount);
        } else {
            bonusVirtualCurrency = "";
        }

        JSONArray jarr          = jobj.optJSONArray("bonus_virtual_items");
        if(jarr != null)
            for(int i = 0; i < jarr.length(); i++) {
                JSONObject jobjNewItem = jarr.optJSONObject(i);
                XBonusItem newBonusItem = new XBonusItem(jobjNewItem.optString("name"), jobjNewItem.optString("quantity"));
                listBonusItems.add(newBonusItem);
            }
    }

    @Override
    public String toString() {
        return "XSubscription{" +
                "isTrial=" + isTrial +
                ", chargeAmount=" + chargeAmount +
                ", chargeAmountWithoutDiscount=" + chargeAmountWithoutDiscount +
                ", period=" + period +
                ", periodTrial=" + periodTrial +
                ", promotionChargeCount=" + promotionChargeCount +
                ", id='" + id + '\'' +
                ", chargeCurrency='" + chargeCurrency + '\'' +
                ", periodUnit='" + periodUnit + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", bonusVirtualCurrency='" + bonusVirtualCurrency + '\'' +
                ", offerLabel='" + offerLabel + '\'' +
                ", listBonusItems=" + listBonusItems +
                '}';
    }
}
