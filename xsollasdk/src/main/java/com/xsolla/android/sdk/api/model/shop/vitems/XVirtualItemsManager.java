package com.xsolla.android.sdk.api.model.shop.vitems;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XVirtualItemsManager implements IParseble {

    public ArrayList<XVirtualItem> listItems;

    public ArrayList<XVirtualItem> getListItems() {
        return listItems;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarrItems = jobj.optJSONArray("virtual_items");
        if(jarrItems != null) {
            listItems = new ArrayList<>(jarrItems.length());
            for (int i = 0; i < jarrItems.length(); i++) {
                XVirtualItem newItem = new XVirtualItem();
                newItem.parse(jarrItems.optJSONObject(i));
                listItems.add(newItem);
            }
        }
    }

    @Override
    public String toString() {
        return "XVirtualItemsManager{" +
                "listItems=" + listItems +
                '}';
    }
}
