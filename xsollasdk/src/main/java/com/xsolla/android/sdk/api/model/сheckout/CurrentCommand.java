package com.xsolla.android.sdk.api.model.сheckout;

/**
 *
 */
public enum CurrentCommand {
    FORM, CREATE, STATUS, CHECKOUT, CHECK, ACCOUNT, UNKNOWN
}
