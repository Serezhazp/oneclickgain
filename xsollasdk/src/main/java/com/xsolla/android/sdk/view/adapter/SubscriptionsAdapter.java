package com.xsolla.android.sdk.view.adapter;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.shop.XSubscription;
import com.xsolla.android.sdk.view.fragment.ItemFragment.OnListFragmentInteractionListener;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class SubscriptionsAdapter extends RecyclerView.Adapter<SubscriptionsAdapter.ViewHolder> {

    private List<XSubscription>                 mItems;
    private OnListFragmentInteractionListener   mListener;
    private String[]                            mLocalTranslations;

    public SubscriptionsAdapter(List<XSubscription> items, OnListFragmentInteractionListener listener, String[] localTranslations) {
        this.mItems     = items;
        this.mListener  = listener;
        this.mLocalTranslations = localTranslations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.xsolla_shop_vitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        XSubscription item = mItems.get(position);
        holder.mSubscription = item;

        ((View)holder.mImageBorder.getParent()).setVisibility(View.GONE);
        holder.mTvLearnMore.setVisibility(View.GONE);
        if(item.isBonus()) {
            holder.mTvBonus.setVisibility(View.VISIBLE);
            holder.mTvBonus.setText(item.getBonusString("Coins"));
        } else {
            holder.mTvBonus.setVisibility(View.GONE);
        }

        holder.mTvTitle.setText(item.getName());
        holder.mTvDescription.setText(item.getDescription());

        if(item.isPriceChanged()) {
            holder.mTvPriceOld.setVisibility(View.VISIBLE);
            holder.mTvPriceOld.setText(item.getPriceWithoutDiscount());
            holder.mTvPriceOld.setPaintFlags(holder.mTvPriceOld.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.mTvPrice.setText(item.getPrice());
        } else {
            holder.mTvPriceOld.setVisibility(View.GONE);
            holder.mTvPrice.setText(item.getPriceWithoutDiscount());
        }

        String chargePeriod = item.getPeriod();
        String periodUnit = "";
        if(item.getPeriodUnit().startsWith("m"))
            periodUnit = mLocalTranslations[1] != null ? mLocalTranslations[1] : item.getPeriodUnit();
        if(item.getPeriodUnit().startsWith("d"))
            periodUnit = mLocalTranslations[2] != null ? mLocalTranslations[2] : item.getPeriodUnit();

        chargePeriod = item.getPeriod(mLocalTranslations[0], periodUnit);

        holder.mTvBuy.setText(chargePeriod);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> map = new HashMap<String, Object>(1);
                map.put("id_package", holder.mSubscription.getId());
                mListener.onOnProductSelected(map, false);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView, mImageBorder;
        public final ImageView mImage;
        public final TextView mOfferLable, mTvTitle, mTvDescription, mTvLearnMore, mTvBonus, mTvPrice, mTvPriceOld, mTvBuy;
        public XSubscription mSubscription;

        public ViewHolder(View itemView) {
            super(itemView);
            mView               = itemView;

            mImageBorder        = itemView.findViewById(R.id.xsolla_offer_label_border);
            mOfferLable         = (TextView) itemView.findViewById(R.id.xsolla_tv_offer_label);

            mImage              = (ImageView) itemView.findViewById(R.id.xsolla_iv);
            mTvTitle            = (TextView) itemView.findViewById(R.id.xsolla_tv_title);
            mTvDescription      = (TextView) itemView.findViewById(R.id.xsolla_tv_description);
            mTvLearnMore        = (TextView) itemView.findViewById(R.id.xsolla_tv_learn_more);
            mTvBonus            = (TextView) itemView.findViewById(R.id.xsolla_tv_bonus);
            mTvPrice            = (TextView) itemView.findViewById(R.id.xsolla_tv_price);
            mTvPriceOld         = (TextView) itemView.findViewById(R.id.xsolla_tv_price_old);
            mTvBuy              = (TextView) itemView.findViewById(R.id.xsolla_tv_buy);
        }

        public void showOfferLabel(int offerType) {
            int offerColor      = Color.LTGRAY;
            String offerText    = "";
            int visibility      = View.VISIBLE;
            switch (offerType) {
                case 0:
                    offerColor = Color.parseColor("#ff809efc");//R.color.xsolla_advice
                    offerText = "ADVICE";
                    break;
                case 1:
                    offerColor = Color.parseColor("#ff9896de");//R.color.xsolla_best
                    offerText = "BEST";
                    break;
                case 2:
                    offerColor = Color.parseColor("#fffcb07f");//R.color.xsolla_special
                    offerText = "OFFER";
                    break;
                default:
                    visibility = View.GONE;
            }
            mImageBorder.setBackgroundColor(offerColor);
            mOfferLable.setBackgroundColor(offerColor);
            mOfferLable.setText(offerText);
            mOfferLable.setVisibility(visibility);
        }

    }
}
