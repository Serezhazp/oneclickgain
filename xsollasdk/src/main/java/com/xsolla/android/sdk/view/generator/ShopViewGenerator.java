package com.xsolla.android.sdk.view.generator;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.util.ImageLoader;
import com.xsolla.android.sdk.view.adapter.MenuElementsAdapter;
import com.xsolla.android.sdk.view.fragment.IShopInteractionListener;

import java.util.ArrayList;

/**
 *
 */
public class ShopViewGenerator {



    public static void findAndAndInitView(final Context context, View rootView, final XUtils utils, final XCountryManager countriesManager, final IShopInteractionListener listener){
        View        viewCountry, viewUserAgreement;
        TextView    tvUsername, tvProjectName, tvBalance, tvRealBalance, tvRealBalanceValue, tvCurrentCountry, tvCountryChange, tvUserAgreement;
        ImageView   ivProjectCurrency;
        ListView    lvMenu;

        tvUsername          = (TextView) rootView.findViewById(R.id.xsolla_tv_user);
        tvProjectName       = (TextView) rootView.findViewById(R.id.xsolla_tv_project);
        tvBalance           = (TextView) rootView.findViewById(R.id.xsolla_tv_balance);
        tvRealBalance       = (TextView) rootView.findViewById(R.id.xsolla_tv_real_balance);
        tvRealBalanceValue  = (TextView) rootView.findViewById(R.id.xsolla_tv_real_balance_value);
        tvCurrentCountry    = (TextView) rootView.findViewById(R.id.xsolla_tv_country);
        tvCountryChange     = (TextView) rootView.findViewById(R.id.xsolla_tv_change);
        tvUserAgreement     = (TextView) rootView.findViewById(R.id.xsolla_tv_user_agreement);

        viewCountry         = rootView.findViewById(R.id.xsolla_country);
        viewUserAgreement   = rootView.findViewById(R.id.xsolla_user_agreement);

        ivProjectCurrency   = (ImageView) rootView.findViewById(R.id.xsolla_iv);
        lvMenu              = (ListView) rootView.findViewById(R.id.xsolla_lv);

        tvUsername.setText(utils.getUser().getRequisites().getValue());
        tvProjectName.setText(utils.getProject().getName());

        if(utils.getUser().isVcBalance()) {
            String currency = utils.getProject().getVirtualCurrencyImage();
            ((View) tvBalance.getParent()).setVisibility(View.VISIBLE);
            if(currency != null && !"".equals(currency)) {
                tvBalance.setText(utils.getUser().getVcBalance());
                ImageLoader.getInstance()
                        .loadImage(ivProjectCurrency, utils.getProject().getVirtualCurrencyImage());
            } else {
                ivProjectCurrency.setVisibility(View.GONE);
                String userBalance = utils.getUser().getVcBalance() + "\n" + utils.getProject().getVirtualCurrencyName();
                tvBalance.setText(userBalance);
            }
        } else {
            ((View)tvBalance.getParent()).setVisibility(View.GONE);
        }

        if(utils.getUser().isUserBalance()){
            ((View)tvRealBalance.getParent()).setVisibility(View.VISIBLE);
            String yourBalance = utils.getTranslations().get("user_nominal_balance_label") + ": ";
            tvRealBalance.setText(yourBalance);
            tvRealBalanceValue.setText(utils.getUser().getUserBalance());
        } else {
            ((View)tvRealBalance.getParent()).setVisibility(View.GONE);
        }

        ArrayList<XUtils.XActiveComponent> components = utils.getActiveComponents();
        MenuElementsAdapter adapter = new MenuElementsAdapter(context, components);
        lvMenu.setAdapter(adapter);
        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                XUtils.XActiveComponent component = (XUtils.XActiveComponent) parent.getAdapter().getItem(position);
                listener.onMenuSelected(component.getAction());
            }
        });

        tvCurrentCountry.setText(countriesManager.getCurrentCountryName());//utils.getUser().getCountry().getValue()
        tvUserAgreement.setText(utils.getTranslations().get("footer_agreement"));
        tvCountryChange.setText(utils.getTranslations().get("country_change"));

        viewCountry.setOnClickListener(onCountryClickListener(context, tvCurrentCountry, countriesManager, listener));

        viewUserAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = utils.getProject().getEulaLink();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                context.startActivity(i);
            }
        });
    }

    public static View.OnClickListener onCountryClickListener(final Context context,
                                                              final TextView tvCurrentCountry,
                                                              final XCountryManager manager,
                                                              final IShopInteractionListener listener) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                builderSingle.setIcon(R.drawable.ic_launcher);

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        context,
                        android.R.layout.select_dialog_item);
                for (String s : manager.getCountries()) {
                    arrayAdapter.add(s);
                }

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String strName = arrayAdapter.getItem(which);
                        final int selectCountryPosition = which;
                        listener.onCountryChanged(manager.getCountry(selectCountryPosition).getIso());
                        tvCurrentCountry.setText(strName);
                    }
                });
                final AlertDialog dialog = builderSingle.create();
                dialog.getListView().post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.getListView().setSelection(manager.getCurrentCountryIndex());
                    }
                });
                dialog.show();
            }
        };
    }


}
