package com.xsolla.android.sdk.api.model.psystems;

import android.support.v4.util.ArrayMap;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XPSAllManager implements IParseble {

    private ArrayList<XPaymentSystem>           listAllPayments;//"instances":[
    private ArrayMap<String, XPaymentSystem>    mapAllPayments;//"instances":[
    private ArrayMap<String, XPaymentSystem>    mapAllPaymentsByPid;//"instances":[
    //"lastPayment":null,


    public ArrayList<XPaymentSystem> getList() {
        return listAllPayments;
    }

    public ArrayMap<String, XPaymentSystem> getMap() {
        return mapAllPayments;
    }

    public ArrayMap<String, XPaymentSystem> getMapByPid() {
        return mapAllPaymentsByPid;
    }

    private void add(XPaymentSystem paymentSystem){
        listAllPayments.add(paymentSystem);
        mapAllPayments.put(paymentSystem.getName(), paymentSystem);
        mapAllPaymentsByPid.put(Integer.toString(paymentSystem.getId()), paymentSystem);
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarr = jobj.optJSONArray("instances");
        if(jarr != null) {
            listAllPayments = new ArrayList<>(jarr.length());
            mapAllPayments = new ArrayMap<>(jarr.length());
            mapAllPaymentsByPid = new ArrayMap<>(jarr.length());
            for (int i = 0; i < jarr.length(); i++) {
                XPaymentSystem newItem = new XPaymentSystem();
                newItem.parse(jarr.optJSONObject(i));
                if(newItem.getId() != 64)
                    add(newItem);
//                listAllPayments.add(newItem);
            }
        }
    }

    @Override
    public String toString() {
        return "XPSAllManager{" +
                "listAllPayments=" + listAllPayments +
                '}';
    }
}
