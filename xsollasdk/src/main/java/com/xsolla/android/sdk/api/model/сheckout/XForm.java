package com.xsolla.android.sdk.api.model.сheckout;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


/**
 *
 * */
public class XForm implements IParseble {

    static final String xpsPrefix = "xps_";
    private int size;
    private ArrayList<XFormElement>         list;
    private ArrayList<XFormElement>         listVisible;
    private HashMap<String, XFormElement>   map;
    private HashMap<String, Object>         mapXps;

    public XForm() {
        list        = new ArrayList<>();
        listVisible = new ArrayList<>();
        map         = new HashMap<>();
        mapXps      = new HashMap<>();
    }

    @Override
    public void parse(JSONObject jobj) {
        for(Iterator<String> iter = jobj.keys(); iter.hasNext();) {
            String key = iter.next();
            JSONObject jobjFormElem = jobj.optJSONObject(key);
            if(jobjFormElem != null) {
                XFormElement element = new XFormElement();
                element.parse(jobjFormElem);
                addElement(element);
            }
        }
    }


    public XFormElement getItem(String name) {
        return map.get(name);
    }

    public HashMap<String, Object> getMapXps() {
        return mapXps;
    }

    public ArrayList<XFormElement> getListVisible() {
        return listVisible;
    }

    private void addElement(XFormElement element) {
        list.add(element);
        String name = element.getName();
        if("paymentWithSavedMethod".equals(name) || "allowSave".equals(name)) {
            element.setValue("0");
            element.isVisible = false;
        }
        if (element.isVisible())
            listVisible.add(element);
        map.put(element.getName(), element);

        if (element.getValue() != null)
            mapXps.put(xpsPrefix + element.getName(), element.getValue());
        size++;
    }

    public void updateElement(String name, String newValue) {
        if (getItem(name) != null) {
            getItem(name).setValue(newValue);
            mapXps.put(xpsPrefix + name, newValue);
        }
    }


    @Override
    public String toString() {
        return "\nXForm{" +
                "size=" + size +
                ", list=" + list +
                '}';
    }

    /**
     *
     * */
    public class XFormElement implements IParseble {
        private String  name;// "name" : "sum",
        private String  title;// "title" : "You pay",
        private String  type;// "type" : "hidden",
        private String  example;// "example" : null,
        private String  value;// "value" : "1",
        private String  tooltip;// "tooltip" : null,

        private boolean isMandatory;// "isMandatory" : "1",
        private boolean isReadonly;// "isReadonly" : "1",
        private boolean isVisible;// "isVisible" : "1",
        //boolean isPackets;// "isPakets" : null, deprecated

        private ArrayList<XOption> options;// "options" : null,

        //String javascript;// "javascript" : {}


        public XFormElement() {
            this.options = new ArrayList<>();
        }

        @Override
        public void parse(JSONObject jobj) {
            this.name           = jobj.optString("name");
            this.title          = jobj.optString("title");
            this.type           = jobj.optString("type");
            this.example        = jobj.optString("example");
            this.value          = jobj.optString("value");
            this.tooltip        = jobj.optString("tooltip");

            this.isMandatory    = jobj.optInt("isMandatory") == 1;
            this.isReadonly     = jobj.optInt("isReadonly") == 1;
            this.isVisible      = jobj.optInt("isVisible") == 1;
            //this.isPackets      = jobj.optBoolean("isPackets");

            JSONArray jarrOptions = jobj.optJSONArray("options");
            if(jarrOptions != null)
                for (int i = 0; i < jarrOptions.length(); i++) {
                    JSONObject jobjOption = jarrOptions.optJSONObject(i);
                    XOption newOption = new XOption();
                    newOption.parse(jobjOption);
                    this.options.add(newOption);
                }
        }

        public String getName() {
            return "null".equals(name) ? "" : name;
        }

        public String getTitle() {
            return "null".equals(title) ? "" : title;
        }

        public String getType() {
            return type;
        }

        public String getExample() {
            return "null".equals(example) ? "" : example;
        }

        public String getValue() {
            return "null".equals(value) ? "" : value;
        }

        public boolean isMandatory() {
            return isMandatory;
        }

        public boolean isReadonly() {
            return isReadonly;
        }

        public boolean isVisible() {
            return isVisible;
        }

        public ArrayList<XOption> getOptions() {
            return options;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "\n\t XFormElement{" +
                    "name='" + name + '\'' +
                    ", title='" + title + '\'' +
                    ", type='" + type + '\'' +
                    ", example='" + example + '\'' +
                    ", value='" + value + '\'' +
                    ", tooltip='" + tooltip + '\'' +
                    ", isMandatory=" + isMandatory +
                    ", isReadonly=" + isReadonly +
                    ", isVisible=" + isVisible +
                    ", options=" + options +
                    '}';
        }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */
        /**
         *
         * */
        public class XOption implements IParseble {

            private String value;
            private String label;

            @Override
            public void parse(JSONObject jobj) {
                this.value = jobj.optString("value");
                this.label = jobj.optString("label");
            }

            public String getValue() {
                return value;
            }

            public String getLabel() {
                return label;
            }

            @Override
            public String toString() {
                return label;
            }
        }
    };
}
