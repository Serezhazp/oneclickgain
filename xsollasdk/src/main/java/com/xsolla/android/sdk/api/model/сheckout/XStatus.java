package com.xsolla.android.sdk.api.model.сheckout;

import com.xsolla.android.sdk.XsollaObject;
import com.xsolla.android.sdk.api.XsollaApiConst;
import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

/**
 *
 */
public class XStatus extends XsollaObject implements IParseble {


    long invoice;//"invoice":189117751,

    String          marketplace;//"marketplace":"paystation",
    String          group;//"group":"done",//invoice delivering done troubled
    String          country;//"country":"RU",
    String          returnRegion;//"return_region":"top",
    String          titleClass;//"title_class":"done",

    boolean         isCancelUser;//"isCancelUser":false,
    boolean         isPreloader;//"isPreloader":false,
    boolean         isShowEmailRequest;//"showEmailRequest":true,
    boolean         isNeedToCheck;//"needToCheck":false,

    XFeedback       feedback;//"feedback":{}
    XEmail          email;//"email":{},
    XStatusData     statusData;//"statusData":{},
    XRepayment      repayment;//"repayment":{},
    XAutoRecharge   autoRecharge;//"autoRecharge":{},
    XText           text;//"text":{},
    //ArrayList<String> alternative;//"alternative":[],


    public XStatus() {
        this.text           = new XText();
        this.autoRecharge   = new XAutoRecharge();
        this.repayment      = new XRepayment();
        this.statusData     = new XStatusData();
        this.email          = new XEmail();
        this.feedback       = new XFeedback();
    }

    public long getInvoice() {
        return invoice;
    }

    public String getGroup() {
        return group;
    }

    public boolean isCancelUser() {
        return isCancelUser;
    }

    public XText getText() {
        return text;
    }

    @Override
    public void parse(JSONObject jobj) {

        this.invoice            = jobj.optLong("invoice");

        this.marketplace        = jobj.optString("marketplace");
        this.group              = jobj.optString("group");
        this.country            = jobj.optString("country");
        this.returnRegion       = jobj.optString("return_region");
        this.titleClass         = jobj.optString("title_class");

        this.isCancelUser       = jobj.optBoolean("isCancelUser");
        this.isPreloader        = jobj.optBoolean("isPreloader");
        this.isShowEmailRequest = jobj.optBoolean("isCanshowEmailRequestcelUser");
        this.isNeedToCheck      = jobj.optBoolean("needToCheck");

        JSONObject jobjFeedback = jobj.optJSONObject("feedback");
        if(jobjFeedback != null)
            feedback.parse(jobjFeedback);

        JSONObject jobjEmail = jobj.optJSONObject("email");
        if(jobjEmail != null)
            email.parse(jobjEmail);

        JSONObject jobjStatusData = jobj.optJSONObject("statusData");
        if(jobjStatusData != null)
            statusData.parse(jobjStatusData);

        JSONObject jobjRepayment = jobj.optJSONObject("repayment");
        if(jobjRepayment != null)
            repayment.parse(jobjRepayment);

        JSONObject jobjAutoRecharge = jobj.optJSONObject("autoRecharge");
        if(jobjAutoRecharge != null)
            autoRecharge.parse(jobjAutoRecharge);

        JSONObject jobjText = jobj.optJSONObject("text");
        if(jobjText != null)
            text.parse(jobjText);

    }

    @Override
    public String toString() {
        return "\nXStatus{" +
                "invoice=" + invoice +
                ", marketplace='" + marketplace + '\'' +
                ", group='" + group + '\'' +
                ", country='" + country + '\'' +
                ", returnRegion='" + returnRegion + '\'' +
                ", titleClass='" + titleClass + '\'' +
                ", isCancelUser=" + isCancelUser +
                ", isPreloader=" + isPreloader +
                ", isShowEmailRequest=" + isShowEmailRequest +
                ", isNeedToCheck=" + isNeedToCheck +
                ", feedback=" + feedback +
                ", email=" + email +
                ", statusData=" + statusData +
                ", repayment=" + repayment +
                ", autoRecharge=" + autoRecharge +
                ", text=" + text +
                '}';
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * *
    * INNER CLASSES
    * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     *
     */
    class XFeedback implements IParseble {
        String url;//"url":"https:\/\/docs.google.com\/forms\/d\/1lFbr_CljVKhMi9tpsOY2dRn-by6QPMNXCoLzIOz5D84\/viewform"

        @Override
        public void parse(JSONObject jobj) {
            url = jobj.optString("url");
        }

        @Override
        public String toString() {
            return "\n\tXFeedback{" +
                    "url='" + url + '\'' +
                    '}';
        }
    }
    /**
     *
     */
    class XEmail implements IParseble {
        long    invoice;//"invoice":189117751,
        String  signature;//"signature":"fac086608d4d3fd597de74a7a378d446"


        @Override
        public void parse(JSONObject jobj) {
            this.invoice    = jobj.optLong("invoice");
            this.signature  = jobj.optString("signature");
        }

        @Override
        public String toString() {
            return "\n\tXEmail{" +
                    "invoice=" + invoice +
                    ", signature='" + signature + '\'' +
                    '}';
        }
    }
    /**
     *
     */
    class XStatusData implements IParseble {

        int state;//"state":3,
        int out;//"out":0,

        String stateText;//"stateText":"done"
        String invoice;//"invoice":"189117751",
        String project;//"project":"15070",
        String pid;//"pid":"26",
        String v1;//"v1":"user_1", - user id
        String v2;//"v2":"John Smith", - user name
        String v3;//"v3":"",
        String email;//"email":null,

        @Override
        public void parse(JSONObject jobj) {
            this.state      = jobj.optInt("state");
            this.out        = jobj.optInt("out");

            this.stateText  = jobj.optString("stateText");
            this.invoice    = jobj.optString("invoice");
            this.project    = jobj.optString("project");
            this.pid        = jobj.optString("pid");
            this.v1         = jobj.optString("v1");
            this.v2         = jobj.optString("v2");
            this.v3         = jobj.optString("v3");
            this.email      = jobj.optString("email");
        }

        @Override
        public String toString() {
            return "\n\tXStatusData{" +
                    "state=" + state +
                    ", out=" + out +
                    ", stateText='" + stateText + '\'' +
                    ", invoice='" + invoice + '\'' +
                    ", project='" + project + '\'' +
                    ", pid='" + pid + '\'' +
                    ", v1='" + v1 + '\'' +
                    ", v2='" + v2 + '\'' +
                    ", v3='" + v3 + '\'' +
                    ", email='" + email + '\'' +
                    '}';
        }
    }
    /**
     *
     */
    class XRepayment implements IParseble {

        String urlRepay;//"url_repay":"?theme=1&project=15070",
        String urlMore;//"url_more":"?theme=100&project=15070",
        String payMoreLabel;//"pay_more_label":"Pay more"

        @Override
        public void parse(JSONObject jobj) {
            this.urlRepay       = jobj.optString("url_repay");
            this.urlMore        = jobj.optString("url_more");
            this.payMoreLabel   = jobj.optString("pay_more_label");

        }

        @Override
        public String toString() {
            return "\n\tXRepayment{" +
                    "urlRepay='" + urlRepay + '\'' +
                    ", urlMore='" + urlMore + '\'' +
                    ", payMoreLabel='" + payMoreLabel + '\'' +
                    '}';
        }
    }
    /**
     *
     */
    class XAutoRecharge implements IParseble {

        String                  labelError;//"label_error":"Technical error has occurred. Please try again later.",
        String                  labelContinue;//"label_continue":"Activate"
        HashMap<String, String> mapParams;//"params":[],
        //"packages":{},

        public XAutoRecharge() {
            this.mapParams = new HashMap<>(4);
        }

        @Override
        public void parse(JSONObject jobj) {
            this.labelError         = jobj.optString("label_error");
            this.labelContinue      = jobj.optString("label_continue");
            JSONArray jarrParams    = jobj.optJSONArray("params");
            if(jarrParams != null)
                for (int i = 0; i < jarrParams.length(); i++) {
                    JSONObject jobjParam = jarrParams.optJSONObject(i);
                    mapParams.put(jobjParam.optString("name"), jobjParam.optString("value"));
                }
        }

        @Override
        public String toString() {
            return "\n\tXAutoRecharge{" +
                    "labelError='" + labelError + '\'' +
                    ", labelContinue='" + labelContinue + '\'' +
                    ", mapParams=" + mapParams +
                    '}';
        }
    }
    /**
     *
     */
    public class XText implements IParseble {

        String                      state;//"state":"Your payment was completed. Thank you for your order!",
        String                      backUrl;//"backurl":"https:\/\/secure.xsolla.com?user_id=user_1",
        String                      backUrlCaption;//"backurl_caption":"Back to the game"
        HashMap<String, XInfoElem>  mapInfo;//"info":{},
        ArrayList<XInfoElem>        listInfo;//"info":{},

        public XText() {
            this.mapInfo    = new HashMap<>(6);
            this.listInfo   = new ArrayList<>(6);
        }

        public String getState() {
            return state;
        }

        public String getBackUrl() {
            return backUrl;
        }

        public String getBackUrlCaption() {
            return backUrlCaption;
        }

        public HashMap<String, XInfoElem> getMapInfo() {
            return mapInfo;
        }

        public ArrayList<XInfoElem> getItems() {
            return listInfo;
        }

        private void sort() {

        }

        public String getProject() {
            XInfoElem elem = mapInfo.get(XsollaApiConst.R_PROJECT);
            return elem.getPref() + " - " + elem.getValue();
        }

        private final String[] order = {"out", "digital_goods", "invoice", "details", "time", "merchant", "recurrentName", "recurrentDescription", "recurrentPeriod", "recurrentDateNextCharge", "userWallet",  "sum"};

        @Override
        public void parse(JSONObject jobj) {
            this.state          = jobj.optString("state");
            this.backUrl        = jobj.optString("backurl");
            this.backUrlCaption = jobj.optString("backurl_caption");

            JSONObject jobjInfo = jobj.optJSONObject("info");
            if(jobjInfo != null) {
                for (Iterator<String> iter = jobjInfo.keys(); iter.hasNext(); ) {
                    String key = iter.next();
                    JSONObject jobjInfoElem = jobjInfo.optJSONObject(key);
                    XInfoElem newElem = new XInfoElem();
                    newElem.parse(jobjInfoElem);
                    mapInfo.put(key, newElem);
                }
                for(String key : order) {
                    if(mapInfo.containsKey(key)) {
                        if ("recurrentPeriod".equals(key)) {//recurrentPeriodType
                            mapInfo.get(key).setValue(mapInfo.get(key).getValue() + " " + mapInfo.get("recurrentPeriodType").getValue());
                        }
                        listInfo.add(mapInfo.get(key));
                    }
                }
            }
        }

        @Override
        public String toString() {
            return "\n\tXText{" +
                    "state='" + state + '\'' +
                    ", backUrl='" + backUrl + '\'' +
                    ", backUrlCaption='" + backUrlCaption + '\'' +
                    ", mapInfo=" + mapInfo +
                    '}';
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * *
        * INNER CLASSES
        * * * * * * * * * * * * * * * * * * * * * * * * * * */
        /**
         *
         */
        public class XInfoElem implements IParseble {
            String key;//"key":"time",
            String pref;//"pref":"Date",
            String value;//"value":"2016-01-20T16:58:45+03:00"

            public String getKey() {
                return key;
            }

            public String getPref() {
                return pref;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            @Override
            public void parse(JSONObject jobj) {
                this.key    = jobj.optString("key");
                this.pref   = jobj.optString("pref");
                this.value  = jobj.optString("value");

                switch (key) {
                    case "time":
                        Date dateTime = stringToDate(this.value);
                        this.value = dateToString(dateTime);
                        break;
                    case "recurrentDateNextCharge":
                        Date date = stringToDate(this.value);
                        if(!"RU".equals(country))
                            this.value = dateToString(date, "MM/dd/yyyy");
                        else
                            this.value = dateToString(date, "dd.MM.yyyy");
                        break;
                    case "userWallet":
                    case "sum":
                        String[] arr = this.value.split(" ");
                        this.value = PriceFormatter.formatPrice(arr[1], arr[0]);
                        break;
                }
            }

            @Override
            public String toString() {
                return "XInfoElem{" +
                        "key='" + key + '\'' +
                        ", pref='" + pref + '\'' +
                        ", value='" + value + '\'' +
                        '}';
            }
        }

    }

    public Date stringToDate(String dateString) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
        try {
            return format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public String dateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
        if("RU".equals(country))
            dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US);
        return dateFormat.format(date);
    }

    public String dateToString(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.US);
        return dateFormat.format(date);
    }

}
