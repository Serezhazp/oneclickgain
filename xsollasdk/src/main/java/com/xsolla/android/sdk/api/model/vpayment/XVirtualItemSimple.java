package com.xsolla.android.sdk.api.model.vpayment;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

/**
 */
public class XVirtualItemSimple implements IParseble {

    private String 	name;
    private String 	imageUrl;
    private int     quantity;

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl.startsWith("http") ? imageUrl : "https:" + imageUrl;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public void parse(JSONObject jobj) {
        name        = jobj.optString("name");
        imageUrl    = jobj.optString("image_url");
        quantity    = jobj.optInt("quantity");
    }

    @Override
    public String toString() {
        return "XVirtualItemSimple{" +
                "name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
