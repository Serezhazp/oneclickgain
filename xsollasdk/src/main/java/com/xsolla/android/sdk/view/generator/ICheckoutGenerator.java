package com.xsolla.android.sdk.view.generator;

import android.content.Context;
import android.view.View;

import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;

import java.util.HashMap;

/**
 *
 */
public interface ICheckoutGenerator {
    public abstract View generate(Context context, XDirectpayment form, HashMap<String, String> translations);
    public abstract String validate();
}
