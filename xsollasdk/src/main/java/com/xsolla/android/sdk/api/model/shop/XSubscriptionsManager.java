package com.xsolla.android.sdk.api.model.shop;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 */
public class XSubscriptionsManager implements IParseble {

    private String                   activeUserPackage;// active_user_package
    private ArrayList<XSubscription> listPackages;// packages


    public String getActiveUserPackage() {
        return activeUserPackage;
    }

    public ArrayList<XSubscription> getListPackages() {
        return listPackages;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONArray jarrList = jobj.optJSONArray("packages");
        if(jarrList != null) {
            listPackages = new ArrayList<>(jarrList.length());
            for (int i = 0; i < jarrList.length(); i++) {
                XSubscription subscription = new XSubscription();
                subscription.parse(jarrList.optJSONObject(i));
                listPackages.add(subscription);
            }
        }

        activeUserPackage = jobj.optString("active_user_package");
    }

    @Override
    public String toString() {
        return "XSubscriptionsManager{" +
                "activeUserPackage='" + activeUserPackage + '\'' +
                ", listPackages=" + listPackages +
                '}';
    }
}
