package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 *
 */
public class XFragment extends Fragment {

    OnFragmentAppearedListener mListener;
    private String mTitle;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentAppearedListener) {
            mListener = (OnFragmentAppearedListener) context;
            mListener.onFragmentAppeared();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onFragmentCreated();
    }

    protected void setTitle(String newTitle) {
        mTitle = newTitle;
    }

    public String getTitle() {
        return mTitle;
    };

    public interface OnFragmentAppearedListener {
        void onFragmentAppeared();
        void onFragmentCreated();
    }
}
