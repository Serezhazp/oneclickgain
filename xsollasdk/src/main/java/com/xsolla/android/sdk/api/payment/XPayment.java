package com.xsolla.android.sdk.api.payment;

import android.util.Pair;

import com.xsolla.android.sdk.api.XPurchase;
import com.xsolla.android.sdk.api.model.XError;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.psystems.XPaymentSystemsManager;
import com.xsolla.android.sdk.api.model.shop.XPricepointsManager;
import com.xsolla.android.sdk.api.model.shop.XSubscriptionsManager;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XVPStatus;
import com.xsolla.android.sdk.api.model.vpayment.XVPSummary;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public interface XPayment {

    /**
     * Init payment process
     */
    void startPayment();

    /**
     * Method used to continue payment process when onFormReceived
     *
     */
    void nextStep(String key, Object value);

    void nextStep(HashMap<String, Object> fields);

    /**
     * Method to check payment status by invoice
     */
    void status(String token, String invoice);

    /**
     * Method to check payment status by invoice
     */
    void setModeSandbox(boolean isSandbox);

    /**
     * Basic payment listener
     */
    interface XPaymentListener {

        void onUtilsReceived(XUtils utils);

        void onUtilsReceivedAlt(Pair<XUtils, XCountryManager> utilsCountry);

        void onShopRequired(XUtils utils);

        // ++++++++++++++++++++++ SHOP ++++++++++++++++++++++++++++
//        public abstract void onVIGroupsReceived();
//
        void onVirtualItemsReceived(ArrayList<Object> groupsItemsList);
//
        void onPricepointsReceived(XPricepointsManager manager);
//
        void onSubscriptionsReceived(XSubscriptionsManager manager);
        // ++++++++++++++++++++ X SHOP X ++++++++++++++++++++++++++

//        public abstract void onSummaryReceived();// VC payment
//
//        public abstract void onProceedReceived();// VC payment

        // ++++++++++++++++++++++ PAYMENT METHODS ++++++++++++++++++++++++++++

        void onPaymentSystemsReceived(XPaymentSystemsManager manager);

        // ++++++++++++++++++++ X PAYMENT METHODS X ++++++++++++++++++++++++++

//        public abstract void onSufficiencyReceived();// SPEC

        // ++++++++++++++++++++++ PAYMENT PROCESS ++++++++++++++++++++++++++++

        void onVpSummaryRecieved(XVPSummary vpSummary);


        void onVPStatusRecieved(XVPStatus vpStatus);

        void onDirectpaymentReceived(XDirectpayment checkout);
//
//        public abstract void onStatusReceived(XDirectpayment status);
        // ++++++++++++++++++++ X PAYMENT PROCESS X ++++++++++++++++++++++++++


        // ++++++++++++++++++++++ PAYMENT PROCESS ++++++++++++++++++++++++++++



        void onErrorReceived(XError error);
    }

    /*
   * Basic payment params minimum required buy api
   * */
    public static class Params {

        protected XPurchase purchase;

        public Params(XPurchase purchase) {
            this.purchase = purchase;
        }

        public XPurchase getPurchase() {
            return purchase;
        }
    }
}
