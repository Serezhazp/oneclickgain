package com.xsolla.android.sdk;

import android.app.Activity;
import android.content.Context;

import com.xsolla.android.sdk.view.XsollaUIHelper;


/**
 * Class for working with xsolla SDK
 * <p>use method <b>createPaymentForm(Context context, XsollaWallet wallet)</b> - to create our payment form</p>
 * <p>use method <b>directPayment(XsollaPaymentSimple payment)</b> - to provide own form</p>
 */
public class XsollaSDK {

    public static final boolean DEBUG = true;
    public static final String SDK_TAG = "XSOLLA SDK";

    /**
     * Method for creation basic xsolla payment form, paystation like
     * result will be returned in onActivityResult, request code
     *
     * @param context context to start payment form Activity;
     * @param accessToken  contains accessToken params for payment request
     * @param isSandboxModeEnabled
     * @see com.xsolla.android.sdk.view.XsollaActivity
     */
   public static void createPaymentForm(Context context, String accessToken, boolean isSandboxModeEnabled) {
        XsollaUIHelper.startXsollaActivity((Activity) context, accessToken, isSandboxModeEnabled);
   }


}
