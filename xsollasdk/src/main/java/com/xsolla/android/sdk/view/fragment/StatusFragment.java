package com.xsolla.android.sdk.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xsolla.android.sdk.R;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XVPStatus;
import com.xsolla.android.sdk.api.model.сheckout.XStatus;
import com.xsolla.android.sdk.view.XsollaActivity;
import com.xsolla.android.sdk.view.adapter.StatusElementsAdapter;
import com.xsolla.android.sdk.view.widget.IconTextView;

public class StatusFragment extends XFragment implements View.OnClickListener {

    public final static String XSOLLA_STATUS = "xsolla_status";

    private XUtils  mUtils;
    private XStatus mStatus;
    private XVPStatus mVPStatus;

    private TextView tvCompleteText, tvProductName;
    private LinearLayout llStatus;
    private View statusTextBg;
    private IconTextView icStatus;
    private Button btnComplete;

    private OnFragmentInteractionListener mListener;

    public static StatusFragment newInstance() {
        return new StatusFragment();
    }

    public StatusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
        if (getArguments() != null) {
//            xsollaForm = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        View rootView = inflater.inflate(R.layout.xsolla_status_form, null);
        findView(rootView);
        if(mStatus != null)
            initView(inflater, mStatus);
        else
            initView(inflater, mVPStatus);
        return rootView;
    }

    private void findView(View rootView) {
        tvCompleteText = (TextView) rootView.findViewById(R.id.tvCompleteText);
        tvProductName = (TextView) rootView.findViewById(R.id.tvProductName);
        llStatus = (LinearLayout) rootView.findViewById(R.id.llStatus);
        statusTextBg = rootView.findViewById(R.id.xsolla_status_title_bg);
        icStatus = (IconTextView) rootView.findViewById(R.id.xsolla_icon);
        btnComplete = (Button) rootView.findViewById(R.id.btnComplete);
    }

    private void initView(LayoutInflater inflater, XStatus xsollaStatus) {
        switch (xsollaStatus.getGroup()){
            case "done":
                statusTextBg.setBackgroundResource(R.color.xsolla_green);
                icStatus.setText("\uE017");
                break;
            case "invoice":
            case "delivering":
                statusTextBg.setBackgroundResource(R.color.xsolla_purple_m);
                icStatus.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.xsolla_rotation));
                icStatus.setText("\uE01C");
                break;
            case "troubled":
                statusTextBg.setBackgroundResource(R.color.xsolla_error);
                icStatus.setText("\uE00C");
                break;
            default:
        }

        if(xsollaStatus.isCancelUser()) {
            statusTextBg.setBackgroundResource(R.color.xsolla_error);
            icStatus.setText("\uE00C");
        }

        tvCompleteText.setText(xsollaStatus.getText().getState());
        tvProductName.setText(xsollaStatus.getText().getProject());//.setVisibility(View.GONE);
        btnComplete.setText(mUtils.getTranslations().get("back_to_store"));
        btnComplete.setOnClickListener(this);

        StatusElementsAdapter adapter = new StatusElementsAdapter(getActivity(), xsollaStatus.getText().getItems());
        for (int i = 0; i < adapter.getCount(); i++) {
            llStatus.addView(adapter.getView(i, null, null));
            if(i < adapter.getCount() - 1) {
                View divider = inflater.inflate(R.layout.xsolla_elem_dotted_divider, null);
                llStatus.addView(divider);
            }
        }
    }

    private void initView(LayoutInflater inflater,XVPStatus xvpStatus) {
        statusTextBg.setBackgroundResource(R.color.xsolla_green);
        icStatus.setText("\uE017");
        tvCompleteText.setText(xvpStatus.getHeader());
        tvProductName.setText(mUtils.getProject().getName());//.setVisibility(View.GONE);
        btnComplete.setText(mUtils.getTranslations().get("back_to_store"));
        btnComplete.setOnClickListener(this);



        View divider = inflater.inflate(R.layout.xsolla_elem_dotted_divider, null);
        llStatus.addView(createElem(inflater ,mUtils.getTranslations().get("virtualstatus_check_operation"), xvpStatus.getOperationId()));
        llStatus.addView(divider);

        divider = inflater.inflate(R.layout.xsolla_elem_dotted_divider, null);
        llStatus.addView(createElem(inflater ,mUtils.getTranslations().get("virtualstatus_check_time"), xvpStatus.getOperationCreated()));
        llStatus.addView(divider);

        divider = inflater.inflate(R.layout.xsolla_elem_dotted_divider, null);
        llStatus.addView(createElem(inflater ,mUtils.getTranslations().get("virtualstatus_check_virtual_items"), xvpStatus.getPurchase(0)));
        llStatus.addView(divider);

        llStatus.addView(createElem(inflater ,mUtils.getTranslations().get("virtualstatus_check_vc_amount"), xvpStatus.getVcAmount() + " " + mUtils.getProject().getVirtualCurrencyName()));

    }

    private View createElem(LayoutInflater inflater, String title, String value) {
        View elem = inflater.inflate(R.layout.xsolla_status_element, null);
        TextView tvTitle = (TextView) elem.findViewById(R.id.tvTitle);
        TextView tvValue = (TextView) elem.findViewById(R.id.tvValue);
        tvTitle.setText(title);
        tvValue.setText(value);
        return elem;
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onClickComplete();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener   = (OnFragmentInteractionListener) context;
            if(((XsollaActivity)getActivity()).getDirectpayment() != null)
                mStatus     =  ((XsollaActivity)getActivity()).getDirectpayment().getStatus();
            mVPStatus   =  ((XsollaActivity)getActivity()).getVPStatus();
            mUtils      =  ((XsollaActivity)getActivity()).getUtils();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        onButtonPressed();
    }

    public interface OnFragmentInteractionListener {
        void onClickComplete();
    }

}
