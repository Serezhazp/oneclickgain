package com.xsolla.android.sdk.api.model.util;

import com.xsolla.android.sdk.api.model.IParseble;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 */
public class XSettings implements IParseble {

    private HashMap<String, Boolean> components;

    public XSettings() {
        this.components = new HashMap<>();
    }

    public HashMap<String, Boolean> getComponents() {
        return components;
    }

    @Override
    public void parse(JSONObject jobj) {
        JSONObject jobjComponents = jobj.optJSONObject("components");
        if(jobjComponents != null) {
            Iterator<String> iter = jobjComponents.keys();
            while (iter.hasNext()) {
                String name = iter.next();
                JSONObject jobjElem = jobjComponents.optJSONObject(name);
                boolean isHidden = jobjElem.optBoolean("hidden");
                components.put(name, isHidden);
            }
        }
    }

}
