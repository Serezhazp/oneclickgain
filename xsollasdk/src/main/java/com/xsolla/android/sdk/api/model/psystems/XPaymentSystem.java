package com.xsolla.android.sdk.api.model.psystems;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

/**
 *
 */
public class XPaymentSystem extends XPaymentSystemBase {

    private String  aliases;// "aliases":"",
    private String  recurrentType;// "recurrent_type":"notify",
    private String  imgUrl;// "imgUrl":"https:\/\/cdn3.xsolla.com\/paymentoptions\/paystation\/theme_33\/69x50\/129.png",
    private String  imgRetinaUrl;// "imgRetinaUrl":"https:\/\/cdn3.xsolla.com\/paymentoptions\/paystation\/theme_33\/69x50\/129@2x.png"

    private int     isHidden;// "hidden":0,
    private int     isRecommended;// "recommended":1,
    private int[]   cat;//"cat":[5],

    public XPaymentSystem() {
    }

    public XPaymentSystem(XPaymentSystemBase base, String imgUrl) {
        super(base.getId(), base.getName());
        this.imgUrl = imgUrl;
    }

    public String getAliases() {
        return aliases;
    }

    public String getRecurrentType() {
        return recurrentType;
    }

    public String getImgUrl() {
        return imgUrl.startsWith("https:") ? imgUrl : "https:" + imgUrl;
    }

    public String getImgRetinaUrl() {
        return imgRetinaUrl.startsWith("https:") ? imgRetinaUrl : "https:" + imgRetinaUrl;
    }

    public int getIsHidden() {
        return isHidden;
    }

    public int getIsRecommended() {
        return isRecommended;
    }

    public int[] getCat() {
        return cat;
    }


    @Override
    public void continueParse(JSONObject jobj) {
        aliases         = jobj.optString("aliases");
        recurrentType   = jobj.optString("recurrent_type");
        imgUrl          = jobj.optString("imgUrl");
        imgRetinaUrl    = jobj.optString("imgRetinaUrl");

        isHidden        = jobj.optInt("hidden");
        isRecommended   = jobj.optInt("recommended");
        JSONArray jarr  =  jobj.optJSONArray("cat");
        if(jarr != null) {
            cat = new int[jarr.length()];
            for (int i = 0; i < jarr.length(); i++) {
                cat[i] = jarr.optInt(i);
            }
        }
    }

    @Override
    public String toString() {
        return "\nXPaymentSystem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", recurrentType='" + recurrentType + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", imgRetinaUrl='" + imgRetinaUrl + '\'' +
                ", isHidden=" + isHidden +
                ", isRecommended=" + isRecommended +
                ", cat=" + Arrays.toString(cat) +
                '}';
    }
}
