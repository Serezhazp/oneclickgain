package com.xsolla.android.sdk.api.request;

import com.xsolla.android.sdk.api.model.additional.XSufficiency;
import com.xsolla.android.sdk.api.model.additional.XToken;
import com.xsolla.android.sdk.api.model.psystems.XCountryManager;
import com.xsolla.android.sdk.api.model.psystems.XPSAllManager;
import com.xsolla.android.sdk.api.model.psystems.XPSManager;
import com.xsolla.android.sdk.api.model.psystems.XPSQuickManager;
import com.xsolla.android.sdk.api.model.shop.XPricepointsManager;
import com.xsolla.android.sdk.api.model.shop.XSubscriptionsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemGroupsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemsManager;
import com.xsolla.android.sdk.api.model.shop.vitems.XVirtualItemsManagerMobile;
import com.xsolla.android.sdk.api.model.util.XUtils;
import com.xsolla.android.sdk.api.model.vpayment.XProceed;
import com.xsolla.android.sdk.api.model.vpayment.XVPStatus;
import com.xsolla.android.sdk.api.model.vpayment.XVPSummary;
import com.xsolla.android.sdk.api.model.сheckout.XDirectpayment;

import java.util.Map;

import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 *
 */
public interface XsollaApi {

    @FormUrlEncoded
    @POST("utils")
    Observable<XUtils> fetchUtils(@FieldMap Map<String, Object> params);


    @FormUrlEncoded
    @POST("virtualitems/groups")
    Observable<XVirtualItemGroupsManager> fetchVIGroups(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualitems/items")
    Observable<XVirtualItemsManager> fetchVItems(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualitems")
    Observable<XVirtualItemsManagerMobile> fetchMobileVItems(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualitems/recent")
    Observable<XVirtualItemsManager> fetchVIRecent(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualitems/favorite")
    Observable<XVirtualItemsManager> fetchVIFavorite(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualitems/setfavorite")
    Observable<Object> setFavorite(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("pricepoints")
    Observable<XPricepointsManager> fetchPricepoints(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("recurring")
    Observable<XSubscriptionsManager> fetchSubscriptions(@FieldMap Map<String, Object> params);


    @FormUrlEncoded
    @POST("paymentlist/quick_payments")
    Observable<XPSQuickManager> fetchQuickPS(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("paymentlist")
    Observable<XPSAllManager> fetchAllPS(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("paymentlist/payment_methods")
    Observable<XPSManager> fetchXPS(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("country")
    Observable<XCountryManager> fetchCountries(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("balance/sufficiency")
    Observable<XSufficiency> sufficiency(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("cart/summary")
    Observable<XVPSummary> fetchSummary(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualpayment/proceed")
    Observable<XProceed> proceed(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("virtualstatus")
    Observable<XVPStatus> getVpStatus(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("directpayment")
    Observable<XDirectpayment> fetchDirectpayment(@FieldMap Map<String, Object> params);

    @GET("token")
    Observable<XToken> fetchToken(@Query("data") String data);

}
