package com.xsolla.android.sdk.api.model.shop.vitems;

import com.xsolla.android.sdk.api.model.IParseble;
import com.xsolla.android.sdk.api.model.shop.XBonusItem;
import com.xsolla.android.sdk.util.PriceFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

/**
 *
 */
public class XVirtualItem implements IParseble {

    private long        id;// "id":39789,

    private int         quantityLimit;// "quantity_limit":1,
    private int         isFavorite;// "is_favorite":0,
    private double       vcAmount;// "vc_amount":900,
    private double       vcAmountWithoutDiscount;// "vc_amount_without_discount":900
    private int         bonusVirtualCurrency;// "bonus_virtual_currency":{"vc_amount":100},

    private float       amount;// "amount":1.99,
    private float       amountWithoutDiscount;// "amount_without_discount":1.99,// ,

    private String      sku;// "sku":"chicken",
    private String      name;// "name":"Chicken",
    private String      currency;// "currency":"USD",
    private String      imgUrl;// "image_url":"\/\/cdn3.xsolla.com\/img\/misc\/merchant-digital-goods\/1f05e30d637e3dd133807b90d0c8969d.png",
    private String      description;// "description":"Chickens are sometimes kept as pets and can be tamed by hand feeding",
    private String      longDescription;// "long_description":"Some people find chickens' behaviour entertaining and educational.",
    private String      label;// "label":null, //string
    private String      offerLabel;// "offer_label":"",
    private String      advertisementType;// "advertisement_type":null, //string
    private String      tryItUrl;// "try_it_url":null //string
    //private String      unsatisfiedInventoryConditions;// "unsatisfied_inventory_conditions":[{"action":"Action",},],

    private ArrayList<XBonusItem> listBonusItems;// "bonus_virtual_items":[{"name":"Egg","quantity":"1",},],

    public XVirtualItem() {
        listBonusItems = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public float getAmount() {
        return amount;
    }

    public float getAmountWithoutDiscount() {
        return amountWithoutDiscount;
    }

    public String getSku() {
        return sku;
    }

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public String getImgUrl() {
        return imgUrl.startsWith("http") ? imgUrl : "https:" + imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getLongDescription() {
        return description + "\n\n" + longDescription;
    }


    public int getLabelType() {
        if(isBonus() || isPriceChanged())
            return 2;
        switch (advertisementType) {
            case "recommended":
                return 0;
            case "best_deal":
                return 1;
            case "special_offer":
                return 2;
            default:
                return 999;
        }
    }

    public boolean isBonus(){
        return bonusVirtualCurrency > 0 || listBonusItems.size() > 0;
    }

    public String getBonusString(String projectVirtualCurrency) {
        StringBuilder builder = new StringBuilder("+");
        if(bonusVirtualCurrency != 0) {
            builder.append(getBonusOut(projectVirtualCurrency))
                    .append(", ");
            if(listBonusItems.size() > 0)
                builder.append(getBonusItems());
        } else {
            builder.append(getBonusItems());
        }
        return builder.toString();
    }

    public String getBonusOut(String projectVirtualCurrency) {
        return bonusVirtualCurrency + " " + projectVirtualCurrency;
    }

    public String getBonusItems() {
        StringBuilder builder = new StringBuilder();
        for(XBonusItem bonusItem : listBonusItems) {
            builder.append(bonusItem.getQuantity()).append(" ").append(bonusItem.getName()).append(", ");
        }
        int size = builder.length();
        builder.delete(size - 2, size - 1);
        return builder.toString();
    }

    public boolean isPriceChanged() {
        return amount != amountWithoutDiscount;
    }

    public String getPrice() {
        //String realPrice = PriceFormatter.formatPrice(getCurrency(), Float.toString(getAmount()));
        return PriceFormatter.formatPriceSimple(getCurrency(), String.format(Locale.US, "%.2f", getAmount()));
    }

    public String getPriceWithoutDiscount() {
        //String realPrice = PriceFormatter.formatPrice(getCurrency(), Float.toString(getAmount()));
        return PriceFormatter.formatPriceSimple(getCurrency(), String.format(Locale.US, "%.2f", getAmountWithoutDiscount()));
    }

    public String getVcAmount() {
        return fmt(vcAmount);
    }

    public String getVcAmountWithoutDiscount() {
        return fmt(vcAmountWithoutDiscount);
    }

    public boolean isVirtualPayment() {
        return vcAmount > 0 || vcAmountWithoutDiscount > 0;
    }


    @Override
    public void parse(JSONObject jobj) {

        id = jobj.optLong("id");

        quantityLimit           = jobj.optInt("quantityLimit");
        isFavorite              = jobj.optInt("is_favorite");

        vcAmount                = jobj.optDouble("vc_amount");
        vcAmountWithoutDiscount = jobj.optDouble("vc_amount_without_discount");

        JSONObject jobjBonus = jobj.optJSONObject("bonus_virtual_currency");
        if(jobjBonus != null)
            bonusVirtualCurrency = jobjBonus.optInt("vc_amount");

        amount                  = (float) jobj.optDouble("amount");
        amountWithoutDiscount   = (float) jobj.optDouble("amount_without_discount");

        sku                     = jobj.optString("sku");
        name                    = jobj.optString("name");
        currency                = jobj.optString("currency");
        imgUrl                  = jobj.optString("image_url");
        description             = jobj.optString("description");
        longDescription         = jobj.optString("long_description");
        label                   = jobj.optString("label");
        offerLabel              = jobj.optString("offer_label");
        advertisementType       = jobj.optString("advertisement_type");
        tryItUrl                = jobj.optString("try_it_url");

        JSONArray jarr = jobj.optJSONArray("bonus_virtual_items");
        if(jarr != null)
            for(int i = 0; i < jarr.length(); i++) {
                JSONObject jobjNewItem = jarr.optJSONObject(i);
                XBonusItem newBonusItem = new XBonusItem(jobjNewItem.optString("name"), jobjNewItem.optString("quantity"));
                listBonusItems.add(newBonusItem);
            }

    }

    public static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }

    @Override
    public String toString() {
        return "XVirtualItem{" +
                "id=" + id +
                ", quantityLimit=" + quantityLimit +
                ", isFavorite=" + isFavorite +
                ", vcAmount=" + vcAmount +
                ", vcAmountWithoutDiscount=" + vcAmountWithoutDiscount +
                ", bonusVirtualCurrency=" + bonusVirtualCurrency +
                ", amount=" + amount +
                ", amountWithoutDiscount=" + amountWithoutDiscount +
                ", sku='" + sku + '\'' +
                ", name='" + name + '\'' +
                ", currency='" + currency + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", description='" + description + '\'' +
                ", longDescription='" + longDescription + '\'' +
                ", label='" + label + '\'' +
                ", offerLabel='" + offerLabel + '\'' +
                ", advertisementType='" + advertisementType + '\'' +
                ", tryItUrl='" + tryItUrl + '\'' +
                ", listBonusItems=" + listBonusItems +
                '}';
    }
}
