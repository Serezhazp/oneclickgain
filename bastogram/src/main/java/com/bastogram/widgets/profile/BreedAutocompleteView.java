package com.bastogram.widgets.profile;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.bastogram.R;
import com.bastogram.loadres.BreedDataLoader;
import com.bastogram.models.Breed;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;

import java.util.List;
import java.util.Locale;

/**
 * Created by serezha on 19.08.16.
 */
public class BreedAutocompleteView extends AutoCompleteTextView {

	private BreedAutocompleteAdapter mAdapter;
	private Breed mSelectedBreed;
	private AdapterView.OnItemSelectedListener mListener;
	private BreedDataLoader mBreedDataLoader;

	public BreedAutocompleteView(Context context) {
		super(context, null);
	}

	public BreedAutocompleteView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSaveEnabled(false);
		setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
		setTextSize(18);
		setThreshold(0);

		setHintTextColor(getResources().getColor(R.color.light_gray));

		setHint(R.string.profile_pet_kind);

		mAdapter = new BreedAutocompleteAdapter();
		setAdapter(mAdapter);

		setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				setSelectedPosition(i);
				if(mListener != null)
					mListener.onItemSelected(adapterView, view, i, l);
			}
		});

		setOnItemSelectedListener(null);

		addTextChangedListener(new SimpleTextListener() {
			@Override
			public void afterTextChanged(Editable s) {
				String text = s.toString().toLowerCase();

				if(mSelectedBreed != null && mSelectedBreed.getTitle().toLowerCase(Locale.getDefault()).equals(text) || text.contains("@")) {
					return;
				}

				mSelectedBreed = null;
				for(int i = 0; i < mAdapter.getCount(); i++) {
					Breed breed = mAdapter.getItem(i);
					if(breed.getTitle().toLowerCase(Locale.getDefault()).equals(text)) {
						mSelectedBreed = breed;
					}
				}
			}
		});

		setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean b) {
				showDropDown();
			}
		});

		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				if(hasFocus()) {
					if(!isPopupShowing())
						showDropDown();
					else
						requestFocus();
				}
				return false;
			}
		});
	}

	public Breed getSelectedItem() {
		return mSelectedBreed;
	}

	public int getSelectedItemId() {
		return mSelectedBreed != null ? mSelectedBreed.getId() : -1;
	}

	public void setBreedDataLoader(BreedDataLoader loader) {
		mBreedDataLoader = loader;
	}

	public void setBreedList(List<Breed> breedList) {
		mAdapter.set(breedList);
		setVisibility(VISIBLE);
	}

	public void setSelectedPosition(int position) {
		mSelectedBreed = mAdapter.getItem(position);
		String breedName = mSelectedBreed.getTitle();
		setText(breedName);
		setSelection(breedName.length()); // Why length is taken for id?
	}

	@Override
	public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener l) {
		mListener = l;
		super.setOnItemSelectedListener(new WrapperOnItemSelecredListener());
	}

	private class WrapperOnItemSelecredListener implements AdapterView.OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			setSelectedPosition(i);

			if(mListener != null)
				mListener.onItemSelected(adapterView, view, i, l);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {
			if(mListener != null)
				mListener.onNothingSelected(adapterView);
		}
	}
}
