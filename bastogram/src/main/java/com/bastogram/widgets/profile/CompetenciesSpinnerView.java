package com.bastogram.widgets.profile;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.bastogram.models.Competence;
import com.bastogram.views.CompetenciesListView;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serezha on 04.08.16.
 */
public class CompetenciesSpinnerView extends Spinner implements CompetenciesListView {

	private SimpleSpinnerAdapter<Competence> mAdapter;

	public CompetenciesSpinnerView(Context context) {
		this(context, null);
	}

	public CompetenciesSpinnerView(Context context, AttributeSet attrs) {
		super(context, attrs);

		setOnItemSelectedListener(null);
		mAdapter = new SimpleSpinnerAdapter<>(new ArrayList<>(0));
		mAdapter.setHasHitItem(true);
		setAdapter(mAdapter);
	}

	@Override
	public void setCompetenciesList(List<Competence> competences) {
		mAdapter.set(competences);
	}

	public void setItemColor(int color) {
		mAdapter.setItemColor(color);
	}

	@Override
	public void setOnItemSelectedListener(OnItemSelectedListener listener) {
		super.setOnItemSelectedListener(listener);
	}

	private class WrappedOnItemSelectedListener implements OnItemSelectedListener {
		private OnItemSelectedListener mListener;

		public WrappedOnItemSelectedListener(OnItemSelectedListener listener) {
			mListener = listener;
		}

		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			if(mListener != null)
				mListener.onItemSelected(adapterView, view, i, l);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {
			if(mListener != null)
				mListener.onNothingSelected(adapterView);
		}
	}
}
