package com.bastogram.widgets.profile.search;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bastogram.R;
import com.bastogram.loadres.BreedDataLoader;
import com.bastogram.models.Nursling;
import com.bastogram.widgets.profile.BreedAutocompleteView;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.fragments.profile.ProfilePrivateInfoFragment;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.views.behaviour.IGrabSearchInfo;
import com.humanet.humanetcore.views.widgets.switch_button.SwitchButton;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by serezha on 21.07.16.
 */
public class SearchPetLayout extends ScrollView implements IGrabSearchInfo, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

	private Nursling mNursling;

	private EditText mNicknameEditText;
	private BreedDataLoader mBreedDataLoader;

	private SwitchButton mGenderSwitch;
	private SwitchButton mVaccinationSwitch;
	private SwitchButton mReadyForChildrenSwitch;

	public SearchPetLayout(Context context) {
		super(context);

		inflate(context, R.layout.layout_search_pet, this);

		mNursling = new Nursling();

		mNicknameEditText = (EditText) findViewById(R.id.field_first_name);

		mBreedDataLoader = new BreedDataLoader(context, (BreedAutocompleteView) findViewById(R.id.sp_breed), -1);

		TextView birthDate = (TextView) findViewById(R.id.txt_birth_field);
		birthDate.setOnClickListener(this);


		mGenderSwitch = (SwitchButton) findViewById(R.id.gender);
		mGenderSwitch.setOnCheckedChangeListener(this);
		findViewById(R.id.male).setOnClickListener(this);
		findViewById(R.id.female).setOnClickListener(this);


		mReadyForChildrenSwitch = (SwitchButton) findViewById(R.id.reproduction);
		mReadyForChildrenSwitch.setOnCheckedChangeListener(this);
		findViewById(R.id.ready_for_child).setOnClickListener(this);
		findViewById(R.id.not_ready_for_child).setOnClickListener(this);


		mVaccinationSwitch = (SwitchButton) findViewById(R.id.vaccination);
		mVaccinationSwitch.setOnCheckedChangeListener(this);
		findViewById(R.id.vaccinated).setOnClickListener(this);
		findViewById(R.id.not_vaccinated).setOnClickListener(this);
	}

	private void invalidateGender(@UserInfo.Sex int sex) {
		mNursling.setGender(sex);

		mGenderSwitch.setOnCheckedChangeListener(null);
		mGenderSwitch.setChecked(sex == UserInfo.FEMALE);
		mGenderSwitch.setOnCheckedChangeListener(this);
	}

	private void invalidateReproduction(boolean readyForChild) {
		mNursling.setChildReady(readyForChild);

		mReadyForChildrenSwitch.setOnCheckedChangeListener(null);
		mReadyForChildrenSwitch.setChecked(readyForChild);
		mReadyForChildrenSwitch.setOnCheckedChangeListener(this);
	}

	private void invalidateVaccination(boolean vaccination) {
		mNursling.setVaccination(vaccination);

		mVaccinationSwitch.setOnCheckedChangeListener(null);
		mVaccinationSwitch.setChecked(vaccination);
		mVaccinationSwitch.setOnCheckedChangeListener(this);
	}

	@Override
	public void grabSearchInfo(ArgsMap params) {
		if (!TextUtils.isEmpty(mNicknameEditText.getText().toString())) {
			params.put("nickname", mNicknameEditText.getText().toString());
		}

		if (mBreedDataLoader.getSelectedBreedId() != -1) {
			params.put("kind", mBreedDataLoader.getSelectedBreedId());
		}

		params.put("birth_date", mNursling.getBirthDate());

		params.put("gender", mGenderSwitch.isChecked());
		params.put("vaccination", mVaccinationSwitch.isChecked());
		params.put("ready_for_children", mReadyForChildrenSwitch.isChecked());
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
			case R.id.gender:
				invalidateGender(isChecked ? UserInfo.FEMALE : UserInfo.MALE);
				break;

			case R.id.reproduction:
				invalidateReproduction(isChecked);
				break;

			case R.id.vaccination:
				invalidateVaccination(isChecked);
				break;
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.male:
				invalidateGender(UserInfo.MALE);
				break;
			case R.id.female:
				invalidateGender(UserInfo.FEMALE);
				break;
			case R.id.not_ready_for_child:
				invalidateReproduction(false);
				break;
			case R.id.ready_for_child:
				invalidateReproduction(true);
				break;
			case R.id.not_vaccinated:
				invalidateVaccination(false);
				break;
			case R.id.vaccinated:
				invalidateVaccination(true);
				break;
			case R.id.txt_birth_field:
				final Calendar c = Calendar.getInstance(Locale.getDefault());
				c.setTimeInMillis(mNursling.getBirthDate() * 1000);
				DatePickerDialog d;
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
					d = new DatePickerDialog(getContext(), com.humanet.humanetcore.R.style.DialogTheme, mOnDateChangeListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				} else {
					d = new DatePickerDialog(getContext(), mOnDateChangeListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
				}
				d.show();
				break;
		}

	}

	private DatePickerDialog.OnDateSetListener mOnDateChangeListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			final Calendar c = Calendar.getInstance(Locale.getDefault());
			c.set(year, monthOfYear, dayOfMonth);
			mNursling.setBirthDate(c.getTimeInMillis() / 1000);

			displayDate();

		}
	};

	private void displayDate() {
		TextView view = (TextView) findViewById(R.id.txt_birth_field);
		view.setText(ProfilePrivateInfoFragment.DATE_FORMAT.format(new Date(mNursling.getBirthDate() * 1000)));
		view.setTextColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
	}
}
