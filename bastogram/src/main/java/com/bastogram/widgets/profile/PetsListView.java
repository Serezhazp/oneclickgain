package com.bastogram.widgets.profile;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bastogram.R;
import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.Nursling;
import com.bastogram.presenters.profile.LoadPetsPresenter;
import com.bastogram.views.IPetsView;
import com.bastogram.views.Updatable;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.TimeIntervalConverter;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Uran on 10.06.2016.
 */
public class PetsListView extends ListView implements IPetsView, Updatable {

    private Adapter mAdapter;
    private SpiceManager mSpiceManager;
    private LoadPetsPresenter mLoadPetsPresenter;
    private int mUserId;
    private Context mContext;
    protected boolean mIsNeedToHighlightOwnPets = true;

    public PetsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public PetsListView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public PetsListView(Context context, int userId) {
        super(context);
        mContext = context;
        init();
        mUserId = userId;
    }

    public PetsListView(Context context, int userId, boolean isNeedToHighlightOwnPets) {
        super(context);
        mContext = context;
        mIsNeedToHighlightOwnPets = isNeedToHighlightOwnPets;
        init();
        mUserId = userId;
    }

    private void init() {
        int padding = (int) ConverterUtil.dpToPix(getContext(), 5);
        setDivider(null);
        setDividerHeight(padding);
        setPadding(padding, padding, padding, padding);


        mAdapter = new Adapter(mContext, mIsNeedToHighlightOwnPets);
        setAdapter(mAdapter);
    }

    public void setSpiceManager(SpiceManager spiceManager) {
        mSpiceManager = spiceManager;
        mLoadPetsPresenter = new LoadPetsPresenter(mSpiceManager, this);
        mLoadPetsPresenter.load(mUserId);
    }


    @Override
    public void showPetList(Nursling[] nurslings) {
        mAdapter.setNurslings(nurslings);
    }

    //--
    private static class Adapter extends BaseAdapter {

        private Context mContext;
        private boolean mIsNeedToHighlightOwnPets;

        public Adapter(Context context, boolean isNeedToHighlightOwnPets) {
            mContext = context;
            mIsNeedToHighlightOwnPets = isNeedToHighlightOwnPets;
        }

        private Nursling[] mNurslings;

        public void setNurslings(Nursling[] nurslings) {
            mNurslings = nurslings;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mNurslings != null ? mNurslings.length : 0;
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getId();
        }

        @Override
        public Nursling getItem(int position) {
            return mNurslings[position];
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Nursling nursling = getItem(position);

            ViewHolder viewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pet, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(mIsNeedToHighlightOwnPets && isOwnNursling(nursling)) {
                ((CardView) convertView).setCardBackgroundColor(mContext.getResources().getColor(com.humanet.humanetcore.R.color.listItemSelected));
            } else {
                ((CardView) convertView).setCardBackgroundColor(mContext.getResources().getColor(com.humanet.humanetcore.R.color.listItemNotSelected) );
            }

            viewHolder.pawAchievementBronze.setVisibility(INVISIBLE);
            viewHolder.pawAchievementSilver.setVisibility(INVISIBLE);
            viewHolder.pawAchievementGold.setVisibility(INVISIBLE);
            viewHolder.pawAchievementRuby.setVisibility(INVISIBLE);
            viewHolder.pawAchievementBrilliant.setVisibility(INVISIBLE);

            if(nursling.getName() != null)
                viewHolder.nameView.setText(nursling.getName());
            if(nursling.getKind() != -1) {
                String[] kindTitles = mContext.getResources().getStringArray(R.array.breeds);
                viewHolder.aboutView.setText(String.format(Locale.getDefault(), "%s, %s", TimeIntervalConverter.convert(mContext, nursling.getBirthDate() * 1000), kindTitles[nursling.getKind()]));
            }
            viewHolder.rateView.setText(String.valueOf(nursling.getRating()));

            if(nursling.getAvatar() != null)
                ImageLoader.getInstance().displayImage(nursling.getAvatar(), viewHolder.avatarView);

            viewHolder.genderView.setImageResource(
                    nursling.getGender() == UserInfo.MALE
                            ? R.drawable.ic_pet_male
                            : R.drawable.ic_pet_female
            );

            viewHolder.vaccinationView.setImageResource(
                    nursling.isVaccinated()
                            ? R.drawable.ic_pet_vaccinated
                            : R.drawable.ic_pet_not_vaccinated
            );

            viewHolder.childReadyView.setImageResource(
                    nursling.isChildReady()
                            ? R.drawable.ic_pet_ready_for_child
                            : R.drawable.ic_pet_not_ready_for_child
            );

            if(nursling.getBages() != null) {
                ArrayList<Integer> bages = new ArrayList<>(nursling.getBages());
                for (Integer bage : bages) {
                    if (bage == Nursling.BRONZE)
                        viewHolder.pawAchievementBronze.setVisibility(VISIBLE);
                    if (bage == Nursling.SILVER)
                        viewHolder.pawAchievementSilver.setVisibility(VISIBLE);
                    if (bage == Nursling.GOLD)
                        viewHolder.pawAchievementGold.setVisibility(VISIBLE);
                    if (bage == Nursling.RUBY)
                        viewHolder.pawAchievementRuby.setVisibility(VISIBLE);
                    if (bage == Nursling.BRILLIANT)
                        viewHolder.pawAchievementBrilliant.setVisibility(VISIBLE);
                }
            }

            return convertView;
        }

        private boolean isOwnNursling(Nursling nursling) {
            for(Nursling myNursling : ((BastogramUserInfo) AppUser.getInstance().get()).getNurslings()) {
                if(myNursling.getId() == nursling.getId())
                    return true;
            }

            return false;
        }

        protected void isNeedToHighlightOwnPets(boolean isNeedToHighlightOwnPets) {
            mIsNeedToHighlightOwnPets = isNeedToHighlightOwnPets;
        }
    }

    private static class ViewHolder {
        ImageView avatarView;
        TextView nameView;
        TextView aboutView;
        TextView rateView;

        ImageView genderView;
        ImageView vaccinationView;
        ImageView childReadyView;

        ImageView pawAchievementBronze;
        ImageView pawAchievementSilver;
        ImageView pawAchievementGold;
        ImageView pawAchievementRuby;
        ImageView pawAchievementBrilliant;


        public ViewHolder(View view) {
            avatarView = (ImageView) view.findViewById(R.id.avatar);
            nameView = (TextView) view.findViewById(R.id.name);
            aboutView = (TextView) view.findViewById(R.id.about);
            rateView = (TextView) view.findViewById(R.id.rating);

            genderView = (ImageView) view.findViewById(R.id.gender);
            vaccinationView = (ImageView) view.findViewById(R.id.vaccination);
            childReadyView = (ImageView) view.findViewById(R.id.ready_for_child);

            pawAchievementBronze = (ImageView) view.findViewById(R.id.pawBronze);
            pawAchievementSilver = (ImageView) view.findViewById(R.id.pawSilver);
            pawAchievementGold = (ImageView) view.findViewById(R.id.pawGold);
            pawAchievementRuby = (ImageView) view.findViewById(R.id.pawRuby);
            pawAchievementBrilliant = (ImageView) view.findViewById(R.id.pawBrilliant);

            view.setTag(this);
        }

    }

    @Override
    public void update() {
        if(mLoadPetsPresenter != null && mSpiceManager != null) {
            mLoadPetsPresenter = new LoadPetsPresenter(mSpiceManager, this);
            mLoadPetsPresenter.load(mUserId);
        }
    }

    public void setIsNeedToHighlightOwnPets(boolean isNeedToHighlightOwnPets) {
        mIsNeedToHighlightOwnPets = isNeedToHighlightOwnPets;
        mAdapter.isNeedToHighlightOwnPets(isNeedToHighlightOwnPets);
    }

}
