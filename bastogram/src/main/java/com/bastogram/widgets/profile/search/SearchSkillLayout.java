package com.bastogram.widgets.profile.search;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.bastogram.R;
import com.bastogram.loadres.SkillSubGroupDataLoader;
import com.bastogram.models.Skill;
import com.bastogram.models.WorkingExperience;
import com.bastogram.widgets.profile.CompetenciesSpinnerView;
import com.bastogram.widgets.profile.SkillSubGroupAutocompleteView;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.IGrabSearchInfo;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by serezha on 22.07.16.
 */
public class SearchSkillLayout extends ScrollView implements IGrabSearchInfo, AdapterView.OnItemSelectedListener {

	private Skill mSkill;

	private Spinner mCompetenciesSpinner;

	private EditText mSpecializationEditText;
	private Spinner mWorkingYearsSpinner;
	private Spinner mEducationSpinner;

	private SkillSubGroupDataLoader mSkillSubGroupDataLoader;

	public SearchSkillLayout(Context context) {
		super(context);
	}

	public SearchSkillLayout(Context context, SpiceManager spiceManager) {
		super(context);

		inflate(context, R.layout.layout_search_skill, this);

		mSkill = new Skill();

		/*SkillsRequest request = new SkillsRequest();
		spiceManager.execute(request, new SimpleRequestListener<SkillResponse>() {
			@Override
			public void onRequestSuccess(SkillResponse skillResponse) {
				super.onRequestSuccess(skillResponse);

				ArrayList<Competence> competences = new ArrayList<Competence>();
				Competence competence = new Competence();
				competence.setTitleFromResources(getResources().getString(R.string.profile_competion));
				competences.add(competence);
				competences.addAll(skillResponse.getSkills());
				SimpleSpinnerAdapter competencesAdapter = new SimpleSpinnerAdapter<>(competences);
				competencesAdapter.setItemColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
				competencesAdapter.setHasHitItem(true);
				mCompetenciesSpinner.setAdapter(competencesAdapter);
				mCompetenciesSpinner.setOnItemSelectedListener(SearchSkillLayout.this);
			}
		});*/

		mSpecializationEditText = (EditText) findViewById(R.id.specialization);

		mWorkingYearsSpinner = (Spinner) findViewById(R.id.sp_working_years);
		SimpleSpinnerAdapter workingYearsAdapter = new SimpleSpinnerAdapter<>(WorkingExperience.getInstance().values());
		workingYearsAdapter.setItemColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
		workingYearsAdapter.setHasHitItem(true);
		mWorkingYearsSpinner.setAdapter(workingYearsAdapter);
		mWorkingYearsSpinner.setOnItemSelectedListener(this);

		mEducationSpinner = (Spinner) findViewById(R.id.sp_education_level);
		SimpleSpinnerAdapter educationLevelAdapter = new SimpleSpinnerAdapter<>(Education.values());
		educationLevelAdapter.setItemColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
		educationLevelAdapter.setHasHitItem(true);
		mEducationSpinner.setAdapter(educationLevelAdapter);
		mEducationSpinner.setOnItemSelectedListener(this);

		mSkillSubGroupDataLoader = new SkillSubGroupDataLoader(getContext(), spiceManager, (SkillSubGroupAutocompleteView) findViewById(R.id.sp_skill_subgroup), (CompetenciesSpinnerView) findViewById(R.id.sp_competencies) , -1);
	}

	@Override
	public void grabSearchInfo(ArgsMap params) {

		if(mSkillSubGroupDataLoader.getSelectedSkillSubGroupId() > 0) {
			params.put("skill_subgroup", mSkillSubGroupDataLoader.getSelectedSkillSubGroupId());
		}

		if (mSkillSubGroupDataLoader.getSelectedCompetenceId() > 0) {
			params.put("skill", mSkillSubGroupDataLoader.getSelectedCompetenceId());
		}

		if (!TextUtils.isEmpty(mSpecializationEditText.getText().toString())) {
			params.put("specialization", mSpecializationEditText.getText().toString());
		}

		if (mEducationSpinner.getSelectedItemPosition() != 0) {
			params.put("degree", mEducationSpinner.getSelectedItemId());
		}

		if (mWorkingYearsSpinner.getSelectedItemPosition() != 0) {
			params.put("experience", mWorkingYearsSpinner.getSelectedItemId());
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		switch (adapterView.getId()) {
			case R.id.sp_skill_subgroup:
				if(i > 0) {
					//TODO: Fill Skill with the correct value
				}
				break;
			case R.id.sp_competencies:
				if(i > 0) {
					//TODO: Fill Skill with the correct value
				}
				break;
			case R.id.sp_education_level:
				if(i > 0) {
					mSkill.setLevel(Education.values().get(i).getId());
				}
				break;
			case R.id.sp_working_years:
				if(i > 0) {
					mSkill.setExperience(WorkingExperience.getInstance().values().get(i).getValue());
				}
				break;
		}	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}
}
