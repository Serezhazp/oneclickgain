package com.bastogram.widgets.profile;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.bastogram.R;
import com.bastogram.loadres.SkillSubGroupDataLoader;
import com.bastogram.models.SkillSubGroup;
import com.bastogram.views.adapters.SkillSubGroupAutoCompleteAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;

import java.util.List;
import java.util.Locale;

/**
 * Created by serezha on 04.08.16.
 */
public class SkillSubGroupAutocompleteView extends AutoCompleteTextView {

	private SkillSubGroupAutoCompleteAdapter mAdapter;

	private SkillSubGroup mSelectedSkillSubGroup;

	private SkillSubGroupDataLoader mSkillSubGroupDataLoader;

	private AdapterView.OnItemSelectedListener mListener;

	public SkillSubGroupAutocompleteView(Context context) {
		this(context, null);
	}

	public SkillSubGroupAutocompleteView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSaveEnabled(false);
		setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
		setTextSize(18);
		setThreshold(0);

		setHintTextColor(getResources().getColor(com.humanet.humanetcore.R.color.light_gray));

		setHint(R.string.profile_skill);

		mAdapter = new SkillSubGroupAutoCompleteAdapter();
		setAdapter(mAdapter);

		setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				setSelectedPosition(i);
				if(mListener != null)
					mListener.onItemSelected(adapterView, view, i, l);
			}
		});

		setOnItemSelectedListener(null);

		addTextChangedListener(new SimpleTextListener() {
			@Override
			public void afterTextChanged(Editable s) {
				String text = s.toString().toLowerCase();

				if(mSelectedSkillSubGroup != null && mSelectedSkillSubGroup.getTitle().toLowerCase(Locale.getDefault()).equals(text) || text.contains("@")) {
					return;
				}
				mSelectedSkillSubGroup = null;
				for(int i = 0; i < mAdapter.getCount(); i++) {
					SkillSubGroup skillSubGroup = mAdapter.getItem(i);
					if(skillSubGroup.getTitle().toLowerCase(Locale.getDefault()).equals(text)) {
						mSelectedSkillSubGroup = skillSubGroup;
					}
				}

				if(mSelectedSkillSubGroup == null && text.length() > 1) {
					//TODO: Was mLocationDataLoader.loadCities(text);
				}
			}
		});

		setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if(hasFocus)
					showDropDown();
			}
		});

		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				if(hasFocus()) {
					if(!isPopupShowing())
						showDropDown();
					else
						requestFocus();
				}
				return false;
			}
		});
	}

	public void setSkillSubGroupDataLoader(SkillSubGroupDataLoader loader) {
		mSkillSubGroupDataLoader = loader;
	}

	public SkillSubGroup getSelectedItem() {
		return mSelectedSkillSubGroup;
	}

	public int getSelectedItemId() {
		return mSelectedSkillSubGroup != null ? mSelectedSkillSubGroup.getId() : -1;
	}

	public void setSelectedPosition(int position) {
		mSelectedSkillSubGroup = mAdapter.getItem(position);
		String cityName = mSelectedSkillSubGroup.getTitle();
		setText(cityName);
		setSelection(cityName.length());
	}

	public void setSkillSubGroupList(List<SkillSubGroup> skillSubGroupList) {
		mAdapter.set(skillSubGroupList);
		setVisibility(VISIBLE);
	}

	@Override
	public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener l) {
		mListener = l;
		super.setOnItemSelectedListener(new WrapperOnItemSelecredListener());
	}

	private class WrapperOnItemSelecredListener implements AdapterView.OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			setSelectedPosition(i);

			if(mListener != null)
				mListener.onItemSelected(adapterView, view, i, l);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {
			if(mListener != null)
				mListener.onNothingSelected(adapterView);
		}
	}
}
