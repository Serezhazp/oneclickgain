package com.bastogram.widgets.profile;

import com.bastogram.models.Breed;
import com.humanet.humanetcore.views.adapters.BaseAutoCompleteAdapter;

/**
 * Created by serezha on 19.08.16.
 */
public class BreedAutocompleteAdapter extends BaseAutoCompleteAdapter<Breed> {
}
