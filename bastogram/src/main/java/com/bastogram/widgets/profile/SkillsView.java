package com.bastogram.widgets.profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bastogram.R;
import com.bastogram.fragments.profile.ViewSkillsFragment;
import com.bastogram.models.SkillSubGroup;
import com.bastogram.presenters.profile.LoadSkillsPresenter;
import com.bastogram.views.ISkillsView;
import com.bastogram.views.Updatable;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.views.widgets.CirclePickerView;
import com.humanet.humanetcore.views.widgets.ShadowImageButton;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Uran on 08.06.2016.
 */
@SuppressLint("ViewConstructor")
public class SkillsView extends RelativeLayout implements
        ISkillsView,
        View.OnClickListener,
        CirclePickerView.OnPickListener,
        Updatable {


    private LoadSkillsPresenter mLoadSkillsPresenter;

    private CirclePickerView mCirclePickerView;

    private SpiceManager mSpiceManager;

    private SkillSubGroup mSelectedSkill = null;
    private ShadowImageButton mNextButton;

    private int mUserId;

    public SkillsView(Context context, SpiceManager spiceManager, int userId) {
        super(context);

        mUserId = userId;

        mSpiceManager = spiceManager;

        LayoutParams pickerLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        pickerLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        mCirclePickerView = new CirclePickerView(getContext());
        addView(mCirclePickerView, pickerLayoutParams);

        mNextButton = new ShadowImageButton(context);
        mNextButton.setImageResource(R.drawable.forward);
        mNextButton.setId(R.id.btn_next);
        mNextButton.setOnClickListener(this);
        mNextButton.setEnabled(false);
        LayoutParams buttonLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        buttonLayoutParams.addRule(ALIGN_PARENT_BOTTOM);
        addView(mNextButton, buttonLayoutParams);

        mLoadSkillsPresenter = new LoadSkillsPresenter(mSpiceManager, this);
        mLoadSkillsPresenter.load(mUserId);

    }

    @Override
    public void showSkillList(SkillSubGroup[] skills) {
        mCirclePickerView.setBackgroundResource(com.humanet.humanetcore.R.drawable.circle_picker_bg);

        ArrayList<SkillSubGroup> list = new ArrayList<>(skills.length);

        Collections.addAll(list, skills);
        mCirclePickerView.fill(list, -1, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (mSelectedSkill != null)
                    ((BaseActivity) getContext()).startFragment(ViewSkillsFragment.newInstance(mSelectedSkill, mUserId), true);
        }
    }

    @Override
    public void onPick(View view, CirclePickerItem element) {
        mSelectedSkill = (SkillSubGroup) element;
        mNextButton.setEnabled(true);
    }

    @Override
    public void update() {
        //mLoadSkillsPresenter = new LoadSkillsPresenter(mSpiceManager, this);
        mLoadSkillsPresenter.load(mUserId);
    }
}
