package com.bastogram;

import com.bastogram.db.DBImpl;
import com.bastogram.fragments.BastogramRaitingsTabsFragment;
import com.bastogram.impls.AdditionalUploadingActionImpl;
import com.bastogram.impls.BalanceChangeTypeImpl;
import com.bastogram.impls.BastogramVideoItemImpl;
import com.bastogram.impls.CurrencyTabViewImpl;
import com.bastogram.impls.RatingImpl;
import com.bastogram.models.BastogramUserInfo;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.requests.user.GetUserInfoRequest;
import com.humanet.humanetcore.api.requests.video.VideoUploadRequest;
import com.humanet.humanetcore.db.DBHelper;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.BalanceHelper;
import com.humanet.humanetcore.utils.GsonHelper;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;

/**
 * Created by ovi on 1/26/16.
 */
public class BastogramApp extends App {

    static {
        API_APP_NAME = "basto";

        DEBUG = BuildConfig.DEBUG;

        GsonHelper.BALANCE_SERIALIZATION_KEY = "tl";

        COINS_CODE = "talant";

        DATABASE_VERSION = 60;

        GetUserInfoRequest.sIGetUserRequest = new GetUserInfoRequest.IGetUserRequest<BastogramUserInfo>() {
            @Override
            public BastogramUserInfo loadDataFromNetwork(Object userApiSet, ArgsMap map) throws Exception {
                 return ((com.bastogram.models.UserApiSet) userApiSet).getBastogarmUserInfo(App.API_APP_NAME, map).getUserInfo();
            }
        };
    }

    @Override
    public void onCreate() {
        super.onCreate();

        VideoItemView.init(new BastogramVideoItemImpl());

        NavigationManager.init(new NavigationManagerImpl());
        DBHelper.init(new DBImpl());

        BalanceHelper.setsImpl(new BalanceChangeTypeImpl());

        BastogramRaitingsTabsFragment.setImpl(new RatingImpl(this));

        CurrencyFragment.setImpl(new CurrencyTabViewImpl(this));

        VideoUploadRequest.init(new AdditionalUploadingActionImpl());

    }

    @Override
    public String getServerEndpoint() {
        return Constants.API;
    }


    @Override
    public Class<? extends UserInfo> getUserClass() {
        return BastogramUserInfo.class;
    }

    public Class getUserApiSetClass(){
        return com.bastogram.models.UserApiSet.class;
    }

}
