package com.bastogram.payment;

/**
 * Created by serezha on 29.08.16.
 */
public enum PaymentType {
	GOOGLE, PAYSERA, XSOLLA;
}
