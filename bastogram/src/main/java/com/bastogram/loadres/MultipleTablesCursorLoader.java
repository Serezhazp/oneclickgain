package com.bastogram.loadres;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.net.URI;

/**
 * Created by serezha on 11.08.16.
 */
public class MultipleTablesCursorLoader extends AsyncTaskLoader<Cursor> {

	private Context mContext;
	private URI[] mURIs;

	public MultipleTablesCursorLoader(Context context, URI... uris) {
		this(context);
		mURIs = uris;
	}

	public MultipleTablesCursorLoader(Context context) {
		super(context);
		mContext = context;
	}

	@Override
	public Cursor loadInBackground() {
		Log.i("MulTabCursorLoader", "loadInBackground");

		/*Cursor cursor = DBHelper.getInstance().getWritableDatabase().query()*/

		return null;
	}
}
