package com.bastogram.loadres;

import android.content.Context;

import com.bastogram.R;
import com.bastogram.models.Breed;
import com.bastogram.widgets.profile.BreedAutocompleteView;

import java.util.ArrayList;

/**
 * Created by serezha on 19.08.16.
 */
public class BreedDataLoader {

	private Context mContext;
	private BreedAutocompleteView mBreedAutocompleteView;
	private ArrayList<Breed> mBreedArrayList;
	private int mId;

	public BreedDataLoader(Context context, BreedAutocompleteView breedAutocompleteView, int id) {
		mContext = context;
		mId = id;
		mBreedAutocompleteView = breedAutocompleteView;
		mBreedAutocompleteView.setBreedDataLoader(this);

		loadBreeds();
	}

	private void loadBreeds() {
		mBreedArrayList = new ArrayList<>();
		String[] breedTitles = mContext.getResources().getStringArray(R.array.breeds);
		for(int i = 0; i < breedTitles.length; i++) {
			mBreedArrayList.add(new Breed(i, breedTitles[i]));
		}

		mBreedAutocompleteView.setBreedList(mBreedArrayList);
		if(mId > -1) {
			for(int i = 0; i < mBreedArrayList.size(); i++) {
				if(mId == mBreedArrayList.get(i).getId()) {
					mBreedAutocompleteView.setSelectedPosition(i);
					break;
				}
			}
			mId = -1;
		}
	}

	public int getSelectedBreedId() {
		return mBreedAutocompleteView.getSelectedItemId();
	}

	public String getSelectedBreedTitle() {
		return mBreedAutocompleteView.getSelectedItem().getTitle();
	}

	public void setBreed(int id) {
		mBreedAutocompleteView.setSelectedPosition(id);
	}

	public void setBreed(String text) {
		mBreedAutocompleteView.setText(text);

	}

}
