package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.models.Skill;
import com.bastogram.models.source.SkillDataSource;
import com.bastogram.models.source.local.SkillLocalDataSource;
import com.bastogram.models.source.remote.SkillRemoteDataSource;
import com.bastogram.views.ISkillsView;
import com.humanet.humanetcore.presenters.BasePresenter;

/**
 * Created by serezha on 14.07.16.
 */
public class AddSkillsPresenter extends BasePresenter<ISkillsView> {

	private final SkillDataSource mSkillDataSource;

	public AddSkillsPresenter(@NonNull ISkillsView view) {
		super(view);
		mSkillDataSource = new SkillDataSource(new SkillLocalDataSource(), new SkillRemoteDataSource());
	}

	public void load() {

		/*List<Skill> skillList = mSkillDataSource.getList();
		Skill[] skills = new Skill[skillList.size()];
		skillList.toArray(skills);

		getView().showSkillList(skills);*/
	}

	public void save(Skill skill) {
		/*if (skill.getId() < 0)
			observable = mSkillDataSource.save(skill);
		else
			observable = mSkillDataSource.edit(skill);*/
		mSkillDataSource.save(skill);

	}

}
