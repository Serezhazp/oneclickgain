package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.api.request.pet.PetEditRequest;
import com.bastogram.models.Nursling;
import com.bastogram.models.source.NurslingDataSource;
import com.bastogram.models.source.local.NurslingLocalDataSource;
import com.bastogram.models.source.remote.NurslingRemoteDataSource;
import com.bastogram.views.IPetView;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.jobs.SaveVideoToUploadJob;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.presenters.BasePresenter;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Uran on 15.06.2016.
 */
public class AddPetPresenter extends BasePresenter<IPetView> {

    private final NurslingDataSource mNurslingDataSource;
    private SpiceManager mSpiceManager;

    public AddPetPresenter(@NonNull IPetView view, SpiceManager spiceManager) {
        super(view);
        mNurslingDataSource = new NurslingDataSource(new NurslingLocalDataSource(), new NurslingRemoteDataSource());
        mSpiceManager = spiceManager;
    }

    public void load(int id) {

        Nursling nursling = mNurslingDataSource.getById(id);
        getView().showPet(nursling);

    }

    public void load(Nursling nursling) {

        getView().showPet(nursling);
    }

    public void save(Nursling nursling, VideoInfo videoInfo) {
        /*if (nursling.getId() < 0)
            mNurslingDataSource.save(nursling);
        else
            mNurslingDataSource.edit(nursling);*/


        uploadAvatarImage(nursling, videoInfo);

    }

    private void uploadAvatarImage(Nursling nursling, VideoInfo videoInfo) {
        if (videoInfo != null) {
            // push pet id as additional params to order retrieve it after image will be uploaded
            videoInfo.setAdditionalParams(nursling.getId());
            videoInfo.setPetId(nursling.getId());
            videoInfo.setAvatarType(VideoInfo.PET_AVATAR);
            SaveVideoToUploadJob saveVideoToUploadJob = new SaveVideoToUploadJob(videoInfo);
            saveVideoToUploadJob.start();
        } else {
            PetEditRequest request = new PetEditRequest(nursling);
            mSpiceManager.execute(request, new SimpleRequestListener<BaseResponse>());
        }
    }

}
