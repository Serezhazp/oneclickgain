package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.Nursling;
import com.bastogram.views.IPetsView;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.GetUserInfoRequest;
import com.humanet.humanetcore.presenters.BaseSpicePresenter;
import com.octo.android.robospice.SpiceManager;

import java.util.Set;

/**
 * Created by Uran on 10.06.2016.
 */
public class LoadPetsPresenter extends BaseSpicePresenter<IPetsView> {

    private SpiceManager mSpiceManager;
    private Set<Nursling> mNurslings;

    public LoadPetsPresenter(@NonNull SpiceManager spiceManager, @NonNull IPetsView view) {
        super(spiceManager, view);

        mSpiceManager = spiceManager;
        /*mNurslingDataSource = new NurslingDataSource(new NurslingLocalDataSource(), new NurslingRemoteDataSource());

        List<Nursling> nurslingList = mNurslingDataSource.getList();
        Nursling[] ns = new Nursling[nurslingList.size()];
        nurslingList.toArray(ns);

        getView().showPetList(ns);*/

    }

    public void load(int id) {

        GetUserInfoRequest userInfoRequest = new GetUserInfoRequest(id == 0 ? AppUser.getInstance().getUid() : id);

        mSpiceManager.execute(userInfoRequest, new SimpleRequestListener() {
            @Override
            public void onRequestSuccess(Object o) {

                //mNurslings = ((BastogramUserInfo) AppUser.getInstance().get()).getNurslings();
                mNurslings = ((BastogramUserInfo) o).getNurslings();
                if(mNurslings != null) {
                    Nursling[] nurslings = new Nursling[mNurslings.size()];
                    mNurslings.toArray(nurslings);

                    getView().showPetList(nurslings);
                }

            }
        });
    }
}
