package com.bastogram.presenters.profile;

import android.support.annotation.NonNull;

import com.bastogram.api.request.database.SkillsRequest;
import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.SkillGroup;
import com.bastogram.models.SkillSubGroup;
import com.bastogram.views.ISkillsView;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.GetUserInfoRequest;
import com.humanet.humanetcore.presenters.BaseSpicePresenter;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Uran on 08.06.2016.
 */
public class LoadSkillsPresenter extends BaseSpicePresenter<ISkillsView> {

    private SpiceManager mSpiceManager;
    private Set<BastogramUserInfoSkill> mBastogramUserInfoSkills;
    private ArrayList<SkillSubGroup> mSkillSubGroups;

    public LoadSkillsPresenter(@NonNull SpiceManager spiceManager, @NonNull ISkillsView view) {
        super(spiceManager, view);

        mSpiceManager = spiceManager;

    }

    public void load(int id) {

        GetUserInfoRequest userInfoRequest = new GetUserInfoRequest(id == 0 ? AppUser.getInstance().getUid() : id);
        mSpiceManager.execute(userInfoRequest, new SimpleRequestListener() {
            @Override
            public void onRequestSuccess(Object o) {

                mBastogramUserInfoSkills = ((BastogramUserInfo) o).getSkills();
                mSkillSubGroups = new ArrayList<>();

                SkillsRequest request = new SkillsRequest();

                mSpiceManager.execute(request, new SimpleRequestListener<SkillGroup[]>() {
                    @Override
                    public void onRequestSuccess(SkillGroup[] skillGroups) {
                        ArrayList<SkillSubGroup> skillSubGroups = new ArrayList<SkillSubGroup>();
                        for(SkillGroup group : skillGroups) {
                            skillSubGroups.addAll(Arrays.asList(group.getSkillSubGroups()));
                        }

                        for(BastogramUserInfoSkill skill : mBastogramUserInfoSkills) {
                            for(SkillSubGroup subGroup : skillSubGroups) {
                                if(skill.getSkillSubGroup() == subGroup.getId()) {
                                    mSkillSubGroups.add(subGroup);
                                    break;
                                }
                            }
                        }

                        SkillSubGroup[] subGroups = new SkillSubGroup[mSkillSubGroups.size()];
                        mSkillSubGroups.toArray(subGroups);
                        getView().showSkillList(subGroups);
                    }
                });
            }
        });

    }
}
