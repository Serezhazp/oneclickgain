package com.bastogram.fragments;

import android.support.v4.app.Fragment;

import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;

/**
 * Created by serezha on 11.08.16.
 */
public class BastogramRaitingsTabsFragment extends RatingsTabFragment {

	@Override
	protected Fragment getFragmentForTabPosition(int position) {
		switch (position) {
			case 1:
				return PetRatingFragment.newInstance(getImpl().getRatingType(position));
			default:
				return super.getFragmentForTabPosition(position);
		}

	}
}
