package com.bastogram.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bastogram.R;
import com.bastogram.models.Nursling;
import com.bastogram.presenters.profile.AddPetPresenter;
import com.bastogram.views.IPetView;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.fragments.VideoListFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.TimeIntervalConverter;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Locale;

/**
 * Created by serezha on 28.07.16.
 */
public class PetVideoListFragment extends VideoListFragment implements IPetView {

	private Nursling mNursling;

	public static class Builder {
		Bundle mArgs = new Bundle();

		public Builder(VideoType videoType) {
			setVideoType(videoType);
		}

		public Builder setUploading(boolean isUploading) {
			mArgs.putBoolean("isUploading", isUploading);
			return this;
		}

		public Builder setReplyId(int replyId) {
			mArgs.putInt(Constants.PARAMS.REPLY_ID, replyId);
			return this;
		}

		public Builder setVideoType(VideoType videoType) {
			mArgs.putSerializable(Constants.PARAMS.TYPE, videoType);
			return this;
		}

		public Builder setCategoryType(Category category) {
			mArgs.putSerializable(Constants.PARAMS.CATEGORY, category);
			return this;
		}

		public Builder setTitle(String title) {
			mArgs.putString(Constants.PARAMS.TITLE, title);
			return this;
		}

		public Builder setCurrentPosition(int position) {
			mArgs.putInt(Constants.PARAMS.POSITION, position);
			return this;
		}

		public Builder setNurslingId(int id) {
			mArgs.putInt(Constants.PARAMS.NURSLING_ID, id);
			return this;
		}

		public Builder setNursling(Nursling nursling) {
			mArgs.putSerializable(Constants.PARAMS.NURSLING, nursling);
			return this;
		}

		public Builder setUserId(int userId) {
			mArgs.putInt(Constants.PARAMS.USER_ID, userId);
			return this;
		}

		public PetVideoListFragment build() {
			PetVideoListFragment fragment = new PetVideoListFragment();
			fragment.setArguments(mArgs);
			return fragment;
		}
	}

	private AddPetPresenter mAddPetPresenter;
	private CardView mPetView;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();

		if(args.containsKey(Constants.PARAMS.NURSLING)) {
			mNursling =  (Nursling) args.getSerializable(Constants.PARAMS.NURSLING);
		}

		mAddPetPresenter = new AddPetPresenter(this, getSpiceManager());
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		LinearLayout rootView = new LinearLayout(getContext());
		rootView.setOrientation(LinearLayout.VERTICAL);
		rootView.setBackgroundColor(getResources().getColor(android.R.color.white));

		View videoListView = super.onCreateView(inflater, container, savedInstanceState);

		mPetView = (CardView) inflater.inflate(R.layout.item_pet, null);

		rootView.addView(mPetView);
		rootView.addView(videoListView);

		return rootView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

		if(mNursling != null)
			mAddPetPresenter.load(mNursling);
	}

	@Override
	public void showPet(Nursling nursling) {
		if(nursling == null)
			return;

		if(nursling.getAvatar() != null)
			ImageLoader.getInstance().displayImage(nursling.getAvatar(), (ImageView) mPetView.findViewById(R.id.avatar));

		if(nursling.getName() != null)
			((TextView) mPetView.findViewById(R.id.name)).setText(nursling.getName());
		if (nursling.getKind() != -1) {
			String[] kindTitles = getContext().getResources().getStringArray(R.array.breeds);
			((TextView) mPetView.findViewById(R.id.about)).setText(
					String.format(
							Locale.getDefault(),
							"%s, %s",
							TimeIntervalConverter.convert(getContext(), nursling.getBirthDate() * 1000), kindTitles[nursling.getKind()]));
		}
		((TextView) mPetView.findViewById(R.id.rating)).setText(String.valueOf(nursling.getRating()));
		((ImageView) mPetView.findViewById(R.id.gender)).setImageResource(
				nursling.getGender() == UserInfo.MALE
						? R.drawable.ic_pet_male
						: R.drawable.ic_pet_female
		);
		((ImageView) mPetView.findViewById(R.id.vaccination)).setImageResource(
				nursling.isVaccinated()
						? R.drawable.ic_pet_vaccinated
						: R.drawable.ic_pet_not_vaccinated
		);
		((ImageView) mPetView.findViewById(R.id.ready_for_child)).setImageResource(
				nursling.isChildReady()
						? R.drawable.ic_pet_ready_for_child
						: R.drawable.ic_pet_not_ready_for_child
		);

		ImageView pawAchievementBronze = (ImageView) mPetView.findViewById(R.id.pawBronze);
		ImageView pawAchievementSilver = (ImageView) mPetView.findViewById(R.id.pawSilver);
		ImageView pawAchievementGold = (ImageView) mPetView.findViewById(R.id.pawGold);
		ImageView pawAchievementRuby = (ImageView) mPetView.findViewById(R.id.pawRuby);
		ImageView pawAchievementBrilliant = (ImageView) mPetView.findViewById(R.id.pawBrilliant);

		pawAchievementBronze.setVisibility(View.INVISIBLE);
		pawAchievementSilver.setVisibility(View.INVISIBLE);
		pawAchievementGold.setVisibility(View.INVISIBLE);
		pawAchievementRuby.setVisibility(View.INVISIBLE);
		pawAchievementBrilliant.setVisibility(View.INVISIBLE);

		if(nursling.getBages() != null) {
			for (Integer bage : nursling.getBages()) {
				if (bage == Nursling.BRONZE)
					pawAchievementBronze.setVisibility(View.VISIBLE);
				if (bage == Nursling.SILVER)
					pawAchievementSilver.setVisibility(View.VISIBLE);
				if (bage == Nursling.GOLD)
					pawAchievementGold.setVisibility(View.VISIBLE);
				if (bage == Nursling.RUBY)
					pawAchievementRuby.setVisibility(View.VISIBLE);
				if (bage == Nursling.BRILLIANT)
					pawAchievementBrilliant.setVisibility(View.VISIBLE);
			}
		}

	}

}
