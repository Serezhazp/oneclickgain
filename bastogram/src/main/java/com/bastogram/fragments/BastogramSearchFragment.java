package com.bastogram.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bastogram.R;
import com.bastogram.activities.SearchActivity;
import com.bastogram.api.request.BastogramSearchUsersRequest;
import com.bastogram.api.responses.BastogramUserSearchResponse;
import com.bastogram.widgets.profile.search.SearchAllLayout;
import com.bastogram.widgets.profile.search.SearchPetLayout;
import com.bastogram.widgets.profile.search.SearchSkillLayout;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleOnTabSelectedListener;
import com.humanet.humanetcore.views.utils.UiUtil;

/**
 * Created by serezha on 21.07.16.
 */
public class BastogramSearchFragment extends BaseTitledFragment implements ViewPagerAdapter.ViewPagerAdapterDelegator, View.OnClickListener {

	private SearchPetLayout mSearchPetLayout = null;
	private SearchAllLayout mSearchAllLayout = null;
	private SearchSkillLayout mSearchSkillLayout = null;

	ViewPager mViewPager;
	TabLayout mTabLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(com.humanet.humanetcore.R.layout.fragment_search, container, false);

		//TODO use TabLayout.setupWithViewPager();
		mTabLayout = (TabLayout) view.findViewById(com.humanet.humanetcore.R.id.tabhost);
		mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_pet));
		mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_questions));
		mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_skill));
		mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

		mViewPager = (ViewPager) view.findViewById(com.humanet.humanetcore.R.id.viewPager);
		mViewPager.setAdapter(new ViewPagerAdapter(this));
		mViewPager.addOnPageChangeListener(mOnPageChangeListener);

		UiUtil.hideKeyboard(getActivity());

		view.findViewById(com.humanet.humanetcore.R.id.search).setOnClickListener(this);

		mViewPager.setCurrentItem(1);

		return view;
	}

	@Override
	public void onPause() {
		UiUtil.hideKeyboard(getActivity());
		super.onPause();
	}

	@Override
	public int getPagesCount() {
		return 3;
	}

	@Override
	public View getPageAt(ViewGroup container, int position) {
		View tabContent = null;
		switch (position) {
			case 0:
				if (mSearchPetLayout == null) {
					mSearchPetLayout = new SearchPetLayout(getActivity());
				}
				tabContent = mSearchPetLayout;
				break;
			case 1:
				if (mSearchAllLayout == null) {
					mSearchAllLayout = new SearchAllLayout(getActivity(), getSpiceManager(), getLoaderManager());
				}
				tabContent = mSearchAllLayout;
				break;
			case 2:
				if (mSearchSkillLayout == null) {
					mSearchSkillLayout = new SearchSkillLayout(getActivity(), getSpiceManager());
				}
				tabContent = mSearchSkillLayout;
				break;
		}
		return tabContent;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return String.valueOf(position);
	}

	@Override
	public void onClick(View v) {
		int viewId = v.getId();
		if (viewId == com.humanet.humanetcore.R.id.search) {
			search();
		}
	}

	private void search() {
		ArgsMap argsMap = new ArgsMap();
		if (mSearchPetLayout != null) {
			mSearchPetLayout.grabSearchInfo(argsMap);
		}
		if (mSearchAllLayout != null) {
			mSearchAllLayout.grabSearchInfo(argsMap);
		}
		if (mSearchSkillLayout != null) {
			mSearchSkillLayout.grabSearchInfo(argsMap);
		}

		App.getInstance().getContentResolver().delete(ContentDescriptor.UserSearchResult.URI, null, null);

		getSpiceManager().execute(new BastogramSearchUsersRequest(argsMap), new SimpleRequestListener<BastogramUserSearchResponse>());

		((SearchActivity) getActivity()).openSearchResult();
	}

	private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
		@Override
		public void onPageSelected(int position) {
			mTabLayout.setOnTabSelectedListener(null);
			mTabLayout.getTabAt(position).select();
			mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
			UiUtil.hideKeyboard(getActivity());
		}
	};

	private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
		@Override
		public void onTabSelected(TabLayout.Tab tab) {
			mViewPager.setCurrentItem(tab.getPosition(), false);
		}
	};



	@Override
	public String getTitle() {
		return getString(com.humanet.humanetcore.R.string.search);
	}
}
