package com.bastogram.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bastogram.R;
import com.bastogram.api.request.pet.PetDeleteRequest;
import com.bastogram.db.DBImpl;
import com.bastogram.models.Nursling;
import com.bastogram.widgets.profile.PetsListView;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.views.dialogs.dialogViews.ConfirmationDialog;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Uran on 13.06.2016.
 */
public class ProfilePetsFragment extends BaseSpiceFragment implements ListView.OnItemClickListener, View.OnClickListener, AdapterView.OnItemLongClickListener {

    private PetsListView mPetsListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_pets, container, false);
        mPetsListView = (PetsListView) view.findViewById(R.id.pet_list_view);
        mPetsListView.setIsNeedToHighlightOwnPets(false);

        view.findViewById(R.id.btn_add).setOnClickListener(this);

        mPetsListView.setOnItemClickListener(this);
        mPetsListView.setOnItemLongClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPetsListView.setSpiceManager(getSpiceManager());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Nursling nursling = (Nursling) parent.getAdapter().getItem(position);
        ((BaseActivity) getActivity()).startFragment(AddPetFragment.newInstance(nursling), true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                ((BaseActivity) getActivity()).startFragment(new AddPetFragment(), true);
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        ConfirmationDialog dialog = new ConfirmationDialog();
        dialog.setPayRunnable(new PetDeleteRunnable(getSpiceManager(), (int) id, mPetsListView));
        dialog.show(getFragmentManager(), "Pet Delete Dialog");
        dialog.setConfirmationText(getResources().getString(R.string.alert_delete_pet));

        return true;
    }

    private static class PetDeleteRunnable implements Runnable {
        private SpiceManager mSpiceManager;
        private int mPetId;
        private PetsListView mPetsListView;

        public PetDeleteRunnable(SpiceManager spiceManager, int petId, PetsListView petsListView) {
            mSpiceManager = spiceManager;
            mPetId = petId;
            mPetsListView = petsListView;
        }

        @Override
        public void run() {
            DBImpl.removeNursling(mPetId);
            PetDeleteRequest request = new PetDeleteRequest(mPetId);
            mSpiceManager.execute(request, new SimpleRequestListener<BaseResponse>() {
                @Override
                public void onRequestSuccess(BaseResponse baseResponse) {
                    mPetsListView.update();
                }
            });
        }
    }
}
