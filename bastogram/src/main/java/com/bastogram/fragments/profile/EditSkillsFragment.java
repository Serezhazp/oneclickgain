package com.bastogram.fragments.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bastogram.R;
import com.bastogram.api.request.database.SkillsRequest;
import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.SkillGroup;
import com.bastogram.models.SkillSubGroup;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CirclePickerView;
import com.humanet.humanetcore.views.widgets.ShadowImageButton;
import com.humanet.humanetcore.views.widgets.items.BaseItemView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Uran on 10.06.2016.
 */
public class EditSkillsFragment extends BaseTitledFragment implements CirclePickerView.OnPickListener, CircleLayout.OnItemSelectedListener, View.OnClickListener {

    private ShadowImageButton mNextButton;
    private CirclePickerView mCirclePickerView;
    private SkillSubGroup mSkillItem;

    @Override
    public String getTitle() {
        return getString(R.string.view_profile_tab_skills);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_skills, container, false);

        SkillsRequest skillsRequest = new SkillsRequest();
        getSpiceManager().execute(skillsRequest, new SimpleRequestListener<SkillGroup[]>() {
            @Override
            public void onRequestSuccess(SkillGroup[] skillGroups) {
                super.onRequestSuccess(skillGroups);

                mNextButton = (ShadowImageButton) view.findViewById(R.id.btn_next);
                mNextButton.setOnClickListener(EditSkillsFragment.this);

                Adapter adapter = new Adapter(skillGroups);
                CircleLayout circleLayout = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
                circleLayout.setVisibility(View.VISIBLE);
                circleLayout.setAdapter(adapter);
                int selectedItem = 0; //First skills group by default
                circleLayout.setSelectedItem(selectedItem);
                circleLayout.setOnItemSelectedListener(EditSkillsFragment.this);

                mCirclePickerView = (CirclePickerView) view.findViewById(R.id.picker);
                ArrayList<SkillSubGroup> initialItems = new ArrayList<>(Arrays.asList(skillGroups[selectedItem].getSkillSubGroups()));

                mCirclePickerView.fill(initialItems, getOwnSkillSubGroups(skillGroups, selectedItem), initialItems.get(0).getId(), EditSkillsFragment.this);
                mSkillItem = (SkillSubGroup) mCirclePickerView.getCurrentElement();

                mNextButton = (ShadowImageButton) view.findViewById(R.id.btn_next);
                mNextButton.setOnClickListener(EditSkillsFragment.this);

                setNextButtonEnabled(true);
            }
        });

        return view;
    }

    @Override
    public void onPick(View view, CirclePickerItem element) {
        setNextButtonEnabled(true);
        mSkillItem = (SkillSubGroup) element;
    }

    @Override
    public void onItemSelected(Object data) {
        SkillGroup group = (SkillGroup) data;

        ArrayList<SkillSubGroup> items = new ArrayList<>(Arrays.asList(group.getSkillSubGroups()));
        mCirclePickerView.fill(items, getOwnSkillSubGroups(group), items.get(0).getId(), this);
        mSkillItem = (SkillSubGroup) mCirclePickerView.getCurrentElement();
    }

    private static class Adapter extends BaseAdapter {
        private SkillGroup[] mGroups;

        public Adapter(SkillGroup[] groups) {
            mGroups = groups;
        }

        @Override
        public int getCount() {
            return mGroups.length;
        }

        @Override
        public SkillGroup getItem(int i) {
            return mGroups[i];
        }

        @Override
        public long getItemId(int i) {
            return getItem(i).getId();
        }

        @Override
        public ItemView getView(int i, View view, ViewGroup viewGroup) {
            ItemView v;
            if(view == null)
                v = new ItemView(viewGroup.getContext());
            else
                v = (ItemView) view;

            v.setData(getItem(i));

            return v;
        }
    }

    @Override
    public void onClick(View view) {
        EditSkillFragment fragment = EditSkillFragment.newInstance(mSkillItem);
        ((BaseActivity) getActivity()).startFragment(fragment, true);
    }

    private static class ItemView extends BaseItemView {
        public ItemView(Context context) {
            super(context);
        }
    }

    private void setNextButtonEnabled(boolean isEnabled) {
        mNextButton.setEnabled(isEnabled);
    }

    private ArrayList<SkillSubGroup> getOwnSkillSubGroups(SkillGroup skillGroup) {
        ArrayList<SkillSubGroup> ownSkills = new ArrayList<SkillSubGroup>();
        for(BastogramUserInfoSkill skill : ((BastogramUserInfo) AppUser.getInstance().get()).getSkills()) {
            for(SkillSubGroup skillSubGroup : skillGroup.getSkillSubGroups()) {
                if(skillSubGroup.getId() == skill.getSkillSubGroup())
                    ownSkills.add(skillSubGroup);
            }
        }

        return ownSkills;
    }

    private ArrayList<SkillSubGroup> getOwnSkillSubGroups(SkillGroup[] skillGroups, int selectedItem) {
        ArrayList<SkillSubGroup> ownSkills = new ArrayList<SkillSubGroup>();
        for(BastogramUserInfoSkill skill : ((BastogramUserInfo) AppUser.getInstance().get()).getSkills()) {
            for(SkillSubGroup skillSubGroup : skillGroups[selectedItem].getSkillSubGroups()) {
                if(skillSubGroup.getId() == skill.getSkillSubGroup())
                    ownSkills.add(skillSubGroup);
            }
        }

        return ownSkills;
    }

}
