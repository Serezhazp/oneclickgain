package com.bastogram.fragments.profile;

import android.widget.Toast;

import com.bastogram.R;
import com.bastogram.models.BastogramUserInfo;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by serezha on 19.07.16.
 */
public class PetsFragment extends BaseVideoCategoryPickerFragment implements IMenuBindFragment {

	@Override
	public String getTitle() {
		return getString(R.string.menu_nurslings);
	}


	@Override
	protected String[] getTabNames() {
		return new String[]{
				getString(R.string.online_tab_all_pets),
				getString(R.string.online_tab_my_pets),
				getString(R.string.online_tab_my_choice),
		};
	}

	@Override
	protected VideoType[] getVideoTypes() {
		return new VideoType[]{
				VideoType.PET_ALL,
				VideoType.PET_MY,
				VideoType.PET_MY_CHOICE,
		};
	}

	@Override
	protected void addNewVideo() {
		if(((BastogramUserInfo) AppUser.getInstance().get()).getNurslings() != null && ((BastogramUserInfo) AppUser.getInstance().get()).getNurslings().size() > 0) {
			super.addNewVideo();
		} else {
			AddPetFragment fragment = new AddPetFragment();
			((BaseActivity) getActivity()).startFragment(fragment, true);
			Toast.makeText(getContext(), getResources().getString(R.string.alert_create_pet), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public int getSelectedMenuItem() {
		return R.id.nurslings;
	}
}
