package com.bastogram.fragments.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bastogram.fragments.PetVideoListFragment;
import com.bastogram.models.Nursling;
import com.bastogram.widgets.profile.PetsListView;
import com.bastogram.widgets.profile.SkillsView;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;

import java.util.HashMap;

/**
 * Created by Uran on 06.06.2016.
 */
public class BastogramProfileFragment extends BaseViewProfileFragment implements AdapterView.OnItemClickListener {

    private SkillsView mSkillsView;
    private PetsListView mPetsListView;
    private Category mCategory;
    private int mUserId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if(args.containsKey(Constants.PARAMS.USER_ID))
            mUserId = args.getInt(Constants.PARAMS.USER_ID);
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {

        switch (position) {
            case 0:
                mPetsListView = new PetsListView(getActivity(), mUserId, false);
                mPetsListView.setSpiceManager(getSpiceManager());
                mPetsListView.setOnItemClickListener(this);
                return mPetsListView;
            case 1:
                return getQuestionnaireView();
            case 2:
                mSkillsView = new SkillsView(getActivity(), getSpiceManager(), mUserId);
                return mSkillsView;
        }
        throw new IllegalArgumentException("unexpected position value:" + position);
    }

    @NonNull
    @Override
    public int[] getTabNames() {
        return new int[]{
                com.humanet.humanetcore.R.string.view_profile_tab_pets,
                com.humanet.humanetcore.R.string.view_profile_tab_questionary,
                com.humanet.humanetcore.R.string.view_profile_tab_skills,
        };
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mSkillsView != null)
            mSkillsView.update();

        if(mPetsListView != null)
            mPetsListView.update();
        //TODO: Implement other views this way
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Nursling nursling = (Nursling) mPetsListView.getAdapter().getItem(position);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("video_type", VideoType.PET_ALL.getRequestParam());

        Category category = new Category(Category.ALL_ID);
        category.setParams(map);

        PetVideoListFragment fragment = new PetVideoListFragment.Builder(VideoType.PET_ALL)
                .setTitle(nursling.getName())
                .setCategoryType(category)
                .setReplyId(0)
                .setCurrentPosition(0)
                .setNursling(nursling)
                .setNurslingId(nursling.getId())
                .setUserId(mUserId)
                .build();
        ((BaseActivity) getActivity()).startFragment(fragment, true);
    }
}
