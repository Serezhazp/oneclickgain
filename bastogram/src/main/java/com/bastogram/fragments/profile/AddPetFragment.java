package com.bastogram.fragments.profile;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bastogram.R;
import com.bastogram.activities.CaptureActivity;
import com.bastogram.api.request.pet.PetAddRequest;
import com.bastogram.api.request.pet.PetEditRequest;
import com.bastogram.api.responses.PetResponse;
import com.bastogram.loadres.BreedDataLoader;
import com.bastogram.models.Nursling;
import com.bastogram.presenters.profile.AddPetPresenter;
import com.bastogram.views.IPetView;
import com.bastogram.widgets.profile.BreedAutocompleteView;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.views.dialogs.ProgressDialog;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.switch_button.SwitchButton;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Uran on 13.06.2016.
 */
public class AddPetFragment extends BaseTitledFragment implements View.OnClickListener, IPetView, CompoundButton.OnCheckedChangeListener {

    public static final int CAPTURE_PHOTO_FOR_AVATAR = 102;

    private static final String NURSLING = "nursling";

    private ProgressDialog mProgressDialog;

    public static AddPetFragment newInstance(Nursling nursling) {
        AddPetFragment fragment = new AddPetFragment();

        Bundle bundle = new Bundle(1);
        bundle.putSerializable(NURSLING, nursling);

        fragment.setArguments(bundle);

        return fragment;
    }

    private Nursling mNursling;

    private AddPetPresenter mAddPetPresenter;
    private BreedDataLoader mBreedDataLoader;

    private SwitchButton mGenderSwitch;
    private SwitchButton mVaccinationSwitch;
    private SwitchButton mReadyForChildrenSwitch;

    private VideoInfo mVideoInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_pet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddPetPresenter = new AddPetPresenter(this, getSpiceManager());

        view.findViewById(R.id.avatar).setOnClickListener(this);
        view.findViewById(R.id.btn_next).setOnClickListener(this);
        view.findViewById(R.id.txt_birth_field).setOnClickListener(this);


        mGenderSwitch = (SwitchButton) view.findViewById(R.id.gender);
        mGenderSwitch.setOnCheckedChangeListener(this);
        view.findViewById(R.id.male).setOnClickListener(this);
        view.findViewById(R.id.female).setOnClickListener(this);


        mReadyForChildrenSwitch = (SwitchButton) view.findViewById(R.id.reproduction);
        mReadyForChildrenSwitch.setOnCheckedChangeListener(this);
        view.findViewById(R.id.ready_for_child).setOnClickListener(this);
        view.findViewById(R.id.not_ready_for_child).setOnClickListener(this);


        mVaccinationSwitch = (SwitchButton) view.findViewById(R.id.vaccination);
        mVaccinationSwitch.setOnCheckedChangeListener(this);
        view.findViewById(R.id.vaccinated).setOnClickListener(this);
        view.findViewById(R.id.not_vaccinated).setOnClickListener(this);

        mBreedDataLoader = new BreedDataLoader(getContext(), (BreedAutocompleteView) view.findViewById(R.id.sp_breed), -1);

        if (savedInstanceState != null && savedInstanceState.containsKey(NURSLING)) {
            showPet((Nursling) savedInstanceState.get(NURSLING));
        } else if (getArguments() != null && getArguments().containsKey(NURSLING)) {
            Nursling nursling = (Nursling) getArguments().get(NURSLING);
            if (nursling != null) {
                mAddPetPresenter.load(nursling);
            }
        } else {
            mNursling = new Nursling();
            mNursling.setUserId(AppUser.getInstance().get().getId());
        }

    }

    @Override
    public void showPet(Nursling nursling) {
        View view = getView();
        if (view == null || nursling == null)
            return;

        mNursling = nursling;

        if(mNursling.getName() != null)
            UiUtil.setTextValue(view, R.id.field_first_name, mNursling.getName());
        if(mNursling.getKind() != -1) {
            mBreedDataLoader.setBreed(mNursling.getKind());
        }

        invalidateGender(mNursling.getGender());
        invalidateReproduction(mNursling.isChildReady());
        invalidateVaccination(mNursling.isVaccinated());

        displayAvatarImage(view);
        displayDate(view);
    }

    private void displayAvatarImage(@NonNull View rootView) {
        if(mNursling.getAvatar() != null)
        ImageLoader.getInstance().displayImage(mNursling.getAvatar(), (ImageView) rootView.findViewById(R.id.avatar));
    }

    private void displayDate(@NonNull View rootView) {
        TextView view = (TextView) rootView.findViewById(R.id.txt_birth_field);
        if(view != null) {
            //Date date = new Date(System.currentTimeMillis() - mNursling.getAge() * 30 * 24 * 60 * 60 * 1000);
            Date date = new Date(mNursling.getBirthDate() * 1000);
            view.setText(ProfilePrivateInfoFragment.DATE_FORMAT.format(date));
            //view.setText(String.valueOf(mNursling.getAge()));
            view.setTextColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
        }
    }

    @Override
    public String getTitle() {
        return getString(com.humanet.humanetcore.R.string.view_profile_tab_pets);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.avatar) {
            Intent intent = new Intent(getActivity(), NavigationManager.getCaptureActivityClass());
            intent.putExtra("update_avatar", VideoInfo.PET_AVATAR);
            intent.putExtra("pet_id", mNursling.getId());
            startActivityForResult(intent, CAPTURE_PHOTO_FOR_AVATAR);
        } else if (v.getId() == R.id.btn_next) {
            save();
        } else if (v.getId() == R.id.male) {
            invalidateGender(UserInfo.MALE);
        } else if (v.getId() == R.id.female) {
            invalidateGender(UserInfo.FEMALE);

        } else if (v.getId() == R.id.not_ready_for_child) {
            invalidateReproduction(false);
        } else if (v.getId() == R.id.ready_for_child) {
            invalidateReproduction(true);

        } else if (v.getId() == R.id.not_vaccinated) {
            invalidateVaccination(false);
        } else if (v.getId() == R.id.vaccinated) {
            invalidateVaccination(true);

        } else {
            final Calendar c = Calendar.getInstance(Locale.getDefault());
            if(mNursling.getBirthDate() != 0) {
                c.setTimeInMillis(mNursling.getBirthDate() * 1000);
            } else {
                c.setTimeInMillis(System.currentTimeMillis() - 157784630000l); //Five years ago
            }
            DatePickerDialog d;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                d = new DatePickerDialog(getActivity(), com.humanet.humanetcore.R.style.DateDialogTheme, mOnDateChangeListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            } else {
                d = new DatePickerDialog(getActivity(), mOnDateChangeListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            }
            d.getDatePicker().setMaxDate(getMinimalDate().getTime());
            d.show();
        }
    }

    private void invalidateGender(@UserInfo.Sex int sex) {
        mNursling.setGender(sex);

        mGenderSwitch.setOnCheckedChangeListener(null);
        mGenderSwitch.setChecked(sex == UserInfo.FEMALE);
        mGenderSwitch.setOnCheckedChangeListener(this);
    }

    private void invalidateReproduction(boolean readyForChild) {
        mNursling.setChildReady(readyForChild);

        mReadyForChildrenSwitch.setOnCheckedChangeListener(null);
        mReadyForChildrenSwitch.setChecked(readyForChild);
        mReadyForChildrenSwitch.setOnCheckedChangeListener(this);
    }

    private void invalidateVaccination(boolean vaccination) {
        mNursling.setVaccination(vaccination);

        mVaccinationSwitch.setOnCheckedChangeListener(null);
        mVaccinationSwitch.setChecked(vaccination);
        mVaccinationSwitch.setOnCheckedChangeListener(this);
    }

    private void save() {
        View view = getView();
        if (view == null)
            return;

        mNursling.setName(UiUtil.getTextValue(view, R.id.field_first_name));
        mNursling.setKind(mBreedDataLoader.getSelectedBreedId());
        mNursling.setAnimal(Nursling.CAT);

        if (validate()) {

            mProgressDialog = getLoader();
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();

            if(mNursling.getId() == -1) {
                PetAddRequest request = new PetAddRequest();
                getSpiceManager().execute(request, new SimpleRequestListener<PetResponse>() {
                    @Override
                    public void onRequestSuccess(PetResponse petResponse) {
                        mNursling.setId(petResponse.getPetId());
                        mAddPetPresenter.save(mNursling, mVideoInfo);
                    }
                });
            } else {
                mAddPetPresenter.save(mNursling, mVideoInfo);
            }

        }
    }

    private DatePickerDialog.OnDateSetListener mOnDateChangeListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            final Calendar c = Calendar.getInstance(Locale.getDefault());
            c.set(year, monthOfYear, dayOfMonth);
            mNursling.setBirthDate(c.getTimeInMillis() / 1000);

            //mNursling.setAge((int)((System.currentTimeMillis() / 1000 - mNursling.getBirthDate()) / (60 * 60 * 24 * 30)));
            //Toast.makeText(getContext(), "Your pet is " + mNursling.getAge() + " months age", Toast.LENGTH_SHORT).show();

            View v;
            if ((v = getView()) != null)
                displayDate(v);

        }
    };

    private Date getMinimalDate() {
        Calendar minimalDate = Calendar.getInstance();
        minimalDate.set(Calendar.HOUR_OF_DAY, 0);
        minimalDate.set(Calendar.MINUTE, 0);
        minimalDate.add(Calendar.YEAR, 0);
        return minimalDate.getTime();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.gender:
                invalidateGender(isChecked ? UserInfo.FEMALE : UserInfo.MALE);
                break;

            case R.id.reproduction:
                invalidateReproduction(isChecked);
                break;

            case R.id.vaccination:
                invalidateVaccination(isChecked);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_PHOTO_FOR_AVATAR && resultCode == CaptureActivity.RESULT_OK) {
            mVideoInfo = (VideoInfo) data.getSerializableExtra("video_info");

            String url = "file://" + mVideoInfo.getImagePath();

            MemoryCacheUtils.removeFromCache(url, ImageLoader.getInstance().getMemoryCache());
            DiskCacheUtils.removeFromCache(url, ImageLoader.getInstance().getDiskCache());

            mNursling.setAvatar(url);
            View v;
            if ((v = getView()) != null)
                displayAvatarImage(v);
        }
    }

    private boolean validate() {
        if (TextUtils.isEmpty(mNursling.getAvatar())) {
            Toast.makeText(getActivity(), R.string.profile_edit_error_no_avatar, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(mNursling.getName())) {
            Toast.makeText(getActivity(), R.string.profile_pet_edit_error_no_pet_nickname, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (mNursling.getBirthDate() == 0) {
            Toast.makeText(getActivity(), R.string.profile_pet_edit_error_no_birth_day, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (mBreedDataLoader.getSelectedBreedId() == -1) {
            Toast.makeText(getActivity(), R.string.profile_pet_edit_error_no_kind, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    /**
     * catch event on avatar image uploading complete.
     * getSpiceManager().addListenerIfPending will not work because uploading request start from external thread
     *
     * @param uploadingMedia - contains url with new avatar
     */
    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UploadingMedia uploadingMedia) {
        if (mNursling != null /*&& mNursling.getId() == getIdFromVideoInfo()*/) {
            if(uploadingMedia != null)
                mNursling.setAvatar(uploadingMedia.getThumb());
            PetEditRequest request = new PetEditRequest(mNursling);
            getSpiceManager().execute(request, new SimpleRequestListener<BaseResponse>());
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Nursling nursling) {
        if(nursling != null) {
            mProgressDialog.dismiss();
            getActivity().onBackPressed();
        }
    }

    private int getIdFromVideoInfo() {
        if (mVideoInfo == null)
            return -1;
        if (mVideoInfo.getAdditionalParams() == null)
            return -1;
        if (mVideoInfo.getAdditionalParams() instanceof Integer)
            return (Integer) mVideoInfo.getAdditionalParams();

        if (mVideoInfo.getAdditionalParams() instanceof Double)
            return ((Double) mVideoInfo.getAdditionalParams()).intValue();

        return -1;
    }

}

