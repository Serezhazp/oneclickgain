package com.bastogram.fragments.profile;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bastogram.R;
import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Competence;
import com.bastogram.models.Portfolio;
import com.bastogram.models.SkillSubGroup;
import com.bastogram.models.WorkingExperience;
import com.bastogram.presenters.profile.LoadPortfolioPresenter;
import com.bastogram.presenters.profile.LoadSkillPresenter;
import com.bastogram.views.IPortfolioView;
import com.bastogram.views.ISkillView;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;
import com.humanet.humanetcore.views.widgets.ShadowImageButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;


public class EditSkillFragment extends BaseTitledFragment implements AdapterView.OnItemSelectedListener, View.OnClickListener, ISkillView, IPortfolioView {

	private LinearLayout mPortfolioHolder;
	private EditText mSpecializationEditText;
	private Spinner mCompetenciesSpinner;
	private Spinner mWorkingYearsSpinner;
	private Spinner mEducationSpinner;

	private SkillSubGroup mSkillSubGroup;

	private LoadSkillPresenter mLoadSkillPresenter;
	private LoadPortfolioPresenter mLoadPortfolioPresenter;

	private ArrayList<Integer> mPortfolioIds;

	private static final String SKILL_SUBGROUP = "SkillSubGroup";


	private BastogramUserInfoSkill mBastogramUserInfoSkill;

	public EditSkillFragment() {
		// Required empty public constructor
	}

	public static EditSkillFragment newInstance(SkillSubGroup skillSubGroup) {

		EditSkillFragment fragment = new EditSkillFragment();

		Bundle bundle = new Bundle(1);
		bundle.putSerializable(SKILL_SUBGROUP, skillSubGroup);

		fragment.setArguments(bundle);

		return fragment;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mSkillSubGroup = (SkillSubGroup) getArguments().getSerializable(SKILL_SUBGROUP);

		BastogramUserInfo user = (BastogramUserInfo) AppUser.getInstance().get();


		mBastogramUserInfoSkill = new BastogramUserInfoSkill();
		mBastogramUserInfoSkill.setSkillSubgroup(mSkillSubGroup.getId());
		for (BastogramUserInfoSkill infoSkill : user.getSkills()){
			if (infoSkill.getSkillSubGroup() == mSkillSubGroup.getId()){
				mBastogramUserInfoSkill = infoSkill.clone();
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_edit_skill, container, false);

		TextView title = (TextView) rootView.findViewById(R.id.skillTitle);
		title.setText(mSkillSubGroup.getTitle());
		/*if(getArguments() != null) {
			title.setText(getArguments().getString("title"));
		}*/

		return rootView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mLoadSkillPresenter = new LoadSkillPresenter(this);
		mLoadPortfolioPresenter = new LoadPortfolioPresenter(this);

		mPortfolioHolder = (LinearLayout) view.findViewById(R.id.portfolio_items_holder);

		mSpecializationEditText = (EditText) view.findViewById(R.id.specialization);
		mSpecializationEditText.addTextChangedListener(new SimpleTextListener() {
			@Override
			public void afterTextChanged(Editable s) {
				mBastogramUserInfoSkill.setTags(s.toString());
			}
		});

		mCompetenciesSpinner = (Spinner) view.findViewById(R.id.sp_competencies);
		List<Competence> skills = new ArrayList<>(mSkillSubGroup.getCompetences().length + 1);
		Competence competence = new Competence();
		competence.setTitleFromResources(getResources().getString(R.string.profile_competion));
		skills.add(competence);
		skills.addAll(Arrays.asList(mSkillSubGroup.getCompetences()));
		SimpleSpinnerAdapter competenciesAdapter = new SimpleSpinnerAdapter<>(skills);
		competenciesAdapter.setItemColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
		competenciesAdapter.setHasHitItem(true);
		mCompetenciesSpinner.setAdapter(competenciesAdapter);
		mCompetenciesSpinner.setOnItemSelectedListener(this);

		mWorkingYearsSpinner = (Spinner) view.findViewById(R.id.sp_working_years);
		SimpleSpinnerAdapter workingYearsAdapter = new SimpleSpinnerAdapter<>(WorkingExperience.getInstance().values());
		workingYearsAdapter.setItemColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
		workingYearsAdapter.setHasHitItem(true);
		mWorkingYearsSpinner.setAdapter(workingYearsAdapter);
		mWorkingYearsSpinner.setOnItemSelectedListener(this);

		mEducationSpinner = (Spinner) view.findViewById(R.id.sp_education_level);
		SimpleSpinnerAdapter educationLevelAdapter = new SimpleSpinnerAdapter<>(Education.values());
		educationLevelAdapter.setItemColor(getResources().getColor(com.humanet.humanetcore.R.color.spinnerItemColor));
		educationLevelAdapter.setHasHitItem(true);
		mEducationSpinner.setAdapter(educationLevelAdapter);
		mEducationSpinner.setOnItemSelectedListener(this);

		ImageButton addPortfolioButton = (ImageButton) view.findViewById(R.id.btn_add_portfolio);
		addPortfolioButton.setOnClickListener(this);

		ShadowImageButton saveSkillButton = (ShadowImageButton) view.findViewById(R.id.btn_done);
		saveSkillButton.setOnClickListener(this);

		mLoadSkillPresenter.load();

	}

	@Override
	public String getTitle() {
		return getResources().getString(R.string.profile_skill);
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		switch (adapterView.getId()) {
			case R.id.sp_competencies:
				if(i > 0) {
					mBastogramUserInfoSkill.setSkill((int) l);
				}
				break;
			case R.id.sp_education_level:
				if(i > 0) {
					mBastogramUserInfoSkill.setEducation(Education.values().get(i).getId());
				}
				break;
			case R.id.sp_working_years:
				if(i > 0) {
					mBastogramUserInfoSkill.setExperience(WorkingExperience.getInstance().values().get(i).getValue());
				}
				break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_add_portfolio:
				addPortfolioLayout(mPortfolioHolder);
				break;
			case R.id.btn_done:
				checkMandatoryFields(mBastogramUserInfoSkill);
				break;
		}
	}

	@Override
	public void showPortfolioList(Portfolio[] portfolios) {
		mPortfolioIds = new ArrayList<>();
		View view;
		for(Portfolio portfolio : portfolios) {
			mPortfolioIds.add(portfolio.getId());
			addPortfolioLayout(mPortfolioHolder);
			view = mPortfolioHolder.getChildAt(mPortfolioHolder.getChildCount() - 1);
			((EditText) view.findViewById(R.id.projectTitle)).setText(portfolio.getName());
			((EditText) view.findViewById(R.id.projectRoleDescription)).setText(portfolio.getDescription());
			((EditText) view.findViewById(R.id.projectLink)).setText(portfolio.getLink());
		}
	}

	@Override
	public void showSkill() {
		if(mBastogramUserInfoSkill == null)
			return;

		mCompetenciesSpinner.setSelection(findSelectionById(mBastogramUserInfoSkill.getSkill(), mSkillSubGroup.getCompetences()));
		mSpecializationEditText.setText(mBastogramUserInfoSkill.getTags());
		mEducationSpinner.setSelection(mBastogramUserInfoSkill.getEducation() + 1);
		mWorkingYearsSpinner.setSelection(mBastogramUserInfoSkill.getExperience());

		mLoadPortfolioPresenter.load(mBastogramUserInfoSkill);
	}

	private int findSelectionById(int id, Competence[] skills) {

		for(int i = 0; i < skills.length; i++) {
			if(skills[i].getId() == id)
				return i + 1;
		}
		return 0;
	}

	private void addPortfolioLayout(LinearLayout linearLayout) {

		LayoutInflater inflater = getActivity().getLayoutInflater();
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_portfolio_edit, linearLayout, false);

		linearLayout.addView(layout);
	}

	private void checkMandatoryFields(BastogramUserInfoSkill skill) {
		if(skill.getSkillSubGroup() == 0) {
			Toast.makeText(getContext(), "Competence is not filled", Toast.LENGTH_SHORT).show();
			return;
		}

		if(skill.getTags() == null) {
			Toast.makeText(getContext(), "Specialization is not filled", Toast.LENGTH_SHORT).show();
			return;
		}

		if(skill.getEducation() < 0) {
			Toast.makeText(getContext(), "Education level is not filled", Toast.LENGTH_SHORT).show();
			return;
		}

		if(skill.getExperience() == 0) {
			Toast.makeText(getContext(), "Experience is not filled", Toast.LENGTH_SHORT).show();
			return;
		}


		((BastogramUserInfo) AppUser.getInstance().get()).getSkills().remove(mBastogramUserInfoSkill);
		((BastogramUserInfo) AppUser.getInstance().get()).getSkills().add(mBastogramUserInfoSkill);

		skill.setPortfolios(getPortfolios(mPortfolioHolder));
		saveSkill(skill);

		getActivity().onBackPressed();

	}

	private Portfolio getPortfolioFromView(View view) {
		Portfolio portfolio = new Portfolio();

		portfolio.setName(((EditText) (view.findViewById(R.id.projectTitle))).getText().toString());
		portfolio.setDescription(((EditText) (view.findViewById(R.id.projectRoleDescription))).getText().toString());
		portfolio.setLink(((EditText) (view.findViewById(R.id.projectLink))).getText().toString());

		return portfolio;
	}

	private ArrayList<Portfolio> getPortfolios(LinearLayout holder) {
		ArrayList<Portfolio> portfolios = new ArrayList<>();

		if(holder.getChildCount() > 0) {
			for(int i = 0; i < holder.getChildCount(); i++) {
				if(!isPortfolioEmpty(holder.getChildAt(i))) {
					Portfolio tempPortfolio = getPortfolioFromView(holder.getChildAt(i));
					if(mPortfolioIds != null && i < mPortfolioIds.size())
						tempPortfolio.setId(mPortfolioIds.get(i));
					portfolios.add(tempPortfolio);
				}
			}
			mPortfolioIds = null;
		}

		return portfolios;
	}

	private boolean isPortfolioEmpty(View view) {
		return (((EditText) (view.findViewById(R.id.projectTitle))).getText().toString().length() == 0 &&
				((EditText) (view.findViewById(R.id.projectRoleDescription))).getText().toString().length() == 0 &&
				((EditText) (view.findViewById(R.id.projectLink))).getText().toString().length() == 0);
	}

	private void saveSkill(BastogramUserInfoSkill skill) {

		BastogramUserInfo bastogramUserInfo = ((BastogramUserInfo) AppUser.getInstance().get());
		Set<BastogramUserInfoSkill> skills = bastogramUserInfo.getSkills();
		skills.add(skill);

		//ArgsMap map = EditUserInfoRequest.userInfoToArgsMap(bastogramUserInfo);
		ArgsMap map = new ArgsMap(true);
		ArrayList<HashMap<String, Object>> skillsList = new ArrayList<>();

		for(BastogramUserInfoSkill bastogramUserInfoSkill : skills) {
			HashMap<String, Object> skillMap = new HashMap<>();
			skillMap.put("skill_group", bastogramUserInfoSkill.getSkillGroup());
			skillMap.put("skill_subgroup", bastogramUserInfoSkill.getSkillSubGroup());
			skillMap.put("skill", bastogramUserInfoSkill.getSkill());
			skillMap.put("tags", bastogramUserInfoSkill.getTags());
			skillMap.put("education", bastogramUserInfoSkill.getEducation());
			skillMap.put("experience", bastogramUserInfoSkill.getExperience());
			skillMap.put("portfolio", bastogramUserInfoSkill.toServerMap());
			skillsList.add(skillMap);
			//skillsList.add(bastogramUserInfoSkill.toServerMap());
		}
		map.put("skills", skillsList);

		getSpiceManager().execute(new EditUserInfoRequest(map), new SimpleRequestListener<BaseResponse>());
	}

}