package com.bastogram.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.bastogram.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.balance.InternalCurrencyFragment;

/**
 * Created by serezha on 03.10.16.
 */

public class MoneyCurrencyFragment extends InternalCurrencyFragment {

	public static final String BUTTON_TEXT_ID = "button_text_id";
	public static final String IS_GIVE = "is_give";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		RelativeLayout rootView = new RelativeLayout(getContext());
		//rootView.setOrientation(LinearLayout.VERTICAL);
		rootView.setBackgroundColor(getResources().getColor(android.R.color.white));


		Button button = new Button(getContext());

		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(56, 0, 56, 56);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

		button.setBackground(getResources().getDrawable(com.humanet.humanetcore.R.drawable.btn_currency_toggle));
		button.setElevation(8);
		button.setOutlineProvider(ViewOutlineProvider.BOUNDS);
		button.setText(getResources().getString(R.string.crowd_give));
		button.setLayoutParams(layoutParams);
		button.setTag("button");

		button.setVisibility(View.GONE); //TODO: Temporary hidden

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		params.setMargins(0, 0, 0, 200);

		View moneyView = super.onCreateView(inflater, container, savedInstanceState);
		//moneyView.setLayoutParams(params);

		button.setOnClickListener(this);

		rootView.addView(moneyView);
		rootView.addView(button);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		if(v.getTag() == "button") {
			launchGiveFragment();
		}

	}

	private void launchGiveFragment() {
		Bundle bundle = new Bundle();
		bundle.putInt(BUTTON_TEXT_ID, R.string.crowd_give);
		bundle.putInt(IS_GIVE, 1);

		BastogramExternalPaymentsFragment paymentsFragment = new BastogramExternalPaymentsFragment();
		paymentsFragment.setArguments(bundle);

		((BaseActivity) getActivity()).startFragment(paymentsFragment, true);
	}
}
