package com.bastogram.fragments;

import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by serezha on 09.09.16.
 */
public class BastogramCrowdFragment extends BaseVideoCategoryPickerFragment implements IMenuBindFragment {

	@Override
	protected VideoType[] getVideoTypes() {
		return new VideoType[]{
				VideoType.CROWD_PET_ASK,
				VideoType.CROWD_PET_GIVE,
		};
	}

	@Override
	protected String[] getTabNames() {
		return new String[]{
				getString(com.humanet.humanetcore.R.string.crowd_ask),
				getString(com.humanet.humanetcore.R.string.crowd_give),
		};
	}

	@Override
	public String getTitle() {
		return getString(com.humanet.humanetcore.R.string.crowd);
	}

	@Override
	public int getSelectedMenuItem() {
		return com.humanet.humanetcore.R.id.balance;
	}
}
