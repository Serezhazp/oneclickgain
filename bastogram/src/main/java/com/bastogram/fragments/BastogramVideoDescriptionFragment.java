package com.bastogram.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.bastogram.R;
import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.Nursling;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.fragments.capture.VideoDescriptionFragment;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleOnItemSelectedListener;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by serezha on 10.08.16.
 */
public class BastogramVideoDescriptionFragment extends VideoDescriptionFragment {

	private static final String LAST_SELECTED_PET_ID = "last_selected_pet_id";
	private static final String IS_NEED_PET_DESCRIPTION = "is_need_pet_description";

	private SimpleSpinnerAdapter mPetsAdapter;
	private Spinner mPetsSpinner;
	private boolean mIsNeedPetDescription;

	public static BastogramVideoDescriptionFragment newInstance() {
		return new BastogramVideoDescriptionFragment();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = super.onCreateView(inflater, container, savedInstanceState);

		if(getArguments() != null && getArguments().containsKey(IS_NEED_PET_DESCRIPTION)) {
			if(getArguments().getInt(IS_NEED_PET_DESCRIPTION) == 1) {
				mIsNeedPetDescription = true;
			}
		}

		mPetsSpinner = (Spinner) view.findViewById(R.id.sp_pet);

		if(mIsNeedPetDescription) {

			mPetsSpinner.setOnItemSelectedListener(new PetSelectListener());

			Set<Nursling> nurslingSet = ((BastogramUserInfo) AppUser.getInstance().get()).getNurslings();
			ArrayList<Nursling> nurslings = new ArrayList<>();
			Nursling nursling = new Nursling();
			nursling.setName(getResources().getString(R.string.menu_nurslings));
			nurslings.add(nursling);
			nurslings.addAll(nurslingSet);
			mPetsAdapter = new SimpleSpinnerAdapter<>(nurslings);
			mPetsAdapter.setHasHitItem(true);
			mPetsSpinner.setAdapter(mPetsAdapter);

			int selectedPetId = PrefHelper.getIntPref(LAST_SELECTED_PET_ID, 0);
			if (selectedPetId != 0 && selectedPetId != -1) {
				setSelectionById(selectedPetId, nurslings, mPetsSpinner);
			} else if (nurslings.size() > 0 && mPetsAdapter.getCount() > 1) {
				mPetsSpinner.setSelection(1);
			}
		} else {
			mPetsSpinner.setVisibility(View.GONE);
		}

		return view;
	}

	private class PetSelectListener extends SimpleOnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			Nursling nursling = (Nursling) adapterView.getAdapter().getItem(i);
			PrefHelper.setIntPref(LAST_SELECTED_PET_ID, nursling.getId());

			NewVideoInfo.get().setPetId(nursling.getId());

			validateFields();
		}
	}

	@Override
	protected void validateFields() {
		if(!mIsNeedPetDescription) {
			super.validateFields();
			return;
		}

		if(mPetsSpinner.getSelectedItemPosition() != 0)
			super.validateFields();
		else
			super.setButtonEnabled(false);
	}

	@Override
	protected VideoType getVideoType() {

		return super.getVideoType();
	}

	private void setSelectionById(int id, ArrayList<Nursling> nurslings, Spinner spinner) {
		for(int i = 0; i < nurslings.size(); i++) {
			if(nurslings.get(i).getId() == id)
				spinner.setSelection(i);
		}
	}

	/*@Override
	protected ArrayList<Category> getCategories() {

		*//*ArrayList<Category> categories = new ArrayList<>();
		for(int i = 0; i < 5; i++) {
			Category category = new Category(i, "Category " + i);
			categories.add(category);
		}
		return categories;*//*

		//CategoriesRequest request = new CategoriesRequest(VideoType.PET_ALL);

		return super.getCategories();
	}*/
}
