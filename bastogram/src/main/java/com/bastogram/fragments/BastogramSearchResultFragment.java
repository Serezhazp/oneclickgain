package com.bastogram.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.bastogram.views.adapters.BastogramSearchResultAdapter;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;

/**
 * Created by serezha on 25.07.16.
 */
public class BastogramSearchResultFragment extends BaseTitledFragment implements LoaderManager.LoaderCallbacks<Cursor> {

	BastogramSearchResultAdapter mAdapter;

	public static BastogramSearchResultFragment newInstance() {
		BastogramSearchResultFragment fragment = new BastogramSearchResultFragment();
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View view = inflater.inflate(com.humanet.humanetcore.R.layout.fragment_search_result, container, false);
		ListView listView = (ListView) view.findViewById(com.humanet.humanetcore.R.id.result);
		mAdapter = new BastogramSearchResultAdapter(getActivity(), getSpiceManager(), null, 0);
		listView.setAdapter(mAdapter);
		getActivity().getSupportLoaderManager().initLoader(133, null, this); //Why is id 133?

		return view;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), ContentDescriptor.UserSearchResult.URI, null, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	@Override
	public String getTitle() {
		return getString(com.humanet.humanetcore.R.string.search);
	}
}
