package com.bastogram.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bastogram.R;
import com.bastogram.activities.MainActivity;
import com.bastogram.payment.PaymentType;
import com.bastogram.views.IPurchaseView;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.CoinsRequest;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.interfaces.OnBalanceChanged;
import com.humanet.humanetcore.model.Purchase;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.utils.UiUtil;

import org.json.JSONObject;

/**
 * Created by serezha on 29.08.16.
 */
public class BastogramExternalPaymentsFragment extends BaseSpiceFragment implements View.OnClickListener, IPurchaseView {


	private OnBalanceChanged mOnBalanceChanged;

	private RadioGroup mPurchaseRadioGroup;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(com.humanet.humanetcore.R.layout.fragment_balance_currency_external, container, false);

		boolean isSimpleGive = false;

		if(getArguments() != null && getArguments().containsKey(MoneyCurrencyFragment.IS_GIVE)) {
			if(getArguments().getInt(MoneyCurrencyFragment.IS_GIVE) == 1) {
				isSimpleGive = true;
			}
		}

		if(isSimpleGive)
			UiUtil.setTextValue(view, com.humanet.humanetcore.R.id.title, getString(R.string.balance_currency_external_give_title));
		else
			UiUtil.setTextValue(view, com.humanet.humanetcore.R.id.title, getString(com.humanet.humanetcore.R.string.balance_currency_external_title, App.COINS_CODE));

		mPurchaseRadioGroup = (RadioGroup) view.findViewById(com.humanet.humanetcore.R.id.currency_radio_group);

		int padding = (int) ConverterUtil.dpToPix(getContext(), 5);
		RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(padding, padding, padding, padding);

		for (int i = 0; i < Purchase.values().length; i++) {
			Purchase purchase = Purchase.values()[i];
			RadioButton radioButton = new RadioButton(container.getContext());
			radioButton.setButtonDrawable(com.humanet.humanetcore.R.drawable.radio_btn);
			radioButton.setTextColor(getResources().getColor(com.humanet.humanetcore.R.color.infoTextColor));
			if(isSimpleGive)
				radioButton.setText(purchase.simpleTitle());
			else
				radioButton.setText(purchase.title());
			radioButton.setTag(purchase);
			radioButton.setId(i);
			radioButton.setTextSize(16);
			radioButton.setPadding(padding, padding, padding, padding);

			mPurchaseRadioGroup.addView(radioButton, layoutParams);
		}
		mPurchaseRadioGroup.check(0);

		if(getArguments() != null && getArguments().containsKey(MoneyCurrencyFragment.BUTTON_TEXT_ID)) {
			((Button) view.findViewById(R.id.btn_add)).setText(getResources().getString(getArguments().getInt(MoneyCurrencyFragment.BUTTON_TEXT_ID)));
		}

		view.findViewById(com.humanet.humanetcore.R.id.btn_add).setOnClickListener(this);
		view.findViewById(com.humanet.humanetcore.R.id.btn_add_paysera).setOnClickListener(this);
		view.findViewById(com.humanet.humanetcore.R.id.btn_add_xsolla).setOnClickListener(this);

		return view;
	}


	@Override
	public void onClick(View v) {
		int viewId = v.getId();
		if (viewId == com.humanet.humanetcore.R.id.btn_add) {
			executeOperation(PaymentType.GOOGLE);
		}
		if (viewId == com.humanet.humanetcore.R.id.btn_add_paysera) {
			executeOperation(PaymentType.PAYSERA);
		}
		if (viewId == com.humanet.humanetcore.R.id.btn_add_xsolla) {
			executeOperation(PaymentType.XSOLLA);
		}
	}

	public void setOnBalanceChanged(OnBalanceChanged onBalanceChanged) {
		mOnBalanceChanged = onBalanceChanged;
	}

	private void executeOperation(PaymentType paymentType) {
		RadioButton radioButton = (RadioButton) mPurchaseRadioGroup.getChildAt(mPurchaseRadioGroup.getCheckedRadioButtonId());
		Purchase purchase = (Purchase) radioButton.getTag();

		((MainActivity) getActivity()).buy(paymentType, purchase, this);
	}

	@Override
	public void buyingDone(String code, JSONObject data, String signature) {

		CoinsRequest coinsRequest;
		if (code !=null && data!=null) {
			coinsRequest = new CoinsRequest.Builder("game", 1).addFunds(Purchase.findByCode(code).getAmount(), data.toString(), signature).build();
		}else {
			coinsRequest = new CoinsRequest.Builder("game", 1).build();
		}

		getLoader().show();

		getSpiceManager().execute(coinsRequest, new SimpleRequestListener<CoinsHistoryListResponse>() {
			@Override
			public void onRequestSuccess(CoinsHistoryListResponse coinsHistoryListResponse) {
				GameBalanceValue.getInstance().setBalance(coinsHistoryListResponse.getBalance());
				getLoader().dismiss();
				mOnBalanceChanged.onChanged();
			}
		});
	}


}
