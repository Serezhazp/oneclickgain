package com.bastogram.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bastogram.R;
import com.bastogram.api.request.pet.PetRatingRequest;
import com.bastogram.models.Nursling;
import com.bastogram.models.PetsRating;
import com.bastogram.widgets.profile.PetsListView;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.HashMap;

/**
 * Created by serezha on 11.08.16.
 */
public class PetRatingFragment extends BaseSpiceFragment implements AdapterView.OnItemClickListener {

	private PetsListView mPetsListView;

	public static PetRatingFragment newInstance(String ratingType) {
		PetRatingFragment petRatingFragment = new PetRatingFragment();
		Bundle bundle = new Bundle();
		bundle.putString("ratingType", ratingType);
		petRatingFragment.setArguments(bundle);
		return petRatingFragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_pet_rating, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();

		String ratingType = getArguments().containsKey("ratingType") ? getArguments().getString("ratingType") : null;

		PetRatingRequest request = new PetRatingRequest(ratingType);

		getSpiceManager().execute(request, ratingType, DurationInMillis.ONE_SECOND, new PetRatingPendingRequestListener());

	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

		Nursling nursling = (Nursling) mPetsListView.getAdapter().getItem(position);

		Category category = new Category(Category.ALL_ID);
		HashMap<String, Object> map = new HashMap<>();
		map.put("video_type", VideoType.PET_ALL.getRequestParam());
		category.setParams(map);

		PetVideoListFragment fragment = new PetVideoListFragment.Builder(VideoType.PET_ALL)
				.setTitle(nursling.getName())
				.setCategoryType(category)
				.setReplyId(0)
				.setCurrentPosition(0)
				.setNursling(nursling)
				.setNurslingId(nursling.getId())
				.setUserId(nursling.getUserId())
				.build();
		((BaseActivity) getActivity()).startFragment(fragment, true);

	}

	private void setPetsRating(PetsRating rating) {
		View view = getView();
		if(view == null || rating == null) {
			return;
		}

		UiUtil.setTextValue(view, com.humanet.humanetcore.R.id.rate, getString(com.humanet.humanetcore.R.string.rating_rate, BalanceUtil.balanceToString(rating.getRating())));
		UiUtil.setTextValue(view, com.humanet.humanetcore.R.id.rate_position, getString(com.humanet.humanetcore.R.string.rating_rate_position, rating.getPercent() + "%"));

		if(rating.getPets() == null)
			return;

		mPetsListView = (PetsListView) view.findViewById(R.id.top_pets_list);
		mPetsListView.setOnItemClickListener(this);
		Nursling[] nurslings = new Nursling[rating.getPets().size()];
		rating.getPets().toArray(nurslings);
		mPetsListView.showPetList(nurslings);
	}

	private class PetRatingPendingRequestListener extends SimpleRequestListener<PetsRating> {
		@Override
		public void onRequestSuccess(PetsRating petsRating) {
			setPetsRating(petsRating);
		}
	}

}
