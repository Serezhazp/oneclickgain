package com.bastogram.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bastogram.R;
import com.bastogram.activities.SearchActivity;
import com.bastogram.api.request.database.SkillsRequest;
import com.bastogram.db.DBImpl;
import com.bastogram.fragments.PetVideoListFragment;
import com.bastogram.fragments.profile.ViewSkillsFragment;
import com.bastogram.models.BastogramUserFinded;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Nursling;
import com.bastogram.models.SearchTag;
import com.bastogram.models.SkillGroup;
import com.bastogram.models.SkillSubGroup;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.utils.TimeIntervalConverter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by serezha on 22.07.16.
 */
public class BastogramSearchResultAdapter extends CursorAdapter implements View.OnClickListener {

	private final static String TAG_PROFILE = "profile";
	private final static String TAG_SKILLS = "skills";
	private final static String TAG_PETS = "pets";

	//TODO: Implement adapter

	LayoutInflater mLayoutInflater;
	Context mContext;
	SpiceManager mSpiceManager;

	SkillGroup[] mSkillGroups;

	BastogramUserFinded mUserFinded;

	public BastogramSearchResultAdapter(Context context, SpiceManager spiceManager, Cursor c, int flags) {
		super(context, c, flags);

		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContext = context;
		mSpiceManager = spiceManager;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {

		RelativeLayout container = (RelativeLayout) mLayoutInflater.inflate(R.layout.item_search, parent, false);

		CardView profileView = (CardView) container.findViewById(R.id.profile);
		profileView.setTag(TAG_PROFILE);
		profileView.setOnClickListener(this);

		LinearLayout skillsContainer = (LinearLayout) container.findViewById(R.id.skillsContainer);
		skillsContainer.setTag(TAG_SKILLS);
		LinearLayout petsContainer = (LinearLayout) container.findViewById(R.id.petsContainer);
		petsContainer.setTag(TAG_PETS);

		skillsContainer.setOnClickListener(this);
		petsContainer.setOnClickListener(this);

		new ViewHolder(container);

		return container;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {

		ViewHolder viewHolder = (ViewHolder) view.getTag();

		mUserFinded = BastogramUserFinded.fromCursor(cursor);

		mUserFinded.setSkills(DBImpl.getSkillsByUserId(mUserFinded.getId()));
		mUserFinded.setNurslings(DBImpl.getNurslingsByUserId(mUserFinded.getId()));

		viewHolder.profileView.setTag(new SearchTag(SearchTag.PROFILE, mUserFinded.getId()));

		if (mUserFinded.getId() == AppUser.getInstance().getUid()) {
			viewHolder.profileView.setCardBackgroundColor(context.getResources().getColor(com.humanet.humanetcore.R.color.listItemSelected));
			viewHolder.skillsHeader.setCardBackgroundColor(context.getResources().getColor(com.humanet.humanetcore.R.color.listItemSelected));
			viewHolder.petsHeader.setCardBackgroundColor(context.getResources().getColor(com.humanet.humanetcore.R.color.listItemSelected));
		} else {
			viewHolder.profileView.setCardBackgroundColor(context.getResources().getColor(com.humanet.humanetcore.R.color.listItemNotSelected));
			viewHolder.skillsHeader.setCardBackgroundColor(context.getResources().getColor(com.humanet.humanetcore.R.color.listItemNotSelected));
			viewHolder.petsHeader.setCardBackgroundColor(context.getResources().getColor(com.humanet.humanetcore.R.color.listItemNotSelected));
		}


		String[] required = mUserFinded.getRequiredFields();
		if (required == null) {
			required = new String[0];
		}

		StringBuilder userName = new StringBuilder();
		String value = validateRequired(
				ContentDescriptor.UserSearchResult.Cols.FIRST_NAME,
				mUserFinded.getFirstName(),
				required
		);

		userName.append(value);
		userName.append(" ");

		value = validateRequired(
				ContentDescriptor.UserSearchResult.Cols.LAST_NAME,
				mUserFinded.getLastName(),
				required
		);
		userName.append(value);
		viewHolder.nameView.setText(Html.fromHtml(userName.toString()));

		StringBuilder location = new StringBuilder();

		if (mUserFinded.getCountry() != null) {
			value = validateRequired(
					ContentDescriptor.UserSearchResult.Cols.COUNTRY_ID,
					mUserFinded.getCountry(),
					mUserFinded.getCountryId(),
					required);

			location.append(value);
			location.append(" ");
		}

		if (mUserFinded.getCity() != null) {
			value = validateRequired(
					ContentDescriptor.UserSearchResult.Cols.CITY_ID,
					mUserFinded.getCity(),
					mUserFinded.getCityId(),
					required);
			location.append(value);
		}
		viewHolder.location.setText(Html.fromHtml(location.toString()));

		StringBuilder details = new StringBuilder();
		if (!TextUtils.isEmpty(mUserFinded.getLang())) {
			Language lang = Language.getFromString(mUserFinded.getLang());

			String requiredLang = getPropertyOfRequiredPair(ContentDescriptor.UserSearchResult.Cols.LANG, required);

			value = lang.getTitle();
			if (requiredLang != null && requiredLang.equals(lang.getRequestParam())) {
				value = "<b>" + value + "</b>";
			}

			details.append(value);
			details.append(", ");
		}

		if (!TextUtils.isEmpty(mUserFinded.getCraft())) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.CRAFT,
					mUserFinded.getCraft(),
					required));

			details.append(", ");
		}

		if (mUserFinded.getHobby() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.HOBBIE,
					mUserFinded.getHobby().getTitle(),
					mUserFinded.getHobby().getId(),
					required));

			details.append(", ");
		}

		if (mUserFinded.getInterest() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.INTEREST,
					mUserFinded.getInterest().getTitle(),
					mUserFinded.getInterest().getId(),
					required));
			details.append(", ");
		}

		if (mUserFinded.getSport() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.SPORT,
					mUserFinded.getSport().getTitle(),
					mUserFinded.getSport().getId(),
					required));
			details.append(", ");
		}

		if (mUserFinded.getPets() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.PETS,
					mUserFinded.getPets().getTitle(),
					mUserFinded.getPets().getId(),
					required));
			details.append(", ");
		}
       /*
        if (user.getPets() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.PETS,
                    user.getPets().getTitle(), user.getPets().getId(),
                    required));
            details.append(", ");
        }
*/
		if (mUserFinded.getReligion() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.RELIGION,
					mUserFinded.getReligion().getTitle(),
					mUserFinded.getReligion().getId(),
					required));
			details.append(", ");
		}
		if (mUserFinded.getGames() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.GAME,
					mUserFinded.getGames().getTitle(),
					mUserFinded.getGames().getId(),
					required));
			details.append(", ");
		}
		if (mUserFinded.getNonGame() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.NON_GAME,
					mUserFinded.getNonGame().getTitle(),
					mUserFinded.getNonGame().getId(),
					required));
			details.append(", ");
		}
		if (mUserFinded.getExtreme() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.EXTREME,
					mUserFinded.getExtreme().getTitle(),
					mUserFinded.getExtreme().getId(),
					required));
			details.append(", ");
		}
		if (mUserFinded.getVirtual() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.VIRTUAL,
					mUserFinded.getVirtual().getTitle(),
					mUserFinded.getVirtual().getId(),
					required));
			details.append(", ");
		}
		if (mUserFinded.getHowILook() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK,
					mUserFinded.getHowILook().getTitle(),
					mUserFinded.getHowILook().getId(),
					required));
			details.append(", ");
		}
		if (mUserFinded.getPartGames() != null) {
			details.append(validateRequired(
					ContentDescriptor.UserSearchResult.Cols.PART_GAMES,
					mUserFinded.getPartGames().getTitle(),
					mUserFinded.getPartGames().getId(),
					required));
			details.append(", ");
		}

		if (details.length() > 1) {
			viewHolder.detailsView.setText(Html.fromHtml(details.substring(0, details.length() - 2)));
		}

		viewHolder.ratingView.setText(BalanceUtil.balanceToString(mUserFinded.getRating()));

		viewHolder.avatarView.setImageDrawable(null);
		if (mUserFinded.getAvatarSmall() != null)
			ImageLoader.getInstance().displayImage(mUserFinded.getAvatarSmall(), viewHolder.avatarView);

		/**
		 * Mock here
		 */
		removeSkillAndPetLayouts(viewHolder);

		if(mUserFinded.getSkills() != null) {
			for (int i = 0; i < mUserFinded.getSkills().size(); i++) {
				SearchTag skillSearchTag = new SearchTag(SearchTag.SKILL, mUserFinded.getId(), i);
				skillSearchTag.setObject(mUserFinded.getSkills().get(i));
				addSkillLayout(viewHolder.skillsContainer, mUserFinded.getSkills().get(i), skillSearchTag);
			}
		}

		if(mUserFinded.getNurslings() != null) {
			for (int i = 0; i < mUserFinded.getNurslings().size(); i++) {
				SearchTag nurslingSearchTag = new SearchTag(SearchTag.PET, mUserFinded.getId(), i);

				Nursling nursling = mUserFinded.getNurslings().get(i);
				int id = mUserFinded.getId();
				nursling.setUserId(id);
				nurslingSearchTag.setObject(nursling);
				addPetLayout(viewHolder.petsContainer, mUserFinded.getNurslings().get(i), nurslingSearchTag);
			}
		}

	}

	@Override
	public void onClick(View view) {

		if(view.getTag() instanceof SearchTag) {
			SearchTag tag = (SearchTag) view.getTag();
			switch (tag.getType()) {
				case SearchTag.PROFILE:
					((SearchActivity) mContext).openUserProfile(tag.getUserId());
					break;
				case SearchTag.PET:
					Nursling nursling = ((Nursling) tag.getObject());

					Category category = new Category(Category.ALL_ID);
					HashMap<String, Object> map = new HashMap<>();
					map.put("video_type", VideoType.PET_ALL.getRequestParam());
					category.setParams(map);

					PetVideoListFragment fragment = new PetVideoListFragment.Builder(VideoType.PET_ALL)
							.setTitle(nursling.getName())
							.setCategoryType(category)
							.setReplyId(0)
							.setCurrentPosition(0)
							.setNursling(nursling)
							.setNurslingId(nursling.getId())
							.setUserId(nursling.getUserId())
							.build();
					((BaseActivity) mContext).startFragment(fragment, true);

					break;
				case SearchTag.SKILL:
					((BaseActivity) mContext).startFragment(ViewSkillsFragment.newInstance(((BastogramUserInfoSkill) tag.getObject()).getSkillSubGroup(), tag.getUserId()), true);
					break;
			}
		}
	}

	private class ViewHolder {
		RelativeLayout container;

		CardView profileView;
		TextView nameView;
		TextView location;
		TextView ratingView;
		TextView detailsView;
		ImageView avatarView;

		CardView skillsHeader;
		LinearLayout skillsContainer;
		CardView petsHeader;
		LinearLayout petsContainer;

		public ViewHolder(View view) {

			container = (RelativeLayout) view;

			profileView = (CardView) container.findViewById(R.id.profile);
			nameView = (TextView) profileView.findViewById(R.id.name);
			location = (TextView) profileView.findViewById(R.id.location);
			detailsView = (TextView) profileView.findViewById(R.id.details);
			ratingView = (TextView) profileView.findViewById(R.id.rating);
			avatarView = (ImageView) profileView.findViewById(R.id.avatar);

			skillsHeader = (CardView) container.findViewById(R.id.skillsHeader);
			skillsContainer = (LinearLayout) container.findViewById(R.id.skillsContainer);
			petsHeader = (CardView) container.findViewById(R.id.petsHeader);
			petsContainer = (LinearLayout) container.findViewById(R.id.petsContainer);

			view.setTag(this);
		}
	}

	private String validateRequired(String propertyName, String propertyValue, String[] requiredFields) {
		if (propertyValue == null)
			return null;
		for (String requiredField : requiredFields) {
			String[] pair = requiredField.split("~");
			if (pair[0].equals(propertyName) && pair[1].equals(propertyValue)) {
				return "<b>" + propertyValue + "</b>";
			}
		}
		return propertyValue;
	}

	private String validateRequired(String propertyName, String propertyValue, int propertyValueId, String[] requiredFields) {
		if (propertyValue == null)
			return null;
		for (String requiredField : requiredFields) {
			String[] pair = requiredField.split("~");
			if (pair[0].equals(propertyName) && pair[1].equals(String.valueOf(propertyValueId))) {
				return "<b>" + propertyValue + "</b>";
			}
		}
		return propertyValue;
	}

	private String getPropertyOfRequiredPair(String requiredKey, String[] requiredFields) {
		for (String requiredField : requiredFields) {
			String[] pair = requiredField.split("~");
			if (pair[0].equals(requiredKey))
				return pair[1];
		}
		return null;
	}

	private void addSkillLayout(LinearLayout container, BastogramUserInfoSkill skill, SearchTag tag) {
		View skillView = fillSkillView(skill);
		skillView.setTag(tag);
		skillView.setOnClickListener(this);
		if (tag.getUserId() == AppUser.getInstance().getUid()) {
			((CardView) skillView).setCardBackgroundColor(mContext.getResources().getColor(com.humanet.humanetcore.R.color.listItemSelected) );
		} else {
			((CardView) skillView).setCardBackgroundColor(mContext.getResources().getColor(com.humanet.humanetcore.R.color.listItemNotSelected) );
		}
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 0, 10);
		skillView.setLayoutParams(params);

		container.addView(skillView);
	}

	private void addPetLayout(LinearLayout container, Nursling nursling, SearchTag tag) {
		View petView = fillPetView(nursling);
		petView.setTag(tag);
		petView.setOnClickListener(this);
		if (tag.getUserId() == AppUser.getInstance().getUid()) {
			((CardView) petView).setCardBackgroundColor(mContext.getResources().getColor(com.humanet.humanetcore.R.color.listItemSelected) );
		} else {
			((CardView) petView).setCardBackgroundColor(mContext.getResources().getColor(com.humanet.humanetcore.R.color.listItemNotSelected) );
		}
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 0, 10);
		petView.setLayoutParams(params);

		container.addView(petView);
	}

	private void removeSkillAndPetLayouts(ViewHolder viewHolder) {
		viewHolder.skillsContainer.removeAllViews();
		viewHolder.petsContainer.removeAllViews();
	}

	private View fillSkillView(BastogramUserInfoSkill skill) {
		View skillView = mLayoutInflater.inflate(R.layout.item_skill, null, false);

		//ImageLoader.getInstance().displayImage(skill.getDrawable(), (ImageView) skillView.findViewById(R.id.skill_icon));

		if(mSkillGroups != null) {
			((TextView) skillView.findViewById(R.id.skill)).setText(getSkillTitleById(skill.getSkillSubGroup(), mSkillGroups));
		} else {
			SkillsRequest request = new SkillsRequest();
			mSpiceManager.execute(request, new SimpleRequestListener<SkillGroup[]>() {
				@Override
				public void onRequestSuccess(SkillGroup[] skillGroups) {
					mSkillGroups = skillGroups;
					((TextView) skillView.findViewById(R.id.skill)).setText(getSkillTitleById(skill.getSkillSubGroup(), mSkillGroups));
				}
			});
		}
		((TextView) skillView.findViewById(R.id.experience)).setText(String.format(Locale.getDefault(), "%s", TimeIntervalConverter.convert(mContext, skill.getExperience())));
		((TextView) skillView.findViewById(R.id.specialization)).setText(skill.getTags());
		((TextView) skillView.findViewById(R.id.education)).setText(Education.values().get(skill.getEducation() + 1).getTitle());

		return skillView;
	}

	private String getSkillTitleById(int id, SkillGroup[] skillGroups) {
		for(SkillGroup skillGroup : skillGroups) {
			for (SkillSubGroup skillSubGroup : skillGroup.getSkillSubGroups()) {
				if(id == skillSubGroup.getId())
					return skillSubGroup.getTitle();
			}
		}

		return "";
	}

	private View fillPetView(Nursling nursling) {
		View petView = mLayoutInflater.inflate(R.layout.item_pet, null, false);

		ImageView pawAchievementBronze = (ImageView) petView.findViewById(R.id.pawBronze);
		ImageView pawAchievementSilver = (ImageView) petView.findViewById(R.id.pawSilver);
		ImageView pawAchievementGold = (ImageView) petView.findViewById(R.id.pawGold);
		ImageView pawAchievementRuby = (ImageView) petView.findViewById(R.id.pawRuby);
		ImageView pawAchievementBrilliant = (ImageView) petView.findViewById(R.id.pawBrilliant);

		pawAchievementBronze.setVisibility(View.INVISIBLE);
		pawAchievementSilver.setVisibility(View.INVISIBLE);
		pawAchievementGold.setVisibility(View.INVISIBLE);
		pawAchievementRuby.setVisibility(View.INVISIBLE);
		pawAchievementBrilliant.setVisibility(View.INVISIBLE);

		if(nursling.getAvatar() != null)
			ImageLoader.getInstance().displayImage(nursling.getAvatar(), (ImageView) petView.findViewById(R.id.avatar));

		if(nursling.getName() != null)
			((TextView) petView.findViewById(R.id.name)).setText(nursling.getName());
		if(nursling.getKind() != -1) {
			String[] kindTitles = mContext.getResources().getStringArray(R.array.breeds);
			((TextView) petView.findViewById(R.id.about)).setText(String.format(Locale.getDefault(), "%s, %s", TimeIntervalConverter.convert(mContext, nursling.getBirthDate() * 1000), kindTitles[nursling.getKind()]));
		}
		((ImageView) petView.findViewById(R.id.gender)).setImageResource(nursling.getGender() == UserInfo.MALE ? R.drawable.ic_pet_male : R.drawable.ic_pet_female);
		((ImageView) petView.findViewById(R.id.vaccination)).setImageResource(nursling.isVaccinated() ? R.drawable.ic_pet_vaccinated : R.drawable.ic_pet_not_vaccinated);
		((ImageView) petView.findViewById(R.id.ready_for_child)).setImageResource(nursling.isChildReady() ? R.drawable.ic_pet_ready_for_child : R.drawable.ic_pet_not_ready_for_child);

		if(nursling.getBages() != null) {
			for(Integer bage : nursling.getBages()) {
				if(bage == Nursling.BRONZE)
					pawAchievementBronze.setVisibility(View.VISIBLE);
				if(bage == Nursling.SILVER)
					pawAchievementSilver.setVisibility(View.VISIBLE);
				if(bage == Nursling.GOLD)
					pawAchievementGold.setVisibility(View.VISIBLE);
				if(bage == Nursling.RUBY)
					pawAchievementRuby.setVisibility(View.VISIBLE);
				if(bage == Nursling.BRILLIANT)
					pawAchievementBrilliant.setVisibility(View.VISIBLE);
			}
		}

		return petView;
	}

}
