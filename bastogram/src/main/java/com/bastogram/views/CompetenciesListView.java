package com.bastogram.views;

import com.bastogram.models.Competence;

import java.util.List;

/**
 * Created by serezha on 04.08.16.
 */
public interface CompetenciesListView {
	void setCompetenciesList(List<Competence> competences);
}
