package com.bastogram.views;

import com.bastogram.models.Portfolio;
import com.humanet.humanetcore.views.BaseView;

/**
 * Created by Uran on 09.06.2016.
 */
public interface IPortfolioView extends BaseView {

    void showPortfolioList(Portfolio[] portfolios);

}
