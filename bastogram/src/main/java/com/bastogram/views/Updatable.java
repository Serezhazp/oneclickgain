package com.bastogram.views;

/**
 * Created by serezha on 19.07.16.
 */
public interface Updatable {
	void update();
}
