package com.bastogram.views;

import com.bastogram.models.SkillSubGroup;
import com.humanet.humanetcore.views.BaseView;

/**
 * Created by Uran on 08.06.2016.
 */
public interface ISkillsView extends BaseView {

    void showSkillList(SkillSubGroup[] skills);



}
