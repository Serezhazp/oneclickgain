package com.bastogram.views;

import com.bastogram.models.Nursling;
import com.humanet.humanetcore.views.BaseView;

/**
 * Created by Uran on 08.06.2016.
 */
public interface IPetView extends BaseView {

	void showPet(Nursling nursling);


}
