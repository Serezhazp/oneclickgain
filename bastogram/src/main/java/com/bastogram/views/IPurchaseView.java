package com.bastogram.views;

import org.json.JSONObject;

/**
 * Created by serezha on 29.08.16.
 */
public interface IPurchaseView {

	void buyingDone(String code, JSONObject data, String signature);

}
