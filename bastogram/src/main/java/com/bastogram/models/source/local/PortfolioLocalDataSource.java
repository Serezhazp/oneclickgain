package com.bastogram.models.source.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bastogram.models.Portfolio;
import com.bastogram.models.Skill;
import com.bastogram.models.source.IDataSource;
import com.humanet.humanetcore.db.DBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class PortfolioLocalDataSource implements IDataSource<Portfolio> {

    public List<Portfolio> getList() {
        Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(Portfolio.TABLE_NAME, null, null, null, null, null, null);

        List<Portfolio> portfolios = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                portfolios.add(Portfolio.fromCursor(cursor));
            } while (cursor.moveToNext());

            cursor.close();
        }
        return portfolios;
    }

    public List<Portfolio> getListBySkill(Skill skill) {
        String where = Portfolio.COLUMN_SKILL_ID + "=?";
        String[] is = new String[1];
        is[0] = String.valueOf(skill.getSkillItem());

        Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(Portfolio.TABLE_NAME, null, where, is, null, null, null);

        List<Portfolio> portfolios = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                portfolios.add(Portfolio.fromCursor(cursor));
            } while (cursor.moveToNext());

            cursor.close();
        }

        return portfolios;
    }

    public Portfolio getById(int id) {
        Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(Portfolio.TABLE_NAME, null, "_id=?", new String[]{String.valueOf(id)}, null, null, null);

        Portfolio nursling = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                nursling = Portfolio.fromCursor(cursor);
            } while (cursor.moveToNext());

            cursor.close();
        }
        return nursling;
    }


    public Portfolio save(Portfolio portfolio) {
        DBHelper.getInstance().getWritableDatabase().insertWithOnConflict(
                Portfolio.TABLE_NAME,
                null,
                Portfolio.toContentValues(portfolio), SQLiteDatabase.CONFLICT_REPLACE);

        return portfolio;

    }

    public Portfolio edit(Portfolio portfolio) {
        DBHelper.getInstance().getWritableDatabase().update(
                Portfolio.TABLE_NAME,
                Portfolio.toContentValues(portfolio),
                String.format("_id=%d", portfolio.getId()),
                null);

        return portfolio;
    }

    public void delete(int id) {
        DBHelper.getInstance().getWritableDatabase().delete(
                Portfolio.TABLE_NAME,
                "_id=?",
                new String[]{String.valueOf(id)}
        );
    }

    public void delete(Portfolio portfolio) {
        delete(portfolio.getId());
    }
}
