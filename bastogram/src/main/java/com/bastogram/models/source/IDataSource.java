package com.bastogram.models.source;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public interface IDataSource<T> {

    List<T> getList();

    T getById(int id);

    T save(T value);

    T edit(T value);

    void delete(int id);

    void delete(T value);

}