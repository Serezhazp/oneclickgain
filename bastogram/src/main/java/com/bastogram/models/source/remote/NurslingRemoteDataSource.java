package com.bastogram.models.source.remote;

import com.bastogram.models.Nursling;
import com.bastogram.models.source.IDataSource;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class NurslingRemoteDataSource implements IDataSource<Nursling> {
    @Override
    public List<Nursling> getList() {
        return null;
    }

    @Override
    public Nursling getById(int id) {
        return null;
    }

    @Override
    public Nursling save(Nursling value) {
        return value;
    }

    @Override
    public Nursling edit(Nursling value) {
        return value;
    }

    @Override
    public void delete(int id) {
    }

    @Override
    public void delete(Nursling value) {
    }
}
