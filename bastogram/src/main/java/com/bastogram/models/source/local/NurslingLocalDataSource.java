package com.bastogram.models.source.local;

import com.bastogram.models.Nursling;
import com.bastogram.models.source.IDataSource;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class NurslingLocalDataSource implements IDataSource<Nursling> {

    @Override
    public List<Nursling> getList() {

        /*Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(Nursling.TABLE_NAME, null, null, null, null, null, null);

        List<Nursling> nurslings = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                nurslings.add(Nursling.fromCursor(cursor));
            } while (cursor.moveToNext());

            cursor.close();
        }

        return nurslings;*/
        return null;
    }



    public Nursling getById(int id) {
        /*Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(Nursling.TABLE_NAME, null, "_id=?", new String[]{String.valueOf(id)}, null, null, null);

        Nursling nursling = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                nursling = Nursling.fromCursor(cursor);
            } while (cursor.moveToNext());

            cursor.close();
        }
        return nursling;*/
        return null;
    }

    @Override
    public Nursling save(Nursling value) {
        /*DBHelper.getInstance().getWritableDatabase().insertWithOnConflict(
                Nursling.TABLE_NAME,
                null,
                Nursling.toContentValues(value), SQLiteDatabase.CONFLICT_IGNORE);

        return value;*/

        return null;
    }

    @Override
    public Nursling edit(Nursling value) {
        /*DBHelper.getInstance().getWritableDatabase().update(
                Nursling.TABLE_NAME,
                Nursling.toContentValues(value),
                "_id=?",
                new String[]{String.valueOf(value.getId())});

        return value;*/

        return null;
    }

    @Override
    public void delete(int id) {
        /*DBHelper.getInstance().getWritableDatabase().delete(
                Nursling.TABLE_NAME,
                "_id=?",
                new String[]{String.valueOf(id)}
        );*/
    }

    @Override
    public void delete(Nursling value) {
        /*delete(value.getId());*/
    }
}
