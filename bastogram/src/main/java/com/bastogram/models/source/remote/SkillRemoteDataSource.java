package com.bastogram.models.source.remote;

import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Skill;
import com.bastogram.models.source.IDataSource;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.sets.UserApiSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uran on 15.06.2016.
 */
public class SkillRemoteDataSource extends BaseRemoteDataSource<UserApiSet> implements IDataSource<Skill> {

    BastogramUserInfo mBastogramUserInfo = (BastogramUserInfo) AppUser.getInstance().get();



    @Override
    public List<Skill> getList() {
        return null;
    }

    @Override
    public Skill getById(int id) {
        return null;
    }

    @Override
    public Skill save(Skill value) {
        return value;
    }

    @Override
    public Skill edit(Skill value) {
        return value;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void delete(Skill value) {

    }

    public Skill getSkillBySubgroup(int subGroup) {
        return getSkillFromBastogramUserInfoSkill(getBastogramUserInfoSkillBySubgroup(subGroup));
    }

    private List<Skill> getSkillsFromBastogramUserInfoSkills(List<BastogramUserInfoSkill> bastogramUserInfoSkills) {
        List<Skill> skills = new ArrayList<>();
        for(BastogramUserInfoSkill userInfoSkill : bastogramUserInfoSkills) {
            skills.add(getSkillFromBastogramUserInfoSkill(userInfoSkill));
        }

        return skills;
    }

    public BastogramUserInfoSkill getBastogramUserInfoSkillBySubgroup(int subgroup) {
        for(BastogramUserInfoSkill infoSkill : mBastogramUserInfo.getSkills()) {
            if(infoSkill.getSkillSubGroup() == subgroup)
                return infoSkill;
        }

        return new BastogramUserInfoSkill();
    }

    private Skill getSkillFromBastogramUserInfoSkill(BastogramUserInfoSkill infoSkill) {
        Skill skill = new Skill();
        //skill.setId(infoSkill.getId());
        skill.setCompetence(infoSkill.getSkill());
        skill.setSpecialization(infoSkill.getTags());
        skill.setLevel(infoSkill.getEducation());
        skill.setExperience(infoSkill.getExperience());
        skill.setSkillGroup(infoSkill.getSkillGroup());
        skill.setSkillSubgroup(infoSkill.getSkillSubGroup());

        return skill;
    }
}
