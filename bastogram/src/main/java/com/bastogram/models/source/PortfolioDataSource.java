package com.bastogram.models.source;

import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Portfolio;
import com.bastogram.models.source.local.PortfolioLocalDataSource;
import com.bastogram.models.source.remote.PortfolioRemoteDataSource;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class PortfolioDataSource extends BaseDataSource<Portfolio> {

    private PortfolioLocalDataSource mPortfolioLocalDataSource;
    private PortfolioRemoteDataSource mPortfolioRemoteDataSource;

    public PortfolioDataSource(IDataSource<Portfolio> localDataSource, IDataSource<Portfolio> remoteDataSource) {
        super(localDataSource, remoteDataSource);

        mPortfolioLocalDataSource = (PortfolioLocalDataSource) localDataSource;
        mPortfolioRemoteDataSource = (PortfolioRemoteDataSource) remoteDataSource;
    }

    @Override
    public List<Portfolio> getList() {
        return super.getList();
    }

    @Override
    public Portfolio getById(int id) {
        return super.getById(id);
    }

    @Override
    public Portfolio save(Portfolio value) {
        return super.save(value);
    }

    @Override
    public Portfolio edit(Portfolio value) {
        return super.edit(value);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public void delete(Portfolio value) {
        super.delete(value);
    }

    public List<Portfolio> getListBySkillId(int id) {
        return mPortfolioRemoteDataSource.getListBySkillId(id);
    }

    public List<Portfolio> getListBySkill(BastogramUserInfoSkill skill) {
        return mPortfolioRemoteDataSource.getListBySkill(skill);
    }

    public List<Portfolio> getListBySubgroup(int subgroup) {
        return mPortfolioRemoteDataSource.getListBySubgroup(subgroup);
    }
}
