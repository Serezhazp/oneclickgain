package com.bastogram.models.source;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public abstract class BaseDataSource<T> implements IDataSource<T> {

    private final IDataSource<T> mLocalDataSource;
    private final IDataSource<T> mRemoteDataSource;

    public BaseDataSource(IDataSource<T> localDataSource, IDataSource<T> remoteDataSource) {
        mLocalDataSource = localDataSource;
        mRemoteDataSource = remoteDataSource;
    }

    //TODO: Implement correct methods

    @Override
    public List<T> getList() {
        return mLocalDataSource.getList();
    }

    @Override
    public T getById(int id) {
        return mLocalDataSource.getById(id);
    }

    @Override
    public T save(T value) {
        return mLocalDataSource.save(value);
    }

    @Override
    public T edit(T value) {
        return mLocalDataSource.edit(value);
    }

    @Override
    public void delete(int id) {
        mLocalDataSource.delete(id);
    }

    @Override
    public void delete(T value) {
        mLocalDataSource.delete(value);
    }
}
