package com.bastogram.models.source.local;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bastogram.models.Skill;
import com.bastogram.models.source.IDataSource;
import com.humanet.humanetcore.db.DBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class SkillLocalDataSource implements IDataSource<Skill> {

    public List<Skill> getList() {

        Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(
                Skill.TABLE_NAME,
                null, null, null, null, null, null
        );
        List<Skill> skills = new ArrayList<>();

        if(cursor.moveToFirst()) {
            do {
                skills.add(Skill.fromCursor(cursor));
            } while (cursor.moveToNext());

            cursor.close();
        }

        return skills;
    }

    public Skill getById(int id) {
        Cursor cursor = DBHelper.getInstance().getReadableDatabase().query(
                Skill.TABLE_NAME,
                null,
                "_id=?",
                new String[]{String.valueOf(id)},
                null, null, null);

        Skill skill = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                skill = Skill.fromCursor(cursor);
            } while (cursor.moveToNext());

            cursor.close();
        }
        return skill;
    }

    public Skill save(Skill skill) {
        DBHelper.getInstance().getWritableDatabase().insertWithOnConflict(
                Skill.TABLE_NAME,
                null,
                Skill.toContentValues(skill), SQLiteDatabase.CONFLICT_REPLACE);

        return skill;

    }

    public Skill edit(Skill skill) {
        DBHelper.getInstance().getWritableDatabase().update(
                Skill.TABLE_NAME,
                Skill.toContentValues(skill),
                String.format("_id=%d", skill.getId()),
                null);

        return skill;
    }

    public void delete(Skill skill) {
        delete(skill.getId());
    }

    public void delete(int id) {
        DBHelper.getInstance().getWritableDatabase().delete(
                Skill.TABLE_NAME,
                "_id=?",
                new String[]{String.valueOf(id)}
        );
    }
}
