package com.bastogram.models.source.remote;

import com.bastogram.models.BastogramUserInfo;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Portfolio;
import com.bastogram.models.source.IDataSource;
import com.humanet.humanetcore.AppUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uran on 15.06.2016.
 */
public class PortfolioRemoteDataSource implements IDataSource<Portfolio> {

    BastogramUserInfo mBastogramUserInfo = (BastogramUserInfo) AppUser.getInstance().get();


    @Override
    public List<Portfolio> getList() {

        return null;
    }

    @Override
    public Portfolio getById(int id) {
        return null;
    }

    @Override
    public Portfolio save(Portfolio value) {

        return null;
    }

    @Override
    public Portfolio edit(Portfolio value) {

        return null;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void delete(Portfolio value) {

    }

    public List<Portfolio> getListBySubgroup(int subgroup) {
        for(BastogramUserInfoSkill infoSkill : ((BastogramUserInfo)AppUser.getInstance().get()).getSkills()) {
            if(infoSkill.getSkillSubGroup() == subgroup)
                return infoSkill.getPortfolios();
        }

        return new ArrayList<>();
    }

    public List<Portfolio> getListBySkillId(int id) {
        for(BastogramUserInfoSkill infoSkill : ((BastogramUserInfo)AppUser.getInstance().get()).getSkills()) {
            //if(infoSkill.getId() == id)
                return infoSkill.getPortfolios();
        }

        return new ArrayList<>();
    }

    public List<Portfolio> getListBySkill(BastogramUserInfoSkill skill) {
        //return getListBySkillId(skill.getId());
        return null;
    }

}
