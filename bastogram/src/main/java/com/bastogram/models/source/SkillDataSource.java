package com.bastogram.models.source;

import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Skill;
import com.bastogram.models.source.local.SkillLocalDataSource;
import com.bastogram.models.source.remote.SkillRemoteDataSource;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class SkillDataSource extends BaseDataSource<Skill> {

    private SkillLocalDataSource mSkillLocalDataSource;
    private SkillRemoteDataSource mSkillRemoteDataSource;

    public SkillDataSource(SkillLocalDataSource localDataSource, SkillRemoteDataSource remoteDataSource) {
        super(localDataSource, remoteDataSource);

        mSkillLocalDataSource = localDataSource;
        mSkillRemoteDataSource = remoteDataSource;
    }


    @Override
    public List<Skill> getList() {
        return mSkillRemoteDataSource.getList();
    }

    @Override
    public Skill getById(int id) {
        return super.getById(id);
    }

    @Override
    public Skill save(Skill value) {
        return super.save(value);
    }

    @Override
    public Skill edit(Skill value) {
        return super.edit(value);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public void delete(Skill value) {
        super.delete(value);
    }

    public Skill getSkillBySubgroup(int subGroup) {
        return mSkillRemoteDataSource.getSkillBySubgroup(subGroup);
    }

    public BastogramUserInfoSkill getBastogramUserInfoSkillBySubgroup(int subGroup) {
        return mSkillRemoteDataSource.getBastogramUserInfoSkillBySubgroup(subGroup);
    }
}
