package com.bastogram.models;

/**
 * Created by serezha on 25.07.16.
 */
public class SearchTag {
	public static final String PROFILE = "profile";
	public static final String SKILL = "skill";
	public static final String PET = "pet";

	private int mUserId = -1;
	private int mPetId = -1;
	private int mNumber = -1;
	private String mType;
	private Object mObject;

	public SearchTag(String type, int userId) {
		this.mType = type;
		this.mUserId = userId;
	}

	public SearchTag(String type, int userId, int number) {
		this.mType = type;
		this.mUserId = userId;
		this.mNumber = number;
	}

	public int getUserId() {
		return mUserId;
	}

	public void setUserId(Integer userId) {
		mUserId = userId;
	}

	public int getPetId() {
		return mPetId;
	}

	public void setPetId(int petId) {
		mPetId = petId;
	}

	public int getNumber() {
		return mNumber;
	}

	public void setNumber(Integer number) {
		mNumber = number;
	}

	public String getType() {
		return mType;
	}

	public void setType(String type) {
		mType = type;
	}

	public Object getObject() {
		return mObject;
	}

	public void setObject(Object object) {
		mObject = object;
	}

	@Override
	public String toString() {
		return "Type: " + this.mType + "\n"
				+ "Id: " + this.mUserId + "\n"
				+ "Number: " + this.mNumber;
	}
}
