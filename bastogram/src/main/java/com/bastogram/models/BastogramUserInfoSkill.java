package com.bastogram.models;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by serezha on 29.07.16.
 */
public class BastogramUserInfoSkill implements Serializable, Cloneable {

	public static final String TABLE_NAME = "skills";

	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_SKILL_SUBGROUP = "skill_subgroup";
	public static final String COLUMN_SKILL = "skill";
	public static final String COLUMN_TAGS = "tags";
	public static final String COLUMN_EDUCATION = "education";
	public static final String COLUMN_EXPERIENCE = "experience";

	@SerializedName("skill_group")
	private int mSkillGroup;

	@SerializedName("skill_subgroup")
	private int mSkillSubgroup;

	@SerializedName("skill")
	private int mSkill;

	@SerializedName("tags")
	private String mTags;

	@SerializedName("education")
	private int mEducation;

	@SerializedName("experience")
	private int mExperience;

	@SerializedName("portfolio")
	private List<Portfolio> mPortfolios;

	public int getSkillGroup() {
		return mSkillGroup;
	}

	public void setSkillGroup(int skillGroup) {
		mSkillGroup = skillGroup;
	}

	public int getSkillSubGroup() {
		return mSkillSubgroup;
	}

	public void setSkillSubgroup(int skillSubgroup) {
		mSkillSubgroup = skillSubgroup;
	}

	public int getSkill() {
		return mSkill;
	}

	public void setSkill(int skill) {
		mSkill = skill;
	}

	public String getTags() {
		return mTags;
	}

	public void setTags(String tags) {
		mTags = tags;
	}

	public int getEducation() {
		return mEducation;
	}

	public void setEducation(int education) {
		mEducation = education;
	}

	public int getExperience() {
		return mExperience;
	}

	public void setExperience(int experience) {
		mExperience = experience;
	}

	public List<Portfolio> getPortfolios() {
		return mPortfolios;
	}

	public void setPortfolios(List<Portfolio> portfolios) {
		mPortfolios = portfolios;
	}

	public ArrayList<HashMap<String, Object>> toServerMap(){

		ArrayList<HashMap<String, Object>> portfolios = new ArrayList<>();
		for (Portfolio portfolio : mPortfolios) {
			HashMap<String, Object> portfolioMap = new HashMap<>();
			portfolioMap.put("id", portfolio.getId());
			portfolioMap.put("title", portfolio.getName());
			portfolioMap.put("url", portfolio.getLink());
			portfolioMap.put("description", portfolio.getDescription());
			portfolios.add(portfolioMap);
		}

		return portfolios;
	}

	@Override
	public int hashCode() {
		return getSkillSubGroup();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BastogramUserInfoSkill){
			return ((BastogramUserInfoSkill) obj).getSkillSubGroup() == mSkillSubgroup;
		}
		return super.equals(obj);
	}

	@Override
	public BastogramUserInfoSkill clone() {
		try {
			Object clone = super.clone();
			return (BastogramUserInfoSkill) clone;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public ContentValues toContentValues() {
		ContentValues contentValues = new ContentValues();

		contentValues.put(COLUMN_SKILL_SUBGROUP, mSkillSubgroup);
		contentValues.put(COLUMN_SKILL, mSkill);
		contentValues.put(COLUMN_TAGS, mTags);
		contentValues.put(COLUMN_EDUCATION, mEducation);
		contentValues.put(COLUMN_EXPERIENCE, mExperience);

		return contentValues;
	}
}
