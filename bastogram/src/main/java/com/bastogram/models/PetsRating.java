package com.bastogram.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by serezha on 11.08.16.
 */
public class PetsRating {

	@SerializedName("pets")
	List<Nursling> mNurslings;

	@SerializedName("my_percent")
	int mPercent;

	@SerializedName("rating")
	int mRating;

	public List<Nursling> getPets() {
		return mNurslings;
	}

	public int getPercent() {
		return mPercent;
	}

	public int getRating() {
		return mRating;
	}
}
