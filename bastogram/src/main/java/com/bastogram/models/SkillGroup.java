package com.bastogram.models;

import com.bastogram.BastogramApp;
import com.bastogram.R;
import com.humanet.humanetcore.model.SimpleListItem;

/**
 * Created by Uran on 10.06.2016.
 */
public class SkillGroup extends BaseSkillModel implements SimpleListItem {


	private SkillSubGroup[] mSkillSubGroups;

	public SkillSubGroup[] getSkillSubGroups() {
		return mSkillSubGroups;
	}

	public void setSkillSubGroups(SkillSubGroup[] skillSubGroups) {
		mSkillSubGroups = skillSubGroups;
	}

	@Override
	public String getImage() {
		return super.getImageLink();
	}

	@Override
	protected String[] getSkillIconNamesArray() {
		return BastogramApp.getInstance().getResources().getStringArray(R.array.skills_groups);
	}
}
