package com.bastogram.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.humanet.humanetcore.interfaces.CirclePickerItem;

import java.io.Serializable;
import java.util.Random;

/**
 * Created by Uran on 06.06.2016.
 */
@Deprecated
public class Skill implements CirclePickerItem, Serializable {

    public static final String TABLE_NAME = "skill";

    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_COMPETENCE = "competence";
    public static final String COLUMN_SPECIALIZATION = "specialization";
    public static final String COLUMN_EXPERIENCE = "experience";
    public static final String COLUMN_LEVEL = "level";
    public static final String COLUMN_SKILL_ITEM = "skill_type";


    private int mId = -1;
    private String mTitle;
    private int mCompetence;
    private String mSpecialization;
    private String mDrawable;
    private int mExperience;
    private int mLevel;
    private int mSkillItem;
    private int mSkillSubgroup;
    private int mSkillGroup;
    private SkillSubGroup[] mCompetencies;

    public SkillSubGroup[] getCompetencies() {
        return mCompetencies;
    }

    public int getCompetence() {
        return mCompetence;
    }

    public String getSpecialization() {
        return mSpecialization;
    }

    public int getExperience() {
        return mExperience;
    }

    public int getLevel() {
        return mLevel;
    }

    @Override
    public String getDrawable() {
        return mDrawable;
    }

    @Override
    public int getId() {
        return mId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getSkillItem() {
        return mSkillItem;
    }

    public void setCompetencies(SkillSubGroup[] competencies) {
        mCompetencies = competencies;
    }

    public void setCompetence(int competence) {
        mCompetence = competence;
    }

    public void setSpecialization(String specialization) {
        mSpecialization = specialization;
    }

    public void setExperience(int experience) {
        mExperience = experience;
    }

    public void setLevel(int level) {
        mLevel = level;
    }

    public void setSkillItem(int skillItem) {
        mSkillItem = skillItem;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setDrawable(String drawable) {
        mDrawable = drawable;
    }

    public int getSkillSubgroup() {
        return mSkillSubgroup;
    }

    public void setSkillSubgroup(int skillSubgroup) {
        mSkillSubgroup = skillSubgroup;
    }

    public int getSkillGroup() {
        return mSkillGroup;
    }

    public void setSkillGroup(int skillGroup) {
        mSkillGroup = skillGroup;
    }

    public static ContentValues toContentValues(Skill skill) {
        ContentValues contentValues = new ContentValues();
        if(skill.mId != -1)
            contentValues.put("_id", skill.mId);
        contentValues.put(COLUMN_TITLE, skill.mTitle);
        contentValues.put(COLUMN_COMPETENCE, skill.mCompetence);
        contentValues.put(COLUMN_SPECIALIZATION, skill.mSpecialization);
        contentValues.put(COLUMN_EXPERIENCE, skill.mExperience);
        contentValues.put(COLUMN_LEVEL, skill.mLevel);
        contentValues.put(COLUMN_SKILL_ITEM, skill.mSkillItem);
        return contentValues;
    }

    public static Skill fromCursor(Cursor cursor) {
        Skill skill = new Skill();
        skill.mId = cursor.getInt(cursor.getColumnIndex("_id"));
        skill.mTitle = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
        skill.mCompetence = cursor.getInt(cursor.getColumnIndex(COLUMN_COMPETENCE));
        skill.mSpecialization = cursor.getString(cursor.getColumnIndex(COLUMN_SPECIALIZATION));
        skill.mExperience = cursor.getInt(cursor.getColumnIndex(COLUMN_EXPERIENCE));
        skill.mLevel = cursor.getInt(cursor.getColumnIndex(COLUMN_LEVEL));
        skill.mSkillItem = cursor.getInt(cursor.getColumnIndex(COLUMN_SKILL_ITEM));
        return skill;
    }

    public static Skill demo(int id) {
        Skill skill = new Skill();
        skill.mId = id;
        skill.mTitle = "skill " + id;
        skill.mCompetence = 0;
        skill.mSpecialization = "Specialization";
        skill.mExperience = 6;
        skill.mLevel = new Random().nextInt(3);
        skill.mSkillItem = 42;

        return skill;
    }



}
