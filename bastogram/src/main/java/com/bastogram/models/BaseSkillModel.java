package com.bastogram.models;

import android.util.Log;

import com.bastogram.BastogramApp;
import com.bastogram.R;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.interfaces.IServerRequestParams;
import com.humanet.humanetcore.model.enums.Language;

import java.io.Serializable;
import java.util.Locale;

/**
 * Created by serezha on 02.08.16.
 */
abstract class BaseSkillModel implements IServerRequestParams, Serializable, CirclePickerItem {

		@SerializedName("id")
		private int mId;

		@SerializedName("ru")
		private String mRu;

		@SerializedName("en")
		private String mEn;

		@SerializedName("es")
		private String mEs;

		@Override
		public String getDrawable() {
			return getImageLink();
		}

		@Override
		public String getTitle() {
			Language lang = Language.getSystem();
			switch (lang) {
				case RUS:
					return mRu;
				case ESP:
					return mEn;
				case ENG:
					return mEn;
				default:
					return mEn;
			}
		}

	public void setTitleFromResources(String title) {
		mRu = title;
		mEn = title;
		mEs = title;
	}

	@Override
	public int getId() {
			return mId;
		}

	@Override
	public String getRequestParam() {
			return String.valueOf(mId);
		}

	protected String suffix = getClass().getSimpleName().toLowerCase(Locale.getDefault());

	protected String[] getSkillIconNamesArray() {
		return BastogramApp.getInstance().getResources().getStringArray(R.array.skills);
	}

	private String getSkillIconFilename(int id) {
		return getSkillIconNamesArray()[id - 1];
	}

	protected String getImageLink() {
		String link = "assets://" + getFolderName() + "/" + getSkillIconFilename(mId) + ".png";
		Log.d("Image link: ", link);
		return link;
	}

	protected String getFolderName() {
		return suffix;
	}



}
