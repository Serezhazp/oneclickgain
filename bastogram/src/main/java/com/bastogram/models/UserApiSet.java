package com.bastogram.models;

import com.bastogram.api.responses.BastogramUserSearchResponse;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

public interface UserApiSet{

    @POST(Constants.BASE_API_URL + "/user.get")
    BastogramUserInfo.UserResponse getBastogarmUserInfo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.search")
    BastogramUserSearchResponse searchUser(@Path("appName") String appName, @Body ArgsMap options);

}
