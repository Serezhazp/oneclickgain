package com.bastogram.models;

import com.bastogram.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.ILocationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serezha on 12.07.16.
 */
public class WorkingExperience implements ILocationModel {

	public static final long MINUTE = 60 * 1000;
	public static final long HOUR = 60 * MINUTE;
	public static final long DAY = 24 * HOUR;
	public static final long MONTH = 31 * DAY;
	public static final long YEAR = 12 * MONTH;

	private static WorkingExperience sInstance;

	private List<WorkingExperience> mWorkingExperienceList;

	private int mId;
	private String mTitle;
	private int mValue;

	public WorkingExperience(int id, String title, int value) {
		mId = id;
		mTitle = title;
		mValue = value;
	}

	private WorkingExperience() {

	}

	public static WorkingExperience getInstance() {
		if(sInstance == null)
			sInstance = new WorkingExperience();

		return sInstance;
	}

	@Override
	public int getId() {
		return mId;
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	public int getValue() {
		return mValue;
	}

	public void setId(int id) {
		mId = id;
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public void setValue(int value) {
		mValue = value;
	}

	private List<WorkingExperience> getWorkingExperienceList() {
		mWorkingExperienceList = new ArrayList<WorkingExperience>() {{
			add(new WorkingExperience(0, App.getInstance().getString(R.string.profile_experience), 0));
			add(new WorkingExperience(1, getExperienceYears()[0], 1));
			add(new WorkingExperience(2, getExperienceYears()[1], 2));
			add(new WorkingExperience(3, getExperienceYears()[2], 3));
			add(new WorkingExperience(4, getExperienceYears()[3], 4));
			add(new WorkingExperience(5, getExperienceYears()[4], 5));
			add(new WorkingExperience(6, getExperienceYears()[5], 6));
			add(new WorkingExperience(7, getExperienceYears()[6], 7));
			add(new WorkingExperience(8, getExperienceYears()[7], 8));
			add(new WorkingExperience(9, getExperienceYears()[8], 9));
			add(new WorkingExperience(10, getExperienceYears()[9], 10));
		}};

		return mWorkingExperienceList;
	}

	private static String[] getExperienceYears() {

		return App.getInstance().getResources().getStringArray(R.array.experiences);
	}

	private static long getCurrentTime() {
		return System.currentTimeMillis();
	}

	public static int getIdFromExperience(int experience) {
		/*long valueAfter;
		long valueBefore;
		for(int i = 1; i < VALUES.size() - 1; i++) {
			valueAfter = VALUES.get(i).getValue();
			valueBefore = VALUES.get(i + 1).getValue();
			if(experience <= valueAfter && experience >= valueBefore) {
				if(valueAfter - experience <= experience - valueBefore)
					return VALUES.get(i).getId();
				else
					return VALUES.get(i + 1).getId();
			}
		}*/

		return experience;
	}

	public List<WorkingExperience> values() {
		return getWorkingExperienceList();
	}
}
