package com.bastogram.models;

import com.humanet.humanetcore.model.ILocationModel;

/**
 * Created by serezha on 19.08.16.
 */
public class Breed implements ILocationModel {

	private int mId;
	private String mTitle;

	public Breed(int id, String title) {
		mId = id;
		mTitle = title;
	}

	@Override
	public int getId() {
		return mId;
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	public void setId(int id) {
		mId = id;
	}

	public void setTitle(String title) {
		mTitle = title;
	}
}
