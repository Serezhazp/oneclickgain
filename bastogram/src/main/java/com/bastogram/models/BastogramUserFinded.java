package com.bastogram.models;

import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.UserFinded;
import com.humanet.humanetcore.model.enums.selectable.Extreme;
import com.humanet.humanetcore.model.enums.selectable.Game;
import com.humanet.humanetcore.model.enums.selectable.Hobby;
import com.humanet.humanetcore.model.enums.selectable.HowILook;
import com.humanet.humanetcore.model.enums.selectable.Interest;
import com.humanet.humanetcore.model.enums.selectable.NonGame;
import com.humanet.humanetcore.model.enums.selectable.PartGames;
import com.humanet.humanetcore.model.enums.selectable.Pet;
import com.humanet.humanetcore.model.enums.selectable.Religion;
import com.humanet.humanetcore.model.enums.selectable.Sport;
import com.humanet.humanetcore.model.enums.selectable.Virtual;

import java.util.Arrays;
import java.util.List;

/**
 * Created by serezha on 11.08.16.
 */
public class BastogramUserFinded extends UserFinded {

	@SerializedName("skills")
	private List<BastogramUserInfoSkill> mSkills;

	@SerializedName("animals")
	private List<Nursling> mNurslings;

	public List<BastogramUserInfoSkill> getSkills() {
		return mSkills;
	}

	public List<Nursling> getNurslings() {
		return mNurslings;
	}

	public void setSkills(List<BastogramUserInfoSkill> skills) {
		mSkills = skills;
	}

	public void setNurslings(List<Nursling> nurslings) {
		mNurslings = nurslings;
	}

	public static BastogramUserFinded fromCursor(Cursor cursor) {
		BastogramUserFinded user = new BastogramUserFinded();
		user.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.ID)));
		user.setAvatar(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.AVATAR)));
		user.setCraft(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.CRAFT)));
		user.setFirstName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.FIRST_NAME)));
		user.setLastName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.LAST_NAME)));

		user.setCity(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.CITY)));
		user.mCityId = (cursor.getInt(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.CITY_ID)));

		user.setCountry(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.COUNTRY)));
		user.mCountryId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.COUNTRY_ID));

		user.setHobby(Hobby.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.HOBBIE))));
		user.setInterest(Interest.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.INTEREST))));
		user.setSport(Sport.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.SPORT))));
		user.setPets(Pet.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.PETS))));
		user.setReligion(Religion.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.RELIGION))));

		user.setGames(Game.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.GAME))));
		user.setNonGame(NonGame.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.NON_GAME))));
		user.setExtreme(Extreme.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.EXTREME))));
		user.setVirtual(Virtual.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.VIRTUAL))));
		user.setHowILook(HowILook.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK))));
		user.setPartGames(PartGames.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.PART_GAMES))));

		String tags = cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.TAGS));
		if (tags != null) {
			user.setTags(Arrays.asList(tags.split(",")));
		}
		String requiredFields = cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.REQUIRED));
		if (requiredFields != null) {
			user.setRequiredFields(requiredFields.split(","));
		}

		user.setLang(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.LANG)));
		user.setRating(cursor.getFloat(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.RATING)));


		return user;
	}

}
