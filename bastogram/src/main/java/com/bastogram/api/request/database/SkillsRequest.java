package com.bastogram.api.request.database;

import android.util.Log;

import com.bastogram.api.responses.SkillResponse;
import com.bastogram.api.sets.DatabaseApiSet;
import com.bastogram.models.Competence;
import com.bastogram.models.SkillGroup;
import com.bastogram.models.SkillSubGroup;
import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serezha on 29.07.16.
 */
public class SkillsRequest extends RetrofitSpiceRequest<SkillGroup[], DatabaseApiSet> {

	public SkillsRequest() {
		super(SkillGroup[].class, DatabaseApiSet.class);
	}

	@Override
	public SkillGroup[] loadDataFromNetwork() throws Exception {

		SkillResponse response = getService().getSkills(App.API_APP_NAME);

		List<SkillGroup> skillGroups =  response.getSkillsGroup();
		List<SkillSubGroup> skillSubGroups = response.getSkillsSubgroup();

		int countInGroup = response.getSkillsSubgroup().size() / response.getSkillsGroup().size();

		for(int i = 0; i < skillGroups.size(); i++){
			List<SkillSubGroup> localSkillSubGroups = skillSubGroups.subList(i * countInGroup, countInGroup * i + countInGroup);
			skillGroups.get(i).setSkillSubGroups(localSkillSubGroups.toArray(new SkillSubGroup[localSkillSubGroups.size()]));
		}

		int countInSubGroup = response.getSkills().size() / response.getSkillsSubgroup().size();

		for(int i = 0; i < skillSubGroups.size(); i ++) {
			Competence[] competences = new Competence[countInSubGroup];
			List<Competence> competenceList = new ArrayList<>(response.getSkills().subList(i * countInSubGroup, i * countInSubGroup + countInSubGroup));
			competenceList.toArray(competences);
			skillSubGroups.get(i).setCompetences(competences);
		}

		SkillGroup[] groups = new SkillGroup[skillGroups.size()];
		skillGroups.toArray(groups);

		Log.d("SkillsRequest Response", new Gson().toJson(response));

		return groups;
	}
}
