package com.bastogram.api.request.pet;

import android.util.Log;

import com.bastogram.api.responses.PetResponse;
import com.bastogram.api.sets.PetApiSet;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by serezha on 03.08.16.
 */
public class PetAddRequest extends RetrofitSpiceRequest<PetResponse, PetApiSet> {

	public PetAddRequest() {
		super(PetResponse.class, PetApiSet.class);
	}

	@Override
	public PetResponse loadDataFromNetwork() throws Exception {

		ArgsMap map = new ArgsMap(true);

		Log.i("API: PetAddRequest", map.toString());

		return getService().petAdd(App.API_APP_NAME, map);
	}
}
