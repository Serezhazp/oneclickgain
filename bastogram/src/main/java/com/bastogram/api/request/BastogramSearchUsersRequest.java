package com.bastogram.api.request;

import android.content.ContentValues;
import android.util.Log;

import com.bastogram.api.responses.BastogramUserSearchResponse;
import com.bastogram.models.BastogramUserFinded;
import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Nursling;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;
import java.util.Set;

/**
 * Created by serezha on 11.08.16.
 */
public class BastogramSearchUsersRequest extends RetrofitSpiceRequest<BastogramUserSearchResponse, com.bastogram.models.UserApiSet> {

	private ArgsMap mParams;

	public ArgsMap getParams() {
		return mParams;
	}

	public BastogramSearchUsersRequest(ArgsMap argsMap) {
		super(BastogramUserSearchResponse.class, com.bastogram.models.UserApiSet.class);
		mParams = argsMap;
	}

	@Override
	public BastogramUserSearchResponse loadDataFromNetwork() throws Exception {
		Log.i("API: BastSearchUser", mParams.toString());
		BastogramUserSearchResponse response = getService().searchUser(App.API_APP_NAME,mParams);

		Set<String> keys = mParams.keySet();
		String[] required = keys.toArray(new String[keys.size()]);
		for (int i = 0; i < required.length; i++) {
			required[i] = required[i] + "~" + mParams.get(required[i]);
		}

		if (response != null && response.isSuccess()) {
			List<BastogramUserFinded> list = response.getUsers();

			if (list != null) {

				ContentValues[] contentValues = new ContentValues[list.size()];
				for (int i = 0; i < list.size(); i++) {
					BastogramUserFinded user = list.get(i);
					ContentValues[] skillValues = new ContentValues[user.getSkills().size()];
					ContentValues[] nurslingValues = new ContentValues[user.getNurslings().size()];

					for(int j = 0; j < user.getSkills().size(); j++) {
						skillValues[j] = user.getSkills().get(j).toContentValues();
						skillValues[j].put(BastogramUserInfoSkill.COLUMN_USER_ID, user.getId());
						skillValues[j].put("_id", user.getId() * 1000 + user.getSkills().get(j).getSkill()); //Magic for id
					}

					for(int j = 0; j < user.getNurslings().size(); j++) {
						nurslingValues[j] = user.getNurslings().get(j).toContentValues();
						nurslingValues[j].put(Nursling.COLUMN_USER_ID, user.getId());
					}

					App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Skill.URI, skillValues);
					App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Nursling.URI, nurslingValues);

					user.setRequiredFields(required);

					contentValues[i] = user.toContentValues();
				}
				App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.UserSearchResult.URI, contentValues);
			}

		}
		return response;
	}
}
