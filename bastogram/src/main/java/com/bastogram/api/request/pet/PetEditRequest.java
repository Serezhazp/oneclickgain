package com.bastogram.api.request.pet;

import android.util.Log;

import com.bastogram.api.sets.PetApiSet;
import com.bastogram.models.Nursling;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by serezha on 08.08.16.
 */
public class PetEditRequest extends RetrofitSpiceRequest<BaseResponse, PetApiSet> {

	private Nursling mNursling;

	public PetEditRequest(Nursling nursling) {
		super(BaseResponse.class, PetApiSet.class);

		mNursling = nursling;
	}

	@Override
	public BaseResponse loadDataFromNetwork() throws Exception {

		ArgsMap petMap = new ArgsMap(true);
		if(mNursling.getId() != -1)
			petMap.put("id_pet", mNursling.getId());
		if(mNursling.getUserId() != -1)
			petMap.put("id_user", mNursling.getUserId());
		if(mNursling.getAvatar() != null)
			petMap.put("avatar", mNursling.getAvatar());
		if(mNursling.getAvatar() != null)
			petMap.put("animal", mNursling.getAnimal());
		if(mNursling.getName() != null)
			petMap.put("name", mNursling.getName());

		petMap.put("birth_date", mNursling.getBirthDate());

		petMap.put("rating", mNursling.getRating());

		if(mNursling.getKind() != -1)
			petMap.put("type", mNursling.getKind());
		if(mNursling.getBages() != null)
			petMap.put("bages", mNursling.getBages());

		petMap.put("gender", mNursling.getGender());
		petMap.put("vaccine", mNursling.getVaccine());
		petMap.put("reproduction", mNursling.getReproduction());

		Log.i("API: PetEditRequest", petMap.toString());

		BaseResponse response = getService().petEdit(App.API_APP_NAME, petMap);

		EventBus.getDefault().post(mNursling);

		return response;
	}
}
