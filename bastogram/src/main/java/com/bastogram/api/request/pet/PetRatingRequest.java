package com.bastogram.api.request.pet;

import android.util.Log;

import com.bastogram.api.sets.PetApiSet;
import com.bastogram.models.PetsRating;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by serezha on 11.08.16.
 */
public class PetRatingRequest extends RetrofitSpiceRequest<PetsRating, PetApiSet> {

	private String mRatingType;

	public PetRatingRequest(String ratingType) {
		super(PetsRating.class, PetApiSet.class);

		mRatingType = ratingType;
	}

	@Override
	public PetsRating loadDataFromNetwork() throws Exception {

		ArgsMap map = new ArgsMap(true);
		map.put("ratingType", mRatingType);

		Log.i("PetRatingRequest", map.toString());

		return getService().petRating(App.API_APP_NAME, map);
	}
}
