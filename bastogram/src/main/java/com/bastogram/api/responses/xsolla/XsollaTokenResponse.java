package com.bastogram.api.responses.xsolla;

import com.google.gson.annotations.SerializedName;

/**
 * Created by serezha on 29.08.16.
 */
public class XsollaTokenResponse {

	@SerializedName("token")
	String mToken;

	public String getToken() {
		return mToken;
	}
}
