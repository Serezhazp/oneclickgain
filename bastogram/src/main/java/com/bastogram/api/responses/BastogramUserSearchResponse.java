package com.bastogram.api.responses;

import com.bastogram.models.BastogramUserFinded;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

import java.util.List;

/**
 * Created by serezha on 11.08.16.
 */
public class BastogramUserSearchResponse extends BaseResponse {

	@SerializedName("users")
	List<BastogramUserFinded> mUsers;

	public List<BastogramUserFinded> getUsers() {
		return mUsers;
	}
}
