package com.bastogram.api.sets;

import com.bastogram.api.responses.PetResponse;
import com.bastogram.models.PetsRating;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by serezha on 03.08.16.
 */
public interface PetApiSet {

	@POST(Constants.BASE_API_URL + "/pet.add")
	PetResponse petAdd(@Path("appName") String appName, @Body ArgsMap options);

	@POST(Constants.BASE_API_URL + "/pet.edit")
	BaseResponse petEdit(@Path("appName") String appName, @Body ArgsMap options);

	@POST(Constants.BASE_API_URL + "/pet.delete")
	BaseResponse petDelete(@Path("appName") String appName, @Body ArgsMap options);

	@POST(Constants.BASE_API_URL + "/pet.rating")
	PetsRating petRating(@Path("appName") String appName, @Body ArgsMap options);

}
