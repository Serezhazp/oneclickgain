package com.bastogram.api.sets;

import com.bastogram.api.responses.xsolla.XsollaTokenResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by serezha on 29.08.16.
 */
public interface XsollaApiSet {

	@GET("/xsolla/")
	Call<XsollaTokenResponse> getToken(@Query("auth_token") String token);

}