package com.bastogram;

import android.support.v4.app.Fragment;

import com.bastogram.activities.AgreementActivity;
import com.bastogram.activities.CaptureActivity;
import com.bastogram.activities.CommentsActivity;
import com.bastogram.activities.ContactsActivity;
import com.bastogram.activities.EditProfileActivity;
import com.bastogram.activities.InputActivity;
import com.bastogram.activities.PlayFlowActivity;
import com.bastogram.activities.PushActivity;
import com.bastogram.activities.SearchActivity;
import com.bastogram.activities.TokenActivity;
import com.bastogram.fragments.BastogramVideoDescriptionFragment;
import com.bastogram.fragments.profile.BastogramProfileFragment;
import com.bastogram.service.AppService;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.octo.android.robospice.SpiceService;

/**
 * Created by ovi on 24.05.2016.
 */
public class NavigationManagerImpl implements NavigationManager.Delegate {
    @Override
    public Class getPlayFlowActivityClass() {
        return PlayFlowActivity.class;
    }

    @Override
    public Class getCommentsActivityClass() {
        return CommentsActivity.class;
    }

    @Override
    public Class getAgreementActivityClass() {
        return AgreementActivity.class;
    }

    @Override
    public Class getSearchActivityClass() {
        return SearchActivity.class;
    }

    @Override
    public Class getInputActivityClass() {
        return InputActivity.class;
    }

    @Override
    public Class getTokenActivityClass() {
        return TokenActivity.class;
    }

    @Override
    public Class getContactsActivityClass() {
        return ContactsActivity.class;
    }

    @Override
    public Class getEditProfileActivityClass() {
        return EditProfileActivity.class;
    }

    @Override
    public Class getCaptureActivityClass() {
        return CaptureActivity.class;
    }

    @Override
    public Class getPushActivityClass() {
        return PushActivity.class;
    }

    @Override
    public Class<? extends SpiceService> getSpiceServiceClass() {
        return AppService.class;
    }

    @Override
    public Class<? extends BaseViewProfileFragment> getViewProfileFragmentClass() {
        return BastogramProfileFragment.class;
    }

    @Override
    public Class<? extends Fragment> getVideoDescriptionFragmentClass() {
        return BastogramVideoDescriptionFragment.class;
    }
}
