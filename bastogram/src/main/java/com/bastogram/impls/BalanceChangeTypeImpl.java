package com.bastogram.impls;

import com.bastogram.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.enums.BalanceChangeType;
import com.humanet.humanetcore.utils.BalanceHelper;

/**
 * Created by ovi on 2/5/16.
 */
public class BalanceChangeTypeImpl implements BalanceHelper.BalanceTypeArray {

    enum Type implements BalanceChangeType {

        REGISTRATION {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_registration);
            }

            @Override
            public int getId() {
                return 500;
            }
        },
        ADD_COMPETENCE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_add_competence);
            }

            @Override
            public int getId() {
                return 501;
            }
        },
        ADD_PET {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_add_pet);
            }

            @Override
            public int getId() {
                return 502;
            }
        },
        FILL_EXTENDED_PROFILE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_fill_profile);
            }

            @Override
            public int getId() {
                return 503;
            }
        },
        INVITE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_invite_friend);
            }

            @Override
            public int getId() {
                return 504;
            }
        },
        ADD_VIDEO {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_add_video);
            }

            @Override
            public int getId() {
                return 505;
            }
        },
        VIDEO_SHARE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_share_video);
            }

            @Override
            public int getId() {
                return 506;
            }
        },
        CREATE_VLOG {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_create_vlog);
            }

            @Override
            public int getId() {
                return 507;
            }
        },
        FIRST_100 {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_first_100);
            }

            @Override
            public int getId() {
                return 508;
            }
        },
        LEFT_LIKE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_left_like);
            }

            @Override
            public int getId() {
                return 509;
            }
        },
        RECEIVE_LIKE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_receive_like);
            }

            @Override
            public int getId() {
                return 510;
            }
        },
        AVATAR_CREATE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_avatar_creation);
            }

            @Override
            public int getId() {
                return 511;
            }
        },
        AVATAR_SHARE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_avatar_share);
            }

            @Override
            public int getId() {
                return 512;
            }
        },
        SUBSCRIBE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_subscribe);
            }

            @Override
            public int getId() {
                return 519;
            }
        },
        NEW_SUBSCRIBER {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_new_subscriber);
            }

            @Override
            public int getId() {
                return 520;
            }
        },
        VOTE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_vote);
            }

            @Override
            public int getId() {
                return 521;
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_menu_vote;
            }
        },
        CREATE_POLL {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_create_poll);
            }

            @Override
            public int getId() {
                return 522;
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_menu_vote;
            }
        },
        CROWD_RECEIVE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_crowd_receive);
            }

            @Override
            public int getId() {
                return 524;
            }
        },
        CROWD_GIVE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_crowd_give);
            }

            @Override
            public int getId() {
                return 525;
            }
        };

        public String getDrawable() {
            return null;
        }

    }


    public BalanceChangeType getById(int id) {
        for (Type type : Type.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }


}
