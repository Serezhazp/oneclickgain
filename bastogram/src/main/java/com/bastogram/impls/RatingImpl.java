package com.bastogram.impls;

import android.content.Context;

import com.bastogram.R;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;

/**
 * Created by ovi on 3/31/16.
 */
public class RatingImpl implements RatingsTabFragment.RatingTabView {

    public static final String LOCAL = "local";
    public static final String OWNER = "owner";
    public static final String PET = "pet";

    private Context mContext;

    public RatingImpl(Context context) {
        mContext = context;
    }

    @Override
    public String[] getTabNames() {
        return new String[]{
                mContext.getString(R.string.rating_type_owner),
                mContext.getString(R.string.rating_type_pet),
                mContext.getString(R.string.rating_type_global),
        };
    }

    @Override
    public int getDefaultTabPosition() {
        return 0;
    }

    @Override
    public String getRatingType(int tabPosition) {
        switch (tabPosition) {
            case 0:
                return LOCAL;
            case 1:
                return PET;
            case 2:
                //global
        }
        return null;
    }
}
