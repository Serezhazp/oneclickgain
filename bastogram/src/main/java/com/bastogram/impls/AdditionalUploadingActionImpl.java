package com.bastogram.impls;

import android.content.ContentValues;

import com.bastogram.models.Nursling;
import com.humanet.humanetcore.api.requests.video.VideoUploadRequest;
import com.humanet.humanetcore.db.DBHelper;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.VideoInfo;

/**
 * Created by Uran on 17.06.2016.
 */
public class AdditionalUploadingActionImpl implements VideoUploadRequest.AdditionalUploadingActionDelegate {

    @Override
    public void additionalAction(VideoInfo videoInfo, UploadingMedia uploadingMedia) {
        if (videoInfo.isAvatar() == VideoInfo.PET_AVATAR) {
            // id was converted from int to double after id saved and fetched.
            int id = ((Double) videoInfo.getAdditionalParams()).intValue();

            ContentValues contentValues = new ContentValues(1);
            contentValues.put(Nursling.COLUMN_AVATAR, uploadingMedia.getThumb());

            DBHelper.getInstance().getWritableDatabase().update(
                    Nursling.TABLE_NAME,
                    contentValues,
                    "_id=?",
                    new String[]{
                            String.valueOf(id)
                    }
            );
        }
    }


}
