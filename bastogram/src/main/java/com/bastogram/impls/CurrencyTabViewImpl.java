package com.bastogram.impls;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.bastogram.R;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.InternalCurrencyFragment;
import com.humanet.humanetcore.interfaces.OnBalanceChanged;

/**
 * Created by ovi on 4/1/16.
 */
public final class CurrencyTabViewImpl implements CurrencyFragment.CurrencyTabView {

    private Context mContext;

    private OnBalanceChanged mOnBalanceChanged;


    public CurrencyTabViewImpl(Context context) {
        mContext = context;
    }

    public void setOnBalanceChangedListener(OnBalanceChanged onBalanceChanged) {
        mOnBalanceChanged = onBalanceChanged;
    }

    @Override
    public String[] getTabNames() {
        return new String[]{
                mContext.getString(R.string.balance_category_type_balance),
                mContext.getString(R.string.balance_category_type_currency),
                //mContext.getString(R.string.balance_category_type_add_funds), //TODO: Temporary hidden
        };
    }

    @Override
    public int getDefaultTabPosition() {
        return 0;
    }

    @Override
    public int getAddBalanceTabPosition() {
        return 1;
    }

    @Override
    public Fragment getFragmentAtPosition(int position) {
        switch (position) {
            case 0:
                InternalCurrencyFragment internalCurrencyFragment = new InternalCurrencyFragment();
                Bundle internalCurrencyBundle = new Bundle(2);
                internalCurrencyBundle.putInt("position", position);
                internalCurrencyBundle.putString("type", "local");
                internalCurrencyFragment.setArguments(internalCurrencyBundle);

                return internalCurrencyFragment;
            case 1:
                com.bastogram.fragments.MoneyCurrencyFragment moneyCurrencyFragment = new com.bastogram.fragments.MoneyCurrencyFragment();
                //MoneyCurrencyFragment moneyCurrencyFragment = new MoneyCurrencyFragment();
                Bundle moneyCurrencyBundle = new Bundle(2);
                moneyCurrencyBundle.putInt("position", position);
                moneyCurrencyBundle.putString("type", "money");
                moneyCurrencyFragment.setArguments(moneyCurrencyBundle);

                return moneyCurrencyFragment;
            /*case 2:
                BastogramExternalPaymentsFragment paymentsFragment = new BastogramExternalPaymentsFragment();
                paymentsFragment.setOnBalanceChanged(mOnBalanceChanged);

                return paymentsFragment;*/ //TODO: Temporary hidden
            default:
                throw new RuntimeException("undefined balance tab position");
        }
    }
}
