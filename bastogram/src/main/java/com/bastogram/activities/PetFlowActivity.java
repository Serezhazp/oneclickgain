package com.bastogram.activities;

import android.view.View;

import com.bastogram.models.Nursling;
import com.bastogram.views.IPetView;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.views.IVideoCacheView;
import com.humanet.humanetcore.views.IVideoListView;

import java.util.List;

/**
 * Created by serezha on 26.07.16.
 */
public class PetFlowActivity extends BaseActivity implements View.OnClickListener, IVideoListView, IVideoCacheView, IPetView {

	@Override
	public void publishCachingProgress(float progress) {

	}

	@Override
	public void publishCachingComplete(boolean success) {

	}

	@Override
	public void addVideos(List<Video> videoList) {

	}

	@Override
	public long getScreenId() {
		return 0;
	}

	@Override
	public long getLastVideoId() {
		return 0;
	}

	@Override
	public void onClick(View view) {

	}

	@Override
	public void showPet(Nursling nursling) {

	}
}
