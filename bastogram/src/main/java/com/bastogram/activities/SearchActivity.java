package com.bastogram.activities;

import android.os.Bundle;
import android.view.View;

import com.bastogram.fragments.BastogramSearchFragment;
import com.bastogram.fragments.BastogramSearchResultFragment;
import com.humanet.humanetcore.activities.base.BaseVideoManageActivity;
import com.humanet.humanetcore.utils.ToolbarHelper;

/**
 * Created by ovi on 24.05.2016.
 */
public class SearchActivity extends BaseVideoManageActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.humanet.humanetcore.R.layout.activity_base_fragment_container);
		toolbar = ToolbarHelper.createToolbar(this);

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		startFragment(new BastogramSearchFragment(), false);
	}

	public void openSearchResult() {
		startFragment(BastogramSearchResultFragment.newInstance(), true);
	}
}
