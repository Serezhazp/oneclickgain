package com.bastogram.activities;

import android.content.Intent;

import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovi on 24.05.2016.
 */
public class CaptureActivity extends com.humanet.humanetcore.activities.CaptureActivity {

    @Override
    protected void saveVideoAndBack() {
        if (getIntent() != null && getIntent().getIntExtra("update_avatar", VideoInfo.NONE) != VideoInfo.PET_AVATAR) {
            super.saveVideoAndBack();
        } else {
            VideoInfo videoInfo = NewVideoInfo.get().clone();
            videoInfo.setVideoType(VideoType.PET_ALL);
            //videoInfo.setAvatarType(VideoInfo.PET_AVATAR);
            Intent intent = new Intent();
            intent.putExtra("video_info", videoInfo);
            setResult(RESULT_OK, intent);
            finish();
        }

    }
}
