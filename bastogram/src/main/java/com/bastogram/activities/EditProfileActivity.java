package com.bastogram.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.bastogram.fragments.profile.EditProfileFragment;

/**
 * Created by ovi on 24.05.2016.
 */
public class EditProfileActivity extends com.humanet.humanetcore.activities.EditProfileActivity {

    private int mTabToOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(EditProfileFragment.TAB_TO_OPEN))
            mTabToOpen = getIntent().getExtras().getInt(EditProfileFragment.TAB_TO_OPEN);

        super.onCreate(savedInstanceState);

        toolbar.setNavigationOnClickListener(v -> {
            int backStack = getSupportFragmentManager().getBackStackEntryCount();
            if (backStack == 0)
                onBackPressed();
            else
                getSupportFragmentManager().popBackStack();
        });
    }

    @Override
    protected void launchStartFragment() {
        Fragment editProfileFragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putInt(EditProfileFragment.TAB_TO_OPEN, mTabToOpen);
        editProfileFragment.setArguments(args);
        startFragment(editProfileFragment, false);
    }
}
