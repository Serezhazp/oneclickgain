package com.bastogram.activities;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.bastogram.R;
import com.bastogram.api.responses.xsolla.XsollaTokenResponse;
import com.bastogram.api.sets.XsollaApiSet;
import com.bastogram.fragments.BastogramCrowdFragment;
import com.bastogram.fragments.BastogramRaitingsTabsFragment;
import com.bastogram.fragments.MenuFragment;
import com.bastogram.fragments.profile.PetsFragment;
import com.bastogram.payment.PaymentType;
import com.bastogram.views.IPurchaseView;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.activities.BaseMainActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.CoinsRequest;
import com.humanet.humanetcore.api.requests.user.PaylogRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.fragments.AlienLookFragment;
import com.humanet.humanetcore.fragments.VistoryFragment;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.info.FeedbackFragment;
import com.humanet.humanetcore.fragments.info.InfoFragment;
import com.humanet.humanetcore.fragments.info.NavigationInfoFragment;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;
import com.humanet.humanetcore.model.Purchase;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.views.dialogs.ProgressDialog;
import com.xsolla.android.sdk.XsollaObject;
import com.xsolla.android.sdk.XsollaSDK;
import com.xsolla.android.sdk.view.XsollaActivity;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseMainActivity {

    // Keys for the responses from InAppBillingService
    public static final String RESPONSE_CODE = "RESPONSE_CODE";

    // Activity code
    public static final int BUY_REQUEST_CODE = 1001;
    public static final int BUY_PAYSERA_REQUEST_CODE = 1002;


    // Billing response codes
    public static final int BILLING_RESPONSE_RESULT_OK = 0;

    private IPurchaseView mPurchaseView;

    private IInAppBillingService mService;

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    protected void launchFirstFragment() {
        startFragment(new PetsFragment(), false, true);
    }

    @Override
    protected MenuFragment newMenuFragmentInstance() {
        return new MenuFragment();
    }

    @Override
    public void onNavigateByViewId(int viewId) {
        switch (viewId) {

            case R.id.alien_look:
                startFragment(new AlienLookFragment(), false, true);
                break;

            case R.id.balance:
                startFragment(com.bastogram.fragments.NavigationBalanceFragment.newInstance(), false, true);
                break;

            case R.id.currency:
                startFragment(new CurrencyFragment(), true, true);
                break;

            case R.id.crowd:
                startFragment(new BastogramCrowdFragment(), true);
                break;

            case R.id.rating:
                startFragment(new BastogramRaitingsTabsFragment(), true, true);
                break;


            case R.id.navigation_info:
                startFragment(new NavigationInfoFragment(), false, true);
                break;

            case R.id.info:
                startFragment(new InfoFragment(), true, true);
                break;

            case R.id.feedback:
                startFragment(new FeedbackFragment(), true, true);
                break;


            case R.id.vote:
                startFragment(new VoteTabFragment(), true, true);
                break;

            case R.id.vistory:
                startFragment(new VistoryFragment(), false);
                break;

            case R.id.nurslings:
                startFragment(new PetsFragment(), false);
                break;


            default:
                Toast.makeText(this, "UNDER CONSTRUCTION", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BUY_REQUEST_CODE && data.getIntExtra("RESPONSE_CODE", 0) == BILLING_RESPONSE_RESULT_OK) {
            consumePurchase(data.getExtras());
        } else if (requestCode == BUY_PAYSERA_REQUEST_CODE) {
            if (mPurchaseView != null) {
                mPurchaseView.buyingDone(null, null, null);
            }
        } else if (requestCode == XsollaActivity.REQUEST_CODE) {
            if (data != null) {
                long objectId = data.getExtras().getLong(XsollaActivity.EXTRA_OBJECT_ID);
                String text = XsollaObject.getRegisteredObject(objectId).toString();

                sendXsollaPaymentStatus(objectId, text);

                if (mPurchaseView != null)
                    mPurchaseView.buyingDone(null, null, null);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };

    public void buy(PaymentType paymentType, Purchase purchase, IPurchaseView purchaseView) {
        mPurchaseView = purchaseView;
        switch (paymentType) {
            case GOOGLE:
                try {
                    Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(), purchase.getCode(), "inapp", null);
                    int responseCode = getResponseCodeFromBundle(buyIntentBundle);
                    if (responseCode == BILLING_RESPONSE_RESULT_OK) {
                        PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                        if (pendingIntent != null)
                            startIntentSenderForResult(pendingIntent.getIntentSender(), BUY_REQUEST_CODE, new Intent(), 0, 0, 0);
                    }
                    if (responseCode == 7) {
                        Bundle bundle = mService.getPurchases(3, getPackageName(), "inapp", null);
                        List<String> purchaseDatas = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                        List<String> purchaseSignatures = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                        if (purchaseDatas != null && purchaseSignatures != null) {
                            for (int i = 0; i < purchaseDatas.size(); i++) {
                                consumePurchase(purchaseDatas.get(i), purchaseSignatures.get(i));
                            }
                        }
                    }
                } catch (Exception ex) {
                    Toast.makeText(this, "Some error happened while purchasing", Toast.LENGTH_SHORT).show();
                    Log.e("BaseBillingActivity", "buy Exception: ", ex);

                }
                break;

            case PAYSERA:
                startActivityForResult(PaySeraPurchaseActivity.newIntent(this, purchase.getPrice(), purchase.getCode()), BUY_PAYSERA_REQUEST_CODE);
                break;

            case XSOLLA:
                makeXsollaPayment();
                break;
        }
    }

    // Workaround to bug where sometimes response codes come as Long instead of Integer
    int getResponseCodeFromBundle(Bundle b) {
        Object o = b.get(RESPONSE_CODE);
        if (o == null) {
            return BILLING_RESPONSE_RESULT_OK;
        } else if (o instanceof Integer) return ((Integer) o).intValue();
        else if (o instanceof Long) return (int) ((Long) o).longValue();
        else {
            Log.e("BaseBillingActivity", "Unexpected type for bundle response code.");
            Log.e("BaseBillingActivity", o.getClass().getName());
            throw new RuntimeException("Unexpected type for bundle response code: " + o.getClass().getName());
        }
    }

    private void consumePurchase(Bundle bundle) {
        String purchaseData = bundle.getString("INAPP_PURCHASE_DATA");
        String signature = bundle.getString("INAPP_DATA_SIGNATURE");
        consumePurchase(purchaseData, signature);
    }

    private void consumePurchase(String purchaseData, String signature) {
        try {
            JSONObject jo = new JSONObject(purchaseData);
            String sku = jo.getString("productId");
            String purchaseToken = jo.getString("purchaseToken");
            mService.consumePurchase(3, getPackageName(), purchaseToken);

            if (mPurchaseView != null) {
                mPurchaseView.buyingDone(sku, jo, signature);
            } else {
                Purchase purchase = Purchase.findByCode(sku);
                CoinsRequest coinsRequest = new CoinsRequest.Builder("game", 1).addFunds(purchase.getAmount(), jo.toString(), signature).build();
                getSpiceManager().execute(coinsRequest, new SimpleRequestListener<CoinsHistoryListResponse>() {
                    @Override
                    public void onRequestSuccess(CoinsHistoryListResponse coinsHistoryListResponse) {
                        GameBalanceValue.getInstance().setBalance(coinsHistoryListResponse.getBalance());
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeXsollaPayment() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Retrofit retrofit = new Retrofit.Builder().baseUrl("https://1clickgain.com/").addConverterFactory(GsonConverterFactory.create()).build();
                XsollaApiSet apiSet = retrofit.create(XsollaApiSet.class);

                try {
                    Response<XsollaTokenResponse> response = apiSet.getToken(PrefHelper.getToken()).execute();

                    final XsollaTokenResponse result = response.body();

                    if (!isFinishing()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                XsollaSDK.createPaymentForm(MainActivity.this, result.getToken(), false);
                            }
                        });
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();


    }

    private void sendXsollaPaymentStatus(long id, String text) {
        getSpiceManager().execute(new PaylogRequest(id, text), new SimpleRequestListener<BaseResponse>());
    }

}
