package com.bastogram.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.humanet.humanetcore.utils.PrefHelper;

/**
 * Created by serezha on 29.08.16.
 */
public class PaySeraPurchaseActivity extends Activity {

	public static Intent newIntent(Context context, int amount, String code) {
		Intent intent = new Intent(context, PaySeraPurchaseActivity.class);
		intent.putExtra("amount", amount);
		intent.putExtra("code", code);
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WebView webView = new WebView(this);

		webView.setWebViewClient(new WebClient());
		webView.getSettings().setJavaScriptEnabled(true);

		setContentView(webView);

		int amount = getIntent().getIntExtra("amount", 0);

		webView.loadUrl("https://1clickgain.com/paysera/?amount=" + amount + "&auth_token=" + PrefHelper.getToken());
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
	}

	private class WebClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url != null && url.contains("thehumanet://clickandgain")) {
				Uri uri = Uri.parse(url);
				String statusString = uri.getQueryParameter("status");

				JsonObject jsonStatus = new JsonParser().parse(statusString).getAsJsonObject();
				String statusValue = jsonStatus.get("status").getAsString();

				if (statusValue.equals("ok")) {
					Intent intent = new Intent();
					intent.putExtra("code", getIntent().getStringExtra("code"));
					setResult(RESULT_OK, intent);
				} else {
					String error = jsonStatus.get("err").getAsString();
					Toast.makeText(PaySeraPurchaseActivity.this, error, Toast.LENGTH_SHORT).show();
				}
				finish();
			}
			return false;
		}


	}

	private class WebChromeClient extends android.webkit.WebChromeClient {

	}

}
