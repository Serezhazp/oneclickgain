package com.bastogram.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bastogram.models.BastogramUserInfoSkill;
import com.bastogram.models.Nursling;
import com.bastogram.models.Portfolio;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.db.DBHelper;

import java.util.ArrayList;

/**
 * Created by Uran on 14.06.2016.
 */
public class DBImpl implements DBHelper.Delegate {

    @Override
    public void createTables(SQLiteDatabase db) {
        String createTableQuery;

        createTableQuery = "CREATE TABLE " + Nursling.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Nursling.COLUMN_USER_ID + " INTEGER,"
                + Nursling.COLUMN_NAME + " TEXT,"
                + Nursling.COLUMN_AVATAR + " TEXT,"
                + Nursling.COLUMN_RATING + " INTEGER,"
                + Nursling.COLUMN_TYPE + " TEXT, "
                + Nursling.COLUMN_BIRTH_DATE + " INTEGER, "
                + Nursling.COLUMN_GENDER + " INTEGER, "
                + Nursling.COLUMN_VACCINATION + " INTEGER, "
                + Nursling.COLUMN_CHILD_READY + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + Portfolio.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Portfolio.COLUMN_SKILL_ID + " INTEGER, "
                + Portfolio.COLUMN_NAME + " TEXT,"
                + Portfolio.COLUMN_LINK + " TEXT, "
                + Portfolio.COLUMN_DESCRIPTION + " TEXT "
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + BastogramUserInfoSkill.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + BastogramUserInfoSkill.COLUMN_USER_ID + " INTEGER,"
                + BastogramUserInfoSkill.COLUMN_SKILL_SUBGROUP + " INTEGER,"
                + BastogramUserInfoSkill.COLUMN_SKILL + " INTEGER,"
                + BastogramUserInfoSkill.COLUMN_EXPERIENCE + " INTEGER,"
                + BastogramUserInfoSkill.COLUMN_EDUCATION + " INTEGER,"
                + BastogramUserInfoSkill.COLUMN_TAGS + " TEXT "
                + ");";
        db.execSQL(createTableQuery);

    }

    @Override
    public void dropTable(SQLiteDatabase db) {
        DBHelper.dropTable(db, Nursling.TABLE_NAME);
        DBHelper.dropTable(db, Portfolio.TABLE_NAME);
        DBHelper.dropTable(db, BastogramUserInfoSkill.TABLE_NAME);
    }

    public static synchronized ArrayList<BastogramUserInfoSkill> getSkillsByUserId(int userId) {
        ArrayList<BastogramUserInfoSkill> skills = new ArrayList<>();

        String skillsQuery = "SELECT * FROM " + BastogramUserInfoSkill.TABLE_NAME +
                " WHERE " + BastogramUserInfoSkill.COLUMN_USER_ID + " = " + userId;

        Cursor cursor = DBHelper.getInstance().getWritableDatabase().rawQuery(skillsQuery, null);

        if(cursor.moveToFirst()) {
            do {
                BastogramUserInfoSkill skill = new BastogramUserInfoSkill();
                skill.setSkillSubgroup(cursor.getInt(cursor.getColumnIndex(BastogramUserInfoSkill.COLUMN_SKILL_SUBGROUP)));
                skill.setSkill(cursor.getInt(cursor.getColumnIndex(BastogramUserInfoSkill.COLUMN_SKILL)));
                skill.setTags(cursor.getString(cursor.getColumnIndex(BastogramUserInfoSkill.COLUMN_TAGS)));
                skill.setExperience(cursor.getInt(cursor.getColumnIndex(BastogramUserInfoSkill.COLUMN_EXPERIENCE)));
                skill.setEducation(cursor.getInt(cursor.getColumnIndex(BastogramUserInfoSkill.COLUMN_EDUCATION)));

                skills.add(skill);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return skills;
    }

    public static synchronized ArrayList<Nursling> getNurslingsByUserId(int userId) {
        ArrayList<Nursling> nurslings = new ArrayList<>();

        String nurslingsQuery = "SELECT * FROM " + Nursling.TABLE_NAME +
                " WHERE " + Nursling.COLUMN_USER_ID + " = " + userId;

        Cursor cursor = DBHelper.getInstance().getWritableDatabase().rawQuery(nurslingsQuery, null);

        if(cursor.moveToFirst()) {
            do {
                Nursling nursling = new Nursling();
                nursling.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                nursling.setName(cursor.getString(cursor.getColumnIndex(Nursling.COLUMN_NAME)));
                nursling.setAvatar(cursor.getString(cursor.getColumnIndex(Nursling.COLUMN_AVATAR)));
                nursling.setRating(cursor.getInt(cursor.getColumnIndex(Nursling.COLUMN_RATING)));
                nursling.setKind(cursor.getInt(cursor.getColumnIndex(Nursling.COLUMN_TYPE)));
                nursling.setGender(cursor.getInt(cursor.getColumnIndex(Nursling.COLUMN_GENDER)));
                nursling.setVaccination(cursor.getInt(cursor.getColumnIndex(Nursling.COLUMN_VACCINATION)) == 1);
                nursling.setChildReady(cursor.getInt(cursor.getColumnIndex(Nursling.COLUMN_CHILD_READY)) == 1);
                nursling.setBirthDate(cursor.getLong(cursor.getColumnIndex(Nursling.COLUMN_BIRTH_DATE)));

                nurslings.add(nursling);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return nurslings;
    }

    public static synchronized void removeNursling(int id) {
        String where = "_id=?";
        String[] whereArgs = new String[]{String.valueOf(id)};

        App.getInstance().getContentResolver().delete(ContentDescriptor.Nursling.URI, where, whereArgs);
    }
}
