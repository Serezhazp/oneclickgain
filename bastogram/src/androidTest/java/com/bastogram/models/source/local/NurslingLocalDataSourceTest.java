package com.bastogram.models.source.local;

import com.bastogram.models.Nursling;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Uran on 14.06.2016.
 */
public class NurslingLocalDataSourceTest {

    NurslingLocalDataSource nurslingLocalDataSource = new NurslingLocalDataSource();


    @Test
    public void testGetList() throws Exception {
        List<Nursling> nurslings = nurslingLocalDataSource.getList();
        Assert.assertNotNull(nurslings);

    }

    @Test
    public void testSave() throws Exception {
        final String name = "test name";
        Nursling nursling = new Nursling();
        nursling.setName(name);

        Nursling saved = nurslingLocalDataSource.save(nursling);
        Assert.assertNotNull(saved);
        Assert.assertEquals(saved.getName(), name);

        nursling = nurslingLocalDataSource.getById(saved.getId());

        Assert.assertEquals(nursling.getId(), saved.getId());
        Assert.assertEquals(nursling.getName(), saved.getName());

        nurslingLocalDataSource.delete(saved.getId());
        Nursling deleted = nurslingLocalDataSource.getById(1);
        Assert.assertNull(deleted);
    }


}