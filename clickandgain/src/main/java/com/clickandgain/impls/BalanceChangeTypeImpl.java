package com.clickandgain.impls;

import com.clickandgain.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.enums.BalanceChangeType;
import com.humanet.humanetcore.utils.BalanceHelper;

/**
 * Created by ovi on 2/5/16.
 */
public class BalanceChangeTypeImpl implements BalanceHelper.BalanceTypeArray {

    enum Type implements BalanceChangeType {

        FLAGS {
            @Override
            public int getId() {
                return 300;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_flags);
            }
        },
        REGISTRATION {
            @Override
            public int getId() {
                return 301;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_registration);
            }
        },
        QUESTIONARY {
            @Override
            public int getId() {
                return 302;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_questionary);
            }
        },
        INVITE {
            @Override
            public int getId() {
                return 303;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_invite);
            }
        },
        VOTING {
            @Override
            public int getId() {
                return 304;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_voting);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_menu_vote;
            }
        },
        NEW_VOTING {
            @Override
            public int getId() {
                return 320;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_new_voting);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_menu_vote;
            }
        },
        HUNDRED_VIDEOS {
            @Override
            public int getId() {
                return 321;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_hundred_videos);
            }
        },
        ADD_VIDEO_RESPONSE {
            @Override
            public int getId() {
                return 322;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_add_video_response);
            }
        },
        FOLLOWING {
            @Override
            public int getId() {
                return 305;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_following);
            }
        },
        NEW_FOLLOWER {
            @Override
            public int getId() {
                return 306;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_new_follower);
            }
        },
        VLOG {
            @Override
            public int getId() {
                return 307;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_vlog);
            }
        },
        NEW_VIDEO {
            @Override
            public int getId() {
                return 308;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_new_video);
            }
        },
        LIKE_SENT {
            @Override
            public int getId() {
                return 309;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_like_sent);
            }
        },
        LIKE_RECIEVE {
            @Override
            public int getId() {
                return 310;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_like_recieve);
            }
        },
        SHARE {
            @Override
            public int getId() {
                return 311;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_share);
            }
        },
        MARKET_SOLD {
            public int getId() {
                return 312;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_market_sold);
            }
        },
        MARKET_BOUGHT {
            public int getId() {
                return 313;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_market_bought);
            }
        },
        ASTRA {
            public int getId() {
                return 314;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_astra);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_balance_astra;
            }
        },
        CREDO {
            public int getId() {
                return 315;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_credo);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_balance_credo;
            }
        },
        CLASSIC {
            public int getId() {
                return 316;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_classic);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_balance_general_game;
            }
        },
        ELSE {
            public int getId() {
                return 317;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_else);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_balance_else;
            }
        },
        MARKED_SHARED_GIVEN {
            public int getId() {
                return 318;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_marked_shared_given);
            }
        },
        MARKED_SHARED_GOT {
            public int getId() {
                return 319;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_marked_shared_got);
            }
        },

        COINS_ADD {
            @Override
            public int getId() {
                return 399;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.balance_change_type_coins_add);
            }

            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_currency_navigation;
            }
        },

        REGISTRATION_2 {
            @Override
            public int getId() {
                return 100;
            }

            public String getTitle() {
                return REGISTRATION.getTitle();
            }
        };


        public String getDrawable() {
            return null;
        }
    }


    public BalanceChangeType getById(int id) {
        for (Type type : Type.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }


}
