package com.clickandgain.impls;

import android.content.Context;

import com.clickandgain.R;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;

/**
 * Created by ovi on 3/31/16.
 */
public class RatingImpl implements RatingsTabFragment.RatingTabView {


    public static final String LOCAL = "local";
    public static final String GAME = "game";


    private Context mContext;

    public RatingImpl(Context context) {
        mContext = context;
    }

    @Override
    public String[] getTabNames() {
        return new String[]{
                mContext.getString(com.humanet.humanetcore.R.string.rating_type_local),
                mContext.getString(R.string.rating_type_game),
                mContext.getString(com.humanet.humanetcore.R.string.rating_type_global),
        };
    }

    @Override
    public int getDefaultTabPosition() {
        return 1; // Game tab
    }

    @Override
    public String getRatingType(int tabPosition) {
        switch (tabPosition) {
            case 0:
                return LOCAL;
            case 1:
                return GAME;

            case 2: // global
            default:
                return null;

        }
    }
}
