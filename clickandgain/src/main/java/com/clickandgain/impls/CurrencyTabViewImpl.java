package com.clickandgain.impls;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.clickandgain.R;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.clickandgain.fragments.ExternalPaymentsFragment;
import com.humanet.humanetcore.fragments.balance.GameCurrencyFragment;
import com.humanet.humanetcore.fragments.balance.InternalCurrencyFragment;
import com.humanet.humanetcore.interfaces.OnBalanceChanged;

/**
 * Created by ovi on 4/1/16.
 */
public final class CurrencyTabViewImpl implements CurrencyFragment.CurrencyTabView {

    private Context mContext;

    private OnBalanceChanged mOnBalanceChanged;


    public CurrencyTabViewImpl(Context context) {
        mContext = context;
    }

    public void setOnBalanceChangedListener(OnBalanceChanged onBalanceChanged) {
        mOnBalanceChanged = onBalanceChanged;
    }

    @Override
    public String[] getTabNames() {
        return new String[]{
                mContext.getString(com.humanet.humanetcore.R.string.balance_currency_tab_internal),
                mContext.getString(R.string.balance_currency_tab_game),
                mContext.getString(com.humanet.humanetcore.R.string.balance_currency_tab_external),
        };
    }

    @Override
    public int getDefaultTabPosition() {
        return 1; // Game tab
    }

    @Override
    public int getAddBalanceTabPosition() {
        return 2;
    }

    @Override
    public Fragment getFragmentAtPosition(int position) {
        if (position == 0) {
            InternalCurrencyFragment fragment = new InternalCurrencyFragment();
            Bundle bundle = new Bundle(2);
            bundle.putInt("position", position);
            bundle.putString("type", "local");
            fragment.setArguments(bundle);
            return fragment;
        } else if (position == 1) {
            GameCurrencyFragment fragment = new GameCurrencyFragment();
            Bundle bundle = new Bundle(2);
            bundle.putInt("position", position);
            bundle.putString("type", "game");
            fragment.setArguments(bundle);
            return fragment;
        } else if (position == 2) {
            ExternalPaymentsFragment paymentsFragment = new ExternalPaymentsFragment();
            paymentsFragment.setOnBalanceChanged(mOnBalanceChanged);

            return paymentsFragment;
        } else {

            throw new RuntimeException("undefined Currency tab position");

        }
    }
}
