package com.clickandgain.views.widgets.game.items;

import android.content.Context;
import android.graphics.Bitmap;

import com.clickandgain.R;
import com.clickandgain.modules.media.AstraMediaFacade;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;
import com.humanet.humanetcore.views.widgets.items.BaseItemView;

/**
 * Created by ovitali on 01.12.2015.
 */
public class ElseItemView extends BaseCredoItem {

    private ItemView mItemView;

    public static ElseItemView create(final VideoSurfaceView videoSurfaceView, final YellowProgressBar progressBar, AstraMediaFacade mediaFacade) {
        ElseItemView itemView = new ElseItemView(videoSurfaceView, progressBar, mediaFacade);
        return itemView;
    }

    private ElseItemView(final VideoSurfaceView videoSurfaceView, final YellowProgressBar progressBar, AstraMediaFacade mediaFacade) {
        super(videoSurfaceView, progressBar, mediaFacade);
        mItemView = new ItemView(videoSurfaceView.getContext());
        mItemView.setTag(this);
    }

    public ItemView getItemView() {
        return mItemView;
    }

    public class ItemView extends BaseItemView {
        public ItemView(Context context) {
            super(context);

            mImageView.setBackgroundResource(R.drawable.circle_white_bg);
        }

        public void setPreview(Bitmap bitmap) {
            if (bitmap != null)
                mImageView.setImageBitmap(bitmap);
            //ImageLoader.getInstance().displayImage(, mImageView, new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build());
        }

        public void setBorderColor(final int argb) {
            mImageView.setBorderColor(argb);
        }
    }

}
