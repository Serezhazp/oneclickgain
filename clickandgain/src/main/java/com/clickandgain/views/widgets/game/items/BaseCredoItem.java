package com.clickandgain.views.widgets.game.items;

import android.media.MediaPlayer;
import android.view.View;

import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.media.AstraMediaFacade;
import com.humanet.humanetcore.modules.media.IMediaEventListener;
import com.humanet.humanetcore.modules.media.MediaFacade;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;

/**
 * Created by ovitali on 26.11.2015.
 */
public class BaseCredoItem implements IMediaEventListener {

    VideoSurfaceView mVideoSurfaceView;
    private YellowProgressBar mProgressBar;

    AstraMediaFacade mMediaFacade;

    protected String localVideoUrl;

    private QuestionMedia mQuestionMedia;

    private int mCurrentPosition = Integer.MIN_VALUE;

    public BaseCredoItem(final VideoSurfaceView videoSurfaceView, final YellowProgressBar progressBar) {
        mVideoSurfaceView = videoSurfaceView;
        mProgressBar = progressBar;

        mMediaFacade = new AstraMediaFacade(mVideoSurfaceView);
        mMediaFacade.setLooping(true);
        mMediaFacade.setMediaEventListener(this);
    }

    public BaseCredoItem(final VideoSurfaceView videoSurfaceView, final YellowProgressBar progressBar, AstraMediaFacade mediaFacade) {
        mVideoSurfaceView = videoSurfaceView;
        mProgressBar = progressBar;
        mMediaFacade = mediaFacade;
    }

    @Override
    public final void onVideoPlayingStarted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        if (mCurrentPosition != Integer.MIN_VALUE) {
            mediaPlayer.seekTo(mCurrentPosition);
            mCurrentPosition = Integer.MIN_VALUE;
        }
    }

    public final void stop() {
        mMediaFacade.closeMediaPlayer();
        mVideoSurfaceView.setVisibility(View.GONE);
    }

    public final void pause() {
        mMediaFacade.closeMediaPlayer();
    }

    public final void resume() {
        if (localVideoUrl != null) {
            mVideoSurfaceView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mVideoSurfaceView.setVisibility(View.VISIBLE);
                    mMediaFacade.play(localVideoUrl);
                }
            }, 100);
        }
    }

    @Override
    public final void onVideoPlayingCompleted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        mediaFacade.replay();
    }


    public final void onPublishProgress(int progress) {
        mProgressBar.setProgress(progress);
    }

    public final void setQuestionMedia(QuestionMedia question) {
        mQuestionMedia = question;
    }

    public QuestionMedia getQuestionMedia() {
        return mQuestionMedia;
    }

    public String getLocalUrl() {
        return localVideoUrl;
    }

    public void setLocalVideoUrl(final String localVideoUrl) {
        this.localVideoUrl = localVideoUrl;
        mProgressBar.setVisibility(View.GONE);
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public void setCurrentPosition(final int currentPosition) {
        mCurrentPosition = currentPosition;
    }
}
