package com.clickandgain.views.widgets.game.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.clickandgain.R;
import com.clickandgain.model.game.QuestionMedia;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.views.utils.BackgroundCutter;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.CircleView;

public class CredoItemView extends BaseCredoItem {

    private CircleView mCircleView;
    private Button mCheckButton;

    private OnCredoItemCheckListener mOnCredoItemCheckListener;

    public static CredoItemView create(LinearLayout container, int index) {
        CircleView circleView = new CircleView(container.getContext());

        CircleView.addSurfaceView(circleView);
        CircleView.addProgressbar(circleView);
        CircleView.addPlayButton(circleView);

        CredoItemView credoItemView = new CredoItemView(
                container,
                circleView,
                index);

        return credoItemView;

    }

    private CredoItemView(LinearLayout container, CircleView circleView, final int index) {
        super(circleView.getSurfaceView(), circleView.getProgressBar());

        Context context = circleView.getContext();

        mCircleView = circleView;

        int padding = (int) ConverterUtil.dpToPix(context, 10);
        int size = App.WIDTH - padding * 2;

        mCircleView.setX(padding);
        mCircleView.setY(padding);
        container.addView(mCircleView, index, new LinearLayout.LayoutParams(size, size));

        int buttonSize = (int) ConverterUtil.dpToPix(context, 70);

        mCheckButton = new Button(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(buttonSize, buttonSize);
        lp.setMargins(0, -buttonSize / 2, 0, 0);
        lp.gravity = Gravity.CENTER;
        container.addView(mCheckButton, index + 1, lp);

        mCheckButton.setBackgroundResource(R.drawable.credo_btn_unchecked);

        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCredoItemCheckListener != null) {
                    mOnCredoItemCheckListener.onCredoItemChecked(CredoItemView.this, getQuestionMedia());
                }
                setChecked(true);
            }
        });


        mCircleView.setVisibility(View.INVISIBLE);
    }

    public final void playIfNotPlaying() {
        if (!mMediaFacade.isPlaying() && localVideoUrl != null) {
            Log.i("CredoItemView", "playing");
            mVideoSurfaceView.setVisibility(View.VISIBLE);
            mMediaFacade.play(localVideoUrl);
        }
    }

    public final void stopIfPlaying() {
        if (mMediaFacade.isPlaying()) {
            Log.i("CredoItemView", "stopped");
            stop();
        }
    }

    public void setChecked(boolean value) {
        if (value) {
            mCheckButton.setBackgroundResource(R.drawable.credo_btn_checked);
        } else {
            mCheckButton.setBackgroundResource(R.drawable.credo_btn_unchecked);
        }
    }

    public void setOnCredoItemCheckListener(OnCredoItemCheckListener listener) {
        mOnCredoItemCheckListener = listener;
    }

    public void createBackground(int h) {
        View view = mCircleView.getOverlayView();

        Bitmap bitmap = BackgroundCutter.createCuttedBitmapFromGraddient(
                mCircleView.getContext(),
                h,
                mCircleView.getMeasuredWidth(),
                mCircleView.getMeasuredHeight(),
                mCircleView.getTop(),
                0
        );

        UiUtil.setBackgroundDrawable(view, bitmap);
        mCircleView.setVisibility(View.VISIBLE);
    }


    public float getY() {
        return mCircleView.getY();
    }

    public float getHeight() {
        return mCircleView.getHeight();
    }

    public interface OnCredoItemCheckListener {
        void onCredoItemChecked(CredoItemView view, QuestionMedia questionMedia);
    }


}
