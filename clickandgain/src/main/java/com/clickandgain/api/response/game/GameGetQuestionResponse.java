package com.clickandgain.api.response.game;

import com.clickandgain.model.game.PromoQuestion;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 20.10.2015.
 */
public class GameGetQuestionResponse {

    @SerializedName("question")
    PromoQuestion[] questions;

    public PromoQuestion[] getQuestions() {
        return questions;
    }
}
