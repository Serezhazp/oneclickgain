package com.clickandgain.api.response.xolla;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Uran on 11.07.2016.
 */
public class XollaTokenResponse {

    @SerializedName("token")
    String mToken;



    public String getToken() {
        return mToken;
    }
}
