package com.clickandgain.api.sets;

import com.clickandgain.api.response.xolla.XollaTokenResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Uran on 11.07.2016.
 */
public interface XollaApiSet {

    @GET("/xsolla/")
    Call<XollaTokenResponse> getToken(@Query("auth_token") String token);

}
