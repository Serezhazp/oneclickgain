package com.clickandgain.api;

import android.support.annotation.IntDef;
import android.util.SparseArray;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by ovi on 3/18/16.
 * <p/>
 * Based on description https://docs.google.com/spreadsheets/d/1rZRNnu1Bpihwp4WeVvYUmSSEK9TXK6cxEmRPHYCxCyU/edit#gid=0
 */
public class GameRequestErrorController {

    public static final int NONE = -1;
    public static final int ACTION_RELOAD_QUESTION_LIST = 1;
    public static final int ACTION_RETRY = 2;
    public static final int ACTION_ABORT = 3;
    public static final int ACTION_NO_INTERNET = 4;


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NONE, ACTION_RELOAD_QUESTION_LIST, ACTION_RETRY, ACTION_ABORT, ACTION_NO_INTERNET})
    public @interface OnErrorAction {
    }


    /**
     * Нужно подождать несколько секунд и повторить запрос. При многократном повторе ошибки - вывести пользователю сообщения об ошибке сервера и периодически пытаться повторить действие.
     */
    private static final int OTHER = 2;

    /**
     * Считать игру начатой (при необходимости вызвать games.edit) и перенаправить игрока на экран игры (вместе с ошибкой вернется game.key для игры)
     */
    public static final int GAME_STARTED = 82;


    /**
     * Выкидываем на стартовый экран игры
     */
    private static final int NOT_YOUR_GAME = 84;

    /**
     * Выкидываем на стартовый экран игры
     */
    public static final int GAME_IS_OVER = 85;


    /**
     * Запрашиваем новую партию вопросов и продолжаем игру. Если ошибка повторяется - начинаем игру заново (перебрасываем пользователя на стартовый экран)
     */
    private static final int WRONG_MEDIA_OR_QUESTION_ID = 86;

    private SparseArray<GameRequestError> mErrorsMap;


    public GameRequestError validateError(int code) {
        GameRequestError gameError = getErrorsMap().get(code);
        if (gameError == null) {
            gameError = new GameRequestError(code);
            mErrorsMap.append(code, gameError);
        }

        gameError.mRetries++;

        return gameError;
    }


    private SparseArray<GameRequestError> getErrorsMap() {
        if (mErrorsMap == null)
            mErrorsMap = new SparseArray<>();
        return mErrorsMap;
    }


    public static class GameRequestError {
        private final int mCode;
        private final int mRetriesLimit;
        private int mRetries;
        private final int mTimeOut;


        private GameRequestError(int code) {
            mCode = code;
            switch (code) {
                case NOT_YOUR_GAME:
                case WRONG_MEDIA_OR_QUESTION_ID:
                    mRetriesLimit = -1;
                    mTimeOut = -1;
                    break;

                case GAME_IS_OVER:
                    mRetriesLimit = 1;
                    mTimeOut = 100;
                    break;

                default:
                    mRetriesLimit = 5;
                    mTimeOut = 2000;
                    break;
            }
        }

        @OnErrorAction
        public int getNextAction() {
            if (mCode == WRONG_MEDIA_OR_QUESTION_ID)
                return ACTION_RELOAD_QUESTION_LIST;

            return mRetries <= mRetriesLimit ? ACTION_RETRY : ACTION_ABORT;
        }

        public int getTimeOut() {
            return mTimeOut;
        }

        public int getCode() {
            return mCode;
        }
    }
}
