package com.clickandgain.api.requests.game;

import com.clickandgain.api.response.game.GameStatusResponse;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.model.enums.GameType;

/**
 * Created by ovitali on 16.09.2015.
 */
public class GameStatusRequest extends BaseGameRequest<GameStatusResponse> {

    private String mGameKey;
    private GameType mGameType;

    public GameStatusRequest(String gameKey, GameType type) {
        super(GameStatusResponse.class);

        mGameKey = gameKey;
        mGameType = type;
    }

    @Override
    public GameStatusResponse loadDataFromNetwork() throws Exception {
        ArgsMap entity = new ArgsMap();
        entity.put("game_key", mGameKey);
        entity.put("game_type", mGameType.getRequestParam());

        log("?games.status", entity);

        return getService().status(entity);

    }
}
