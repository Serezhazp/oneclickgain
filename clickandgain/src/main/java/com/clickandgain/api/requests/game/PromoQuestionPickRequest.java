package com.clickandgain.api.requests.game;

import com.humanet.humanetcore.api.ArgsMap;
import com.clickandgain.api.sets.GameApiSet;
import com.clickandgain.model.game.PromoQuestionResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Sends selected answer to server
 */

public class PromoQuestionPickRequest extends RetrofitSpiceRequest<PromoQuestionResponse, GameApiSet> {

    ArgsMap argsMap = new ArgsMap();

    public PromoQuestionPickRequest(int countryCode, int answerId, int questionId) {
        super(PromoQuestionResponse.class, GameApiSet.class);

        argsMap.put("game_type", "start");
        argsMap.put("id_answer", String.valueOf(answerId));
        argsMap.put("id_question", String.valueOf(questionId));
    }

    @Override
    public PromoQuestionResponse loadDataFromNetwork() throws Exception {
        return getService().questionPickPromo(argsMap);
    }
}
