package com.clickandgain.api.requests.game;

import android.util.Log;

import com.humanet.humanetcore.api.ArgsMap;
import com.clickandgain.api.response.game.GameGetQuestionResponse;
import com.clickandgain.api.sets.GameApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class PromoQuestionGetRequest extends RetrofitSpiceRequest<GameGetQuestionResponse, GameApiSet> {

    ArgsMap mArgsMap;

    /**
     * Constructor for promo game
     *
     * @param countryCode country phoned code
     */
    public PromoQuestionGetRequest(int countryCode) {
        super(GameGetQuestionResponse.class, GameApiSet.class);
        mArgsMap = new ArgsMap();
        mArgsMap.put("game_type", "start");
        mArgsMap.put("country_code", String.valueOf(countryCode));

    }

    @Override
    public GameGetQuestionResponse loadDataFromNetwork() throws Exception {
        Log.i("API: PromoQuestionGet", mArgsMap.toString());
        return getService().questionGetPromo(mArgsMap);
    }
}
