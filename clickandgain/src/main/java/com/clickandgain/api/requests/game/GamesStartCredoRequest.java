package com.clickandgain.api.requests.game;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.util.Log;

import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.api.response.game.GameStartResponse;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.games.exceptions.GameException;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;

import static com.clickandgain.views.widgets.game.SportCategoryPickerView.SportType;

/**
 *
 */
public class GamesStartCredoRequest extends BaseGamesStartRequest {

    private GameType mGameType;
    private int mMultiplier;
    private int mRate;
    @SportType
    private String mSport;

    private int mQuestionCount = 1;

    public GamesStartCredoRequest(GameType gameType, int multiplier, int rate, @SportType String sport, int count) {
        this(gameType, multiplier, rate, sport);
        mQuestionCount = count;
    }

    public GamesStartCredoRequest(GameType gameType, int multiplier, int rate, @SportType String sport) {
        super();
        mGameType = gameType;
        mMultiplier = multiplier;
        mSport = sport;
        mRate = rate;
    }

    @Override
    public GameStartResponse loadDataFromNetwork() throws Exception {
        GameStartResponse startResponse = startGame();

        if (startResponse.getErrorsMessage() != null && startResponse.getErrorsMessage().contains("start"))
            editGame(startResponse.getGameToken());

        loadQuestions(startResponse.getGameToken());

        return startResponse;
    }

    private GameStartResponse startGame() throws GameException {
        final ArgsMap argsMap = buildStartGameRequestParams(mGameType, mMultiplier, mRate, mSport);
        log("games.start", argsMap);
        final GameStartResponse response = new Task<GameStartResponse>() {
            @Override
            protected GameStartResponse doTask() {
                return getService().start(argsMap);
            }
        }.execute(null, mGameType);
        return response;
    }

    private BaseResponse editGame(String gameKey) throws GameException {
        final ArgsMap argsMap = buildEditGameRequestParams(gameKey, mMultiplier, mRate, mSport);

        log("games.edit", argsMap);

        BaseResponse response = new Task<BaseResponse>() {
            @Override
            protected BaseResponse doTask() {
                return getService().edit(argsMap);
            }
        }.execute(gameKey, mGameType);

        return response;
    }

    private void loadQuestions(String gameKey) throws Exception {
        final ArgsMap argsMap = buildGetQuestionsGameRequestParams(gameKey, mQuestionCount);

        log("questions.get", argsMap);
        GameGetQuestionsResponse gameGetQuestionResponse = new Task<GameGetQuestionsResponse>() {
            @Override
            protected GameGetQuestionsResponse doTask() {
                return getService().questionsGet(argsMap);
            }
        }.execute(gameKey, mGameType);

        removeAllQuestions(gameKey);
        ContentResolver contentResolver = getContentResolver();

        for (Question question : gameGetQuestionResponse.getQuestions()) {
            ContentValues contentValues = Question.toContentValues(gameKey, question);
            contentResolver.insert(ContentDescriptor.Question.URI, contentValues);

            Log.v("question", "-->" + question.getId());

            for (int i = 0; i < question.getQuestionMedias().length; i++) {
                QuestionMedia questionMedia = question.getQuestionMedias()[i];
                contentValues = QuestionMedia.toContentValues(question.getId(), i, gameKey, questionMedia);

                Log.v("question", "----->" + questionMedia.getId());

                contentResolver.insert(ContentDescriptor.QuestionMedia.URI, contentValues);
            }
        }
    }

    //---
    public static ArgsMap buildStartGameRequestParams(GameType gameType, int multiplier, int rate, @SportType String sport) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("game_type", gameType.getRequestParam());
        argsMap.put("rate", String.valueOf(rate));
        argsMap.put("multiplier", String.valueOf(multiplier));
        argsMap.put("sport", sport);


        return argsMap;
    }

    public static ArgsMap buildGetQuestionsGameRequestParams(String gameKey, int questionCount) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("game_key", gameKey);
        argsMap.put("count", questionCount);
        return argsMap;
    }

    public static ArgsMap buildEditGameRequestParams(String gameKey, int multiplier, int rate, @SportType String sport) {
        ArgsMap map = new ArgsMap();
        map.put("game_key", gameKey);
        map.put("multiplier", multiplier);
        map.put("rate", rate);
        map.put("sport", sport);
        return map;
    }


}
