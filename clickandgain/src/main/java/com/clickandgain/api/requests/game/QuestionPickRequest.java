package com.clickandgain.api.requests.game;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.util.Log;

import com.clickandgain.model.game.QuestionPickResponse;
import com.clickandgain.modules.games.exceptions.GameException;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;

/**
 * Sends selected answer to server
 */

public class QuestionPickRequest extends BaseGameRequest<QuestionPickResponse> {

    private final ArgsMap mArgsMap;

    private final String mGameKey;
    private final GameType mGameType;

    public QuestionPickRequest(GameType gameType, String gameKey, int answerId, int questionId, int mediaId) {
        super(QuestionPickResponse.class);
        mGameType = gameType;
        mGameKey = gameKey;
        mArgsMap = buildRequestParams(gameKey, answerId, questionId, mediaId);

        Log.v("question", "pick---->" + "qid:" + questionId + "| aid:" + answerId + " | mid:" + mediaId);
    }

    @Override
    public QuestionPickResponse loadDataFromNetwork() throws Exception {

        log("questions.pick", mArgsMap);

        ContentResolver contentResolver = App.getInstance().getContentResolver();

        ContentValues contentValues = new ContentValues();
        contentValues.put(ContentDescriptor.Question.Cols.ANSWERED, 1);

        contentResolver.update(ContentDescriptor.Question.URI, contentValues,
                ContentDescriptor.QuestionMedia.Cols.QUESTION_ID + "=" + mArgsMap.get("id_question"), null);

        QuestionPickResponse response = new Task<QuestionPickResponse>() {
            @Override
            protected QuestionPickResponse doTask() {
                return getService().questionPick(mArgsMap);
            }
        }.execute(mGameKey, mGameType);

        validatePickResponse("questions.pick", mGameKey, mGameType, mArgsMap, response);

        return response;
    }

    public void validatePickResponse(String requestName, String gameKey, GameType gameType, ArgsMap argsMap, QuestionPickResponse questionPickResponse) throws GameException {
        if (questionPickResponse.getMedia() == null) {
            logToCrashlitics(requestName, gameKey, gameType, argsMap, questionPickResponse);
        }
    }


    public static ArgsMap buildRequestParams(String gameKey, int answerId, int questionId, int mediaId) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("id_answer", String.valueOf(answerId));
        argsMap.put("id_question", String.valueOf(questionId));
        argsMap.put("id_media", String.valueOf(mediaId));
        argsMap.put("game_key", gameKey);
        return argsMap;
    }
}
