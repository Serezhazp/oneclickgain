package com.clickandgain.api.requests.game;

import com.clickandgain.api.response.game.GameEndResponse;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.model.enums.GameType;

/**
 * Created by ovitali on 16.09.2015.
 */
public class GameEndRequest extends BaseGameRequest<GameEndResponse> {

    private String mGameKey;
    private GameType mGameType;

    @Override
    public int getPriority() {
        return PRIORITY_HIGH;
    }

    public GameEndRequest(String gameKey, GameType type) {
        super(GameEndResponse.class);

        mGameKey = gameKey;
        mGameType = type;
    }


    @Override
    public GameEndResponse loadDataFromNetwork() throws Exception {
        ArgsMap argsMap = buildRequestParams(mGameKey, mGameType);

        log("games.end", argsMap);

        return getService().end(argsMap);
    }

    public static ArgsMap buildRequestParams(String gameKey, GameType gameType) {
        ArgsMap argsMap = new ArgsMap();
        argsMap.put("game_key", gameKey);
        argsMap.put("game_type", gameType.getRequestParam());
        return argsMap;
    }
}
