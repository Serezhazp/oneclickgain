package com.clickandgain;

import com.clickandgain.impls.BalanceChangeTypeImpl;
import com.clickandgain.impls.CurrencyTabViewImpl;
import com.clickandgain.impls.RatingImpl;
import com.clickandgain.impls.VideoStatisticsImpl;
import com.clickandgain.service.GameApiService;
import com.clickandgain.service.VideoCacheService;
import com.clickandgain.utils.AnalyticsImpl;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.BalanceHelper;
import com.humanet.humanetcore.utils.GsonHelper;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by ovi on 2/2/16.
 */
public class ClickandgainApp extends App {


    static {
        API_APP_NAME = "1cg";
        App.DEBUG = BuildConfig.DEBUG;
        GsonHelper.BALANCE_SERIALIZATION_KEY = "gc";
    }

    @Override
    public void onCreate() {
        super.onCreate();

        AnalyticsHelper.setAnalytics(new AnalyticsImpl());

        BalanceHelper.setsImpl(new BalanceChangeTypeImpl());

        RatingsTabFragment.setImpl(new RatingImpl(this));

        CurrencyFragment.setImpl(new CurrencyTabViewImpl(this));

        VideoStatistic.setVideoStatisticDelegator(new VideoStatisticsImpl());

        BalanceUtil.checkBalance = true;

        NavigationManager.init(new NavigationManagerImpl());
    }

    @Override
    public String getServerEndpoint() {
        return "https://baseapi.1clickgain.com/";
    }

    private static SpiceManager sGameSpiceManager;

    public static SpiceManager getGameSpiceManager() {
        if (sGameSpiceManager == null || !sGameSpiceManager.isStarted()) {
            sGameSpiceManager = new SpiceManager(GameApiService.class);
            sGameSpiceManager.start(getInstance());
        }
        return sGameSpiceManager;
    }

    // video cache service instance
    private static SpiceManager sVideoCacheSpiceManager;

    public static SpiceManager getVideoCacheSpiceManager() {
        if (sVideoCacheSpiceManager == null || !sVideoCacheSpiceManager.isStarted()) {
            sVideoCacheSpiceManager = new SpiceManager(VideoCacheService.class);
            sVideoCacheSpiceManager.start(getInstance());
        }
        return sVideoCacheSpiceManager;
    }
}
