package com.clickandgain.service;

import com.humanet.humanetcore.App;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

public final class VideoCacheService extends RetrofitGsonSpiceService {


    @Override
    protected String getServerUrl() {
        return App.getInstance().getServerEndpoint();
    }

}
