package com.clickandgain.payment;

/**
 * Created by Deni on 24-Jun-16.
 */
public enum PaymentType {
    GOOGLE, PAYSERA, XSOLLA;
}
