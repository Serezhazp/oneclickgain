package com.clickandgain.activities.games;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.clickandgain.R;
import com.clickandgain.fragments.games.astra.AstraGameFragment;
import com.clickandgain.modules.games.presenters.AstraPresenter;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.utils.AnalyticsHelper;

public class AstraActivity extends BaseActivity {

    public static void startNewInstance(Context context, float balance, @SportCategoryPickerView.SportType String sport) {
        Intent intent = new Intent(context, AstraActivity.class);
        intent.putExtra("balance", balance);
        intent.putExtra("sport", sport);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_no_toolbar_container);

        AnalyticsHelper.trackEvent(this, AnalyticsHelper.GAME_ASTRA);

        if (savedInstanceState == null)
            newQuestion(getIntent().getFloatExtra("balance", 0));
    }


    public void newQuestion(float balance) {
        if (AstraPresenter.getInstance().isGameCreatedTimeValid() && AstraPresenter.getInstance().getQuestion() != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_NONE);
            ft.replace(R.id.container, AstraGameFragment.newInstance(balance), AstraGameFragment.class.getSimpleName()).commit();
        } else {
            AstraPresenter.getInstance().endGame();
            finish();
        }
    }
}
