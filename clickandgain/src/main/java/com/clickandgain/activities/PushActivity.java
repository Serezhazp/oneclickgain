package com.clickandgain.activities;

import android.util.Log;

import com.clickandgain.fragments.games.astra.AstraTutorialFragment;
import com.clickandgain.fragments.games.credo.tutorials.CredoTutorialFragment;
import com.clickandgain.fragments.games.credo.tutorials.ElseGameTutorialFragment;
import com.clickandgain.fragments.games.credo.tutorials.GeneralGameTutorialFragment;

/**
 * Created by serezha on 06.09.16.
 */
public class PushActivity extends com.humanet.humanetcore.activities.PushActivity {

	@Override
	protected void showGame() {
		switch (mObjectId) {
			case 1:
				startFragment(AstraTutorialFragment.newInstance(), false, true);
				logGameStart("Astra");
				break;
			case 2:
				startFragment(CredoTutorialFragment.newInstance(), false, true);
				logGameStart("Credo");
				break;
			case 3:
				startFragment(GeneralGameTutorialFragment.newInstance(), false, true);
				logGameStart("Classic");
				break;
			case 4:
				startFragment(ElseGameTutorialFragment.newInstance(), false, true);
				logGameStart("Else");
				break;
			default:

				break;
		}
	}

	private void logGameStart(String text) {
		Log.d("GAME START:", text);
	}
}
