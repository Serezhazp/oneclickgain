package com.clickandgain.activities;

import android.content.Intent;

import com.humanet.humanetcore.activities.BaseSplashActivity;

/**
 * Created by ovi on 1/25/16.
 */
public class SplashActivity extends BaseSplashActivity {

    @Override
    public void openMainActivity() {
        MainActivity.startNewInstance(this);
    }

    @Override
    public void openRegistrationActivity() {
        startActivity(new Intent(this, RegistrationActivity.class));
    }
}
