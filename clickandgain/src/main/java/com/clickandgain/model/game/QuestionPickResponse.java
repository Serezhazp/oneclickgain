package com.clickandgain.model.game;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

import java.io.Serializable;

/**
 * Created by ovitali on 03.09.2015.
 */
public class QuestionPickResponse extends BaseResponse implements Serializable {

    @SerializedName("correct")
    private int mCorrect;

    /**
     * количество валюты, на которое поменялся баланс
     */
    @SerializedName("change")
    private float mChange;
    /**
     * текущий игровой баланс
     */
    @SerializedName("balance")
    private float mBalance;

    @SerializedName("answer")
    private Answer mAnswer;

    @SerializedName("info")
    private ElseVideoInfo mVideoInfo;

    public String getMedia() {
        return mAnswer.media;
    }


    public String getText() {
        return mAnswer.text;
    }


    public boolean isCorrect() {
        return mCorrect == 1;
    }

    public float getChange() {
        return mChange;
    }

    public float getBalance() {
        return mBalance;
    }

    public ElseVideoInfo getVideoInfo() {
        return mVideoInfo;
    }

    class Answer implements Serializable {
        String text;
        String media;
    }

}
