package com.clickandgain.modules.games.presenters;

import android.content.ContentResolver;
import android.content.ContentValues;

import com.clickandgain.api.requests.game.GamesStartCredoRequest;
import com.clickandgain.api.requests.game.GetCredoQuestionsRequest;
import com.clickandgain.api.response.game.GameGetQuestionsResponse;
import com.clickandgain.api.response.game.GameStartResponse;
import com.clickandgain.model.game.Question;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.GameType;
import com.octo.android.robospice.persistence.DurationInMillis;

import static com.clickandgain.views.widgets.game.SportCategoryPickerView.SportType;

/**
 * Created by ovitali on 04.12.2015.
 */
public class CredoGamePresenter extends BaseGamePresenter {

    private static final int LIMIT = 2;

    //---

    CredoGamePresenter(final GameType gameType) {
        super(gameType);
    }


    public void startGame(int multiplier, final int rate, @SportCategoryPickerView.SportType final String sport) {
        GamesStartCredoRequest gamesStartRequest = new GamesStartCredoRequest(getGameType(), multiplier, rate, sport, LIMIT);
        startGame(gamesStartRequest);
    }

    public void restartGame(int multiplier, final int rate, @SportType final String sport) {
        endGame();
        reloadGameQuestions();

        GamesStartCredoRequest gamesStartRequest = new GamesStartCredoRequest(getGameType(), multiplier, rate, sport, LIMIT);
        getSpiceManager().execute(gamesStartRequest, "", DurationInMillis.ALWAYS_EXPIRED, new GameStartRequestListener() {
            @Override
            public void onRequestSuccess(GameStartResponse gameStartResponse) {
                super.onRequestSuccess(gameStartResponse);
                if (mQuestionReadyListener != null) {
                    reloadGameQuestions();
                    mQuestionReadyListener.questionsAreReady(true);
                }
            }
        });
    }

    public void loadNewQuestionsPart() {

        getSpiceManager().execute(
                new GetCredoQuestionsRequest(getGameToken(), getGameType(), LIMIT),
                new GameRequestListener<GameGetQuestionsResponse>() {
                    @Override
                    public void onRequestSuccess(GameGetQuestionsResponse gameGetQuestionsResponse) {
                        reloadGameQuestions();
                        checkIfVideoDownloaded();
                    }
                }
        );

    }

    public int getNotAnsweredQuestionsCount() {
        Question[] questions = getGameQuestions();
        if (questions == null)
            return 0;

        int count = 0;

        for (Question question : questions)
            if (!question.isAswered())
                count++;

        return count;
    }


    boolean isLastGameWasUnComplete() {
        Question[] questions = getGameQuestions();
        if (questions == null)
            return false;
        if (questions.length == 0)
            return false;

        for (Question question : questions)
            if (question.isAswered())
                return true;

        return false;
    }

    /**
     * mark all questions for current game as "opened" to delete them on next game launch
     */
    public final void questionWasOpened() {
        final ContentResolver contentResolver = App.getInstance().getContentResolver();

        final ContentValues contentValues = new ContentValues(1);
        contentValues.put(ContentDescriptor.Question.Cols.OPENED, 1);

        final String where = String.format("%s='%s'",
                ContentDescriptor.Question.Cols.GAME_KEY,
                getGameToken()
        );

        contentResolver.update(ContentDescriptor.Question.URI, contentValues, where, null);
    }
}