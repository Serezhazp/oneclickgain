package com.clickandgain.modules.games.views;

/**
 * Created by ovitali on 03.12.2015.
 */
interface BaseView extends StoppableGameView {

    void bindToPresenter();

    void unbindFromPresenter();
}
