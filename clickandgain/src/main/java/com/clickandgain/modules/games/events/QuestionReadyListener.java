package com.clickandgain.modules.games.events;

/**
 * Created by ovitali on 04.12.2015.
 */
public interface QuestionReadyListener {

    void questionsAreReady(boolean isSuccess);

}
