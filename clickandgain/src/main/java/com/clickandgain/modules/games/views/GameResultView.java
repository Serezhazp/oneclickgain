package com.clickandgain.modules.games.views;

/**
 * Created by ovi on 3/16/16.
 */
public interface GameResultView extends BaseView{

    void setResult(int result);

}
