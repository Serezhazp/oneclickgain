package com.clickandgain.modules.games.events;

/**
 * Created by ovitali on 04.12.2015.
 */
public interface VideoLoadingListener {

    void onVideoProgressChanged(int progress);

    void onVideoLoadingComplete();

}
