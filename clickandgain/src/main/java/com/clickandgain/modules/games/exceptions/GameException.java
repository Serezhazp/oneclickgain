package com.clickandgain.modules.games.exceptions;

import com.google.gson.Gson;
import com.humanet.humanetcore.model.enums.GameType;

import static com.clickandgain.api.GameRequestErrorController.OnErrorAction;

/**
 * Created by ovi on 2/18/16.
 */
public class GameException extends Exception {

    private String mGameKey;
    private GameType mGameType;

    private String mMessage;

    private String mResponse;

    @OnErrorAction
    private int mAction;

    public GameException(int action, String gameKey, GameType gameType, String message, Object response) {
        mAction = action;

        mGameKey = gameKey;
        mGameType = gameType;
        mMessage = message;
        mResponse = new Gson().toJson(response);
    }

    public String getGameKey() {
        return mGameKey;
    }

    public GameType getGameType() {
        return mGameType;
    }

    @OnErrorAction
    public int getAction() {
        return mAction;
    }

    @Override
    public String toString() {
        return String.format("%s \n action: %d \n gameKey:%s \n gameType:%s \n message:%s \n server response:%s",
                super.toString(),
                mAction,
                mGameKey,
                mGameType.toString(),
                mMessage,
                mResponse
        );
    }
}
