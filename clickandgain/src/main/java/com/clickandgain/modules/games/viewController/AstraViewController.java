package com.clickandgain.modules.games.viewController;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.fragments.games.astra.AstraGameFragment;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.views.utils.BackgroundCutter;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import static com.clickandgain.fragments.games.astra.AstraGameFragment.QUESTION_LOADING;
import static com.clickandgain.fragments.games.astra.AstraGameFragment.QUESTION_PLAYING;
import static com.clickandgain.fragments.games.astra.AstraGameFragment.RESPONSE_PLAYING;
import static com.clickandgain.fragments.games.astra.AstraGameFragment.RESPONSE_PLAYING_COMPLETE;
import static com.clickandgain.fragments.games.astra.AstraGameFragment.RESPONSE_RESULT;
import static com.clickandgain.fragments.games.astra.AstraGameFragment.Status;
import static com.clickandgain.fragments.games.astra.AstraGameFragment.WAIT_FOR_ANSWER;

/**
 * Created by ovitali on 04.12.2015.
 */
public class AstraViewController implements View.OnClickListener {

    private TextView mBalanceTextView;
    private TextView mBalanceChangeTextView;

    private YellowProgressBar mProgressBar;

    private View mNextButton;
    private View mBackButton;
    private VideoSurfaceView mVideoSurfaceView;

    private AstraGameFragment mAstraGameFragment;

    private CircleView mCircleView;

    private View mAnswerButtonsContainer;

    private View mReplayContainer;
    private TextView mRemainedSecondsView;

    private
    @Status
    int mLastViewStatus = -1;

    public AstraViewController(AstraGameFragment astraGameFragment, View view) {
        mAstraGameFragment = astraGameFragment;
        mBalanceTextView = (TextView) view.findViewById(R.id.balance);
        mBalanceChangeTextView = (TextView) view.findViewById(R.id.balance_change);

        mNextButton = view.findViewById(R.id.btn_next);
        mBackButton = view.findViewById(R.id.btn_back);

        mReplayContainer = view.findViewById(R.id.replayContainer);
        mRemainedSecondsView = (TextView) mReplayContainer.findViewById(R.id.remainedSeconds);

        if (UiUtil.isLandscape(astraGameFragment.getActivity())) {
            mVideoSurfaceView = (VideoSurfaceView) view.findViewById(R.id.video_view);
            mProgressBar = (YellowProgressBar) view.findViewById(R.id.progress_bar);
            mCircleView = null;
        } else {
            mCircleView = (CircleView) view.findViewById(R.id.video_view);
            mVideoSurfaceView = mCircleView.getSurfaceView();

            mProgressBar = mCircleView.getProgressBar();
        }

        view.findViewById(R.id.btn_yes).setOnClickListener(this);
        view.findViewById(R.id.btn_no).setOnClickListener(this);
        view.findViewById(R.id.btn_back).setOnClickListener(this);

        mAnswerButtonsContainer = view.findViewById(R.id.answerButtons);

        mNextButton.setOnClickListener(this);

        View replayButton = mReplayContainer.findViewById(R.id.replay_btn);
        replayButton.setOnClickListener(this);

    }

    public VideoSurfaceView getVideoSurfaceView() {
        return mVideoSurfaceView;
    }

    public void manageVisibilitiesForState(@Status int state) {
        if (state == mLastViewStatus)
            return;
        mLastViewStatus = state;

        Log.i("Astra", "game state " + state);

        switch (state) {
            case QUESTION_LOADING:
                mProgressBar.setVisibility(View.VISIBLE);
                mAnswerButtonsContainer.setVisibility(View.GONE);
                mReplayContainer.setVisibility(View.GONE);
                mNextButton.setVisibility(View.GONE);
                mBalanceTextView.setVisibility(View.INVISIBLE);
                mBalanceChangeTextView.setVisibility(View.INVISIBLE);
                mBackButton.setVisibility(View.INVISIBLE);
                break;

            case QUESTION_PLAYING:
                mProgressBar.setVisibility(View.GONE);

                mNextButton.setVisibility(View.GONE);
                mBackButton.setVisibility(View.VISIBLE);

                mAnswerButtonsContainer.setVisibility(View.VISIBLE);

                mReplayContainer.setVisibility(View.GONE);

                mBalanceTextView.setVisibility(View.INVISIBLE);
                mBalanceChangeTextView.setVisibility(View.INVISIBLE);

                break;

            case WAIT_FOR_ANSWER:
                mProgressBar.setVisibility(View.GONE);

                mNextButton.setVisibility(View.GONE);
                mBackButton.setVisibility(View.VISIBLE);

                mAnswerButtonsContainer.setVisibility(View.VISIBLE);

                mReplayContainer.setVisibility(View.VISIBLE);
                mRemainedSecondsView.setVisibility(View.VISIBLE);

                mBalanceTextView.setVisibility(View.INVISIBLE);
                mBalanceChangeTextView.setVisibility(View.INVISIBLE);

                break;

            case RESPONSE_PLAYING:
                mProgressBar.setVisibility(View.GONE);

                mNextButton.setVisibility(View.GONE);
                mBackButton.setVisibility(View.INVISIBLE);

                mAnswerButtonsContainer.setVisibility(View.GONE);
                mReplayContainer.setVisibility(View.GONE);

                mBalanceTextView.setVisibility(View.INVISIBLE);
                mBalanceChangeTextView.setVisibility(View.INVISIBLE);

                break;

            case RESPONSE_RESULT:
                mProgressBar.setVisibility(View.GONE);
                mAnswerButtonsContainer.setVisibility(View.GONE);

                mNextButton.setVisibility(View.VISIBLE);
                mBackButton.setVisibility(View.VISIBLE);

                mBalanceTextView.setVisibility(View.VISIBLE);
                mBalanceChangeTextView.setVisibility(View.VISIBLE);

                mReplayContainer.setVisibility(View.GONE);

                break;

            case RESPONSE_PLAYING_COMPLETE:
                mProgressBar.setVisibility(View.GONE);
                mAnswerButtonsContainer.setVisibility(View.GONE);

                mNextButton.setVisibility(View.VISIBLE);
                mBackButton.setVisibility(View.VISIBLE);

                mBalanceTextView.setVisibility(View.VISIBLE);
                mBalanceChangeTextView.setVisibility(View.VISIBLE);

                mRemainedSecondsView.setVisibility(View.GONE);
                mReplayContainer.setVisibility(View.VISIBLE);

                break;
        }

    }

    public void setBalance(float balance, float change) {
        mBalanceTextView.setText(BalanceUtil.balanceToString(balance));
        if (change > 0) {
            mBalanceChangeTextView.setTextColor(Color.GREEN);
            mBalanceChangeTextView.setText(BalanceUtil.balanceToString(change));
        } else {
            mBalanceChangeTextView.setTextColor(Color.RED);
            mBalanceChangeTextView.setText(BalanceUtil.balanceToString(change));
        }
    }

    public void updateCountdownTimerValue(long seconds) {
        mRemainedSecondsView.setText(String.valueOf(seconds / 1000));
    }

    public void setCover(String cover) {
        if (mCircleView == null || cover == null) {
            return;
        }
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().cacheOnDisk(true).build();
        ImageLoader.getInstance().loadImage(cover, displayImageOptions, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (!mAstraGameFragment.isVisible())
                    return;
                Bitmap image = BackgroundCutter.blurImage(mAstraGameFragment.getView(), loadedImage);
                UiUtil.setBackgroundDrawable(mAstraGameFragment.getView(), image);
                mCircleView.refresh();
            }
        });
    }

    @Override
    public void onClick(final View v) {
        mAstraGameFragment.onClick(v);
    }

    public void setProgress(final int progress) {
        mProgressBar.setProgress(progress);
    }
}
