package com.clickandgain.modules.games.presenters;

import com.humanet.humanetcore.model.enums.GameType;

/**
 * Created by ovi on 4/4/16.
 */
public class PresenterHelper {

    /**
     * instance of presenter for credo games
     */
    private static CredoGamePresenter sCredoGamePresenterInstance;

    /**
     * returns instance of presenter for credo games
     */
    private static CredoGamePresenter getCredoGamePresenterInstance() {
        if (sCredoGamePresenterInstance == null) {
            sCredoGamePresenterInstance = new CredoGamePresenter(GameType.CREDO);
        }
        return sCredoGamePresenterInstance;
    }

    /**
     * instance of presenter for general game(classic) games
     */
    private static CredoGamePresenter sGeneralGameGamePresenterInstance;

    /**
     * returns of presenter for general game(classic) games
     */
    private static CredoGamePresenter getGeneralGamePresenterInstance() {
        if (sGeneralGameGamePresenterInstance == null) {
            sGeneralGameGamePresenterInstance = new CredoGamePresenter(GameType.GENERAL_GAME);
        }
        return sGeneralGameGamePresenterInstance;
    }

    private static CredoGamePresenter sElseGamePresenterInstance;

    private static CredoGamePresenter getElsePresenterInstance() {
        if (sElseGamePresenterInstance == null) {
            sElseGamePresenterInstance = new CredoGamePresenter(GameType.ELSE);
        }
        return sElseGamePresenterInstance;
    }

    public static CredoGamePresenter getPresenter(GameType gameType) {
        switch (gameType) {
            case CREDO:
                return getCredoGamePresenterInstance();
            case GENERAL_GAME:
                return getGeneralGamePresenterInstance();
            case ELSE:
                return getElsePresenterInstance();

            default:
                throw new RuntimeException("wrong game type");
        }
    }

}
