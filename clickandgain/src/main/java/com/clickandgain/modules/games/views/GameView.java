package com.clickandgain.modules.games.views;

import com.clickandgain.model.game.QuestionPickResponse;

/**
 * Created by ovitali on 03.12.2015.
 */
public interface GameView extends BaseView {

    void publishQuestionResponse(QuestionPickResponse response);

}
