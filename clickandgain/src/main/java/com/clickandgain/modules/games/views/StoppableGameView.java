package com.clickandgain.modules.games.views;

import android.view.View;

import com.clickandgain.api.GameRequestErrorController;

/**
 * Created by ovi on 3/16/16.
 */
public interface StoppableGameView {
    void gameStopped(@GameRequestErrorController.OnErrorAction int action);

    View getView();
}
