package com.clickandgain.modules.media;

import com.humanet.humanetcore.modules.media.MediaFacade;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;

/**
 * Created by ovitali on 25.11.2015.
 */
public class AstraMediaFacade extends MediaFacade {

    public AstraMediaFacade(final VideoSurfaceView surfaceView) {
        super(surfaceView);
    }

    public int getPosition() {
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            return mediaPlayer.getCurrentPosition();
        return Integer.MIN_VALUE;
    }
}
