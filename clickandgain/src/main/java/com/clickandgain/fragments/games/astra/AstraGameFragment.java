package com.clickandgain.fragments.games.astra;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.clickandgain.R;
import com.clickandgain.activities.games.AstraActivity;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.fragments.games.BaseGameVideoScreenFragment;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionPickResponse;
import com.clickandgain.modules.games.events.QuestionReadyListener;
import com.clickandgain.modules.games.events.VideoLoadingListener;
import com.clickandgain.modules.games.presenters.AstraPresenter;
import com.clickandgain.modules.games.viewController.AstraViewController;
import com.clickandgain.modules.games.views.GameResultView;
import com.clickandgain.modules.games.views.GameView;
import com.clickandgain.modules.media.AstraMediaFacade;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.modules.media.IMediaEventListener;
import com.humanet.humanetcore.modules.media.MediaFacade;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class AstraGameFragment extends BaseGameVideoScreenFragment<AstraMediaFacade>
        implements View.OnClickListener,
        VideoLoadingListener,
        IMediaEventListener,
        GameView,
        GameResultView {

    private static final int WAIT_FOR_ANSWER_TIMEOUT = 5000;

    private static final String QUESTION = "question";
    private static final String QUESTION_RESPONSE = "question_response";
    private static final String BALANCE = "balance";

    public static final int QUESTION_LOADING = 0;
    public static final int QUESTION_PLAYING = 1;
    public static final int WAIT_FOR_ANSWER = 3;
    public static final int RESPONSE_PLAYING = 4;
    public static final int RESPONSE_RESULT = 5;
    public static final int RESPONSE_PLAYING_COMPLETE = 6;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({QUESTION_LOADING, QUESTION_PLAYING, WAIT_FOR_ANSWER, RESPONSE_PLAYING, RESPONSE_RESULT, RESPONSE_PLAYING_COMPLETE})
    public @interface Status {
    }

    public static AstraGameFragment newInstance(float balance) {
        AstraGameFragment astraGameFragment = new AstraGameFragment();
        Bundle bundle = new Bundle();
        bundle.putFloat(BALANCE, balance);
        astraGameFragment.setArguments(bundle);
        return astraGameFragment;
    }

    private AstraPresenter mAstraPresenter;
    private AstraViewController mAstraView;

    private Question mQuestion;
    private QuestionPickResponse mQuestionPickResponse = null;

    private long mTimerSecondsRemained;
    private CountDownTimer mCountDownTimer;

    @Status
    private int mCurrentState = QUESTION_LOADING;

    private int mPlayingPosition = Integer.MIN_VALUE;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAstraPresenter = AstraPresenter.getInstance();
        mQuestion = mAstraPresenter.getQuestion();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_astra_game, container, false);

        mAstraView = new AstraViewController(this, view);

        mediaFacade = new AstraMediaFacade(mAstraView.getVideoSurfaceView());
        mediaFacade.setMediaEventListener(this);

        if (mQuestion.isDownloaded()) {
            showQuestion(mQuestion);
        } else {
            mCurrentState = QUESTION_LOADING;
            mAstraView.manageVisibilitiesForState(QUESTION_LOADING);
            mAstraPresenter.setVideoLoadingListener(this);
            mAstraPresenter.checkIfVideoDownloaded();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAstraView.getVideoSurfaceView().setVisibility(View.VISIBLE);
        resumePlaying();
    }

    @Override
    public void onStop() {
        stopTimer();

        mAstraView.getVideoSurfaceView().setVisibility(View.GONE);

        super.onStop();
    }

    @Override
    public void onClick(View v) {
        v.setOnClickListener(null);
        switch (v.getId()) {
            case R.id.btn_back:
                endGame(true);
                break;

            case R.id.btn_yes:
                setResult(true);
                break;

            case R.id.btn_no:
                setResult(false);
                break;

            case R.id.replay_btn:
                replay();
                break;

            case R.id.btn_next:
                nextQuestion();
                break;
        }
    }

    private void replay() {
        if (mCurrentState == RESPONSE_PLAYING_COMPLETE)
            mCurrentState = RESPONSE_RESULT;
        if (mCurrentState == WAIT_FOR_ANSWER)
            mCurrentState = QUESTION_PLAYING;

        stopTimer();

        mPlayingPosition = Integer.MIN_VALUE;

        mediaFacade.replay();
    }

    private void endGame(boolean isRequestMustBePostedToServer) {
        mAstraPresenter.endGame(isRequestMustBePostedToServer);
        getActivity().finish();
    }

    private void nextQuestion() {
        if (mAstraPresenter.getQuestion() != null)
            if(mQuestionPickResponse != null)
                ((AstraActivity) getActivity()).newQuestion(mQuestionPickResponse.getBalance());
            else
                ((AstraActivity) getActivity()).newQuestion(mAstraPresenter.mQuestionPickResponse.getBalance());
        else
            endGame(true);
    }

    private void setResult(boolean yesPressed) {
        int answerId = yesPressed ? 1 : 0;
        setResult(answerId);
    }

    @Override
    public final void setResult(final int result) {
        mCurrentState = RESPONSE_PLAYING;
        mAstraView.manageVisibilitiesForState(mCurrentState);

        mediaFacade.closeMediaPlayer();
        stopTimer();

        mAstraPresenter.setResult(result, mQuestion.getQuestionMedias()[0].getId());
    }

    @Override
    public void gameStopped(@GameRequestErrorController.OnErrorAction int action) {
        if (action == GameRequestErrorController.ACTION_ABORT || action == GameRequestErrorController.ACTION_NO_INTERNET) {
            getActivity().finish();
            if (action == GameRequestErrorController.ACTION_NO_INTERNET)
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            @SportCategoryPickerView.SportType final String sport = getActivity().getIntent().getStringExtra("sport");
            mAstraPresenter.setQuestionReadyListener(new QuestionReadyListener() {
                @Override
                public void questionsAreReady(boolean isSuccess) {
                    getActivity().finish();
                    AstraActivity.startNewInstance(getActivity(), 0, sport);
                    mAstraPresenter.setQuestionReadyListener(null);
                }
            });
            mAstraPresenter.startGame(0, sport);
        }
    }

    @Override
    public void publishQuestionResponse(final QuestionPickResponse response) {
        mQuestionPickResponse = response;
        getLoader().dismiss();

        mCurrentState = RESPONSE_PLAYING;
        mAstraView.manageVisibilitiesForState(mCurrentState);

        mAstraView.setBalance(response.getBalance(), response.getChange());

        localVideoUrl = mAstraPresenter.getAnswerVideoUrl();
        resumePlaying();

        mAstraPresenter.removeLastQuestion();

        mAstraPresenter.setGameView(null);
        mAstraPresenter.setGameResultView(this);
    }

    @Override
    public void onVideoPlayingStarted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        if (mPlayingPosition != Integer.MIN_VALUE) {
            mediaPlayer.seekTo(mPlayingPosition);
            mPlayingPosition = Integer.MIN_VALUE;
        }
        if (mCurrentState == RESPONSE_PLAYING_COMPLETE) {
            mediaPlayer.seekTo(mediaPlayer.getDuration());
            mediaPlayer.pause();
        }

        if (mCurrentState == QUESTION_LOADING) {
            mCurrentState = QUESTION_PLAYING;
        }

        if (mCurrentState == WAIT_FOR_ANSWER) {
            mediaPlayer.seekTo(mediaPlayer.getDuration());
            mediaPlayer.pause();
        }

        mAstraView.manageVisibilitiesForState(mCurrentState);

        if (mCurrentState == RESPONSE_PLAYING) {
            int length = mediaPlayer.getDuration();
            int delay = Math.min(length / 3 * 2, Constants.ANSWER_DELAY);
            if (getView() != null) {
                getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible()) {
                            onVideoPlayingPostDelay();
                        }
                    }
                }, delay);
            }
        }
    }

    @Override
    public void onVideoPlayingCompleted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        if (mCurrentState <= WAIT_FOR_ANSWER) {
            startTimer(WAIT_FOR_ANSWER_TIMEOUT);

            if (mCurrentState == QUESTION_PLAYING)
                mCurrentState = WAIT_FOR_ANSWER;
        } else {
            mCurrentState = RESPONSE_PLAYING_COMPLETE;
        }

        mAstraView.manageVisibilitiesForState(mCurrentState);
    }

    private void startTimer(int time) {
        stopTimer();
        mTimerSecondsRemained = time;
        mCountDownTimer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(final long millisUntilFinished) {
                mTimerSecondsRemained = millisUntilFinished;
                mAstraView.updateCountdownTimerValue(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                mTimerSecondsRemained = 0;
                setResult(-1);
            }
        };
        mAstraView.updateCountdownTimerValue(time);
        mCountDownTimer.start();
    }

    private void stopTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    @Override
    public void onVideoProgressChanged(final int progress) {
        mAstraView.setProgress(progress);
    }

    @Override
    public void onVideoLoadingComplete() {
        showQuestion(mQuestion);
        mAstraPresenter.setVideoLoadingListener(null);
    }

    private void showQuestion(Question question) {
        mAstraView.setCover(question.getCover());
        mCurrentState = QUESTION_PLAYING;
        mAstraView.manageVisibilitiesForState(QUESTION_PLAYING);

        localVideoUrl = mAstraPresenter.getVideoUrl();

        resumePlaying();
    }


    public void onVideoPlayingPostDelay() {
        View v = getView();
        if (v == null) {
            return;
        }

        mCurrentState = RESPONSE_RESULT;
        mAstraView.manageVisibilitiesForState(mCurrentState);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mQuestion != null)
            outState.putSerializable(QUESTION, mQuestion);
        if (mQuestionPickResponse != null)
            outState.putSerializable(QUESTION_RESPONSE, mQuestionPickResponse);

        outState.putInt("state", mCurrentState);
        outState.putInt("playingPosition",  mediaFacade.getPosition());
        outState.putLong("timerPosition", mTimerSecondsRemained);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState == null)
            return;

        Log.i("Astra - restore", savedInstanceState.toString());

        if (savedInstanceState.containsKey(QUESTION)) {
            mQuestion = (Question) savedInstanceState.getSerializable(QUESTION);
        }
        if (savedInstanceState.containsKey(QUESTION_RESPONSE)) {
            mQuestionPickResponse = (QuestionPickResponse) savedInstanceState.getSerializable(QUESTION_RESPONSE);
        }

        //noinspection ResourceType
        mCurrentState = savedInstanceState.getInt("state");
        mPlayingPosition = savedInstanceState.getInt("playingPosition");
        long timerPosition = savedInstanceState.getLong("timerPosition");

        localVideoUrl = savedInstanceState.getString("videoUrl");

        mAstraView.manageVisibilitiesForState(mCurrentState);

        if (mCurrentState == RESPONSE_RESULT || mCurrentState == RESPONSE_PLAYING || mCurrentState == RESPONSE_PLAYING_COMPLETE) {
            mAstraView.setBalance(mQuestionPickResponse.getBalance(), mQuestionPickResponse.getChange());
        }


        if (mCurrentState == WAIT_FOR_ANSWER) {
            timerPosition = (int) (1000d * Math.ceil(timerPosition / 1000d));
            startTimer((int) timerPosition);
        }
    }

    @Override
    public void bindToPresenter() {
        if (mCurrentState == QUESTION_LOADING || mCurrentState == WAIT_FOR_ANSWER || mCurrentState == QUESTION_PLAYING) {
            mAstraPresenter.setGameView(this);
            Log.d("AstraPresenter", "setGameView");
        }
        else {
            mAstraPresenter.setGameResultView(this);
            Log.d("AstraPresenter", "setGameResultView");
        }
    }

    @Override
    public void unbindFromPresenter() {
        Log.d("AstraPresenter", "unbindFromPresenter");
        mAstraPresenter.setGameView(null);
        mAstraPresenter.setGameResultView(null);
    }


}