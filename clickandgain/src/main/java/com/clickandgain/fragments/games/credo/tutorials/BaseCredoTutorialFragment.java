package com.clickandgain.fragments.games.credo.tutorials;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clickandgain.R;
import com.clickandgain.activities.games.CredoActivity;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.api.response.game.GameStatusResponse;
import com.clickandgain.fragments.games.BaseTutorialFragment;
import com.clickandgain.modules.games.events.QuestionReadyListener;
import com.clickandgain.modules.games.presenters.CredoGamePresenter;
import com.clickandgain.modules.games.presenters.PresenterHelper;
import com.clickandgain.views.widgets.game.AstraScroll;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.modules.BalanceUtil;

import static com.clickandgain.views.widgets.game.SportCategoryPickerView.SportType;

/**
 * Created by ovitali on 26.11.2015.
 */
abstract class BaseCredoTutorialFragment extends BaseTutorialFragment
        implements View.OnClickListener,
        QuestionReadyListener {

    private RadioButton mSelectedMultiplier;

    protected AstraScroll mAstraScroll;

    protected abstract GameType getGameType();

    protected CredoGamePresenter getPresenter() {
        return PresenterHelper.getPresenter(getGameType());
    }

    protected abstract
    @SportType
    String getSportCategory();

    int balance = 0;

    protected int getRate() {
        return mAstraScroll.getValue();
    }

    protected abstract int getViewId();

    protected int getMultiplier() {
        int multiplier = ((ViewGroup) mSelectedMultiplier.getParent()).indexOfChild(mSelectedMultiplier);
        multiplier = (multiplier + 1) * 2;
        return multiplier;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewId(), container, false);
        view.findViewById(R.id.btn_next).setOnClickListener(this);

        mAstraScroll = (AstraScroll) view.findViewById(R.id.progress_bar);

        RelativeLayout multipliersContainer = (RelativeLayout) view.findViewById(R.id.multipliers);
        RadioButton[] multipliers = new RadioButton[]{
                (RadioButton) multipliersContainer.getChildAt(0),
                (RadioButton) multipliersContainer.getChildAt(1),
                (RadioButton) multipliersContainer.getChildAt(2)
        };

        mSelectedMultiplier = multipliers[0];
        mSelectedMultiplier.setChecked(true);

        for (RadioButton radioButton : multipliers) {
            radioButton.setOnClickListener(this);
        }

        return view;
    }

    @Override
    public final void onClick(View v) {
        if (v instanceof RadioButton) {
            mSelectedMultiplier.setChecked(false);

            mSelectedMultiplier = (RadioButton) v;
            mSelectedMultiplier.setChecked(true);
        }

        switch (v.getId()) {
            case R.id.btn_next:
                if (BalanceUtil.isAllowedTransaction(GameBalanceValue.get(), getActivity(), getRate())) {
                    getPresenter().setQuestionReadyListener(this);
                    getPresenter().restartGame(getMultiplier(), getRate(), getSportCategory());

                    getLoader().show();
                }
                break;
        }
    }

    @Override
    public final void questionsAreReady(boolean isSuccess) {
        getLoader().dismiss();
        if (isSuccess)
            navigateToGame();
    }

    @Override
    public final void bindToPresenter() {
        getPresenter().setStatusView(this);
    }

    @Override
    public final void unbindFromPresenter() {
        getPresenter().setStatusView(null);
        getPresenter().setQuestionReadyListener(null);
    }

    @Override
    public void updateStatus(final GameStatusResponse gameStatusResponse) {
        if (getView() != null)
            ((TextView) getView().findViewById(R.id.myPrize)).setText(getString(R.string.game_my_last_prize, BalanceUtil.balanceToString(gameStatusResponse.getBalanceChange())));

        balance = gameStatusResponse.getAverageWinValue();
    }

    @Override
    public void navigateToGame() {
        CredoActivity.startNewInstance(getActivity(), getMultiplier(), getGameType(), balance, getRate(), getSportCategory());
    }

    @Override
    public void gameStopped(@GameRequestErrorController.OnErrorAction int action) {
        getLoader().dismiss();
        if (action == GameRequestErrorController.ACTION_NO_INTERNET)
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getActivity(), R.string.error_bad_request, Toast.LENGTH_SHORT).show();
    }

//---------------
}
