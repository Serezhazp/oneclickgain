package com.clickandgain.fragments.games.credo.game;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clickandgain.R;
import com.clickandgain.activities.MainActivity;
import com.clickandgain.activities.games.CredoActivity;
import com.clickandgain.modules.games.viewController.CredoGameResultViewController;
import com.clickandgain.modules.games.viewController.ElseGameResultViewController;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.GameBalanceValue;

/**
 * Created by ovitali on 10.09.2015.
 */
public class ElseGameResultFragment extends CredoGameResultFragment implements View.OnClickListener{

    @Override
    protected CredoGameResultViewController createViewController(View view) {
        return new ElseGameResultViewController(this, view);
    }

    @Override
    protected boolean isRotatable() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_else_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ElseGameResultViewController) viewController).setVideo(mQuestionPickResponse.getVideoInfo());
    }

    /**
     * end game if ELSE doesn't contain more questions or response was incorrect
     */
    protected void nextGame() {
        if (getPresenter().getQuestion() != null && mQuestionPickResponse.isCorrect() && GameBalanceValue.get() - getActivityIntExtraValue("rate") > 0) {
            getPresenter().checkIfVideoDownloaded();
            ((CredoActivity) getActivity()).newQuestion(mBalance);
        } else {
            endGame(true);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.stream_item_user_name:
                v.setOnClickListener(null);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(Constants.PARAMS.ACTION, Constants.ACTION.NAVIGATE_TO_PROFILE);
                intent.putExtra(Constants.PARAMS.PARAMS, mQuestionPickResponse.getVideoInfo().getId());
                endGame(true);
                startActivity(intent);
                break;
        }
    }
}
