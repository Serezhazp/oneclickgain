package com.clickandgain.fragments.games.promo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clickandgain.ClickandgainApp;
import com.clickandgain.R;
import com.clickandgain.api.requests.game.PromoQuestionGetRequest;
import com.clickandgain.api.requests.game.PromoQuestionPickRequest;
import com.clickandgain.api.response.game.GameGetQuestionResponse;
import com.clickandgain.model.game.PromoQuestion;
import com.clickandgain.model.game.PromoQuestionResponse;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.views.utils.BackgroundCutter;
import com.humanet.humanetcore.views.utils.FlowMediaFacade;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.items.OkItemView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by ovitali on 19.08.2015.
 */
public class GuessFragment extends BaseTitledFragment implements View.OnClickListener, CircleLayout.OnItemSelectedListener {

    public static GuessFragment newInstance(int countryCode) {
        GuessFragment fragment = new GuessFragment();
        Bundle bundle = new Bundle(1);
        bundle.putInt("countryCode", countryCode);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getTitle() {
        return null;
    }

    private CircleView mVideoView;

    private int mYesPresses;

    private OnGuessListener mOnGuessListener;

    private CircleLayout mInfoList;

    private PromoQuestion[] mQuestions;

    private FlowMediaFacade mFlowMediaFacade;

    private Item mItems[];

    private TextView mGuessInfoTextView;

    @Override
    public final SpiceManager getSpiceManager() {
        throw new RuntimeException("Use ClickandgainApp.getGameSpiceManager() instead this spiceManager");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnGuessListener = (OnGuessListener) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guess, container, false);

        view.findViewById(R.id.btn_yes).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.btn_no).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.btn_next).setVisibility(View.INVISIBLE);

        view.findViewById(R.id.btn_yes).setOnClickListener(this);
        view.findViewById(R.id.btn_no).setOnClickListener(this);
        view.findViewById(R.id.btn_next).setOnClickListener(this);

        mVideoView = (CircleView) view.findViewById(R.id.video_view);
        mFlowMediaFacade = mVideoView.getFlowMediaFacade();

        mInfoList = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mInfoList.setOnItemSelectedListener(this);
        mInfoList.setVisibility(View.INVISIBLE);

        mGuessInfoTextView = (TextView) view.findViewById(R.id.guess_info);
        mGuessInfoTextView.setVisibility(View.INVISIBLE);
        view.findViewById(R.id.guess_info_container).setVisibility(View.INVISIBLE);

        view.findViewById(R.id.btn_next).setVisibility(View.INVISIBLE);

        ClickandgainApp.getGameSpiceManager().execute(new PromoQuestionGetRequest(getArguments().getInt("countryCode")), new QuestionGetRequestListener());

        return view;
    }

    private class QuestionGetRequestListener extends SimpleRequestListener<GameGetQuestionResponse> {

        @Override
        public void onRequestSuccess(GameGetQuestionResponse response) {
            if (getView() == null)
                return;

            mQuestions = response.getQuestions();
            if (mQuestions == null) {
                return;
            }


            getView().findViewById(R.id.btn_yes).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.btn_no).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.btn_next).setVisibility(View.VISIBLE);

            PromoQuestion question = mQuestions[0];
            if (question.isVideo())
                mFlowMediaFacade.loadVideoPreview(question.getMedia());
            else
                mFlowMediaFacade.setImage(question.getMedia());

            TextView textView = (TextView) getView().findViewById(R.id.question);
            textView.setText(question.getQuestion());

            if (question.getCover() != null) {
                DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().cacheOnDisk(true).build();
                ImageLoader.getInstance().loadImage(question.getCover(), displayImageOptions, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (!isVisible())
                            return;
                        Bitmap image = BackgroundCutter.blurImage(getView(), loadedImage);
                        UiUtil.setBackgroundDrawable(getView(), image);
                        mVideoView.refresh();
                    }
                });
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                setResult(true);
                break;

            case R.id.btn_no:
                setResult(false);
                break;

            case R.id.btn_next:
                mOnGuessListener.onGuess();
                break;
        }
    }

    private void setResult(boolean yesPressed) {

        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.GAME_PROMO);

        int answerId = yesPressed ? 1 : 0;
        mYesPresses = answerId;

        ClickandgainApp.getGameSpiceManager().execute(
                new PromoQuestionPickRequest(getArguments().getInt("countryCode"), answerId, mQuestions[0].getId()),
                new QuestionPickPromoRequestListener());

        mVideoView.disableVideoPlayback();

        getLoader().show();
    }

    private class QuestionPickPromoRequestListener extends SimpleRequestListener<PromoQuestionResponse> {
        @Override
        public void onRequestSuccess(PromoQuestionResponse questionResponse) {
            View v = getView();
            if (v == null)
                return;

            mItems = new Item[3];
            mItems[0] = new Item(questionResponse.getRightText(), questionResponse.getRightImage());
            mItems[2] = new Item(questionResponse.getWrongText(), questionResponse.getWrongImage());

            mInfoList.setAdapter(new Adapter());
            mInfoList.setSelectedItem(1);

            TextView textView = (TextView) v.findViewById(R.id.title);
            textView.setText(mYesPresses == questionResponse.getCorrect() ? R.string.guess_result_right : R.string.guess_result_wrong);
            textView.setTypeface(textView.getTypeface(), Typeface.BOLD);

            textView = (TextView) v.findViewById(R.id.guess_info);
            textView.setVisibility(View.VISIBLE);
            v.findViewById(R.id.guess_info_container).setVisibility(View.VISIBLE);

            mInfoList.setVisibility(View.VISIBLE);

            v.findViewById(R.id.btn_next).setVisibility(View.VISIBLE);

            v.findViewById(R.id.question).setVisibility(View.INVISIBLE);
            v.findViewById(R.id.btn_yes).setVisibility(View.GONE);
            v.findViewById(R.id.btn_no).setVisibility(View.GONE);

            getLoader().dismiss();
        }
    }

    @Override
    public void onItemSelected(Object data) {
        if (data == null) {
            mGuessInfoTextView.setText(R.string.guess_result_your_prize);

            PromoQuestion question = mQuestions[0];
            if (question.isVideo())
                mFlowMediaFacade.loadVideoPreview(question.getMedia());
            else
                mFlowMediaFacade.setImage(question.getMedia());

        } else {
            Item item = (Item) data;
            mGuessInfoTextView.setText(item.title);
            mFlowMediaFacade.setImage(item.imageId);
        }
    }

    public interface OnGuessListener {
        void onGuess();
    }

    //----

    private class Item {
        String title;
        String imageId;

        public Item(String title, String imageId) {
            this.title = title;
            this.imageId = imageId;
        }
    }

    //----

    private class Adapter extends BaseAdapter {
        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Item getItem(int position) {
            return mItems[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return position == 1 ? 0 : 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            OkItemView okItemView;
            if (position == 1) {
                okItemView = new OkItemView(parent.getContext());
                okItemView.setData(0, R.drawable.ic_info);
            } else {
                okItemView = new OkItemView(parent.getContext(), 2);
                Item item = getItem(position);
                String answer = getString(position == 0 ? R.string.guess_right : R.string.guess_wrong);
                okItemView.setData(answer, item.imageId);
            }

            return okItemView;
        }
    }

}
