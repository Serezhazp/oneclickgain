package com.clickandgain.fragments.games.credo.game;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.views.widgets.game.items.CredoItemView;
import com.humanet.humanetcore.model.enums.GameType;

public class CredoGameFragment extends MultiVideoGameFragment<CredoItemView>
        implements CredoItemView.OnCredoItemCheckListener {

    @Override
    protected GameType getGameType() {
        return GameType.CREDO;
    }

    private ScrollView mScrollView;

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        mScrollView = new ScrollView(container.getContext());
        mScrollView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mScrollView.setBackgroundResource(R.drawable.background_page);

        View view = inflater.inflate(R.layout.fragment_credo_base_game, mScrollView, false);
        view.setVisibility(View.INVISIBLE);
        mScrollView.addView(view);

        return mScrollView;
    }

    protected CredoItemView[] showQuestion(View view, final Question question) {
        final LinearLayout container = (LinearLayout) view.findViewById(R.id.container);
        container.setVisibility(View.VISIBLE);

        container.removeViewAt(1); // remove spacer

        final TextView questionView = (TextView) view.findViewById(R.id.question);
        //questionView.setText(R.string.game_credo_question);
        questionView.setVisibility(View.GONE);

        QuestionMedia[] medias = question.getQuestionMedias();

        final CredoItemView[] credoItemViews = new CredoItemView[medias.length];

        for (int i = 0; i < medias.length; i++) {
            credoItemViews[i] = CredoItemView.create(container, i * 2 + 1);
            credoItemViews[i].setOnCredoItemCheckListener(this);
            credoItemViews[i].setQuestionMedia(medias[i]);
        }

        container.post(new Runnable() {
            @Override
            public void run() {
                if (!isVisible()) return;
                int h = container.getMeasuredHeight();

                for (CredoItemView credoItemView : credoItemViews) {
                    credoItemView.createBackground(h);
                }

                resumeMedia();
            }
        });

        return credoItemViews;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScrollView.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScrollView.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
    }

    @Override
    public void onCredoItemChecked(CredoItemView view, QuestionMedia questionMedia) {
        if (getView() == null) {
            return;
        }

        next(questionMedia);
    }

    @Override
    public void stopMedia() {
        if (getItems() != null) {
            for (CredoItemView credoItemView : getItems()) {
                credoItemView.stop();
            }
        }
    }

    public void pauseMedia() {
        if (getItems() != null) {
            for (CredoItemView itemView : getItems()) {
                itemView.stopIfPlaying();
            }
        }
    }

    @Override
    public void resumeMedia() {
        if (getItems() != null) {
            for (CredoItemView itemView : getItems()) {
                invalidateItemPlaying(itemView);
            }
        }
    }

    @Override
    public void manageState(final int state) {

    }

    @Override
    protected void removeItemsClickListeners() {
        for (CredoItemView itemView : getItems()) {
            itemView.setOnCredoItemCheckListener(null);
        }
    }

    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener = new ViewTreeObserver.OnScrollChangedListener() {
        @Override
        public void onScrollChanged() {

            if (getItems() == null)
                return;


            for (int i = 0; i < getItems().length; i++) {
                CredoItemView itemView = getItems()[i];

                invalidateItemPlaying(itemView);
            }
        }
    };

    private void invalidateItemPlaying(CredoItemView itemView) {
        int topBound = mScrollView.getScrollY();
        int bottomBound = topBound + mScrollView.getMeasuredHeight();

        if (itemView.getY() > topBound - itemView.getHeight()
                && itemView.getY() < bottomBound
                ) {
            itemView.playIfNotPlaying();
        } else {
            itemView.stopIfPlaying();
        }

    }
}
