package com.clickandgain.fragments.games.credo.game;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.model.game.Question;
import com.clickandgain.model.game.QuestionMedia;
import com.clickandgain.modules.media.AstraMediaFacade;
import com.clickandgain.views.widgets.game.items.ElseItemView;
import com.humanet.humanetcore.model.enums.GameType;
import com.humanet.humanetcore.modules.media.IMediaEventListener;
import com.humanet.humanetcore.modules.media.MediaFacade;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;

import java.util.HashMap;

// Скролл
public class ElseGameFragment extends BaseGameFragment<ElseItemView> implements
        CircleLayout.OnItemSelectedListener, IMediaEventListener {

    private int mPlayingPosition = Integer.MIN_VALUE;

    private final HashMap<String, Bitmap> mBitmapCacheMap = new HashMap<>(6);

    private Bitmap getBitmap(String fileName) {
        return mBitmapCacheMap.get(fileName);
    }

    private ElseItemView mCheckedItem;
    private AstraMediaFacade mediaFacade;
    private YellowProgressBar mYellowProgressBar;
    private VideoSurfaceView mVideoSurfaceView;
    private View mCheckButton;
    private CircleLayout mListView;

    @Override
    protected GameType getGameType() {
        return GameType.ELSE;
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_credo_promo_game, container, false);
    }

    @Override
    protected ElseItemView[] showQuestion(final View view, final Question question) {
        final TextView questionView = (TextView) view.findViewById(R.id.question);
        //questionView.setText(R.string.game_else_question);
        questionView.setVisibility(View.GONE);

        CircleView circleView = (CircleView) view.findViewById(R.id.video_view);

        mVideoSurfaceView = circleView.getSurfaceView();
        mYellowProgressBar = circleView.getProgressBar();

        mediaFacade = new AstraMediaFacade(mVideoSurfaceView);
        mediaFacade.setMediaEventListener(this);

        mListView = (CircleLayout) view.findViewById(R.id.horizontal_list_view);

        QuestionMedia[] medias = question.getQuestionMedias();

        ElseItemView[] credoItems = new ElseItemView[medias.length];

        for (int i = 0; i < medias.length; i++) {
            ElseItemView elseItemView = ElseItemView.create(mVideoSurfaceView, mYellowProgressBar, mediaFacade);
            elseItemView.setQuestionMedia(medias[i]);
            credoItems[i] = elseItemView;
        }

        mCheckButton = view.findViewById(R.id.checkButton);
        mCheckButton.setOnClickListener(this);
        mCheckButton.setVisibility(View.INVISIBLE);

        manageState(STATE_LOADING);

        return credoItems;
    }

    @Override
    public void onVideoPlayingStarted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        manageState(STATE_PLAYING);
        if (mPlayingPosition != Integer.MIN_VALUE) {
            mediaPlayer.seekTo(mPlayingPosition);
            mPlayingPosition = Integer.MIN_VALUE;
        }
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                endGame();
                break;

            case R.id.checkButton:
                mCheckButton.setBackgroundResource(R.drawable.credo_btn_checked);
                //do the same as "next button". break unneeded
            case R.id.btn_next:
                if (mCheckedItem != null)
                    next(mCheckedItem.getQuestionMedia());
                break;
        }
    }

    @Override
    public void onVideoPlayingCompleted(final MediaFacade mediaFacade, final MediaPlayer mediaPlayer) {
        mediaFacade.replay();
    }

    @Override
    public void stopMedia() {
        mediaFacade.closeMediaPlayer();
        mVideoSurfaceView.setVisibility(View.GONE);
    }

    public void pauseMedia() {
        mediaFacade.closeMediaPlayer();
    }

    @Override
    public void manageState(final int state) {
        switch (state) {
            case STATE_LOADING:
                mYellowProgressBar.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
                mCheckButton.setVisibility(View.INVISIBLE);

                break;

            case STATE_PLAYING:
                mYellowProgressBar.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
                mVideoSurfaceView.setVisibility(View.VISIBLE);
                mCheckButton.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void resumeMedia() {
        if (getView() != null && mCheckedItem != null) {
            mVideoSurfaceView.setVisibility(View.VISIBLE);
            getView().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            mediaFacade.replay();
                        }
                    }, 100
            );
        }
    }

    @Override
    public void onVideoLoadingComplete() {
        setLocalUrlsToItems();
        getPresenter().setVideoLoadingListener(null);
    }

    @Override
    protected void setLocalUrlsToItems() {
        super.setLocalUrlsToItems();

        cacheImages();
    }

    @Override
    public void onVideoProgressChanged(final int progress) {
        mYellowProgressBar.setProgress(progress);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putInt("position", mPlayingPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(final Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState == null)
            return;

        mPlayingPosition = savedInstanceState.getInt("position");
    }

    @Override
    public void onItemSelected(Object data) {
        if (mCheckedItem != null) {
            mCheckedItem.getItemView().setBorderColor(Color.argb(255, 33, 150, 243));
            itemReloadImage(mCheckedItem);
        }

        mCheckedItem = (ElseItemView) data;
        mCheckedItem.getItemView().setBorderColor(Color.WHITE);
        itemReloadImage(mCheckedItem);

        mediaFacade.play(mCheckedItem.getLocalUrl());
    }

    private ElseItemView.ItemView itemReloadImage(ElseItemView item) {
        ElseItemView.ItemView view = item.getItemView();

        String localUrl = item.getLocalUrl();

        String fileName = FilePathHelper.getFileName(localUrl);
        Bitmap bitmap = getBitmap(fileName);

        view.setPreview(bitmap);

        return view;
    }

    private class Adapter extends BaseAdapter {

        public Adapter() {
        }

        @Override
        public int getCount() {
            return getItems().length;
        }

        @Override
        public ElseItemView getItem(int position) {
            return getItems()[position];
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getQuestionMedia().getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ElseItemView item = getItem(position);
            return itemReloadImage(item);
        }
    }

    private void cacheImages() {
        if (mImageCacheThread == null) {
            mImageCacheThread = new ImageCacheThread();
            mImageCacheThread.setListView(mListView);
            mImageCacheThread.start();
        } else {
            mImageCacheThread.setListView(mListView);
        }
    }

    @Override
    public void onDestroy() {
        mBitmapCacheMap.clear();
        super.onDestroy();
    }

    private ImageCacheThread mImageCacheThread;

    //--------------
    private class ImageCacheThread extends Thread {

        private CircleLayout mListView;

        public void setListView(final CircleLayout listView) {
            mListView = listView;
        }

        @Override
        public void run() {
            Bitmap bitmap;
            int n = getPresenter().getQuestion().getQuestionMedias().length;
            for (int i = 0; i < n; i++) {

                // return to avoid crash - if "end game" was pressed current question will be null
                if (getPresenter().getQuestion() == null)
                    return;

                String localUrl = getPresenter().getVideoUrl(i);
                try {
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(localUrl);
                    bitmap = mediaMetadataRetriever.getFrameAtTime();
                    if (bitmap == null)
                        bitmap = mediaMetadataRetriever.getFrameAtTime(1000);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    bitmap = ThumbnailUtils.createVideoThumbnail(localUrl, MediaStore.Images.Thumbnails.MICRO_KIND);
                }
                if (bitmap != null && bitmap.getWidth() > 0) {

                    //crop to square
                    if (bitmap.getWidth() != bitmap.getHeight()) {
                        int size = Math.min(bitmap.getWidth(), bitmap.getHeight());
                        bitmap = Bitmap.createBitmap(bitmap, (bitmap.getWidth() - size) / 2, (bitmap.getHeight() - size) / 2, size, size);
                    }
                    // scale to 100x100
                    bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);

                    mBitmapCacheMap.put(FilePathHelper.getFileName(localUrl), bitmap);
                }
            }

            if (!isVisible()) {
                return;
            }
            mListView.post(new Runnable() {
                @Override
                public void run() {
                    manageState(STATE_PLAYING);

                    mListView.setOnItemSelectedListener(ElseGameFragment.this);
                    mListView.setAdapter(new Adapter());
                    mListView.setSelectedItem(getItems().length / 2);
                }
            });

            mImageCacheThread = null;
        }
    }

    @Override
    protected void removeItemsClickListeners() {
        mCheckButton.setEnabled(false);
    }
}
