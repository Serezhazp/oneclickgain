package com.clickandgain.fragments.games.credo.tutorials;

import com.clickandgain.R;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.model.enums.GameType;

/**
 * Created by ovitali on 09.09.2015.
 */
public class ElseGameTutorialFragment extends BaseCredoTutorialFragment implements IMenuBindFragment {

    public static ElseGameTutorialFragment newInstance() {
        return new ElseGameTutorialFragment();
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_else);
    }

    @Override
    protected GameType getGameType() {
        return GameType.ELSE;
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_else_tutorial;
    }

    @Override
    public int getSelectedMenuItem() {
        return R.id.else_game;
    }

    @Override
    protected String getSportCategory() {
        return null;
    }

}
