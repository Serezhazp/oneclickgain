package com.clickandgain.fragments.games.credo.game;

import android.os.Bundle;
import android.view.View;

import com.clickandgain.views.widgets.game.items.BaseCredoItem;

abstract class MultiVideoGameFragment<T extends BaseCredoItem> extends BaseGameFragment<T> implements
        View.OnClickListener {


    private int mCurrentState;

    @Override
    public void onVideoProgressChanged(final int progress) {
        for (T credoItemView : getItems()) {
            credoItemView.onPublishProgress(progress);
        }
    }

    @Override
    public void onVideoLoadingComplete() {
        getPresenter().setVideoLoadingListener(null);
        setLocalUrlsToItems();
        resumeMedia();
    }

    @Override
    public void manageState(final int state) {
        mCurrentState = state;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        if (mCurrentState == STATE_PLAYING) {
            int[] positions = new int[getItems().length];
            for (int i = 0; i < getItems().length; i++) {
                positions[i] = getItems()[i].getCurrentPosition();
            }
            outState.putIntArray("position", positions);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(final Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState == null)
            return;

        int[] positions = savedInstanceState.getIntArray("position");
        if (positions != null) {
            for (int i = 0; i < getItems().length; i++) {
                getItems()[i].setCurrentPosition(positions[i]);
            }
        }
    }
}
