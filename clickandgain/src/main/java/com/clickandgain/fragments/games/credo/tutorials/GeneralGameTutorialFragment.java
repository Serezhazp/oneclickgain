package com.clickandgain.fragments.games.credo.tutorials;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.clickandgain.R;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.model.enums.GameType;

/**
 * Created by ovitali on 09.09.2015.
 */
public class GeneralGameTutorialFragment extends BaseCredoTutorialFragment implements IMenuBindFragment {

    public static GeneralGameTutorialFragment newInstance() {
        return new GeneralGameTutorialFragment();
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_general_game);
    }

    @Override
    protected GameType getGameType() {
        return GameType.GENERAL_GAME;
    }

    @Override
    protected int getViewId() {
        return R.layout.fragment_credo_tutorial;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.game_general_game_title);
    }

    @Override
    public int getSelectedMenuItem() {
        return R.id.general_game;
    }

    @Override
    protected String getSportCategory() {
        View view = getView();
        assert view != null;

        return ((SportCategoryPickerView) view.findViewById(R.id.sportCategoryPicker)).getSelectedCategory();
    }

}
