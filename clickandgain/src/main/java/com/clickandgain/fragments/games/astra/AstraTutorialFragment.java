package com.clickandgain.fragments.games.astra;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.clickandgain.R;
import com.clickandgain.activities.games.AstraActivity;
import com.clickandgain.api.GameRequestErrorController;
import com.clickandgain.api.response.game.GameStatusResponse;
import com.clickandgain.fragments.games.BaseTutorialFragment;
import com.clickandgain.modules.games.events.QuestionReadyListener;
import com.clickandgain.modules.games.presenters.AstraPresenter;
import com.clickandgain.views.widgets.game.AstraScroll;
import com.clickandgain.views.widgets.game.SportCategoryPickerView;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.modules.BalanceUtil;

public class AstraTutorialFragment extends BaseTutorialFragment implements View.OnClickListener, QuestionReadyListener, IMenuBindFragment {

    public static AstraTutorialFragment newInstance() {
        return new AstraTutorialFragment();
    }

    public AstraPresenter mAstraPresenter;

    private float mScore = 0;

    private GameStatusResponse mGameStatusResponse;

    @Override
    public String getTitle() {
        return getString(R.string.menu_astra);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_astra_tutorial, container, false);
        view.findViewById(R.id.btn_next).setOnClickListener(this);

        AstraScroll astraScroll = (AstraScroll) view.findViewById(R.id.progress_bar);
        astraScroll.setValue(10);

        return view;
    }

    @Override
    public void bindToPresenter() {
        if (mAstraPresenter == null) {
            mAstraPresenter = AstraPresenter.getInstance();
        }
        mAstraPresenter.setStatusView(this);

    }

    @Override
    public void unbindFromPresenter() {
        mAstraPresenter.setStatusView(null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:

                if (BalanceUtil.isAllowedTransaction(GameBalanceValue.get(), getActivity(), 10))
                    navigateToGame();

                break;
        }
    }

    @Override
    public void navigateToGame() {
        int countOfPlayedGames = mGameStatusResponse != null ? mGameStatusResponse.getCount() : 0;
        countOfPlayedGames = countOfPlayedGames % 10;
        mAstraPresenter.startGame(countOfPlayedGames, getSportCategory());
        mAstraPresenter.setQuestionReadyListener(this);
        getLoader().show();
    }

    @Override
    public void gameStopped(@GameRequestErrorController.OnErrorAction int action) {
        getLoader().dismiss();
        if (action == GameRequestErrorController.ACTION_NO_INTERNET)
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getActivity(), R.string.error_bad_request, Toast.LENGTH_SHORT).show();
    }

    /**
     * redirect to game screen when question will ready
     */
    @Override
    public void questionsAreReady(boolean isSuccess) {
        getLoader().dismiss();
        if (isSuccess)
            AstraActivity.startNewInstance(getActivity(), mScore, getSportCategory());
    }

    @Override
    public void updateStatus(final GameStatusResponse gameStatusResponse) {
        View view = getView();
        if (view == null) {
            return;
        }

        mScore = gameStatusResponse.getBalanceChange();

        ((TextView) view.findViewById(R.id.averagePrize)).setText(getString(R.string.game_last_prize, gameStatusResponse.getAverageWinValue()));
        ((TextView) view.findViewById(R.id.myPrize)).setText(getString(R.string.game_my_last_prize,
                BalanceUtil.balanceToString(gameStatusResponse.getBalanceChange())));

        mGameStatusResponse = gameStatusResponse;
    }

    @Override
    public int getSelectedMenuItem() {
        return R.id.astra;
    }

    @SportCategoryPickerView.SportType
    private String getSportCategory() {
        View view = getView();
        assert view != null;

        return ((SportCategoryPickerView) view.findViewById(R.id.sportCategoryPicker)).getSelectedCategory();
    }


}