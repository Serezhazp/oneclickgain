package com.clickandgain.fragments.games.credo.game;

/**
 * Created by ovitali on 01.12.2015.
 */
interface IChangeStateBehaviour {
    void manageState(int state);

    void resumeMedia();

    void stopMedia();

    void pauseMedia();
}
