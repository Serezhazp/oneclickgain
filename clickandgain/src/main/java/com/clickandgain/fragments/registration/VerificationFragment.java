package com.clickandgain.fragments.registration;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.clickandgain.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.fragments.registration.BaseVerificationFragment;

/**
 * Created by ovi on 2/2/16.
 */
public class VerificationFragment extends BaseVerificationFragment {

    public static VerificationFragment newInstance() {
        return new VerificationFragment();
    }

    private VideoView mVideoBackgroundView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verification, container, false);

        mVideoBackgroundView = (VideoView) view.findViewById(R.id.backgroundVideo);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mVideoBackgroundView.setVisibility(View.VISIBLE);
        mVideoBackgroundView.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.bg_video));
        mVideoBackgroundView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);

                float scaleX = (float) mp.getVideoWidth() / (float) App.WIDTH;
                float scaleY = (float) mp.getVideoHeight() / (float) App.HEIGHT;

                float width;
                float height;

                float scale = Math.min(scaleX, scaleY);

                width = mp.getVideoWidth() / scale;
                height = mp.getVideoHeight() / scale;

                mVideoBackgroundView.getLayoutParams().width = (int) width;
                mVideoBackgroundView.getLayoutParams().height = (int) height;

                mVideoBackgroundView.setX(-(width - App.WIDTH) / 2);
                mVideoBackgroundView.setY(-(height - App.HEIGHT) / 2);

                mVideoBackgroundView.start();
            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        mVideoBackgroundView.stopPlayback();
        mVideoBackgroundView.setVisibility(View.GONE);
    }
}
