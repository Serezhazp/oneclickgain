package com.clickandgain.fragments.registration;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clickandgain.R;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.items.OkItemView;

public class OkFragment extends BaseTitledFragment implements View.OnClickListener {

    private int mAction;

    private String mShareUrl;
    private int mVideoId;

    public static OkFragment newInstance(int action, String shareUrl, int videoId) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_ACTION, action);
        args.putString("shareUrl", shareUrl);
        args.putInt("videoId", videoId);
        fragment.setArguments(args);
        return fragment;
    }

    public static OkFragment newInstance(int action) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }

    private OnCongratulationListener mOpenVerificationListener;

    public OkFragment() {
    }

    private Item[] mItems = new Item[]{
            new Item(R.string.menu_astra, R.drawable.ic_astra),
            new Item(R.string.menu_credo, R.drawable.ic_credo),
            new Item(R.string.menu_info, R.drawable.ic_info),
            new Item(R.string.menu_general_game, R.drawable.ic_general_game),
            new Item(R.string.menu_else, R.drawable.ic_else),
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ok, container, false);

        mAction = getArguments().getInt(Constants.EXTRA_ACTION);
        switch (mAction) {
            case Constants.CongratulationsType.PROFILE_OK:
                break;
        }
        view.findViewById(R.id.btn_next).setOnClickListener(this);

        final TextView congratulationTextView = (TextView) view.findViewById(R.id.congratulationTextView);


        CircleLayout circleView = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        circleView.setAdapter(new Adapter());
        circleView.setSelectedItem(mItems.length / 2);
        circleView.setOnItemSelectedListener(new CircleLayout.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Object data) {
                Item selectedItem = (Item) data;

                if (selectedItem.title == R.string.menu_astra) {
                    congratulationTextView.setText(R.string.game_astra_title);

                } else if (selectedItem.title == R.string.menu_general_game) {
                    congratulationTextView.setText(R.string.game_general_game_title);

                } else if (selectedItem.title == R.string.menu_else) {
                    congratulationTextView.setText(R.string.game_else_title);

                } else if (selectedItem.title == R.string.menu_credo) {
                    congratulationTextView.setText(R.string.game_credo_title);

                } else if (selectedItem.title == R.string.menu_info) {
                    congratulationTextView.setText(R.string.ok_description);

                }
            }
        });

        return view;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_next) {
            nextScreen();

        }
    }

    private void nextScreen() {
        switch (mAction) {
            case Constants.CongratulationsType.PROFILE_OK:
                mOpenVerificationListener.onRegistrationProcedureComplete();
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOpenVerificationListener = (OnCongratulationListener) activity;
    }

    public interface OnCongratulationListener {
        void onRegistrationProcedureComplete();
    }


    private class Adapter extends BaseAdapter {
        @Override
        public int getCount() {
            return mItems.length;
        }

        @Override
        public Item getItem(int position) {
            return mItems[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            OkItemView okItemView;
            if (convertView == null) {
                okItemView = new OkItemView(parent.getContext());
            } else {
                okItemView = (OkItemView) convertView;
            }

            Item item = getItem(position);
            okItemView.setData(item.title, item.imageId);

            return okItemView;
        }
    }

    private class Item {
        int title;
        int imageId;

        public Item(int title, int imageId) {
            this.title = title;
            this.imageId = imageId;
        }
    }


}
