package com.clickandgain.api.requests;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.requests.database.CitiesRequest;
import com.humanet.humanetcore.api.sets.DatabaseApiSet;
import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.model.Country;

/**
 * Created by ovi on 2/12/16.
 */
public class DatabaseSetApiTest extends BaseApiTestCase<DatabaseApiSet> {

    private static final boolean ENABLED_ALL = true;

    private static final boolean ENABLED_GET_COUNTRIES = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_CITY = ENABLED_ALL | false;

    public DatabaseSetApiTest() {
        super(DatabaseApiSet.class);
    }

    public void testGetCountries() {
        if (!ENABLED_GET_COUNTRIES) return;

        Country.Response response = getApiSet().getCountries(App.API_APP_NAME, new ArgsMap());

        assertNotNull(response);
        assertNull(response.getErrorsMessage());

        Country[] countries = response.getCountries();
        assertNotNull(countries);
        assertTrue(countries.length > 0);
    }

    public void testGetCities() {
        if (!ENABLED_GET_CITY) return;

        final String checkingCityName = "Москв";

        City.Response response = getApiSet().getCities(App.API_APP_NAME, CitiesRequest.buildRequestParams(1, checkingCityName));

        assertNotNull(response);
        assertNull(response.getErrorsMessage());

        City[] cities = response.getCities();
        assertNotNull(cities);
        boolean cityFound = false;

        for (City city : cities) {
            assertNotNull(city);
            assertTrue(city.getId() != 0);
            assertNotNull(city.getTitle());

            if (city.getTitle().contains(checkingCityName)) {
                cityFound = true;
            }
        }

        assertTrue("cities response does not contains city that starts with " + checkingCityName, cityFound);

    }

}
