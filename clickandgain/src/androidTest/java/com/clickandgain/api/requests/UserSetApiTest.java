package com.clickandgain.api.requests;

import android.content.IntentFilter;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.user.AuthResponse;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.api.response.user.UserSearchResponse;
import com.humanet.humanetcore.api.response.user.VerifyCodeResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.events.SmsReceiverListener;
import com.humanet.humanetcore.model.UserFinded;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.selectable.Education;
import com.humanet.humanetcore.model.enums.selectable.Hobby;
import com.humanet.humanetcore.model.enums.selectable.Interest;
import com.humanet.humanetcore.model.enums.selectable.Job;
import com.humanet.humanetcore.model.enums.selectable.Pet;
import com.humanet.humanetcore.model.enums.selectable.Religion;
import com.humanet.humanetcore.model.enums.selectable.Sport;
import com.humanet.humanetcore.receivers.SmsReceiver;
import com.humanet.humanetcore.utils.PrefHelper;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by ovi on 2/10/16.
 */
public class UserSetApiTest extends BaseApiTestCase<UserApiSet> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_CHECK_TOKEN = ENABLED_ALL | false;
    private static final boolean ENABLED_GET_USER = ENABLED_ALL | false;
    private static final boolean ENABLED_EDIT = ENABLED_ALL | false;
    private static final boolean ENABLED_CHECK_BALANCE = ENABLED_ALL | false;
    private static final boolean ENABLED_SEARCH = ENABLED_ALL | false;
    private static final boolean ENABLED_FOLLOW_UNFOLLOW = ENABLED_ALL | false;
    private static final boolean ENABLED_AUTH = ENABLED_ALL | false;


    private String mTestName;

    public UserSetApiTest() {
        super(UserApiSet.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mTestName = "testName" + System.currentTimeMillis();
    }

    public void testCheckToken() {
        if (!ENABLED_CHECK_TOKEN) return;

        String token = PrefHelper.getToken();
        assertNotNull(token);
    }

    public void testGetUser() throws Exception {
        if (!ENABLED_GET_USER) return;

        UserInfo.UserResponse userResponse = getApiSet().getUserInfo(App.API_APP_NAME, new ArgsMap());

        assertNotNull(userResponse);

        UserInfo userInfo = userResponse.getUserInfo();
        assertNotNull(userInfo);

        assertNotNull(userInfo.getId() != 0);
        assertNotNull(userInfo.getAvatar());
        assertNotNull(userInfo.getAvatarSmall());
        assertNotNull(userInfo.getCountry());
        assertNotNull(userInfo.getCity());
    }

    public void testEditUser() {
        if (!ENABLED_EDIT) return;

        final int INDEX = 2;
        final String QUESTIONNAIRE_ID = String.valueOf(INDEX);


        UserInfo.UserResponse userResponse = restAdapter.create(UserApiSet.class).getUserInfo(App.API_APP_NAME, new ArgsMap());

        UserInfo userInfo = userResponse.getUserInfo();
        userInfo.setFName(mTestName);
        userInfo.setLName(mTestName);
        userInfo.setHobbie(Hobby.getById(QUESTIONNAIRE_ID));
        userInfo.setSport(Sport.getById(QUESTIONNAIRE_ID));
        userInfo.setPet(Pet.getById(QUESTIONNAIRE_ID));
        userInfo.setInterest(Interest.getById(QUESTIONNAIRE_ID));
        userInfo.setReligion(Religion.getById(QUESTIONNAIRE_ID));

        userInfo.setJobType(Job.getById(QUESTIONNAIRE_ID));
        userInfo.setDegree(Education.getById(QUESTIONNAIRE_ID));

        userInfo.setCountryId(1);
        userInfo.setCityId(1);

        userInfo.setJobTitle("craft");

        userInfo.setBirthDate(INDEX);

        //---
        BaseResponse editResponse = getApiSet().editUserInfo(App.API_APP_NAME, EditUserInfoRequest.userInfoToArgsMap(userInfo));
        //---
        assertNotNull(editResponse);

        userResponse = getApiSet().getUserInfo(App.API_APP_NAME, new ArgsMap());
        userInfo = userResponse.getUserInfo();

        assertEquals(mTestName, userInfo.getFName());
        assertEquals(mTestName, userInfo.getLName());
        assertEquals(INDEX, userInfo.getBirthDate());

        assertEquals(userInfo.getHobbie().getId(), INDEX);
        assertEquals(userInfo.getSport().getId(), INDEX);
        assertEquals(userInfo.getPet().getId(), INDEX);
        assertEquals(userInfo.getInterest().getId(), INDEX);
        assertEquals(userInfo.getReligion().getId(), INDEX);

        assertEquals(userInfo.getJobType().getId(), INDEX);
        assertEquals(userInfo.getDegree().getId(), INDEX);

        assertNotSame(userInfo.getJobTitle(), "craft");

        assertEquals(userInfo.getCityId(), 1);
        assertEquals(userInfo.getCountryId(), 1);

    }


    public void testBalance() {
        if (!ENABLED_CHECK_BALANCE) return;

        UserInfo.UserResponse userResponse = getApiSet().getUserInfo(App.API_APP_NAME, new ArgsMap());
        UserInfo userInfo = userResponse.getUserInfo();

        CoinsHistoryListResponse coinsHistoryListResponse = restAdapter.create(UserApiSet.class).getCoinsHistory(App.API_APP_NAME, new ArgsMap(true));

        assertNotNull("coinsHistoryListResponse is null", coinsHistoryListResponse);

        assertEquals(userInfo.getBalance(), coinsHistoryListResponse.getBalance());

        if (userInfo.getBalance() != 0) {
            assertNotNull("coinsHistoryList con not be null", coinsHistoryListResponse.getHistory());
            assertNotSame("coinsHistoryList con not be empty", coinsHistoryListResponse.getHistory().size(), 0);
        }
    }

    public void testSearch() {
        if (!ENABLED_SEARCH) return;

        UserSearchResponse userSearchResponse = getApiSet().searchUser(App.API_APP_NAME, new ArgsMap());

        assertNotNull(userSearchResponse);

        List<UserFinded> foundUsersList = userSearchResponse.getUsers();

        assertNotNull(foundUsersList);
        assertNotSame("must me at least one user", foundUsersList.size(), 0);

        for (UserFinded userFinded : foundUsersList) {

            assertNotNull(userFinded);
            assertTrue(userFinded.getId() != 0);
            assertNotNull(userFinded.getAvatarSmall());
            assertNotNull(userFinded.getFirstName());
            assertNotNull(userFinded.getLastName());
            assertNotNull(userFinded.getLang());

            assertNotNull(userFinded.getCity());
            assertNotNull(userFinded.getCountry());
        }
    }


    public void testFollowUnFollow() {
        if (!ENABLED_FOLLOW_UNFOLLOW) return;


        UserSearchResponse userSearchResponse = getApiSet().searchUser(App.API_APP_NAME, new ArgsMap());

        assertNotNull(userSearchResponse);

        List<UserFinded> foundUsersList = userSearchResponse.getUsers();

        for (UserFinded userFinded : foundUsersList) {
            int userId = userFinded.getId();
            if (userId == AppUser.getInstance().getUid())
                continue;

            ArgsMap argsMap = new ArgsMap();
            argsMap.put("id_user", userId);

            // follow user
            BaseResponse response = getApiSet().followUser(App.API_APP_NAME, argsMap);

            assertNotNull(response);

            // check relationship
            UserInfo.UserResponse userResponse = getApiSet().getUserInfo(App.API_APP_NAME, argsMap);

            assertTrue(userResponse.getUserInfo().getRelationship() == 1);


            //unfollow user
            response = getApiSet().unfollowUser(App.API_APP_NAME, argsMap);
            assertNotNull(response);

            // check relationship
            userResponse = getApiSet().getUserInfo(App.API_APP_NAME, argsMap);

            assertTrue(userResponse.getUserInfo().getRelationship() == 0);

            break;
        }
    }


    private String mSmsCode;

    private CountDownLatch mCountDownLatch;

    public void testAuth() {
        if (!ENABLED_AUTH) return;

        ArgsMap map = new ArgsMap(false);
        map.put("token", "");
        map.put("os", "android");

        AuthResponse authResponse = getApiSet().auth(App.API_APP_NAME, map);

        assertNotNull(authResponse);

        String token = authResponse.getToken();
        assertNotNull(token);

        PrefHelper.setStringPref(Constants.PREF.TOKEN, token);

        App.getInstance().registerReceiver(new SmsReceiver(
                new SmsReceiverListener() {
                    @Override
                    public void sendVerificationCode(CharSequence code) {
                        mSmsCode = code.toString();

                        mCountDownLatch.countDown();
                    }
                }
        ), new IntentFilter(SmsReceiver.SMS_RECEIVED));

        map.put("phone", "380997746073");

        BaseResponse baseResponse = getApiSet().getSmsCode(App.API_APP_NAME, map);
        assertNotNull(baseResponse);

        mCountDownLatch = new CountDownLatch(1);

        try {
            mCountDownLatch.await(10000, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        assertNotNull("sms code can not be null", mSmsCode);

        map = new ArgsMap(true);
        map.put("code", mSmsCode);
        VerifyCodeResponse verifyCodeResponse = getApiSet().verifySmsCode(App.API_APP_NAME, map);

        assertNull(verifyCodeResponse.getErrorsMessage());
        assertTrue("userId can not be null", verifyCodeResponse.getUserId() != 0);
        assertTrue("there is existing user, status can not be 0", verifyCodeResponse.getStatus() != 0);
    }


}
