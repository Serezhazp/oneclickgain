package com.clickandgain.api.requests;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.requests.database.CategoriesRequest;
import com.humanet.humanetcore.api.response.BaseCoinsResponse;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.video.CommentsResponse;
import com.humanet.humanetcore.api.response.video.FlowResponse;
import com.humanet.humanetcore.api.sets.FlowApiSet;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.humanet.humanetcore.events.OnUpdateVideoListener;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Comment;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovi on 2/11/16.
 */
public class VideoSetApiTest extends BaseApiTestCase<VideoApiSet> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_VIDEO_CATEGORIES = ENABLED_ALL | false;
    private static final boolean ENABLED_LIKES = ENABLED_ALL | false;
    private static final boolean ENABLED_COMMENTS = ENABLED_ALL | false;
    private static final boolean ENABLED_BUYING_IN_MARKET = ENABLED_ALL | false;

    public VideoSetApiTest() {
        super(VideoApiSet.class);
    }

    public void testVideoCategoriesApi() {
        if (!ENABLED_VIDEO_CATEGORIES) return;

        VideoType[] videoTypes = VideoType.values();

        for (VideoType videoType : videoTypes) {
            ArgsMap map = new ArgsMap();
            map.put("type", "full");
            map.put(CategoriesRequest.VIDEO_TYPE_PARAM, videoType.getRequestParam());
            if (videoType.getAdditionParam() != null) {
                map.put(CategoriesRequest.PARAMS, videoType.getAdditionParam());
            }
            Category.Response response = restAdapter.create(FlowApiSet.class).getCategories(App.API_APP_NAME, map);

            assertNotNull(response);
            assertNull(response.getErrorsMessage());

            Category[] categories = response.getCategories();

            assertNotNull(categories);
            assertTrue(response.getCategories().length != 0);

            for (Category category : categories) {

                assertNotNull(category.getTitle());
                assertNotNull(category.getParams());

                if (category.getVideosCount() > 0) {
                    assertNotNull(category.getImage());

                    map = new ArgsMap();
                    map.putAll(category.getParams());
                    FlowResponse flowResponse = restAdapter.create(FlowApiSet.class).getFlow(App.API_APP_NAME, map);

                    assertNotNull(flowResponse);
                    assertNull(flowResponse.getErrorsMessage());


                    ArrayList<Video> videos = flowResponse.getFlow();
                    assertNotNull(videos);
                    assertTrue(videos.size() != 0);

                    for (Video video : videos) {
                        assertNotNull(video.getAuthorName());

                        assertTrue(video.getVideoId() != 0);
                        assertNotNull(getVideoErrorMessage(category, "getAuthorId"), video.getAuthorId());
                        assertNotNull(getVideoErrorMessage(category, "getAuthorName"), video.getAuthorName());
                        assertNotNull(getVideoErrorMessage(category, "getThumbUrl"), video.getThumbUrl());
                        assertNotNull(getVideoErrorMessage(category, "getUrl"), video.getUrl());
                        assertNotNull(getVideoErrorMessage(category, "getShortUrl"), video.getShortUrl());
                        assertNotNull(getVideoErrorMessage(category, "getCity"), video.getCity());
                        assertNotNull(getVideoErrorMessage(category, "getCountry"), video.getCountry());
                    }
                }
            }
        }
    }

    public void testLikes() {
        if (!ENABLED_LIKES) return;

        Video video = getVideo();

        assertNotNull(video);

        // like video
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", video.getVideoId());
        map.put("mark", OnUpdateVideoListener.LIKED);
        BaseResponse baseResponse = getApiSet().markVideo(App.API_APP_NAME, map);

        assertNotNull(baseResponse);
        assertNotNull(baseResponse.getErrorMessages());

        video = getVideo();
        assertNotNull(video);

        assertTrue(video.getMyMark() == OnUpdateVideoListener.LIKED);

        //unlike video
        map = new ArgsMap(true);
        map.put("id_video", video.getVideoId());
        map.put("mark", OnUpdateVideoListener.UN_LIKED);
        baseResponse = getApiSet().markVideo(App.API_APP_NAME, map);

        assertNotNull(baseResponse);
        assertNotNull(baseResponse.getErrorMessages());

        video = getVideo();
        assertNotNull(video);

        assertTrue(video.getMyMark() == OnUpdateVideoListener.UN_LIKED);

    }

    public void testComments() {
        if (!ENABLED_COMMENTS) return;


        Video video = getVideo();

        String newCommentText = "test comment " + System.currentTimeMillis();

        // add new comment
        ArgsMap map = new ArgsMap();
        map.put("id_video", video.getVideoId());
        map.put("comment", newCommentText);
        BaseResponse baseResponse = getApiSet().commentVideo(App.API_APP_NAME, map);

        assertNotNull(baseResponse);
        assertNull(baseResponse.getErrorsMessage());


        // check if comment list contains new added comment
        map = new ArgsMap();
        map.put("id_video", video.getVideoId());
        CommentsResponse commentsResponse = getApiSet().getComments(App.API_APP_NAME, map);

        assertNotNull(commentsResponse);
        assertNull(commentsResponse.getErrorsMessage());

        Comment[] comments = commentsResponse.getComments();
        assertNotNull(comments);
        assertTrue("Must be at least one comment", comments.length != 0);

        boolean commentFound = false;
        for (Comment comment : comments) {
            assertNotNull(comment);

            assertNotNull(comment.getAuthor());
            assertTrue(comment.getAuthorId() != 0);
            assertNotNull(comment.getAvatarUrl());
            assertNotNull(comment.getText());
            assertTrue(comment.getTime() != 0);

            if (comment.getText().equals(newCommentText)) {
                commentFound = true;
            }
        }

        assertTrue("new added comment not found in comment list", commentFound);
    }

    public void testBuyingVideo() {
        if (!ENABLED_BUYING_IN_MARKET) return;

        VideoType[] videoTypes = {
                VideoType.MARKET_SELL,
                VideoType.MARKET_SHARE
        };

        Video availableMarketVideo = null;
        Category availableVideoCategory = null;

        for (VideoType videoType : videoTypes) {
            ArgsMap map = new ArgsMap();
            map.put("type", "full");
            map.put(CategoriesRequest.VIDEO_TYPE_PARAM, videoType.getRequestParam());
            if (videoType.getAdditionParam() != null) {
                map.put(CategoriesRequest.PARAMS, videoType.getAdditionParam());
            }
            Category.Response response = restAdapter.create(FlowApiSet.class).getCategories(App.API_APP_NAME, map);

            assertNotNull(response);
            assertNull(response.getErrorsMessage());

            Category[] categories = response.getCategories();

            for (Category category : categories) {

                if (category.getVideosCount() > 0) {

                    map = new ArgsMap();
                    map.putAll(category.getParams());
                    FlowResponse flowResponse = restAdapter.create(FlowApiSet.class).getFlow(App.API_APP_NAME, map);

                    ArrayList<Video> videos = flowResponse.getFlow();


                    for (Video video : videos) {
                        assertNotNull(getVideoErrorMessage(category, "isAvailable"), video.isAvailable());

                        if (video.isAvailable()) {
                            availableMarketVideo = video;
                            availableVideoCategory = category;
                            break;
                        }
                    }

                }
            }
        }

        if (availableMarketVideo == null) {
            fail("available for buying video not found in market");
        } else {
            ArgsMap argsMap = new ArgsMap();

            argsMap.put("id_video", availableMarketVideo.getVideoId());
            BaseCoinsResponse coinsResponse = getApiSet().payForVideo(App.API_APP_NAME, argsMap);

            assertNotNull(coinsResponse);
            assertNull(coinsResponse.getErrorsMessage());

            assertTrue(coinsResponse.getCoinsChange() == -availableMarketVideo.getCost());

            argsMap = new ArgsMap();
            argsMap.putAll(availableVideoCategory.getParams());
            FlowResponse flowResponse = restAdapter.create(FlowApiSet.class).getFlow(App.API_APP_NAME, argsMap);

            boolean availableVideoFoundInNewRequest = false;

            List<Video> videos = flowResponse.getFlow();
            for (Video video : videos) {
                if (video.getVideoId() == availableMarketVideo.getVideoId()) {
                    availableVideoFoundInNewRequest = true;
                    assertFalse("bought video still available in market", video.isAvailable());
                }
            }

            if (!availableVideoFoundInNewRequest) {
                fail("video not found after buying");
            }

        }
    }

    public void testShare() {
        Video video = getVideo();

        ArgsMap map = new ArgsMap();
        map.put("numbers", "380997746073");
        map.put("id_video", video.getVideoId());

        BaseResponse baseResponse = getApiSet().shareVideo(App.API_APP_NAME, map);

        assertNotNull(baseResponse);
        assertNull(baseResponse.getErrorsMessage());
    }

    public Video getVideo() {
        ArgsMap map = new ArgsMap();
        map.put("type", "full");
        map.put("id_user", AppUser.getInstance().getUid());

        FlowResponse flowResponse = restAdapter.create(FlowApiSet.class).getFlow(App.API_APP_NAME, map);

        return flowResponse.getFlow().get(0);
    }

    private String getVideoErrorMessage(Category category, String value) {
        return String.format("Video value '%s' in category '%s'", value, category.getTitle());
    }

}
