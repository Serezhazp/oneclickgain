package com.clickandgain.api.requests;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.requests.message.AddMessageRequest;
import com.humanet.humanetcore.api.requests.message.GetMessagesRequest;
import com.humanet.humanetcore.api.response.feedback.AddMessageResponse;
import com.humanet.humanetcore.api.response.feedback.MessageListsResponse;
import com.humanet.humanetcore.api.response.feedback.MessagesResponse;
import com.humanet.humanetcore.api.sets.FeedbackRestApi;
import com.humanet.humanetcore.model.Feedback;
import com.humanet.humanetcore.model.FeedbackMessage;

import java.util.List;

/**
 * Created by ovi on 2/12/16.
 */
public class FeedbackSetApiTest extends BaseApiTestCase<FeedbackRestApi> {

    private static final boolean ENABLED_ALL = false;

    private static final boolean ENABLED_GET_LIST = ENABLED_ALL | false;
    private static final boolean ENABLED_ADD_NEW_LIST = ENABLED_ALL | false;
    private static final boolean ENABLED_ADD_NEW_MESSAGE_TO_LIST = ENABLED_ALL | false;

    public FeedbackSetApiTest() {
        super(FeedbackRestApi.class);
    }

    public void testGetList() {
        Log.i("FeedbackSetApiTest", "testGetList");
        if (!ENABLED_GET_LIST) return;

        MessageListsResponse response = getApiSet().getMessageLists(App.API_APP_NAME, new ArgsMap());
        assertNotNull(response);
        assertNull(response.getErrorsMessage());

        List<Feedback> feedbackList = response.getLists();

        if (feedbackList != null) {
            for (Feedback feedback : feedbackList) {
                assertNotNull(feedback);
                assertTrue(feedback.getCreated() != 0);

                assertNotNull(feedback.getLastMessage());

                assertTrue(feedback.getType() == Feedback.COMPLAIN
                        || feedback.getType() == Feedback.ERROR
                        || feedback.getType() == Feedback.MOTION);

                MessagesResponse messagesResponse = getApiSet().getMessages(App.API_APP_NAME, GetMessagesRequest.buildRequestArgs(feedback.getId()));

                assertNotNull(messagesResponse);
                assertNull(messagesResponse.getErrorsMessage());

                List<FeedbackMessage> feedbackMessageList = messagesResponse.getMessages();
                assertNotNull(feedbackMessageList);
                assertTrue(feedbackMessageList.size() != 0);

                for (FeedbackMessage feedbackMessage : feedbackMessageList) {
                    assertNotNull(feedbackMessage);
                    assertNotNull(feedbackMessage.getMessage());
                    assertNotNull(feedbackMessage.getTime());
                }
            }
        }
    }

    public void testAddNewFeedback() {
        Log.i("FeedbackSetApiTest", "testAddNewFeedback");
        if (!ENABLED_ADD_NEW_LIST) return;
        String newTestMessageFeedback = "test feedback " + (System.currentTimeMillis() / 1000);
        AddMessageResponse addMessageResponse = getApiSet().addMessage(
                App.API_APP_NAME,
                AddMessageRequest.buildRequestArgs(-1, newTestMessageFeedback, Feedback.ERROR)
        );

        assertNotNull(addMessageResponse);
        assertNull(addMessageResponse.getErrorsMessage());

        int newFeedbackListId = addMessageResponse.getListId();

        assertTrue(newFeedbackListId != 0);

        MessagesResponse messagesResponse = getApiSet().getMessages(App.API_APP_NAME, GetMessagesRequest.buildRequestArgs(newFeedbackListId));

        assertNotNull(messagesResponse);
        assertNull(messagesResponse.getErrorsMessage());

        List<FeedbackMessage> feedbackMessageList = messagesResponse.getMessages();
        assertNotNull(feedbackMessageList);
        assertTrue("new feedback list must have just one message", feedbackMessageList.size() == 1);

        FeedbackMessage feedbackMessage = feedbackMessageList.get(0);
        assertNotNull(feedbackMessage);
        assertNotNull(feedbackMessage.getTime());
        assertEquals(newTestMessageFeedback, feedbackMessage.getMessage());
    }


    public void testAddNewMessageToList() {
        Log.i("FeedbackSetApiTest", "testAddNewFeedback");
        if (!ENABLED_ADD_NEW_MESSAGE_TO_LIST) return;

        MessageListsResponse response = getApiSet().getMessageLists(App.API_APP_NAME, new ArgsMap());
        int feedbackListId = response.getLists().get(0).getId();

        String newTestMessageFeedback = "test feedback " + (System.currentTimeMillis() / 1000);
        AddMessageResponse addMessageResponse = getApiSet().addMessage(
                App.API_APP_NAME,
                AddMessageRequest.buildRequestArgs(feedbackListId, newTestMessageFeedback, Feedback.ERROR)
        );

        assertTrue(addMessageResponse.getListId() == feedbackListId);

        MessagesResponse messagesResponse = getApiSet().getMessages(App.API_APP_NAME, GetMessagesRequest.buildRequestArgs(feedbackListId));

        boolean newMessageFound = false;

        for (FeedbackMessage feedbackMessage : messagesResponse.getMessages()) {
            assertNotNull(feedbackMessage);

            if (feedbackMessage.getMessage().equals(newTestMessageFeedback)) {
                newMessageFound = true;
                break;
            }
        }

        assertTrue("new added message not found in new feedback list", newMessageFound);
    }

}
