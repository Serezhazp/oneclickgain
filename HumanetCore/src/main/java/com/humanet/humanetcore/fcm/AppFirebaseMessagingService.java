package com.humanet.humanetcore.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.PushActivity;
import com.humanet.humanetcore.model.PushNotification;
import com.humanet.humanetcore.model.enums.NotificationType;

/**
 * Created by serezha on 29.08.16.
 */
public class AppFirebaseMessagingService extends FirebaseMessagingService {

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {

		Gson gson = new Gson();
		String json = gson.toJson(remoteMessage.getData());
		PushNotification pushNotification = gson.fromJson(json, PushNotification.class);

		logNotification(pushNotification);
		showNotification(pushNotification);
	}

	private void showNotification(PushNotification pushNotification) {
		String title = App.getInstance().getString(R.string.app_name);
		PendingIntent contentIntent;
		Notification notification;

		int notificationId = 1984;
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		Class myClass = NavigationManager.getPushActivityClass();

		Intent intent = new Intent(this, myClass);
		intent.putExtra(PushActivity.TYPE, NotificationType.toInt(pushNotification.getType()));
		intent.putExtra(PushActivity.OBJECT_ID, pushNotification.getId());

		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

		contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		NotificationCompat.Builder builder =
				new NotificationCompat.Builder(this)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentTitle(title)
				.setSound(alarmSound)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(title))
				.setContentText(pushNotification.getTitle());

		builder.setContentIntent(contentIntent);
		notification = builder.build();
		notification.flags = Notification.FLAG_AUTO_CANCEL;

		notificationManager.notify(notificationId, notification);
	}

	private void logNotification(PushNotification pushNotification) {
		String TAG = "NOTIFICATION";

		Log.d(TAG, "Title: " + pushNotification.getTitle());
		Log.d(TAG, "Type: " +  pushNotification.getType());
		Log.d(TAG, "Id: " + pushNotification.getId());
	}
}
