package com.humanet.humanetcore.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.utils.PrefHelper;

/**
 * Created by serezha on 29.08.16.
 */
public class AppFirebaseInstanceIdService extends FirebaseInstanceIdService {

	@Override
	public void onTokenRefresh() {
		String refreshedToken = FirebaseInstanceId.getInstance().getToken();
		PrefHelper.setStringPref(Constants.PREF.FCM_TOKEN, refreshedToken);
		Log.d("FCM", "Refreshed token: " + refreshedToken);

	}

	public static String getFcmToken() {
		String token;
		token = PrefHelper.getStringPref(Constants.PREF.FCM_TOKEN);

		token = token != null ? token : FirebaseInstanceId.getInstance().getToken();
		PrefHelper.setStringPref(Constants.PREF.FCM_TOKEN, token);

		return token;
	}

}
