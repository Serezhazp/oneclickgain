package com.humanet.humanetcore.fragments.capture;

import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.humanet.camera.CameraApi;
import com.humanet.camera.CameraApi16Impl;
import com.humanet.camera.CameraApi21Impl;
import com.humanet.filters.FilterController;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.GrabFramesEvent;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.dialogs.DialogBuilder;
import com.humanet.humanetcore.views.dialogs.ProgressDialog;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.VideoTextureView;
import com.humanet.video.ProcessCapturedVideoRunnable;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class VideoRecordFragment extends BaseTitledFragment {

    private static final String LOG_TAG = "VideoRecordFragment";

    private static final int VIDEO_MAX_LENGTH = 7;
    private static final int PROGRESS_MAX = VIDEO_MAX_LENGTH * 1000;


    private ProgressDialog mProgressDialog;

    private long mTimeStartRecording;

    private boolean mIsRecording;
    // <--- recorder params


    private TextureView mSurfaceView;
    private ImageButton mStartRecordingButton, mCameraFlashButton, mChangeCameraButton;

    private boolean mIsFlashOn = false;
    private boolean mIsFacingCamera;
    private SurfaceHolder mPreviewHolder;
    private HolderCallback mHolderCallback;

    private TextView mTitleText;

    private UpdateTimeTask mTimeTask;

    private ProgressBar mProgressBar;

    private OnVideoRecordCompleteListener mListener = null;

    private CameraApi mCameraApi;

    private boolean mIsFragmentPaused;

    public static VideoRecordFragment newInstance() {
        VideoRecordFragment fragment = new VideoRecordFragment();
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.video_recording);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int camerasCount = 0;
        //
        if (Build.VERSION.SDK_INT < 21) {
            mCameraApi = new CameraApi16Impl();
            camerasCount = mCameraApi.getCamerasCount();
        } else {
            mCameraApi = new CameraApi21Impl(getActivity().getApplicationContext());
            try {
                camerasCount = mCameraApi.getCamerasCount();
            } catch (AssertionError error) {
                if (!App.DEBUG) {
                    Crashlytics.getInstance().core.logException(error);
                }
                // on same devices don's support camera2, and they can throw AssertionError
                // if AssertionError happens switch api to older
                mCameraApi = new CameraApi16Impl();
                camerasCount = mCameraApi.getCamerasCount();
            }
        }

        if (camerasCount > 1) {
            if(NewVideoInfo.get() == null)
                mIsFacingCamera = true;

            if (NewVideoInfo.get().getVideoType() == null || NewVideoInfo.get().getVideoType().equals(VideoType.VLOG) || NewVideoInfo.get().getSmileId() > 0) {
                mIsFacingCamera = true;
            }
            if (NewVideoInfo.get().getVideoType() != null && NewVideoInfo.get().getVideoType().equals(VideoType.PET_ALL)) {
                mIsFacingCamera = false;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_record, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initProgressBar(view);
        initSurfaceView(view);

        mTitleText = (TextView) view.findViewById(R.id.video_recording_title);

        mStartRecordingButton = (ImageButton) view.findViewById(R.id.btn_rec);
        mStartRecordingButton.setOnClickListener(mRecordOnClickListener);

        mHolderCallback = new HolderCallback();
        mSurfaceView.setSurfaceTextureListener(mHolderCallback);

        initChangeCameraButton(view);
    }

    @Override
    public void onResume() {
        super.onResume();

        mIsFragmentPaused = false;

        mListener = (OnVideoRecordCompleteListener) getActivity();

        if (mHolderCallback.isAvailable()) {
            initCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mIsFragmentPaused = true;

        EventBus.getDefault().unregister(this);

        if (mIsRecording) {
            stopRecording();
        }

        mCameraApi.releaseCamera();
    }

    private void initFlashButton(View view) {
        mCameraFlashButton = (ImageButton) view.findViewById(R.id.btn_flash);
        if (mCameraApi.hasFlash()) {
            mCameraFlashButton.setEnabled(true);
            mCameraFlashButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mIsFlashOn = !mIsFlashOn;
                    mCameraApi.turnFlash(!mIsFlashOn);
                }
            });

        } else {
            mCameraFlashButton.setEnabled(false);
        }
    }

    public void initProgressBar(View view) {
        mProgressBar = (ProgressBar) view.findViewById(R.id.video_recording_progress);
        mProgressBar.setMax(PROGRESS_MAX);
        mProgressBar.setProgress(0);
    }


    private void initSurfaceView(View view) {
        final CircleView circleView = (CircleView) view.findViewById(R.id.surface_view);
        mSurfaceView = circleView.getTextureView();

        mSurfaceView.setOnTouchListener(mOnTouchListener);
    }

    private void initChangeCameraButton(View view) {
        mChangeCameraButton = (ImageButton) view.findViewById(R.id.btn_change_camera);
        if (mCameraApi.getCamerasCount() > 1) {
            mChangeCameraButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mIsFlashOn) {
                        mCameraApi.turnFlash(false);
                        mIsFlashOn = false;
                    }
                    mIsFacingCamera = !mIsFacingCamera;

                    initCamera();
                }
            });
        } else {
            mChangeCameraButton.setEnabled(false);
        }
    }


    private void initCamera() {
        try {
            if (mHolderCallback != null && mHolderCallback.isAvailable()) {
                mCameraApi.setCameraFacing(mIsFacingCamera);

                mCameraApi.setSurfaceHolder(mSurfaceView);

                initFlashButton(getView());

            }
        } catch (CameraApi.CameraException ex) {
            ex.printStackTrace();
            Toast.makeText(getActivity(), R.string.video_record_error_camera_failure, Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    private void initSurfaceViewSize() {

        View parent = (View) mSurfaceView.getParent();

        VideoTextureView view = (VideoTextureView) mSurfaceView;

        float width = mCameraApi.getCapturedImageHeight();
        float height = mCameraApi.getCapturedImageWidth();

        float parentWidth = parent.getMeasuredWidth();
        float parentHeight = parent.getMeasuredHeight();
        float scale = Math.max(parentWidth / width, parentHeight / height);
        float scaledWidth = (width * scale);
        float scaledHeight = (height * scale);

        if (view.setCalculatedSizes((int) scaledWidth, (int) scaledHeight)) {
            view.setCalculatedX((int) ((parentWidth - scaledWidth) / 2));
            view.setCalculatedY((int) ((parentHeight - scaledHeight) / 2));

            view.requestLayout();
        }
    }


    private class HolderCallback implements TextureView.SurfaceTextureListener {

        private boolean mAvailable;

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            mAvailable = true;
            initCamera();
            initSurfaceViewSize();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            mAvailable = false;
            mCameraApi.releaseCamera();
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }


        public boolean isAvailable() {
            return mAvailable;
        }
    }

    private View.OnClickListener mRecordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!mIsRecording) {
                startRecording();
            } else {
                stopRecording();
            }

        }
    };

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void startRecording() {

        File f = FilePathHelper.getVideoTmpFile();
        if (f.exists() && f.delete()) {
            try {
                f.createNewFile();
            } catch (Exception ignore) {
            }
        }

        mTimeTask = new UpdateTimeTask();

        mProgressBar.post(new Runnable() {
            @Override
            public void run() {
                mTimeStartRecording = System.currentTimeMillis();
                new Timer().schedule(mTimeTask, 30, 30);
                mIsRecording = true;

                invalidateViewsVisibilitiesForRecordingStatus();
            }
        });

        mCameraApi.startRecord(f);
    }

    private void invalidateViewsVisibilitiesForRecordingStatus() {
        if (mIsRecording) {
            mChangeCameraButton.setVisibility(View.INVISIBLE);
            mCameraFlashButton.setVisibility(View.INVISIBLE);

            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setProgress(0);
        } else {

            mChangeCameraButton.setVisibility(View.VISIBLE);
            mCameraFlashButton.setVisibility(View.VISIBLE);

            mProgressBar.setVisibility(View.INVISIBLE);
            mProgressBar.setProgress(0);

            mTitleText.setText(R.string.video_record_title);
        }

    }


    public void stopRecording() {
        long recordLength = System.currentTimeMillis() - mTimeStartRecording;

        if (mTimeTask != null)
            mTimeTask.cancel();
        mTimeTask = null;
        //  btnRecStart.setImageResource(R.drawable.ic_record_start);
        mStartRecordingButton.setEnabled(true);

        if (mIsRecording) {
            Log.v(LOG_TAG, "Finishing recording, calling stop and release on recorder");
            try {
                mCameraApi.stopRecord();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!mIsFragmentPaused) {
            if (recordLength > 1500) {
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setTitle(R.string.video_processing);
                mProgressDialog.show();

                int size = Math.min(mCameraApi.getCapturedImageWidth(), mCameraApi.getCapturedImageHeight());
                NewVideoInfo.get().setSizes(size, size);

                cleanUpDirectories();

                mCameraApi.releaseCamera();

                new Thread(new ProcessCapturedVideoRunnable.Builder(App.getInstance())
                        .setCameraId(mIsFacingCamera ? 1 : 0)
                        .setCameraSideSize(size)
                        .setFramesFolder(FilePathHelper.getVideoFrameFolder())
                        .setSourceVideoFilePath(FilePathHelper.getVideoTmpFile())
                        .setTmpVideoFilePath(FilePathHelper.getVideoTmpFile2())
                        .setAudioStreamPath(FilePathHelper.getAudioStreamFile())
                        .setListener(new ProcessCapturedVideoRunnable.OnVideoProcessedListener() {
                            @Override
                            public void onVideoProcessed(boolean success) {
                                EventBus.getDefault().postSticky(new GrabFramesEvent(success));
                            }
                        })
                        .build())
                        .start();
            } else {
                showErrorAboutNoVideo();
                initCamera();
            }
        }

        mIsRecording = false;

        invalidateViewsVisibilitiesForRecordingStatus();
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(GrabFramesEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        if (mProgressDialog != null && !mProgressDialog.isDismissed()) {
            mProgressDialog.dismiss();
        }
        if (event.isSuccesses()) {

            FilterController.init(App.getInstance().getApplicationContext(), mIsFacingCamera ? 1 : 0);
            mListener.onVideoRecordComplete(FilePathHelper.getVideoTmpFile().getAbsolutePath(), mIsFacingCamera ? 1 : 0);

        } else {
            new DialogBuilder(getActivity())
                    .setMessage(R.string.video_record_error_grab_frames)
                    .setCancelable(true)
                    .create()
                    .show();
        }
    }


    private void showErrorAboutNoVideo() {
        new DialogBuilder(getActivity())
                .setMessage(R.string.video_record_error_to_short)
                .create()
                .show();
    }


    private class UpdateTimeTask extends TimerTask {
        @Override
        public void run() {
            mTitleText.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        long d = System.currentTimeMillis() - mTimeStartRecording;
                        Date date = new Date(d);
                        mTitleText.setText(new SimpleDateFormat("mm:ss", Locale.getDefault()).format(date));
                        int videoRecordingLength = (int) date.getTime();
                        if (videoRecordingLength > VIDEO_MAX_LENGTH * 1000) {
                            stopRecording();
                        }
                        mProgressBar.setProgress(videoRecordingLength);
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        if(!App.DEBUG)
                            Crashlytics.logException(new Throwable(ex));

                        stopRecording();
                    }
                }

            });
        }
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (MotionEvent.ACTION_DOWN == event.getAction()) {
                mCameraApi.focus();
            }
            return false;
        }
    };

    private void cleanUpDirectories() {
        new Thread() {
            @Override
            public void run() {
                try {
                    FileUtils.cleanDirectory(FilePathHelper.getVideoFrameFolder());
                    FileUtils.deleteDirectory(FilePathHelper.getFilterFolder());
                } catch (IOException | IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public interface OnVideoRecordCompleteListener {
        void onVideoRecordComplete(String videoPath, int cameraId);
    }

}
