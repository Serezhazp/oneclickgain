package com.humanet.humanetcore.fragments.vote;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.InputActivity;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.vote.GetMyVotesListRequest;
import com.humanet.humanetcore.api.requests.vote.GetVotesListRequest;
import com.humanet.humanetcore.api.response.vote.VoteListsResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.vote.Vote;
import com.humanet.humanetcore.views.adapters.VoteListAdapter;

/**
 * Created by ovitali on 16.11.2015.
 */
public class VoteListFragment extends BaseTitledFragment implements View.OnClickListener, AbsListView.OnItemClickListener {


    public static VoteListFragment newInstance(@VoteTabFragment.VotePeriod String period, int periodId) {
        VoteListFragment voteListFragment = new VoteListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("period", period);
        bundle.putInt("periodId", periodId);
        voteListFragment.setArguments(bundle);
        return voteListFragment;
    }

    public static VoteListFragment newInstance() {
        return new VoteListFragment();
    }

    private static final int POLL_LOADER_ID = 2;

    @Override
    public String getTitle() {
        return getString(R.string.vote_title);
    }

    private VoteListAdapter mVoteListAdapter;

    private boolean mIsMine;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vote_list, container, false);

        mVoteListAdapter = new VoteListAdapter(getActivity(), null);

        ListView mVoteListView = (ListView) view.findViewById(R.id.vote_list);
        mVoteListView.setOnItemClickListener(this);
        mVoteListView.setAdapter(mVoteListAdapter);

        view.findViewById(R.id.btn_add).setOnClickListener(this);

        if (getArguments() != null && getArguments().containsKey("period")) {
            @VoteTabFragment.VotePeriod String period = getArguments().getString("period");
            int periodId = getArguments().getInt("periodId");
            getSpiceManager().execute(new GetVotesListRequest(period, periodId), new SimpleRequestListener<VoteListsResponse>());
        } else {
            mIsMine = true;
            mVoteListAdapter.setMyVotes(true);
            getSpiceManager().execute(new GetMyVotesListRequest(), new SimpleRequestListener<VoteListsResponse>());
        }

        getLoaderManager().initLoader(POLL_LOADER_ID, Bundle.EMPTY, new LoaderCallback());

        return view;
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_add) {
            Context context = v.getContext();
            context.startActivity(InputActivity.newPollIntent(context));

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Vote vote = Vote.fromCursor((Cursor) mVoteListAdapter.getItem(position));
        ((BaseActivity) getActivity()).startFragment(VoteFragment.newInstance(vote, mIsMine), true);
    }


    //--------------- internal classes --------------- //
    private class LoaderCallback extends SimpleLoaderCallbacks<Cursor> {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String where;

            if (!mIsMine)
                where = ContentDescriptor.Votes.Cols.TYPE + " = " + getArguments().getInt("periodId")
                        + " AND " +
                        ContentDescriptor.Votes.Cols.MY_INITIATION + " != " + 1;
            else
                where = ContentDescriptor.Votes.Cols.MY_INITIATION + " = " + 1;

            return new CursorLoader(getActivity(),
                    ContentDescriptor.Votes.URI,
                    null,
                    where,
                    null,
                    null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mVoteListAdapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mVoteListAdapter.changeCursor(null);
            mVoteListAdapter = null;
        }
    }
}
