package com.humanet.humanetcore.fragments.info;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.InputActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.message.GetMessageListsRequest;
import com.humanet.humanetcore.api.response.feedback.MessageListsResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.Feedback;
import com.humanet.humanetcore.views.adapters.FeedbackAdapter;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleOnTabSelectedListener;

/**
 * Created by Denis on 03.03.2015.
 */
public class FeedbackFragment extends BaseTitledFragment implements
        View.OnClickListener,
        ViewPagerAdapter.ViewPagerAdapterDelegator, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private int mCurrentTab;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private FeedbackAdapter[] mAdapters = new FeedbackAdapter[3];


    public static FeedbackFragment newInstance() {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);

        View addButton = view.findViewById(R.id.btn_add);
        addButton.setOnClickListener(this);

        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.feedback_tab_motion));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.feedback_tab_problems));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.feedback_tab_complains));
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);


        for (int i = 0; i < 3; i++) {
            mAdapters[i] = new FeedbackAdapter(getActivity(), null, 0);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getSpiceManager().execute(
                new GetMessageListsRequest(),
                new SimpleRequestListener<MessageListsResponse>()
        );

        loadFromDb(mCurrentTab);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Feedback feedback = Feedback.fromCursor((Cursor) parent.getAdapter().getItem(position));
        openFeedback(feedback.getId());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.Feedback.Cols.TYPE + " = " + args.getInt("type");
        return new CursorLoader(getActivity(), ContentDescriptor.Feedback.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapters[loader.getId()].swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapters[loader.getId()].swapCursor(null);
    }


    protected void openFeedback(int id) {
        startActivity(InputActivity.newFeedbackIntent(getActivity(), id, mCurrentTab));
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_add) {
            openFeedback(-1);
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.feedback);
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {
        ListView listView = new ListView(getActivity());
        listView.setDivider(null);
        listView.setAdapter(mAdapters[position]);
        listView.setOnItemClickListener(this);
        return listView;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }
    private void loadFromDb(int position) {
        Bundle args = new Bundle(1);
        args.putInt("type", position);

        getLoaderManager().restartLoader(position, args, this);
    }


    private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

            mCurrentTab = position;
            loadFromDb(position);
        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
        }
    };
}
