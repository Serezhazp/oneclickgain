package com.humanet.humanetcore.fragments.capture;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.adapters.VideoFilterListAdapter;
import com.humanet.humanetcore.views.widgets.CircleLayout;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Владимир on 30.10.2014.
 */
public class FilterFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {


    private VideoFilterListAdapter mAdapter;
    private ArrayList<IFilter> mList;

    private IFilter mSelectedFilter;

    private static int EMPTY;

    public static FilterFragment newInstance() {
        FilterFragment fragment = new FilterFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.video_record_filter);

        mAdapter = new VideoFilterListAdapter(getActivity());

        mListView.setOnItemSelectedListener(this);
        mNextButton.setOnClickListener(this);

        initFilterList();

        return view;
    }

    private void initFilterList() {

        new ScaleImageTask() {
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mList = new ArrayList<>();
                Collections.addAll(mList, FilterController.getFilters());

                EMPTY = (int) Math.ceil(mList.size() / 2);

                mAdapter.setImagePath(FilePathHelper.getVideoPreviewImageSmallPath());
                mAdapter.setData(mList);
                mListView.setAdapter(mAdapter);

                int selectedIndex = EMPTY;
                if (NewVideoInfo.get().getFilter() != null) {
                    for (int i = 0; i < mList.size(); i++) {
                        IFilter f = mList.get(i);
                        if (FilterController.areFiltersSame(NewVideoInfo.get().getFilter(), f)) {
                            selectedIndex = i;
                            break;
                        }
                    }
                }

                mListView.setSelectedItem(selectedIndex);

                onItemSelected(mList.get(selectedIndex));
            }
        }.executeOnExecutor(ScaleImageTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int i = v.getId();
        if (i == R.id.button_done) {
            if (mSelectedFilter != null) {
                NewVideoInfo.get().setFilter(mSelectedFilter);
                VideoProcessTask.getInstance().applyVideoFilter();
            }
            getActivity().onBackPressed();

        }
    }


    @Override
    public void onItemSelected(Object data) {
        mSelectedFilter = (IFilter) data;
        releaseMediaPlayer();

        if (mSelectedFilter == null) {
            NewVideoInfo.get().setVideoPath(null);
            NewVideoInfo.get().setImagePath(null);
        }

        NewVideoInfo.get().setFilter(mSelectedFilter);
        VideoProcessTask.getInstance().applyPreviewFilter();
    }


    private class ScaleImageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {

                while (NewVideoInfo.isIsFilterBuilding())
                    Thread.sleep(50);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }
    }


}