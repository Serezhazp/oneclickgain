package com.humanet.humanetcore.fragments.base;

import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.IMenuBindActivity;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.utils.NetworkUtil;

public abstract class BaseTitledFragment extends BaseSpiceFragment {

    @Override
    public void onResume() {
        super.onResume();

        ((BaseActivity) getActivity()).setActionBarTitle(getTitle());
        ((BaseActivity) getActivity()).setActionBarOfflineMode(NetworkUtil.isOfflineMode());

        if (getActivity() instanceof IMenuBindActivity && this instanceof IMenuBindFragment) {
            ((IMenuBindActivity) getActivity()).selectMenuItem(((IMenuBindFragment) this).getSelectedMenuItem());
        }
    }

    public abstract String getTitle();

}
