package com.humanet.humanetcore.fragments.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.message.AddMessageRequest;
import com.humanet.humanetcore.api.response.feedback.AddMessageResponse;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.EditWithCounterView;

import static com.humanet.humanetcore.model.Feedback.Type;

/**
 * Created by ovitali on 14.09.2015.
 */
public class NewFeedbackFragment extends BaseTitledFragment implements View.OnClickListener {

    public static NewFeedbackFragment newInstance(int feedbackId, @Type int type) {
        NewFeedbackFragment fragment = new NewFeedbackFragment();
        Bundle args = new Bundle();
        args.putInt("feedback_id", feedbackId);
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    private int mFeedbackId;

    @Type
    private int mFeedbackType;


    private EditWithCounterView mNewMessageView;


    @Override
    public String getTitle() {
        return getString(R.string.feedback_new_message);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey("feedback_id")) {
            mFeedbackId = args.getInt("feedback_id");
        }

        @Type int type = args.getInt("type");
        mFeedbackType = type;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback_new_message, container, false);

        mNewMessageView = (EditWithCounterView) view.findViewById(R.id.new_message);

        view.findViewById(R.id.btn_next).setOnClickListener(this);


        return view;
    }

    private void sendComment(String message) {
        getLoader().show();
        UiUtil.hideKeyboard(mNewMessageView.getEditView());
        mNewMessageView.setText("");

        getSpiceManager().execute(
                new AddMessageRequest(mFeedbackId, message, mFeedbackType),
                new SimpleRequestListener<AddMessageResponse>() {
                    @Override
                    public void onRequestSuccess(AddMessageResponse response) {
                        super.onRequestSuccess(response);
                        getLoader().dismiss();
                        getActivity().finish();
                    }
                });


        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.FEEDBACK_SEND);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_next) {
            String comment = mNewMessageView.getText().toString();
            if (!comment.isEmpty()) {
                sendComment(comment);
            }

        }
    }
}
