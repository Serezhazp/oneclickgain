package com.humanet.humanetcore.fragments.info;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.views.adapters.TipsAdapter;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleOnTabSelectedListener;

import org.apache.commons.io.IOUtils;

import java.util.HashMap;

/**
 * Created by Denis on 03.03.2015.
 */

public class InfoFragment extends Fragment implements
        ViewPagerAdapter.ViewPagerAdapterDelegator {

    private HashMap<String, String[]> mTexts = new HashMap<>();

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        view.findViewById(R.id.btn_add).setVisibility(View.GONE);

        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info_tab_agreement));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info_tab_privacy));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info_tab_about));
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

        return view;
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {

        TipsAdapter tipsAdapter = new TipsAdapter(getActivity());

        String text;
        switch (position) {
            case 0:
                text = "terms";
                break;
            case 1:
                text = "privacy";
                break;
            default:
                text = "tips";
                break;
        }


        new TextLoader(tipsAdapter).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, buildLink(text));

        ListView listView = new ListView(getActivity());
        listView.setDivider(null);
        listView.setAdapter(tipsAdapter);
        return listView;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }


    private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
        }
    };

    private String buildLink(String textName) {
        return String.format("texts/%s/%s.html",
                textName,
                Language.getSystem().getRequestParam());
    }

    private class TextLoader extends AsyncTask<String, Void, String[]> {

        final TipsAdapter tipsAdapter;

        public TextLoader(TipsAdapter tipsAdapter) {
            this.tipsAdapter = tipsAdapter;
        }

        @Override
        protected String[] doInBackground(String... params) {
            try {

                String[] resultTexts = mTexts.get(params[0]);
                if (resultTexts == null) {
                    String text = IOUtils.toString(getActivity().getAssets().open(params[0]));
                    text = text.replaceAll("&", "");
                    resultTexts = text.split("<br/>");
                    mTexts.put(params[0], resultTexts);

                }
                return resultTexts;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String[] texts) {
            super.onPostExecute(texts);

            if (isVisible()) {
                tipsAdapter.setData(texts);
            }
        }
    }
}
