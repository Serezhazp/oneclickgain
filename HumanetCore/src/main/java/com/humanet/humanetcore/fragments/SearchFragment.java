package com.humanet.humanetcore.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.SearchActivity;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.SearchUserRequest;
import com.humanet.humanetcore.api.response.user.UserSearchResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleOnTabSelectedListener;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.search.SearchContentLayout;
import com.humanet.humanetcore.views.widgets.search.SearchProfileLayout;
import com.humanet.humanetcore.views.widgets.search.SearchQuestionnaireLayout;

/**
 * Created by Deni on 23.06.2015.
 */
public class SearchFragment extends BaseTitledFragment implements ViewPagerAdapter.ViewPagerAdapterDelegator, View.OnClickListener {

    private SearchProfileLayout mSearchProfileLayout = null;
    private SearchQuestionnaireLayout mSearchQuestionaryLayout = null;
    private SearchContentLayout mSearchContentLayout = null;

    ViewPager mViewPager;
    TabLayout mTabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        //TODO use TabLayout.setupWithViewPager();
        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_profile));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_questions));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_content));
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        UiUtil.hideKeyboard(getActivity());

        view.findViewById(R.id.search).setOnClickListener(this);

        return view;
    }

    @Override
    public void onPause() {
        UiUtil.hideKeyboard(getActivity());
        super.onPause();
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {
        View tabContent = null;
        switch (position) {
            case 0:
                if (mSearchProfileLayout == null) {
                    mSearchProfileLayout = new SearchProfileLayout(getActivity(), getSpiceManager());
                }
                tabContent = mSearchProfileLayout;
                break;
            case 1:
                if (mSearchQuestionaryLayout == null) {
                    mSearchQuestionaryLayout = new SearchQuestionnaireLayout(getActivity());
                }
                tabContent = mSearchQuestionaryLayout;
                break;
            case 2:
                if (mSearchContentLayout == null) {
                    mSearchContentLayout = new SearchContentLayout(getActivity(), getSpiceManager(), getLoaderManager());
                }
                tabContent = mSearchContentLayout;
                break;
        }
        return tabContent;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.search) {
            search();
        }
    }

    private void search() {
        ArgsMap argsMap = new ArgsMap();
        if (mSearchProfileLayout != null) {
            mSearchProfileLayout.grabSearchInfo(argsMap);
        }
        if (mSearchQuestionaryLayout != null) {
            mSearchQuestionaryLayout.grabSearchInfo(argsMap);
        }
        if (mSearchContentLayout != null) {
            mSearchContentLayout.grabSearchInfo(argsMap);
        }

        App.getInstance().getContentResolver().delete(ContentDescriptor.UserSearchResult.URI, null, null);

        getSpiceManager().execute(new SearchUserRequest(argsMap), new SimpleRequestListener<UserSearchResponse>());

        ((SearchActivity) getActivity()).openSearchResult();
    }

    private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
            UiUtil.hideKeyboard(getActivity());
        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
        }
    };

    @Override
    public String getTitle() {
        return getString(R.string.search);
    }
}
