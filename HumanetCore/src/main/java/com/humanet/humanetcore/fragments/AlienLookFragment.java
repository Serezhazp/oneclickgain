package com.humanet.humanetcore.fragments;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovitali on 27.08.2015.
 */
public class AlienLookFragment extends BaseVideoCategoryPickerFragment implements IMenuBindFragment {

    @Override
    public String getTitle() {
        return getString(R.string.menu_alien_look);
    }


    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.alien_look_tasks),
                getString(R.string.alien_look_challenge),
        };
    }

    @Override
    protected VideoType[] getVideoTypes() {
        return new VideoType[]{
                VideoType.ALIEN_LOOK_TASK,
                VideoType.ALIEN_LOOK_CHALLENGE,
        };
    }


    @Override
    public int getSelectedMenuItem() {
        return R.id.alien_look;
    }
}
