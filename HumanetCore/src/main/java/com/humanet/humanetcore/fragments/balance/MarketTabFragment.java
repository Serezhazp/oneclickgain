package com.humanet.humanetcore.fragments.balance;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovitali on 26.08.2015.
 */
public class MarketTabFragment extends BaseVideoCategoryPickerFragment {

    @Override
    public String getTitle() {
        return getString(R.string.market).replace("\n", " ");
    }

    @Override
    protected VideoType[] getVideoTypes() {
        return new VideoType[]{
                VideoType.MARKET_SHARE,
                VideoType.MARKET_SELL
        };
    }

    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.market_share),
                getString(R.string.market_sell)
        };
    }
}
