package com.humanet.humanetcore.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.GetUserInfoRequest;
import com.humanet.humanetcore.api.requests.user.LogoutRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.events.OnViewProfileListener;
import com.humanet.humanetcore.interfaces.OnNavigateListener;
import com.humanet.humanetcore.jobs.ClearDatabaseJob;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.utils.PrefHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import retrofit.RetrofitError;

public abstract class BaseMenuFragment extends Fragment implements View.OnClickListener {

    private static final String LAST_SELECTED_MENU_ITEM = "last_selected_item";

    protected OnNavigateListener mOnNavigateListener;
    protected OnViewProfileListener mOnViewProfileListener;

    protected View mFragmentContainerView;

    protected DrawerLayout mDrawerLayout;

    protected DrawerLayout.DrawerListener mDrawerToggle;

    protected RelativeLayout mContentFrame;
    protected float lastTranslate = 0.0f;

    private int mSelectedItemId = Integer.MIN_VALUE;

    protected SpiceManager mSpiceManager = null;

    protected TextView mBalanceTextView;

    protected abstract Class newSplashActivityInstance();


    @Override
    public void onResume() {
        super.onResume();
        if (AppUser.getInstance().get() != null) {
            fillUserInfo(AppUser.getInstance().get());
        }

        loadUserInfo();
    }

    public SpiceManager getSpiceManager() {
        if (mSpiceManager == null) {
            mSpiceManager = new SpiceManager(NavigationManager.getSpiceServiceClass());
            mSpiceManager.start(getActivity());
        }

        return mSpiceManager;
    }


    @Override
    public void onStop() {
        if (mSpiceManager != null && mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
            mSpiceManager = null;
        }

        super.onStop();
    }

    private void loadUserInfo() {

        getSpiceManager().execute(new GetUserInfoRequest<>(), new SimpleRequestListener<UserInfo>() {
            @Override
            public void onRequestSuccess(UserInfo userInfo) {
                if (userInfo == null || TextUtils.isEmpty(userInfo.getFName())) {
                    logout();
                    return;
                }
                AppUser.getInstance().set(userInfo);
                fillUserInfo(AppUser.getInstance().get());
            }

            @Override
            public void onRequestFailure(SpiceException spiceException) {
                RetrofitError throwable = (RetrofitError) spiceException.getCause();
                if (throwable != null && throwable.getResponse() != null) {
                    int responseCode = throwable.getResponse().getStatus();
                    if (responseCode == 401) {
                        logout();
                    }
                }
            }
        });


    }

    protected void fillUserInfo(UserInfo userInfo) {
        View v = getView();
        if (v == null || userInfo == null) {
            return;
        }

        v.findViewById(R.id.avatar).setOnClickListener(this);

        TextView textView;
        String text;

        textView = (TextView) v.findViewById(R.id.first_name);
        textView.setText(userInfo.getFName() + " " + userInfo.getLName());
        textView.setOnClickListener(this);

        textView = (TextView) v.findViewById(R.id.location);
        text = userInfo.getCountry() + ", " + userInfo.getCity();
        textView.setText(text);


        if (userInfo.getAvatarSmall() != null)
            ImageLoader.getInstance().displayImage(userInfo.getAvatarSmall(), (ImageView) v.findViewById(R.id.avatar));

        if (mBalanceTextView != null)// clickclap has no this view, that's why it can be null
            mBalanceTextView.setText(BalanceUtil.balanceToString(AppUser.getInstance().get().getRating()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnNavigateListener = (OnNavigateListener) activity;
        mOnViewProfileListener = (OnViewProfileListener) activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(
                R.id.menu_fragment,
                R.id.content,
                R.id.drawer_layout
        );
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, int contentId, int drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mContentFrame = (RelativeLayout) getActivity().findViewById(contentId);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(drawerLayout);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        mDrawerToggle = createDrowerToogle();
        initDrawer();
    }

    protected DrawerLayout.DrawerListener createDrowerToogle() {
        return new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.string.app_name,  /* "open drawer" description for accessibility */
                R.string.app_name  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (drawerView.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mContentFrame.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    mContentFrame.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };
    }

    protected void initDrawer() {
        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                if (mDrawerToggle instanceof ActionBarDrawerToggle)
                    ((ActionBarDrawerToggle) mDrawerToggle).syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if (!isVisible())
                    return;

                if (getActivity().getSupportFragmentManager().getBackStackEntryCount() == 0) {

                    mDrawerLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mDrawerToggle instanceof ActionBarDrawerToggle)
                                ((ActionBarDrawerToggle) mDrawerToggle).syncState();
                        }
                    });

                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    if (mDrawerToggle instanceof ActionBarDrawerToggle)
                        ((ActionBarDrawerToggle) mDrawerToggle).setDrawerIndicatorEnabled(true);


                } else {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    if (mDrawerToggle instanceof ActionBarDrawerToggle)
                        ((ActionBarDrawerToggle) mDrawerToggle).setDrawerIndicatorEnabled(false);
                }
            }
        });
    }


    protected void logout() {
        getSpiceManager().cancelAllRequests();
        getSpiceManager().execute(new LogoutRequest(), new SimpleRequestListener<BaseResponse>());

        new ClearDatabaseJob().start();

        PrefHelper.clear();
        AppUser.getInstance().clear();
        ImageLoader.getInstance().clearDiskCache();
        Intent intent = new Intent(getActivity(), newSplashActivityInstance());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle instanceof ActionBarDrawerToggle)
            ((ActionBarDrawerToggle) mDrawerToggle).onConfigurationChanged(newConfig);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void toggle() {
        if (isDrawerOpen()) {
            mDrawerLayout.closeDrawers();
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    public void selectMenuItem(int itemId) {
        if (isAdded())
            selectMenuItem(getView(), itemId);
    }

    private void selectMenuItem(View v, int itemId) {
        if (itemId == mSelectedItemId) {
            return;
        }

        if (mSelectedItemId != Integer.MIN_VALUE) {
            v.findViewById(mSelectedItemId).setSelected(false);
        }
        mSelectedItemId = itemId;

        if (itemId != Integer.MIN_VALUE)
            v.findViewById(itemId).setSelected(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(LAST_SELECTED_MENU_ITEM, mSelectedItemId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            selectMenuItem(getView(), savedInstanceState.getInt(LAST_SELECTED_MENU_ITEM));
    }
}
