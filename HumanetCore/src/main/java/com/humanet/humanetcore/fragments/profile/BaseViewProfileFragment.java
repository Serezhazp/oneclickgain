package com.humanet.humanetcore.fragments.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.EditProfileActivity;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.FollowUnfollowUserRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.presenters.profile.LoadUserProfilePresenter;
import com.humanet.humanetcore.views.profile.ViewProfileView;
import com.humanet.humanetcore.views.widgets.profile.QuestionnaireView;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uran on 08.06.2016.
 */
public abstract class BaseViewProfileFragment extends BaseTitledFragment implements View.OnClickListener,
        ViewPagerAdapter.ViewPagerAdapterDelegator,
        ViewProfileView {

    public static Fragment newInstance(int userId) {
        return newInstance(userId, 0);
    }

    public static Fragment newInstance(int userId, int defTab) {
        try {
            Fragment fragment = NavigationManager.getViewProfileFragmentClass().getConstructor().newInstance();
            Bundle args = new Bundle();
            args.putInt(Constants.PARAMS.USER_ID, userId);
            args.putInt("def_tab", defTab);
            fragment.setArguments(args);
            return fragment;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    //---------------


    private int mUserId;

    private UserInfo mUserInfo;

    private ImageButton mActionButton;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private int mDefTab = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getInt(Constants.PARAMS.USER_ID);
            if (getArguments().containsKey("def_tab")) {
                mDefTab = getArguments().getInt("def_tab");
            }
        }
        if (savedInstanceState != null) {
            mDefTab = savedInstanceState.getInt("def_tab");
        }
    }


    @Override
    public final String getTitle() {
        return getString(R.string.search_tab_profile);
    }

    @NonNull
    public abstract int[] getTabNames();

    private QuestionnaireView mQuestionnaireView;

    private LoadUserProfilePresenter mLoadUserProfilePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_profile, container, false);

        mLoadUserProfilePresenter = new LoadUserProfilePresenter(getSpiceManager(), this);


        mViewPager = (ViewPager) v.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));

        mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
        mTabLayout.setupWithViewPager(mViewPager);


        mActionButton = (ImageButton) v.findViewById(R.id.action);
        mActionButton.setOnClickListener(this);

        if (mUserId == AppUser.getInstance().getUid()) {
            mUserInfo = AppUser.getInstance().get();
        } else {
            mActionButton.setImageResource(R.drawable.friend_button);
        }

        if (mDefTab != 0) {
            //noinspection ConstantConditions
            mTabLayout.getTabAt(mDefTab).select();
        }


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        showTokenButton();

        mLoadUserProfilePresenter.load(mUserId);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideTokenButton();
    }


    protected void showTokenButton() {
        ImageButton toolbarButton = (ImageButton) ((BaseActivity) getActivity()).getToolbar().findViewById(R.id.token);
        toolbarButton.setVisibility(View.VISIBLE);
        toolbarButton.setTag(mUserId);
        toolbarButton.setOnClickListener(this);

        if (mUserId == AppUser.getInstance().getUid()) {
            toolbarButton.setImageResource(R.drawable.ic_token);
        } else {
            toolbarButton.setImageResource(R.drawable.ic_phone);
        }
    }

    @Override
    public void showUser(UserInfo userInfo) {
        View v = getView();
        if (v != null && isAdded() && userInfo != null) {
            TextView textView;
            mUserInfo = userInfo;

            textView = (TextView) v.findViewById(R.id.first_name);
            textView.setText(userInfo.getFName() + " " + userInfo.getLName());

            textView = (TextView) v.findViewById(R.id.location);
            List<String> texts = new ArrayList<>(3);

            if (!TextUtils.isEmpty(userInfo.getCountry()))
                texts.add(userInfo.getCountry());
            if (!TextUtils.isEmpty(userInfo.getCity()))
                texts.add(userInfo.getCity());
            if (userInfo.getLanguage() != null)
                texts.add(userInfo.getLanguage().getTitle());
            textView.setText(TextUtils.join(", ", texts));

            if (userInfo.getAvatarSmall() != null) {
                ImageView avatar = (ImageView) v.findViewById(R.id.avatar);
                ImageLoader.getInstance().displayImage(userInfo.getAvatarSmall(), avatar);
            }

            textView = (TextView) v.findViewById(R.id.user_rating);
            textView.setText(BalanceUtil.balanceToString(userInfo.getRating()));

            if (!mUserInfo.isMe()) {
                mActionButton.setImageResource(R.drawable.friend_button);

                if (mUserInfo.getRelationship() == 1) {
                    mActionButton.setSelected(true);
                }
            }

            if (mQuestionnaireView != null)
                mQuestionnaireView.fillQuestionnaireInfo(userInfo);
        }
    }

    @NonNull
    public QuestionnaireView getQuestionnaireView() {
        if (mQuestionnaireView == null) {
            mQuestionnaireView = new QuestionnaireView(getActivity());
            if (mUserInfo != null) {
                mQuestionnaireView.fillQuestionnaireInfo(mUserInfo);
            }
        }
        return mQuestionnaireView;
    }

    protected void hideTokenButton() {
        View toolbarButton = ((BaseActivity) getActivity()).getToolbar().findViewById(R.id.token);
        toolbarButton.setVisibility(View.GONE);
        toolbarButton.setOnClickListener(null);
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.action) {
            onActionButtonClick();

        } else if (i == R.id.token) {
            if (AppUser.getInstance().getUid() == mUserId) {
                startActivity(new Intent(getActivity(), NavigationManager.getTokenActivityClass()));
            } else {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                String phone = mUserInfo.getPhone();

                phone = "+" + phone.replaceAll("\\D+", "");

                intent.setData(Uri.parse("tel:" + phone));
                startActivity(intent);
            }
        }
    }

    private void onActionButtonClick() {
        if (mUserInfo != null) {
            if (mUserInfo.isMe()) {
                EditProfileActivity.startNewInstance(getActivity(), mTabLayout.getSelectedTabPosition());
            } else {
                mUserInfo.setRelationship(mUserInfo.getRelationship() == 1 ? 0 : 1);
                mActionButton.setSelected(mUserInfo.getRelationship() == 1);
                getSpiceManager().execute(new FollowUnfollowUserRequest(mUserInfo.getId(), mUserInfo.getRelationship() == 1), new SimpleRequestListener<BaseResponse>());
            }
        }
    }


    @Override
    public int getPagesCount() {
        return getTabNames().length;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return getContext().getString(getTabNames()[position]);
    }


    @Nullable
    public UserInfo getUserInfo() {
        return mUserInfo;
    }

    public int getUserId() {
        return mUserId;
    }
}
