package com.humanet.humanetcore.fragments.capture;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.SimpleOnItemSelectedListener;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;

import java.util.ArrayList;
import java.util.List;

public class VideoDescriptionFragment extends Fragment implements View.OnClickListener {

    public static VideoDescriptionFragment newInstance() {
        return new VideoDescriptionFragment();
    }

    private SimpleSpinnerAdapter mCategoriesAdapter;
    private Spinner mCategoriesView;

    private ImageButton mNextButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_description, container, false);

        mNextButton = (ImageButton) view.findViewById(R.id.button_done);
        mNextButton.setOnClickListener(this);

        mCategoriesView = (Spinner) view.findViewById(R.id.sp_category);
        mCategoriesView.setOnItemSelectedListener(new CategorySelectListener());
        final ArrayList<Category> categories = getCategories();

        if(NewVideoInfo.get().getCategory() != 0 && NewVideoInfo.get().getCategory() != -1)
            mCategoriesView.setVisibility(View.GONE);

        mCategoriesAdapter = new SimpleSpinnerAdapter<>(categories);
        mCategoriesAdapter.setHasHitItem(true);
        mCategoriesView.setAdapter(mCategoriesAdapter);

        EditText tags = (EditText) view.findViewById(R.id.tags);
        tags.addTextChangedListener(new TagsChangeTextWatcher());

        if (isMarketSell() || isCrowdSell()) {
            EditText amount = (EditText) view.findViewById(R.id.amount);
            amount.addTextChangedListener(new AmountChangeTextWatcher());
        } else {
            view.findViewById(R.id.amount).setVisibility(View.GONE);
        }


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN |
                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void onStop() {
        super.onStop();

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN |
                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.button_done) {
            setButtonEnabled(false);
            OnEnterDescriptionListener onEnterDescriptionListener = (OnEnterDescriptionListener) getActivity();
            onEnterDescriptionListener.onDescriptionEntered();
        }
    }

    protected ArrayList<Category> getCategories() {
        final ArrayList<Category> categories = new ArrayList<>();

        String selection;

        List<String> selections = new ArrayList<>();

        VideoType videoType = getVideoType();
        if (videoType != null && videoType.equals(VideoType.MY_CHOICE)) {
            videoType = VideoType.NEWS;
        }
        if (videoType != null) {
            selections.add(ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + videoType.getId());
        }

        selections.add(ContentDescriptor.Categories.Cols.TECH + " = " + 0);

        selection = TextUtils.join(" AND ", selections);

        Cursor cursor = null;
        try {
            cursor = getActivity().getContentResolver().query(
                    ContentDescriptor.Categories.URI,
                    null,
                    selection,
                    null,
                    null);
            categories.clear();
            categories.add(new Category(-1, getString(R.string.video_record_description_select_category)));
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Category category = Category.fromCursor(cursor);
                    if (category.getId() != 0) {
                        categories.add(category);
                    }
                } while (cursor.moveToNext());
            }

        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return categories;
    }

    protected VideoType getVideoType() {
        VideoType videoType = null;
        Intent intent = getActivity().getIntent();
        if (intent != null && intent.hasExtra("type")) {
            videoType = (VideoType) intent.getSerializableExtra("type");
        }
        return videoType;
    }

    private class CategorySelectListener extends SimpleOnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Category category = (Category) parent.getAdapter().getItem(position);

            NewVideoInfo.get().setCategoryId(category.getId());
            validateFields();
        }
    }


    private class AmountChangeTextWatcher extends SimpleTextListener {
        @Override
        public void afterTextChanged(Editable s) {
            int cost = 0;
            String costString = s.toString();
            if (!TextUtils.isEmpty(costString)) {
                try {
                    cost = Integer.parseInt(costString);
                } catch (Exception ignore) {
                }
            }
            NewVideoInfo.get().setCost(cost);
            validateFields();
        }
    }

    private class TagsChangeTextWatcher extends SimpleTextListener {
        @Override
        public void afterTextChanged(Editable s) {
            NewVideoInfo.get().setTags(s.toString());
        }
    }

    protected void validateFields() {
        boolean validate = true;

        if (mCategoriesView.getSelectedItemPosition() == 0 && mCategoriesView.getVisibility() == View.VISIBLE) {
            validate = false;
        }
        if (isMarketSell() || isCrowdSell()) {
            if (NewVideoInfo.get().getCost() == 0) {
                validate = false;
            }
        }
        setButtonEnabled(validate);
    }

    protected void setButtonEnabled(boolean isEnabled) {
        mNextButton.setEnabled(isEnabled);
    }

    boolean isMarketSell() {
        return VideoType.MARKET_SELL.equals(getVideoType());
    }

    boolean isCrowdSell() {
        return VideoType.CROWD_ASK.equals(getVideoType()) || VideoType.CROWD_PET_ASK.equals(getVideoType());
    }

    //  boolean isMarketShare() {
    //     return VideoType.MARKET_SHARE.equals(getVideoType());
    //}

    //

    public interface OnEnterDescriptionListener {
        void onDescriptionEntered();
    }


}
