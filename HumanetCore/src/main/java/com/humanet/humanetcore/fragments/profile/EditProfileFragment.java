package com.humanet.humanetcore.fragments.profile;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.EditProfileActivity;
import com.humanet.humanetcore.fragments.base.BaseFragmentTabHostingFragment;

@SuppressLint("ValidFragment")
public class EditProfileFragment extends BaseFragmentTabHostingFragment {
    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.profile_view_tab_questionnaire),
                getString(R.string.profile_view_tab_info),
        };
    }

    @Override
    protected Fragment getFragmentForTabPosition(int position) {
        if (position == 0)
            return ProfileInterestsFragment.newInstance();
        if (position == 1) {
            Fragment fragment = ProfilePrivateInfoFragment.newInstance(true);
            ((EditProfileActivity) getActivity()).setProfilePrivateInfoFragment(fragment);
            return fragment;
        }

        throw new RuntimeException("unknown position: " + position);
    }

    @Override
    public String getTitle() {
        return getString(R.string.profile_edit);
    }
}
