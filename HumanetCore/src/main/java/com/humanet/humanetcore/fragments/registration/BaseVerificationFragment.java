package com.humanet.humanetcore.fragments.registration;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.countryflags.PhoneView;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.GetSmsCodeRequest;
import com.humanet.humanetcore.api.requests.user.VerifyCodeRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.user.VerifyCodeResponse;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.PinEntryView;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.tbruyelle.rxpermissions.RxPermissions;


public abstract class BaseVerificationFragment extends BaseTitledFragment implements TextView.OnEditorActionListener, View.OnClickListener, PhoneView.EnterPhoneListener {

    private static final int SMS_TIMEOUT;

    static {
        if (App.DEBUG)
            SMS_TIMEOUT = 10 * 1000;
        else
            SMS_TIMEOUT = 20 * 1000;
    }

    private static final int SMS_NOT_RECEIVED = 1;

    private boolean mIsSmsReceived = false;

    private Activity mActivity;

    private OnVerifyListener mOnVerifyListener;

    private PinEntryView mPinEntryView;

    private View mPhoneNumberHolder;
    private View mPinNumberHolder;

    private PhoneView mPhoneView;

    private View mNextButton;

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        mActivity = getActivity();

        mPhoneNumberHolder = v.findViewById(R.id.phoneNumberHolder);
        mPhoneNumberHolder.findViewById(R.id.agreement).setOnClickListener(this);

        mPhoneView = (PhoneView) mPhoneNumberHolder.findViewById(R.id.phoneNumber);
        mPhoneView.setOnEnterPhoneListener(this);


        mPinNumberHolder = v.findViewById(R.id.pinNumberHolder);
        mPinNumberHolder.setVisibility(View.GONE);
        mPinEntryView = (PinEntryView) mPinNumberHolder.findViewById(R.id.pin);
        mPinEntryView.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                Log.i("mPinEntryView", "mPinEntryView");
                mNextButton.setEnabled(mPinEntryView.getText().length() == 4);
                if (mPinEntryView.getText().length() == 4) {
                    sendVerificationCode(mPinEntryView.getText());
                }
            }
        });

        mPinNumberHolder.findViewById(R.id.btn_resend).setOnClickListener(this);

        mNextButton = v.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(this);


        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(() -> RxPermissions.getInstance(getActivity()).request(Manifest.permission.RECEIVE_SMS).subscribe(granted -> Log.e("smspermission", "granted " + granted)), 1000);
    }


    @Override
    public void onResume() {
        super.onResume();

        ((VerificationContainer) getActivity()).setVerificationFragment(this);

        if (mPhoneNumberHolder.getVisibility() == View.VISIBLE) {
            mPhoneView.focus();
        } else {
            mPinEntryView.focus();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        UiUtil.hideKeyboard(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        ((VerificationContainer) getActivity()).setVerificationFragment(null);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnVerifyListener = (OnVerifyListener) activity;
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_resend) {
            showResend();

        } else if (viewId == R.id.btn_next) {
            if (mPhoneNumberHolder.getVisibility() == View.VISIBLE) {
                onSend(mPhoneView.getPhoneNumber());
            } else {
                sendVerificationCode(mPinEntryView.getText());
            }

        } else if (viewId == R.id.agreement) {
            Intent intent = new Intent(getActivity(), NavigationManager.getAgreementActivityClass());
            startActivity(intent);

        }
    }

    private void showResend() {
        if (!getLoader().isDismissed()) {
            getLoader().dismiss();
        }

        mNextButton.setEnabled(mPhoneView.validate());

        mPinNumberHolder.setVisibility(View.GONE);
        mPhoneNumberHolder.setVisibility(View.VISIBLE);

        mPhoneView.focus();
    }

    @Override
    public void onSend(String phoneNumber) {
        phoneNumber = "+" + phoneNumber.replaceAll("\\D+", "");

        requestCode(phoneNumber);

        mPhoneNumberHolder.setVisibility(View.GONE);
        mPinNumberHolder.setVisibility(View.VISIBLE);

        mPinEntryView.focus();

        mNextButton.setEnabled(mPinEntryView.getText().length() == 4);
    }

    @Override
    public void onNumberIsValid(boolean isValid) {
        mNextButton.setEnabled(isValid);
    }

    public void requestCode(String phoneNumber) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.CODE_REQUEST);
        getSpiceManager().execute(new GetSmsCodeRequest(phoneNumber), new SimpleRequestListener<BaseResponse>() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                if (baseResponse == null || !baseResponse.isSuccess()) {
                    onRequestFailure(null);
                }
            }

            @Override
            public void onRequestFailure(SpiceException spiceException) {
                showResend();
                getLoader().dismiss();
            }
        });

        mIsSmsReceived = false;
        getLoader().setTitle(getString(R.string.verification_code_get));
        getLoader().show();
        waitForSms();
    }

    public void sendVerificationCode(CharSequence code, String token) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.NUMBER_VERIFICATION);

        mIsSmsReceived = true;
        getSpiceManager().execute(
                new VerifyCodeRequest(code.toString(), token),
                new SimpleRequestListener<VerifyCodeResponse>() {
                    @Override
                    public void onRequestSuccess(VerifyCodeResponse verifyCodeResponse) {
                        if (verifyCodeResponse.isSuccess()) {
                            PrefHelper.setStringPref(Constants.PREF.PHONE, mPhoneView.getPhoneNumber());
                            PrefHelper.setStringPref(Constants.PREF.COUNTRY_CODE, mPhoneView.getSelectedCountryCode());

                            mOnVerifyListener.onVerify(verifyCodeResponse.getStatus() == 0, mPhoneView.getCountryId());

                            mPhoneNumberHolder.setVisibility(View.VISIBLE);
                            mPinNumberHolder.setVisibility(View.GONE);

                            mNextButton.setEnabled(true);
                        } else {
                            Toast.makeText(mActivity, verifyCodeResponse.getErrorsMessage(), Toast.LENGTH_SHORT).show();
                        }

                        getLoader().dismiss();
                    }

                });

        getLoader().setTitle(getString(R.string.verification_code_send));
        getLoader().show();
    }

    public void sendVerificationCode(CharSequence code) {
        sendVerificationCode(code, PrefHelper.getStringPref(Constants.PREF.FCM_TOKEN));
    }

    private void waitForSms() {
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                try {
                    Thread.sleep(SMS_TIMEOUT);
                } catch (InterruptedException ignore) {
                }

                if (mIsSmsReceived) {
                    return -1;
                } else {
                    return SMS_NOT_RECEIVED;
                }
            }

            @Override
            protected void onPostExecute(Integer res) {
                getLoader().dismiss();

                if (res > 0) {
                    if (isVisible())
                        mPinEntryView.focus();
                }


            }
        }.execute();
    }


    public interface OnVerifyListener {
        void onVerify(boolean isNewUser, int countryId);
    }

    public interface VerificationContainer {
        void setVerificationFragment(BaseVerificationFragment verificationFragment);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        switch (actionId) {
            case EditorInfo.IME_ACTION_DONE:
            case EditorInfo.IME_ACTION_SEND:
                if (mPinEntryView.getText().length() == 4)
                    sendVerificationCode(mPinEntryView.getText());
                handled = true;
                break;
        }
        return handled;
    }
}
