package com.humanet.humanetcore.fragments.balance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.fragments.base.BaseFragmentTabHostingFragment;

/**
 * Created by ovitali on 26.08.2015.
 */
public class RatingsTabFragment extends BaseFragmentTabHostingFragment {

    private static RatingTabView sImpl;

    public static void setImpl(RatingTabView impl) {
        sImpl = impl;
    }

    protected static RatingTabView getImpl() {
        return sImpl;
    }

    @Override
    protected String[] getTabNames() {
        return sImpl.getTabNames();
    }

    @Override
    protected Fragment getFragmentForTabPosition(int position) {
        return RatingFragment.newInstance(sImpl.getRatingType(position));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            openTab(sImpl.getDefaultTabPosition());
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.balance_category_type_rating);
    }


    public interface RatingTabView {
        String[] getTabNames();

        String getRatingType(int tabPosition);

        int getDefaultTabPosition();
    }

}
