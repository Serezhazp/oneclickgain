package com.humanet.humanetcore.fragments.profile;

import android.app.Activity;

import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.UserInfo;

/**
 * Created by ovitali on 16.01.2015.
 */
public abstract class BaseProfileFragment extends BaseSpiceFragment {
    protected OnProfileFillListener mOnProfileFillListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnProfileFillListener = (OnProfileFillListener) activity;
    }

    public interface OnProfileFillListener {
        void onFillProfile(UserInfo userInfo);
        UserInfo getUser();
    }
}
