package com.humanet.humanetcore.fragments.balance;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseVideoManageActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.RatingRequest;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.Rating;
import com.humanet.humanetcore.model.RatingUser;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.persistence.DurationInMillis;

/**
 * Created by ovitali on 26.08.2015.
 * <p>
 * Loading and displaying content of rating tab.
 * Data type defined by ratingType.
 */
public class RatingFragment extends BaseSpiceFragment implements AdapterView.OnItemClickListener {

    /**
     * @param ratingType type of rating data. Value can be "local", "global" or other depending on application
     * @return new fragment for rating tab
     */
    public static RatingFragment newInstance(String ratingType) {
        RatingFragment ratingFragment = new RatingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ratingType", ratingType);
        ratingFragment.setArguments(bundle);
        return ratingFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rating, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        String ratingType = getArguments().containsKey("ratingType") ? getArguments().getString("ratingType") : null;

        RatingRequest ratingRequest = new RatingRequest(ratingType);

        getSpiceManager().execute(
                ratingRequest,
                ratingType,
                DurationInMillis.ONE_SECOND,
                new RatingPendingRequestListener()
        );
    }

    private void setRating(Rating rating) {
        View view = getView();
        if (view == null || rating == null) {
            return;
        }

        UiUtil.setTextValue(view, R.id.rate, getString(R.string.rating_rate, BalanceUtil.balanceToString(rating.getRating())));
        UiUtil.setTextValue(view, R.id.rate_position, getString(R.string.rating_rate_position, rating.getPosition() + "%"));

        ListView listView = (ListView) view.findViewById(R.id.tops_list);
        listView.setOnItemClickListener(this);
        listView.setAdapter(new Adapter(sortUsers(rating.getTopUsers())));
    }

    private RatingUser[] sortUsers(RatingUser[] topUsers) {
        RatingUser bufferUser;

        for(int i = 0; i < topUsers.length - 1; i++) {
            boolean isSwapped = false;
            for(int j = 0; j < topUsers.length - i - 1; j++) {
                if(topUsers[j].getRating() < topUsers[j + 1].getRating()) {
                    bufferUser = topUsers[j];
                    topUsers[j] = topUsers[j + 1];
                    topUsers[j + 1] = bufferUser;
                    isSwapped = true;
                }
            }
            if(!isSwapped)
                break;
        }

        return topUsers;
    }

    private class RatingPendingRequestListener extends SimpleRequestListener<Rating> {

        @Override
        public void onRequestSuccess(Rating rating) {
            setRating(rating);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ((BaseVideoManageActivity) getActivity()).openUserProfile((int) id);
    }

    private static class Adapter extends BaseAdapter {
        private RatingUser[] mList;

        private static final int ME = 0;
        private static final int OTHER = 1;

        public Adapter(RatingUser[] list) {
            mList = list;
        }

        @Override
        public int getCount() {
            if (mList == null)
                return 0;
            return mList.length;
        }

        @Override
        public RatingUser getItem(int position) {
            return mList[position];
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getId();
        }

        @Override
        public int getItemViewType(int position) {
            return getItemId(position) == AppUser.getInstance().getUid() ? ME : OTHER;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rating_user, parent, false);
                viewHolder = new ViewHolder(convertView);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            ((CardView) convertView).setCardBackgroundColor(
                    getItemViewType(position) == ME
                            ? convertView.getContext().getResources().getColor(R.color.listItemSelected)
                            : convertView.getContext().getResources().getColor(R.color.listItemNotSelected)
            );


            RatingUser user = getItem(position);

            viewHolder.avatarView.setImageDrawable(null);
            if (user.getAvatarSmall() != null)
                ImageLoader.getInstance().displayImage(user.getAvatarSmall(), viewHolder.avatarView);

            viewHolder.rateView.setText(BalanceUtil.balanceToString(user.getRating()));

            String location = !TextUtils.isEmpty(user.getCity()) ? user.getCountry() : "";
            if (!TextUtils.isEmpty(user.getCity()) && !TextUtils.isEmpty(user.getCountry()))
                location += ", ";
            if (!TextUtils.isEmpty(user.getCountry()))
                location += user.getCity();

            viewHolder.locationView.setText(location);
            viewHolder.nameView.setText(user.getUserName());

            return convertView;
        }


    }

    private static class ViewHolder {
        ImageView avatarView;
        TextView nameView;
        TextView locationView;
        TextView rateView;

        public ViewHolder(View view) {
            avatarView = (ImageView) view.findViewById(R.id.avatar);
            nameView = (TextView) view.findViewById(R.id.name);
            locationView = (TextView) view.findViewById(R.id.location);
            rateView = (TextView) view.findViewById(R.id.rating);

            view.setTag(this);
        }

    }
}
