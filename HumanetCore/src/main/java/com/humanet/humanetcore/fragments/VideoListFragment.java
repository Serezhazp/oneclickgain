package com.humanet.humanetcore.fragments;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.Constants.PARAMS;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.PlayFlowActivity;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.Screen;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.presenters.video.VideoCacherPresenter;
import com.humanet.humanetcore.presenters.video.VideoListLoaderPresenter;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.humanet.humanetcore.views.IVideoListView;
import com.humanet.humanetcore.views.adapters.VideoListAdapter;
import com.humanet.humanetcore.views.dialogs.ProgressDialog;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Владимир on 26.11.2014.
 */
public class VideoListFragment extends BaseTitledFragment implements
        VideoItemView.OnVideoChooseListener,
        IVideoListView,
        AbsListView.OnScrollListener, LoaderManager.LoaderCallbacks<Cursor> {

    public static class Builder {
        Bundle mArgs = new Bundle();

        public Builder(VideoType videoType) {
            setVideoType(videoType);
        }

        public Builder setUploading(boolean isUploading) {
            mArgs.putBoolean("isUploading", isUploading);
            return this;
        }

        public Builder setReplyId(int replyId) {
            mArgs.putInt(PARAMS.REPLY_ID, replyId);
            return this;
        }

        public Builder setVideoType(VideoType videoType) {
            mArgs.putSerializable(PARAMS.TYPE, videoType);
            return this;
        }

        public Builder setCategoryType(Category category) {
            mArgs.putSerializable(PARAMS.CATEGORY, category);
            return this;
        }

        public Builder setTitle(String title) {
            mArgs.putString(PARAMS.TITLE, title);
            return this;
        }

        public Builder setCurrentPosition(int position) {
            mArgs.putInt(PARAMS.POSITION, position);
            return this;
        }

        public Builder setNurslingId(int id) {
            mArgs.putInt(PARAMS.NURSLING_ID, id);
            return this;
        }

        public VideoListFragment build() {
            VideoListFragment fragment = new VideoListFragment();
            fragment.setArguments(mArgs);
            return fragment;
        }
    }

    private VideoCacherPresenter mVideoCacherPresenter;
    private VideoListLoaderPresenter mVideoListLoaderPresenter;


    private static final int LOADER = 1000;
    private static final int PRELOAD_COUNT = 3;
    private VideoListAdapter mVideoListAdapter;
    private int mScrollTo;

    private Video mVideoParent;

    private String mTitle;

    private int mLoadedCount;

    private ListView mVideoListView;

    private int mLastVisiblePosition;

    private VideoType mVideoType;
    private Category mCategory;
    private int mReplyId;
    private int mNurslingId;
    private int mUserId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mVideoCacherPresenter = new VideoCacherPresenter();
        mVideoListLoaderPresenter = new VideoListLoaderPresenter();
        mVideoListLoaderPresenter.setVideoListView(this);

        mVideoCacherPresenter.setSpiceManager(getSpiceManager());
        mVideoListLoaderPresenter.setSpiceManager(getSpiceManager());

        Bundle args = getArguments();
        if (args.containsKey(PARAMS.TYPE)) {
            mVideoType = (VideoType) args.getSerializable(PARAMS.TYPE);
        }

        if (args.containsKey(PARAMS.CATEGORY)) {
            mCategory = (Category) args.getSerializable(PARAMS.CATEGORY);
            if (mCategory != null)
                mVideoListLoaderPresenter.setFlowRequestParams(mCategory.getParams());
        }

        if (args.containsKey(PARAMS.REPLY_ID)) {
            mReplyId = args.getInt(PARAMS.REPLY_ID);
            mVideoListLoaderPresenter.setReplayId(mReplyId);
        }

        if (args.containsKey(PARAMS.NURSLING_ID)) {
            mNurslingId = args.getInt(PARAMS.NURSLING_ID);
            mVideoListLoaderPresenter.setPetId(mNurslingId);
        }

        if (args.containsKey(PARAMS.USER_ID)) {
            mUserId = args.getInt(PARAMS.USER_ID);
            mVideoListLoaderPresenter.setUserId(mUserId);
        }

        if (args.containsKey("parent")) {
            mVideoParent = (Video) args.getSerializable("parent");
        }

        if (args.containsKey("position")) {
            mLastVisiblePosition = args.getInt("position");
        }

        mTitle = args.getString("title");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_video_list, container, false);
        mVideoListView = (ListView) view.findViewById(R.id.video_list_view);

        mVideoListAdapter = new VideoListAdapter(getActivity(), null, AppUser.getInstance().getUid(), mVideoType, mCategory, mVideoParent);
        mVideoListView.setAdapter(mVideoListAdapter);
        mVideoListAdapter.setOnVideoChooseListener(this);


        Bundle bundle = new Bundle(1);
        bundle.putInt("screen", getScreen().ordinal());

        getActivity().getLoaderManager().restartLoader(LOADER, bundle, this);

        loadList();

        if (getArguments() != null && getArguments().getBoolean("isUploading") && !NetworkUtil.isOfflineMode(getActivity())) {
            ProgressDialog progressDialog = getLoader();
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        return view;
    }

    private void loadList() {
        if (!NetworkUtil.isOfflineMode()) {
            mVideoListLoaderPresenter.loadPage();
        }
    }

    @Override
    public void addVideos(List<Video> videoList) {
        int count = videoList.size();
        mIsLastPage = count < Constants.LIMIT;
    }

    @Override
    public long getLastVideoId() {
        if (mVideoListAdapter.getCount() > 0) {
            Video lastVideo = Video.fromCursor((Cursor) mVideoListAdapter.getItem(mVideoListView.getCount() - 1));
            return lastVideo.getVideoId();
        }
        return 0;
    }


    private Screen getScreen() {
        Screen screen;
        if (getArguments().getBoolean("isMyStream")) {
            screen = Screen.MY_PROFILE;
        } else {
            screen = Screen.LIST_FLOW;
        }
        return screen;
    }

    @Override
    public void onResume() {
        super.onResume();

        mVideoListView.scrollTo(0, mLastVisiblePosition + 1);
        mVideoListView.setOnScrollListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mVideoListView.setOnScrollListener(null);

        mVideoCacherPresenter.stopCache();
    }

    private boolean mIsLastPage;

    private void runPreloader() {
        int preloadCount = Math.min(PRELOAD_COUNT, mVideoListAdapter.getCount()) - mLoadedCount;
        mLoadedCount += preloadCount;

        if (preloadCount > 0) {
            String[] urls = new String[preloadCount];
            for (int i = 0; i < preloadCount; i++) {
                try {
                    Video lastVideo = Video.fromCursor((Cursor) mVideoListAdapter.getItem(i));
                    urls[i] = lastVideo.getUrl();
                } catch (Exception ignore) {
                    return;
                }
            }
            mVideoCacherPresenter.runCacher(urls);
        }
    }

    public void loadNextPage() {
        mVideoListView.setOnScrollListener(null);

        mVideoListView.setOnScrollListener(null);

        if (mVideoListView.getCount() > 0) {
            loadList();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        mLastVisiblePosition = firstVisibleItem;
        if (!mIsLastPage && totalItemCount >= Constants.LIMIT && firstVisibleItem + visibleItemCount > totalItemCount - 5) {
            loadNextPage();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int screen = args.getInt("screen");

        String where = ContentDescriptor.Videos.Cols.SCREEN + "=" + screen;
        return new CursorLoader(getActivity(), ContentDescriptor.Videos.URI, null, where, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mVideoListAdapter.changeCursor(data);
        if (mScrollTo > 0) {
            mVideoListView.setSelection(mScrollTo);
            mScrollTo = -1;
        }

        if (data != null && data.getCount() > 0) {
            runPreloader();
        }

        mVideoListView.requestFocus();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public String getTitle() {
        return mTitle;
    }


    public long getScreenId() {
        return getScreen().ordinal();
    }

    @Override
    public void onVideoChosen(Video video) {
        ArrayList<Video> videosToWatch = new ArrayList<>();
        int currentVideoId = 0;
        for (int i = 0; i < mVideoListAdapter.getCount(); i++) {
            Video v = Video.fromCursor((Cursor) mVideoListAdapter.getItem(i));
            if (video.getUrl().equals(v.getUrl())) {
                currentVideoId = i;
            }
            videosToWatch.add(v);
        }
        if (videosToWatch.size() > 0) {
            PlayFlowActivity.startNewInstance(getActivity(), mCategory, currentVideoId, mReplyId, mNurslingId, mUserId, mTitle, videosToWatch, getScreenId());
        }
    }

    /**
     * catch event on video uploading complete.
     * getSpiceManager().addListenerIfPending will not work because uploading request start from external thread
     *
     * @param uploadingMedia - contains url with new avatar
     */
    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UploadingMedia uploadingMedia) {
        if (mVideoListLoaderPresenter != null)
            mVideoListLoaderPresenter.loadPage(0);

        getLoader().dismiss();
    }

}
