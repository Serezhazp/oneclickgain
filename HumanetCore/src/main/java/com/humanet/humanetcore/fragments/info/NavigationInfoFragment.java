package com.humanet.humanetcore.fragments.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BasePickerFragment;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.interfaces.OnNavigateListener;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ovitali on 26.08.2015.
 */
public class NavigationInfoFragment extends BasePickerFragment implements IMenuBindFragment {
    @Override
    protected ArrayList<CirclePickerItem> getItems() {
        return new ArrayList<CirclePickerItem>(Arrays.asList(SubItem.values()));
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_info);
    }

    @Override
    protected int getPickerAngle() {
        return -90;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_picker, container, false);

        mOnNavigateListener = (OnNavigateListener) getActivity();

        mNextButton = view.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelected != null)
                    mOnNavigateListener.onNavigateByViewId(mSelected.getId());
            }
        });

        CirclePickerView circlePickerView = (CirclePickerView) view.findViewById(R.id.picker);
        circlePickerView.fill(getItems(), getPickerAngle(), 360 / 3,
                -1,
                new CirclePickerView.OnPickListener() {
                    @Override
                    public void onPick(View view, CirclePickerItem element) {
                        mSelected = element;
                    }
                });

        circlePickerView.getChildAt(circlePickerView.getChildCount() - 1).performClick();

        return view;
    }


    private enum SubItem implements CirclePickerItem {
        VOTE {
            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_menu_vote;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.vote_title);
            }

            @Override
            public int getId() {
                return R.id.vote;
            }
        },
        INFO {
            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_info_menu;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.info_chooser_about);
            }

            @Override
            public int getId() {
                return R.id.info;
            }
        },
        FEEDBACK {
            @Override
            public String getDrawable() {
                return "drawable://" + R.drawable.ic_feedback;
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.info_chooser_feedback);
            }

            @Override
            public int getId() {
                return R.id.feedback;
            }
        },
    }

    @Override
    public int getSelectedMenuItem() {
        return R.id.navigation_info;
    }
}
