package com.humanet.humanetcore.fragments.balance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.fragments.base.BaseFragmentTabHostingFragment;
import com.humanet.humanetcore.interfaces.OnBalanceChanged;

/**
 * Created by ovitali on 25.08.2015.
 */
public class CurrencyFragment extends BaseFragmentTabHostingFragment implements OnBalanceChanged {

    private static final String DEF_TAB = "defTab";

    private static CurrencyTabView sImpl;

    public static void setImpl(CurrencyTabView impl) {
        sImpl = impl;
    }

    public static int getAddBalanceTabPosition() {
        return sImpl.getAddBalanceTabPosition();
    }

    public static CurrencyFragment newInstance(int tab) {
        CurrencyFragment currencyFragment = new CurrencyFragment();

        Bundle bundle = new Bundle(1);
        bundle.putInt(DEF_TAB, tab);

        currencyFragment.setArguments(bundle);

        return currencyFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sImpl.setOnBalanceChangedListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null) {
            if (getArguments() != null) {
                openTab(getArguments().getInt(DEF_TAB, 0));
            } else {
                openTab(sImpl.getDefaultTabPosition());
            }
        }
    }

    @Override
    protected String[] getTabNames() {
        return sImpl.getTabNames();
    }

    @Override
    public String getTitle() {
        return getString(R.string.balance_category_type_currency);
    }

    @Override
    protected Fragment getFragmentForTabPosition(int position) {
        return sImpl.getFragmentAtPosition(position);
    }

    @Override
    public void onChanged() {
        openTab(sImpl.getDefaultTabPosition());
    }

    public interface CurrencyTabView {
        String[] getTabNames();

        int getAddBalanceTabPosition();

        Fragment getFragmentAtPosition(int position);

        void setOnBalanceChangedListener(OnBalanceChanged onBalanceChanged);


        int getDefaultTabPosition();
    }


}
