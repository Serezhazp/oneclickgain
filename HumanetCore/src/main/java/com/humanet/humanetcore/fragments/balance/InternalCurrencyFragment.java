package com.humanet.humanetcore.fragments.balance;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimplePendingRequestListener;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.CoinsRequest;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.fragments.base.BaseSpiceFragment;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.views.adapters.BalanceListAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.persistence.DurationInMillis;

public class InternalCurrencyFragment extends BaseSpiceFragment implements
        View.OnClickListener, AbsListView.OnScrollListener {

    private static final int LOADER_ID = 1001;

    private static final int LIMIT = 25;

    private BalanceListAdapter mAdapterHistory;

    protected TextView balanceTextView;

    private boolean mIsLastPage;

    private String mBalanceTabType;

    private int mPosition;

    private ListView mListView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_balance_currency_internal, container, false);
        Context context = container.getContext();

        mBalanceTabType = getArguments().getString("type");
        mPosition = getArguments().getInt("position");

        balanceTextView = ((TextView) view.findViewById(R.id.balance));
        //  mBalanceTextView.setText(String.format("%s GC", BalanceUtil.balanceToString(user.getBalance())));

        mAdapterHistory = new BalanceListAdapter(context, null);

        mListView = (ListView) view.findViewById(R.id.balance_list);
        mListView.setAdapter(mAdapterHistory);

        //getLoaderManager().initLoader(LOADER_ID + mPosition, null, new BalanceLoaderCallbacks());

        loadServerBalance();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UserInfo user = AppUser.getInstance().get();

        if(user != null)
            fillUserInfo(user, view);

        getSpiceManager().addListenerIfPending(UserInfo.class, "user_info", new SimplePendingRequestListener<UserInfo>() {
            @Override
            public void onRequestSuccess(UserInfo userInfo) {
                super.onRequestSuccess(userInfo);

                fillUserInfo(userInfo, view);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        getLoaderManager().initLoader(LOADER_ID + mPosition, null, new BalanceLoaderCallbacks());

    }

    private void loadServerBalance() {

        mListView.setOnScrollListener(null);

        CoinsRequest coinsRequest = new CoinsRequest.Builder(mBalanceTabType, mPosition).build();
        long lastId = 0;
        if (mAdapterHistory != null && mAdapterHistory.getCount() > 0) {

            lastId = mAdapterHistory.getItemId(mAdapterHistory.getCount() - 1);
            lastId = lastId & ~(1L << 32);

            coinsRequest.setLastId(lastId);
        }

        getSpiceManager().execute(
                coinsRequest, mBalanceTabType + lastId, DurationInMillis.ONE_SECOND,
                new SimpleRequestListener<CoinsHistoryListResponse>() {
                    @Override
                    public void onRequestSuccess(CoinsHistoryListResponse coinsHistoryListResponse) {
                        obtainBalanceResponse(coinsHistoryListResponse);
                        mIsLastPage = coinsHistoryListResponse.getHistory() != null && coinsHistoryListResponse.getHistory().size() < LIMIT;
                    }
                }
        );
    }

    protected void obtainBalanceResponse(CoinsHistoryListResponse coinsHistoryListResponse) {
        balanceTextView.setText(
                String.format("%s %s",
                        BalanceUtil.balanceToString(coinsHistoryListResponse.getBalance()),
                        App.COINS_CODE
                ));
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mIsLastPage)
            return;

        if (firstVisibleItem + visibleItemCount + 5 < totalItemCount) {
            return;
        }

        loadServerBalance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }


    private class BalanceLoaderCallbacks extends SimpleLoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(), ContentDescriptor.Balances.URI, null, ContentDescriptor.Balances.whereByTabType(mPosition), null, ContentDescriptor.Balances.Cols.CREATED_AT + " DESC");
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapterHistory.swapCursor(data);
            mListView.setOnScrollListener(InternalCurrencyFragment.this);

        }
    }

    private void fillUserInfo(UserInfo user, View view) {
        if (user.getFName() != null)
            ((TextView) view.findViewById(R.id.first_name)).setText(user.getFName());

        if (user.getAvatarSmall() != null)
            ImageLoader.getInstance().displayImage(user.getAvatarSmall(), (ImageView) view.findViewById(R.id.avatar));

    }


}
