package com.humanet.humanetcore.fragments.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.CaptureActivity;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.CategoriesLoadedEvent;
import com.humanet.humanetcore.fragments.VideoListFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.modules.CategoriesDataLoader;
import com.humanet.humanetcore.views.adapters.CategoryListAdapter;
import com.humanet.humanetcore.views.adapters.ViewPagerAdapter;
import com.humanet.humanetcore.views.behaviour.CategoriesListView;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by ovi on 3/25/16.
 */
public abstract class BaseVideoCategoryPickerFragment extends BaseTitledFragment implements ViewPagerAdapter.ViewPagerAdapterDelegator, View.OnClickListener {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private View mNavigationButton;

    private int mSelectedTab;

    protected abstract VideoType[] getVideoTypes();

    protected abstract String[] getTabNames();

    private PageView[] mPageViews = new PageView[getVideoTypes().length];


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        view.findViewById(R.id.new_video_shot).setOnClickListener(this);
        mNavigationButton = view.findViewById(R.id.btn_nav);
        mNavigationButton.setOnClickListener(this);

        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);

        mViewPager.addOnPageChangeListener(mOnViewPagerChangeListener);

        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            String title = getTabNames()[mSelectedTab];
            VideoType videoType = getVideoTypes()[mSelectedTab];
            Category category = Category.getFromDatabase(data.getExtras().getInt(Constants.PARAMS.CATEGORY), videoType);
            loadFlowList(category, videoType, title, true);
        }
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.new_video_shot) {
            addNewVideo();
        } else if (i == R.id.btn_nav || i == R.id.video_preview) {
            loadFlowList(false);
        }
    }

    private void loadFlowList(boolean isUploading) {
        if (getView() == null) {
            return;
        }

        String title = getTabNames()[mSelectedTab];
        VideoType videoType = getVideoTypes()[mSelectedTab];
        Category category = mPageViews[mSelectedTab].mSelectedCategory;

        loadFlowList(category, videoType, title, isUploading);
    }

    protected void addNewVideo() {
        Intent intent = CaptureActivity.createNewInstance(getActivity(), getVideoTypes()[mSelectedTab]);
        startActivityForResult(intent, 100);
    }

    private void loadFlowList(Category category, VideoType videoType, String title, boolean isUploading) {
        loadFlowList(category, videoType, 0, title, isUploading);
    }

    private void loadFlowList(Category category, VideoType videoType, int nurslingId, String title, boolean isUploading) {
        if (category == null || videoType == null || title == null)
            return;

        VideoListFragment.Builder builder = new VideoListFragment.Builder(videoType);
        builder.setCategoryType(category);
        builder.setTitle(title);
        builder.setNurslingId(nurslingId);
        builder.setUploading(isUploading);
        ((BaseActivity) getActivity()).startFragment(builder.build(), true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("tab", mSelectedTab);
        super.onSaveInstanceState(outState);
    }


    private ViewPager.SimpleOnPageChangeListener mOnViewPagerChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void onPageSelected(int position) {
            UiUtil.hideKeyboard(getActivity());
            mSelectedTab = position;
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CategoriesLoadedEvent event) {
        if (event.getCount() == 0) {
            mNavigationButton.setVisibility(View.INVISIBLE);
        } else {
            mNavigationButton.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getPagesCount() {
        return getVideoTypes().length;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {
        mPageViews[position] = new PageView(container.getContext(), getVideoTypes()[position], getSpiceManager(), getLoaderManager());
        mPageViews[position].setOnPreviewClickListener(this);
        return mPageViews[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return getTabNames()[position];
    }

    @SuppressLint("ViewConstructor")
    private static class PageView extends LinearLayout implements CategoriesListView, CircleLayout.OnItemSelectedListener {
        private CategoryListAdapter mCategoriesAdapter;
        private CircleLayout mCircleLayout;

        private CircleView mCircleView;

        private Category mSelectedCategory;

        private CategoriesDataLoader mCategoriesDataLoader;

        public PageView(Context context, VideoType videoType, final SpiceManager spiceManager, final LoaderManager loaderManager) {
            super(context, null);
            setBackgroundResource(R.drawable.background_page);
            setGravity(Gravity.CENTER_HORIZONTAL);
            setOrientation(VERTICAL);
            inflate(context, R.layout.layout_select_category, this);

            mCircleView = (CircleView) findViewById(R.id.video_preview);

            mCircleLayout = (CircleLayout) findViewById(R.id.horizontal_list_view);
            mCircleLayout.setOnItemSelectedListener(this);

            mCategoriesAdapter = new CategoryListAdapter(context);
            mCircleLayout.setAdapter(mCategoriesAdapter);


            mCategoriesDataLoader = new CategoriesDataLoader(spiceManager, loaderManager, this, false);
            mCategoriesDataLoader.loadCategories(videoType);
        }

        @Override
        public void onItemSelected(Object data) {
            mSelectedCategory = (Category) data;
            mCircleView.setImage(mSelectedCategory.getImage());
        }

        @Override
        public void setCategories(List<Category> categories) {
            if (categories == null || categories.size() == 0) {
                mCircleView.setVisibility(INVISIBLE);
                return;
            } else {
                mCircleView.setVisibility(VISIBLE);
            }

            EventBus.getDefault().post(new CategoriesLoadedEvent(categories.size()));

            if (!mCategoriesAdapter.isNewDataIdentical(categories)) {
                mCategoriesAdapter.setData(categories);
                mCircleLayout.setAdapter(mCategoriesAdapter);
                mCircleLayout.setSelectedItem(mCategoriesAdapter.getInitialSelectionPosition());
                mCircleLayout.invalidateAll();
            }
        }

        @Override
        public Context getActivity() {
            return getContext();
        }

        public void setOnPreviewClickListener(View.OnClickListener onClickListener) {
            mCircleView.setOnClickListener(onClickListener);
        }
    }


}
