package com.humanet.humanetcore.interfaces;

/**
 * Created by Denis on 28.04.2015.
 * <p/>
 * Used as request param
 */
public interface IServerRequestParams {
    String getRequestParam();
}
