package com.humanet.humanetcore.interfaces;

/**
 * Created by ovi on 4/21/16.
 */
public interface OnBalanceChanged {
    void onChanged();
}

