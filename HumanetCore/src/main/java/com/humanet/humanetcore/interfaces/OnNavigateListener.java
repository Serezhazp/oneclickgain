package com.humanet.humanetcore.interfaces;

/**
 * Created by ovitali on 25.08.2015.
 */
public interface OnNavigateListener {
    void onNavigateByViewId(int viewId);
}
