package com.humanet.humanetcore;

import android.graphics.Color;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constants {
    public static final int GAME_ITEM_COLOR = Color.WHITE; // Color.argb(255, 33, 150, 243);

    public static final String EXTRA_ACTION = "action";

    public static final String OTHER_OPTION = "OTHER_OPTION";

    public static final String VIDEO_INFO = "video_info";

    public static final String VIDEO_EXTENTION = "mp4";
    public static final String IMAGE_EXTENTION = "png";
    public static final String AUDIO_EXTENTION = "mp3";

    public static final SimpleDateFormat USER_DATE_FORMAT = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm", Locale.getDefault());

    public static final int ANSWER_DELAY = App.DEBUG ? (int) (1 * 1000) : 8 * 1000;

    public static final class ACTION {
        public static final int SEARCH = 1;
        public static final int SHARE = 2;
        public static final int GET_CONTACTS = 3;
        public static final int TOKEN = 5;

        public static final int PLAY_FLOW = 4;
        public static final int PLAY_FLOW_SINGLE_VIDEO = 5;

        public static final int FILL_PROFILE = 6;
        public static final int SHOW_PROFILE = 7;

        public static final int NEW_FEED_BACK = 8;
        public static final int NEW_POLL = 9;

        public static final int NAVIGATE_TO_PROFILE = 11;
    }

    public static final class PARAMS {
        public static final String ACTION = "action";
        public static final String VIDEO_LIST = "videoList";
        public static final String CURRENT_VIDEO = "currentVideo";
        public static final String TYPE = "type";
        public static final String PARAMS = "params";
        public static final String TITLE = "title";
        public static final String POSITION = "position";
        public static final String REPLY_ID = "reply_id";
        public static final String USER_ID = "userId";
        public static final String RELATIONSHIP = "relationship";
        public static final String CONTACTS = "contacts";
        public static final String CATEGORY = "category";
        public static final String NURSLING_ID = "nursling_id";
        public static final String NURSLING = "nursling";
    }

    public static final class PREF {
        public static final String GCM_TOKEN = "gcm_token";
        public static final String FCM_TOKEN = "fcm_token";
        public static final String PHONE = "phone_number";
        public static final String COUNTRY_CODE = "country_code";
        public static final String REG = "registered";
        public static final String TOKEN = "auth_token";
        //    public static final String PREF_SMILES_COUNT = "smiles_count";
        public static final String SUFFIX = "suffix";
    }


    //Directories for save files
    public static final String DIR_BASE = "clikclap";
    public static final String DIR_AUDIO = "music";

    public static final String API = "http://api.thehumanet.com/";
    public static final String BASE_API_URL = "/{appName}";

    public static final String GAMES_API_URL = "https://gameapi.1clickgain.com";


    public static class CongratulationsType {
        public static final int VERIFY_OK = 3;
        public static final int PROFILE_OK = 4;
    }

    public static final int ASRTA_GAME_QUESTION_LIMIT = 10;
    public static final int ASRTA_GAME_QUESTION_MINIMUM = 1;


    public static int LIMIT = 25;
}
