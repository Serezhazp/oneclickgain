package com.humanet.humanetcore.api.requests.vote;

import android.content.ContentValues;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.vote.VoteResponse;
import com.humanet.humanetcore.api.sets.VoteApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.vote.VoteOption;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVoteRequest extends RetrofitSpiceRequest<VoteResponse, VoteApiSet> {
    int mVoteId;

    public GetVoteRequest(int voteId) {
        super(VoteResponse.class, VoteApiSet.class);
        mVoteId = voteId;
    }

    @Override
    public VoteResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = buildRequestArgs(mVoteId);
        Log.i("API: GetVote", map.toString());
        VoteResponse voteResponse = getService().getVote(App.API_APP_NAME, map);

        VoteResponse.Response response = voteResponse.getResponse();


        List<VoteOption> optionsList = response.getOptions();
        int totalVotes = 0;
        if (optionsList != null) {
            int count = optionsList.size();

            ContentValues[] contentValues = new ContentValues[count];
            for (int i = 0; i < count; i++) {
                VoteOption option = optionsList.get(i);
                totalVotes += option.getVotes();
                ContentValues values = option.toContentValues();
                values.put(ContentDescriptor.VoteOptions.Cols.VOTE_ID, mVoteId);
                contentValues[i] = values;
            }

            App.getInstance().getContentResolver().delete(ContentDescriptor.VoteOptions.URI,
                    ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId, null);
            App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.VoteOptions.URI,
                    contentValues);
        }

        ContentValues voteValues = new ContentValues(5);
        voteValues.put(ContentDescriptor.Votes.Cols.MY_VOTE, response.getMyVote());
        voteValues.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, totalVotes);

        App.getInstance().getContentResolver().update(ContentDescriptor.Votes.URI,
                voteValues,
                "_id" + " = " + mVoteId,
                null);

        return voteResponse;
    }

    public static ArgsMap buildRequestArgs(int voteId) {
        ArgsMap map = new ArgsMap(true);
        map.put("id_vote", voteId);
        return map;
    }
}
