package com.humanet.humanetcore.api.response.feedback;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.FeedbackMessage;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class MessagesResponse extends BaseResponse {

    @SerializedName("messages")
    List<FeedbackMessage> mMessages;

    public List<FeedbackMessage> getMessages() {
        return mMessages;
    }
}