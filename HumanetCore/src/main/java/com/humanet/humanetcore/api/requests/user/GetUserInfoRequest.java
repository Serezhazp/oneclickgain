package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.GsonHelper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class GetUserInfoRequest<T extends UserInfo, R> extends RetrofitSpiceRequest<T, R> {

    int mUserId;

    public static IGetUserRequest sIGetUserRequest = new GetUserRequest();

    public GetUserInfoRequest(int userId) {
        super((Class<T>) App.getInstance().getUserClass(), (Class<R>) App.getInstance().getUserApiSetClass());
        mUserId = userId;
    }

    public GetUserInfoRequest() {
        super((Class<T>) App.getInstance().getUserClass(), (Class<R>) App.getInstance().getUserApiSetClass());
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);

        Log.i("API: GetUserInfoRequest", map.toString());
        if (mUserId != 0) {
            map.put("id_user", String.valueOf(mUserId));
        }

        T userInfo = (T) sIGetUserRequest.loadDataFromNetwork(getService(), map);

        Log.i("GetUserInfo", map.toString());
        Log.i("GetUserInfo", GsonHelper.getGson().toJson(userInfo));

        return userInfo;
    }

    private static class GetUserRequest implements IGetUserRequest<UserInfo> {
        @Override
        public UserInfo loadDataFromNetwork(Object userApiSet, ArgsMap map) throws Exception{
            return ((UserApiSet)userApiSet).getUserInfo(App.API_APP_NAME, map).getUserInfo();
        }
    }

   public interface IGetUserRequest<T extends UserInfo> {

      T  loadDataFromNetwork(Object userApiSet, ArgsMap map) throws Exception;

    }
}
