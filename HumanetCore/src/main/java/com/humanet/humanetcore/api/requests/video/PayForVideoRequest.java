package com.humanet.humanetcore.api.requests.video;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseCoinsResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class PayForVideoRequest extends RetrofitSpiceRequest<BaseCoinsResponse, VideoApiSet> {
    private int mVideoId;

    public PayForVideoRequest(int videoId) {
        super(BaseCoinsResponse.class, VideoApiSet.class);
        mVideoId = videoId;
    }

    @Override
    public BaseCoinsResponse loadDataFromNetwork() throws Exception {
        ArgsMap params = new ArgsMap(true);
        params.put("id_video", mVideoId);

        Log.d("PayForVideoRequest", params.toString());

        BaseCoinsResponse response = getService().payForVideo(App.API_APP_NAME, params);

        ContentResolver resolver = App.getInstance().getContentResolver();
        int earn = 0;//already earn sum
        Cursor cursor = resolver.query(ContentDescriptor.Videos.URI, new String[]{ContentDescriptor.Videos.Cols.EARN}, ContentDescriptor.Videos.Cols.ID + "=" + mVideoId, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                earn += cursor.getInt(0);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        int amount = response.getCoinsChange();
        earn += Math.abs(amount);

        ContentValues values = new ContentValues(2);
        values.put(ContentDescriptor.Videos.Cols.EARN, earn);
        values.put(ContentDescriptor.Videos.Cols.AVAILABLE, 0);
        resolver.update(ContentDescriptor.Videos.URI,
                values,
                ContentDescriptor.Videos.Cols.ID + " = " + mVideoId,
                null);

        Log.d("PayForVideo Response", new Gson().toJson(response));

        return response;
    }
}
