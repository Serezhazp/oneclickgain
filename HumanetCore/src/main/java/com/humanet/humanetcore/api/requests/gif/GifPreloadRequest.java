package com.humanet.humanetcore.api.requests.gif;

import com.humanet.humanetcore.api.requests.video.VideoPreloadRequest;
import com.humanet.humanetcore.utils.FilePathHelper;

import java.io.File;

/**
 * Created by serezha on 27.07.16.
 */
public class GifPreloadRequest extends VideoPreloadRequest {

	public GifPreloadRequest(String... videoUrls) {
		super(videoUrls);
	}

	@Override
	public File getCacheDirectory() {
		return FilePathHelper.getGifsPath();
	}

	@Override
	public File getTempCacheDirectory() {
		return FilePathHelper.getGifTmpCacheDirectory();
	}
}
