package com.humanet.humanetcore.api.requests.message;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.feedback.AddMessageResponse;
import com.humanet.humanetcore.api.sets.FeedbackRestApi;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import static com.humanet.humanetcore.model.Feedback.Type;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddMessageRequest extends RetrofitSpiceRequest<AddMessageResponse, FeedbackRestApi> {
    private int mListId;
    private String mMessage;
    private int mType;

    public AddMessageRequest(int listId, String message, @Type int type) {
        super(AddMessageResponse.class, FeedbackRestApi.class);
        mListId = listId;
        mMessage = message;
        mType = type;
    }

    @Override
    public AddMessageResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = buildRequestArgs(mListId, mMessage, mType);

        Log.i("API: AddMessageRequest", map.toString());

        return getService().addMessage(App.API_APP_NAME, map);
    }

    /**
     *
     * @param listId - id of open message list. set -1 to create new message list
     * @param message - new message text
     * @param type - feedback type
     * @return - build request params map
     */

    public static ArgsMap buildRequestArgs(int listId, String message, @Type int type) {
        ArgsMap map = new ArgsMap(true);
        map.put("type", type);
        map.put("message", message);
        if (listId > 0) {
            map.put("id_list", listId);
        }
        return map;
    }
}
