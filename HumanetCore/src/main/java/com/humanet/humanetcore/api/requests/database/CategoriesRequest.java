package com.humanet.humanetcore.api.requests.database;

import android.content.ContentValues;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.sets.FlowApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;

/**
 * Created by ovitali on 13.10.2015.
 */
public class CategoriesRequest extends RetrofitSpiceRequest<Category[], FlowApiSet> {

    public static final String VIDEO_TYPE_PARAM = "video_type";
    public static final String PARAMS = "params";
    public static final String USER_ID = "id_user";

    private VideoType mType;

    private ArgsMap mArgs = new ArgsMap(true);

    public CategoriesRequest(VideoType videoType) {
        super(Category[].class, FlowApiSet.class);
        mType = videoType;
        mArgs.put(VIDEO_TYPE_PARAM, videoType.getRequestParam());
        if (videoType.getAdditionParam() != null) {
            mArgs.put(PARAMS, videoType.getAdditionParam());
        }
        if (videoType == VideoType.PET_MY || videoType == VideoType.PET_ALL || videoType == VideoType.PET_MY_CHOICE) {
            mArgs.put(USER_ID, videoType.getUserId());
        }
        mArgs.put("type", "full");
    }


    @Override
    public Category[] loadDataFromNetwork() throws Exception {
        String sparams = mArgs.toString();
        Log.i("API: GetCategories", sparams);
        Category.Response response = getService().getCategories(App.API_APP_NAME, mArgs);

        Category[] categoryList = response.getCategories();

        String deleteSelect = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + mType.getId();
        App.getInstance().getContentResolver().delete(ContentDescriptor.Categories.URI, deleteSelect, null);

        if (categoryList != null) {
            ArrayList<ContentValues> contentValueses = new ArrayList<>();

            for (Category category : categoryList) {
                ContentValues values = category.toContentValues();
                values.put(ContentDescriptor.Categories.Cols.VIDEO_TYPE, mType.getId());
                contentValueses.add(values);
            }

            ContentValues[] contentValuesToInsert = new ContentValues[contentValueses.size()];
            contentValueses.toArray(contentValuesToInsert);

            App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Categories.URI, contentValuesToInsert);
        }

        return categoryList;
    }

}
