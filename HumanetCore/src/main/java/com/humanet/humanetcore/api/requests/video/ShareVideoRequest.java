package com.humanet.humanetcore.api.requests.video;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class ShareVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoApiSet> {
    int mVideoId;
    boolean mIsGrimace;
    String mPhones;

    public ShareVideoRequest(int videoId, boolean isGrimace, String phones) {
        super(BaseResponse.class, VideoApiSet.class);
        mVideoId = videoId;
        mIsGrimace = isGrimace;
        mPhones = phones;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        if (mIsGrimace) {
            map.put("type", "grimace");
            map.put("numbers", "");
        } else {
            map.put("numbers", mPhones);
        }
        return getService().shareVideo(App.API_APP_NAME,map);
    }
}
