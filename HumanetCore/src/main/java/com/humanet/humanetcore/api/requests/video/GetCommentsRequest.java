package com.humanet.humanetcore.api.requests.video;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.video.CommentsResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovitali on 13.10.2015.
 */
public class GetCommentsRequest extends RetrofitSpiceRequest<CommentsResponse, VideoApiSet> {

    private ArgsMap mArgsMap;

    public GetCommentsRequest(int videoId) {
        super(CommentsResponse.class, VideoApiSet.class);
        mArgsMap = new ArgsMap(true);
        mArgsMap.put("id_video", videoId);
    }

    @Override
    public CommentsResponse loadDataFromNetwork() throws Exception {
        return getService().getComments(App.API_APP_NAME,mArgsMap);
    }
}
