package com.humanet.humanetcore.api.requests.user;

import android.text.TextUtils;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.model.UserInfo;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 17.09.15.
 */
public class EditUserInfoRequest extends RetrofitSpiceRequest<BaseResponse, UserApiSet> {
    private UserInfo mUserInfo;
    private ArgsMap mArgsMap;

    public EditUserInfoRequest(ArgsMap map) {
        super(BaseResponse.class, UserApiSet.class);
        //this.mUserInfo = user;
        this.mArgsMap = map;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        //ArgsMap map = userInfoToArgsMap(mUserInfo);

        Log.i("API: EditUserInfo", mArgsMap.toString());

        return getService().editUserInfo(App.API_APP_NAME, mArgsMap);
    }

    /*public static ArgsMap bastogramUserInfoToArgsMap(BastogramUserInfo userInfo) {
        ArgsMap map = userInfoToArgsMap((BastogramUserInfo) userInfo);

        if(userInfo.getSkills != null)
            map.put("skills", userInfo.getSkills());
    }*/

    public static ArgsMap userInfoToArgsMap(UserInfo userInfo) {
        ArgsMap map = new ArgsMap(true);

        map.put("first_name", userInfo.getFName());

        if (!TextUtils.isEmpty(userInfo.getPatronymic())) {
            map.put("patronymic", userInfo.getPatronymic());
        }

        if (!TextUtils.isEmpty(userInfo.getLName())) {
            map.put("last_name", userInfo.getLName());
        }

        // do not save local file
        if (!userInfo.getAvatar().startsWith("file"))
            map.put("photo", userInfo.getAvatar());

        map.put("lang", userInfo.getLanguage().getRequestParam());
        map.put("id_country", userInfo.getCountryId());
        map.put("id_city", userInfo.getCityId());
        map.put("gender", userInfo.getGender());
        map.put("birth_date", userInfo.getBirthDate());


        if (userInfo.getDegree() != null) {
            map.put("degree", userInfo.getDegree().getRequestParam());
        }

        if (userInfo.getJobType() != null) {
            map.put("job", userInfo.getJobType().getRequestParam());
        }

        if (userInfo.getHobbie() != null) {
            map.put("hobbie", userInfo.getHobbie().getRequestParam());
        }

        if (userInfo.getSport() != null) {
            map.put("sport", userInfo.getSport().getRequestParam());
        }

        if (userInfo.getPet() != null) {
            map.put("pets", userInfo.getPet().getRequestParam());
        }

        if (userInfo.getInterest() != null) {
            map.put("interest", userInfo.getInterest().getRequestParam());
        }

        if (userInfo.getReligion() != null) {
            map.put("religion", userInfo.getReligion().getRequestParam());
        }

        if (userInfo.getProfession() != null) {
            map.put("craft", userInfo.getProfession());
        }

        if (userInfo.getGames() != null) {
            map.put("game", userInfo.getGames().getRequestParam());
        }
        if (userInfo.getNonGame() != null) {
            map.put("non_game", userInfo.getNonGame().getRequestParam());
        }
        if (userInfo.getExtreme() != null) {
            map.put("extreme", userInfo.getExtreme().getRequestParam());
        }
        if (userInfo.getVirtual() != null) {
            map.put("virtual", userInfo.getVirtual().getRequestParam());
        }
        if (userInfo.getHowILook() != null) {
            map.put("how_i_look", userInfo.getHowILook().getRequestParam());
        }
        if (userInfo.getPartGames() != null) {
            map.put("part_games", userInfo.getPartGames().getRequestParam());
        }

        if (userInfo.isWidget()) {
            map.put("widget", 1);
        }


        return map;
    }
}
