package com.humanet.humanetcore.api.response.video;

import com.humanet.humanetcore.api.response.BaseResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 13.10.2015.
 */
public class AddVideoResponse extends BaseResponse {

    @SerializedName("id_video")
    int mVideoId;

    @SerializedName("short_url")
    String mShortUrl;

    @SerializedName("coins_change")
    int mCoinsChange;

    @SerializedName("coins")
    int mCoins;



    public int getCoins() {
        return mCoins;
    }

    public int getCoinsChange() {
        return mCoinsChange;
    }

    public String getShortUrl() {
        return mShortUrl;
    }

    public int getVideoId() {
        return mVideoId;
    }


}
