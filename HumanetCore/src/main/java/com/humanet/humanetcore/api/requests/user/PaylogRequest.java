package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.utils.GsonHelper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.HashMap;

/**
 * Created by serezha on 14.07.16.
 */
public class PaylogRequest extends RetrofitSpiceRequest<BaseResponse, UserApiSet> {

	private ArgsMap mParams = new ArgsMap();

	public PaylogRequest(long objectId, String text) {
		super(BaseResponse.class, UserApiSet.class);


		HashMap<String, Object> params = new HashMap<>();

		params.put("objectId", objectId);
		params.put("data", text);

		mParams.put("text", new Gson().toJson(params));
	}

	@Override
	public BaseResponse loadDataFromNetwork() throws Exception {

		ArgsMap map = new ArgsMap(true);
		if (mParams != null) {
			map.put("text", String.valueOf(mParams));
			Log.i("API: PaylogRequest", mParams.toString());
		}

		BaseResponse baseResponse = getService().paylog(App.API_APP_NAME, mParams);

		Log.i("PaylogResponse", GsonHelper.getGson().toJson(baseResponse));

		return baseResponse;
	}
}
