package com.humanet.humanetcore.api.sets;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.user.AuthResponse;
import com.humanet.humanetcore.api.response.user.CoinsHistoryListResponse;
import com.humanet.humanetcore.api.response.user.UserSearchResponse;
import com.humanet.humanetcore.api.response.user.VerifyCodeResponse;
import com.humanet.humanetcore.model.Rating;
import com.humanet.humanetcore.model.UserInfo;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

public interface UserApiSet {

    @POST(Constants.BASE_API_URL + "/user.add")
    AuthResponse auth(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.get")
    UserInfo.UserResponse getUserInfo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.edit")
    BaseResponse editUserInfo(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.token")
    BaseResponse sendToken(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.follow")
    BaseResponse followUser(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.unfollow")
    BaseResponse unfollowUser(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.coinsHistory")
    CoinsHistoryListResponse getCoinsHistory(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.search")
    UserSearchResponse searchUser(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.getCode")
    BaseResponse getSmsCode(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.verify")
    VerifyCodeResponse verifySmsCode(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.rating")
    Rating getRating(@Path("appName") String appName, @Body ArgsMap options);


    @POST(Constants.BASE_API_URL + "/user.logout")
    BaseResponse logout(@Path("appName") String appName, @Body ArgsMap options);


    @POST(Constants.BASE_API_URL + "/user.addCoins")
    CoinsHistoryListResponse addCoins(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/user.paylog")
    BaseResponse paylog(@Path("appName") String appName, @Body ArgsMap options);

}
