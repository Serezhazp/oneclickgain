package com.humanet.humanetcore.api.response.user;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.Balance;

import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class CoinsHistoryListResponse extends BaseResponse{

    @SerializedName("balance")
    float mBalance;

    @SerializedName("history")
    List<Balance> mHistory;

    public float getBalance() {
        return mBalance;
    }

    public List<Balance> getHistory() {
        return mHistory;
    }

}
