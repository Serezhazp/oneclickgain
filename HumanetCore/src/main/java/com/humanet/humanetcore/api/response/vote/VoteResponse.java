package com.humanet.humanetcore.api.response.vote;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.vote.VoteOption;

import java.util.List;

/**
 * Created by ovitali on 16.11.2015.
 */
public class VoteResponse extends BaseResponse {

    @SerializedName("vote")
    private Response mResponse;

    public Response getResponse() {
        return mResponse;
    }

    public static class Response {

        @SerializedName("options")
        List<VoteOption> mOptions;

        @SerializedName("my_vote")
        int mMyVote;

        public List<VoteOption> getOptions() {
            return mOptions;
        }


        public int getMyVote() {
            return mMyVote;
        }
    }
}
