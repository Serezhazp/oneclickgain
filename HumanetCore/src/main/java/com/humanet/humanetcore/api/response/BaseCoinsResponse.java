package com.humanet.humanetcore.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 13.10.2015.
 */
public class BaseCoinsResponse extends BaseResponse {

    @SerializedName("coins_change")
    int mCoinsChange;

    public int getCoinsChange() {
        return mCoinsChange;
    }

}
