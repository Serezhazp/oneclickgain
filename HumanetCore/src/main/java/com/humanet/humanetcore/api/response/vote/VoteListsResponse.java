package com.humanet.humanetcore.api.response.vote;

import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.vote.Vote;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 17.11.2015.
 */
public class VoteListsResponse extends BaseResponse {
    @SerializedName("votes")
    Vote[] mVotes;

    public Vote[] getVotes() {
        return mVotes;
    }
}
