package com.humanet.humanetcore.api.requests.video;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class ComplaintVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoApiSet> {
    private int mVideoId;

    public ComplaintVideoRequest(int videoId) {
        super(BaseResponse.class, VideoApiSet.class);
        mVideoId = videoId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap params = new ArgsMap(true);
        params.put("id_video", mVideoId);
        return getService().complaintVideo(App.API_APP_NAME,params);
    }
}
