package com.humanet.humanetcore.api.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class BaseResponse implements Serializable{

    @SerializedName("err")
    int[] mErrorCodes;

    @SerializedName("err_msg")
    String[] mErrorMessages;

    public int[] getErrorCodes() {
        return mErrorCodes;
    }

    public String[] getErrorMessages() {
        return mErrorMessages;
    }

    public boolean isSuccess() {
        return (mErrorCodes == null || mErrorCodes.length == 0 || mErrorCodes[0] == 0);
    }

    public String getErrorsMessage() {
        String msg = null;
        if (mErrorMessages != null) {
            msg = TextUtils.join(", ", mErrorMessages);
        }
        if (msg != null && (msg.equals("") || msg.equals("null")))
            return null;
        return msg;
    }

}
