package com.humanet.humanetcore.api.requests.database;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.sets.DatabaseApiSet;
import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.model.enums.Language;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovitali on 12.10.2015.
 */
public class CitiesRequest extends RetrofitSpiceRequest<City[], DatabaseApiSet> {
    private int mCountryId;
    private String mTypedName;

    public CitiesRequest(int countryId, String typedName) {
        super(City[].class, DatabaseApiSet.class);
        mCountryId = countryId;
        mTypedName = typedName;
    }

    @Override
    public City[] loadDataFromNetwork() throws Exception {
        Thread.sleep(50);
        if (isCancelled())
            return null;

        City[] cities;
        ArgsMap argsMap = buildRequestParams(mCountryId, mTypedName);

        Log.i("CitiesRequest", argsMap.toString());
        cities = getService().getCities(App.API_APP_NAME, argsMap).getCities();
        if (cities == null)
            cities = new City[0];

        return cities;
    }

    public static ArgsMap buildRequestParams(int countryId, String typedName) {
        ArgsMap argsMap = new ArgsMap(true);
        argsMap.put("id_country", countryId);
        if (typedName != null) {
            argsMap.put("q", typedName);
            argsMap.put("lang", Language.getSystem().getRequestParam());
        }

        return argsMap;
    }

}
