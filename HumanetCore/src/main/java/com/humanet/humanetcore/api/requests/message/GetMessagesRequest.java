package com.humanet.humanetcore.api.requests.message;

import android.content.ContentValues;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.feedback.MessagesResponse;
import com.humanet.humanetcore.api.sets.FeedbackRestApi;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.FeedbackMessage;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMessagesRequest extends RetrofitSpiceRequest<MessagesResponse, FeedbackRestApi> {
    int mListId;

    public GetMessagesRequest(int listId) {
        super(MessagesResponse.class, FeedbackRestApi.class);
        mListId = listId;
    }

    @Override
    public MessagesResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = buildRequestArgs(mListId);

        MessagesResponse response = getService().getMessages(App.API_APP_NAME, map);

        List<FeedbackMessage> list = response.getMessages();
        if (list != null) {
            int count = list.size();

            ContentValues[] contentValues = new ContentValues[count];
            for (int i = 0; i < count; i++) {
                ContentValues values = list.get(i).toContentValues();
                values.put(ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK, mListId);
                contentValues[i] = values;
            }

            App.getInstance().getContentResolver().delete(ContentDescriptor.FeedbackMessages.URI,
                    ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK + " = " + mListId, null);
            App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.FeedbackMessages.URI,
                    contentValues);
        }

        return response;
    }

    public static ArgsMap buildRequestArgs(int listId) {
        ArgsMap map = new ArgsMap(true);
        map.put("id_list", listId);
        return map;
    }
}
