package com.humanet.humanetcore.api.requests.video;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class MarkVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoApiSet> {




    private int mVideoId;
    private int mMark;

    public MarkVideoRequest(int videoId, int mark) {
        super(BaseResponse.class, VideoApiSet.class);
        mVideoId = videoId;
        mMark = mark;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put("mark", mMark);

        return getService().markVideo(App.API_APP_NAME, map);
    }
}
