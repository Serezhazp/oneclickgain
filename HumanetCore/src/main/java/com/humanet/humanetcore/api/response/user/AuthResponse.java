package com.humanet.humanetcore.api.response.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class AuthResponse {

    @SerializedName("auth_token")
    String mToken;

    @SerializedName("id_user")
    int mUserId;

    public int getUserId() {
        return mUserId;
    }

    public String getToken() {
        return mToken;
    }
}