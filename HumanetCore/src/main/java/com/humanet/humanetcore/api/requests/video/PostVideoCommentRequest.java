package com.humanet.humanetcore.api.requests.video;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.video.CommentsResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovitali on 13.10.2015.
 */
public class PostVideoCommentRequest extends RetrofitSpiceRequest<CommentsResponse, VideoApiSet> {

    private ArgsMap mArgsMap;

    public PostVideoCommentRequest(int videoId, String comment) {
        super(CommentsResponse.class, VideoApiSet.class);
        mArgsMap = new ArgsMap(true);
        mArgsMap.put("id_video", videoId);
        mArgsMap.put("comment", comment);
    }

    @Override
    public CommentsResponse loadDataFromNetwork() throws Exception {
        Log.i("PostVideoCommentRequest", mArgsMap.toString());
        getService().commentVideo(App.API_APP_NAME,mArgsMap);
        return getService().getComments(App.API_APP_NAME,mArgsMap);
    }
}
