package com.humanet.humanetcore.api.requests.vote;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.vote.VoteListsResponse;
import com.humanet.humanetcore.api.sets.VoteApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.db.DBHelper;
import com.humanet.humanetcore.model.vote.Vote;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import static com.humanet.humanetcore.fragments.vote.VoteTabFragment.VotePeriod;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVotesListRequest extends RetrofitSpiceRequest<VoteListsResponse, VoteApiSet> {


    @VotePeriod
    private String mType;

    private int mTypeId;

    public GetVotesListRequest(@VotePeriod String type, int typeId) {
        super(VoteListsResponse.class, VoteApiSet.class);
        mType = type;
        mTypeId = typeId;
    }

    @Override
    public VoteListsResponse loadDataFromNetwork() throws Exception {

        ArgsMap map = buildRequestArgs(mType);

        Log.i("API: GetVotesList", map.toString());

        VoteListsResponse response = getService().getVoteList(App.API_APP_NAME, map);


        boolean changed = false;
        Vote[] voteList = response.getVotes();

        if (voteList != null) {
            for (final Vote vote : voteList) {
                ContentValues values = vote.toContentValues();
                values.put(ContentDescriptor.Votes.Cols.TYPE, mTypeId);

                if (DBHelper.getInstance().getReadableDatabase().insertWithOnConflict(ContentDescriptor.Votes.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE) > 0) {
                    changed = true;
                }
            }
            if (changed)
                App.getInstance().getContentResolver().notifyChange(ContentDescriptor.Votes.URI, null);

        }
        return response;
    }

    public static ArgsMap buildRequestArgs(@VotePeriod String period) {
        ArgsMap map = new ArgsMap(true);
        map.put("type", period);
        return map;
    }
}
