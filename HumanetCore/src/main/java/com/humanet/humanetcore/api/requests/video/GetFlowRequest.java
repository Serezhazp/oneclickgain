package com.humanet.humanetcore.api.requests.video;

/**
 * Created by ovitali on 15.10.2015.
 */

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.video.FlowResponse;
import com.humanet.humanetcore.api.sets.FlowApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Video;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class GetFlowRequest extends RetrofitSpiceRequest<FlowResponse, FlowApiSet> {
    HashMap<String, Object> mParams;
    int mPetId;
    int mReplyId;
    long mLast;
    int mUserId;

    //
    private long mScreen;
    private boolean mPagination;

    public GetFlowRequest(HashMap<String, Object> params) {
        super(FlowResponse.class, FlowApiSet.class);
        mParams = params;
    }

    public GetFlowRequest() {
        super(FlowResponse.class, FlowApiSet.class);
    }

    public void setReplyId(int replyId) {
        mReplyId = replyId;
    }

    public void setLast(long last) {
        mLast = last;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public void setPetId(int petId) {
        mPetId = petId;
    }

    public void saveData(long screen, boolean isPagination) {
        mScreen = screen;
        mPagination = isPagination;
    }


    @Override
    public FlowResponse loadDataFromNetwork() throws Exception {
        ArgsMap argsMap = new ArgsMap(true);

        if (mUserId != 0) {
            argsMap.put("id_user", mUserId);
        }

        if (mPetId != 0) {
            argsMap.put("id_pet", mPetId);
        }

        if (mReplyId != 0) {
            argsMap.put("id_reply", mReplyId);
        } else {
            if (mParams != null) {
                argsMap.putAll(mParams);
            }
        }

        if (mLast > 0) {
            argsMap.put("last", mLast);
        }

        Log.i("API: GetFlowRequest", argsMap.toString());
        FlowResponse flowResponse = getService().getFlow(App.API_APP_NAME, argsMap);

        Context context = App.getInstance();
        ArrayList<Video> videos = new ArrayList<>();
        if (!mPagination) {
            context.getContentResolver().delete(
                    ContentDescriptor.Videos.URI,
                    ContentDescriptor.Videos.Cols.SCREEN + " = " + mScreen,
                    null);
        }

        ArrayList<Video> list = flowResponse.getFlow();

        if (list != null) {
            int count = list.size();

            final ContentResolver contentResolver = context.getContentResolver();

            ContentValues[] contentValueses = new ContentValues[count];
            for (int i = 0; i < count; i++) {
                Video video = list.get(i);
                videos.add(video);
                ContentValues values = video.toContentValues();
                values.put(ContentDescriptor.Videos.Cols.SCREEN, mScreen);
                contentValueses[i] = values;
            }
            contentResolver.bulkInsert(ContentDescriptor.Videos.URI, contentValueses);
        }

        return flowResponse;
    }
}