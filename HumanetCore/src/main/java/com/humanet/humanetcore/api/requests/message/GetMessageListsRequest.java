package com.humanet.humanetcore.api.requests.message;

import android.content.ContentValues;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.feedback.MessageListsResponse;
import com.humanet.humanetcore.api.sets.FeedbackRestApi;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.Feedback;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMessageListsRequest extends RetrofitSpiceRequest<MessageListsResponse, FeedbackRestApi> {

    public GetMessageListsRequest() {
        super(MessageListsResponse.class, FeedbackRestApi.class);
    }

    @Override
    public MessageListsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        MessageListsResponse response = getService().getMessageLists(App.API_APP_NAME, map);
        if (response != null && response.isSuccess()) {
            List<Feedback> list = response.getLists();
            if (list != null) {
                int count = list.size();

                ArrayList<ContentValues> contentValues = new ArrayList<>(count);
                for (int i = 0; i < count; i++) {
                    Feedback feedback = list.get(i);
                    if (feedback.getId() != 0) {
                        ContentValues values = feedback.toContentValues();
                        contentValues.add(values);
                    }
                }

                App.getInstance().getContentResolver().delete(ContentDescriptor.Feedback.URI, null, null);

                if (contentValues.size() > 0)
                    App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Feedback.URI, contentValues.toArray(new ContentValues[contentValues.size()]));
            }
        }

        return response;
    }
}
