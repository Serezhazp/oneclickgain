package com.humanet.humanetcore.api.requests.video;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.CountingFileRequestBody;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.api.response.video.AddVideoResponse;
import com.humanet.humanetcore.api.sets.VideoApiSet;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.dialogs.ShareDialog;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by ovitali on 19.10.2015.
 */
public final class VideoUploadRequest extends RetrofitSpiceRequest<UploadingMedia, VideoApiSet> {

    private static final String TAG = "VideoUploadRequest";

    private static final int NOTIFICATION_ID = 2;

    private static AdditionalUploadingActionDelegate sAdditionalUploadingActionDelegate;

    public static void init(AdditionalUploadingActionDelegate additionalUploadingActionDelegate) {
        sAdditionalUploadingActionDelegate = additionalUploadingActionDelegate;
    }


    @VideoInfo.AvatarType
    private int mAvatarType;

    private VideoInfo mVideoInfo;

    private int mVideoId;
    private int mPetId;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    public VideoUploadRequest(int videoId) {
        super(UploadingMedia.class, VideoApiSet.class);

        mVideoId = videoId;

        Context context = App.getInstance();

        mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle("Video Upload").setSmallIcon(android.R.drawable.stat_sys_upload);
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        mBuilder.setLargeIcon(bm);
    }

    public VideoUploadRequest(int videoId, int petId) {
        this(videoId);
        mPetId = petId;
    }

    @Override
    public UploadingMedia loadDataFromNetwork() throws Exception {
        try {
            mVideoInfo = getVideoInfo();
            if (mVideoInfo == null)
                return null;

            mAvatarType = mVideoInfo.isAvatar();

            changeVideoPostingStatus(VideoInfo.STATUS_UPLOADING);

            //upload video and image file to server
            UploadingMedia uploadingMedia = uploadVideo();

            uploadAdditionalDataForVideo(uploadingMedia);

            AddVideoResponse addVideoResponse = saveVideo(uploadingMedia);
            uploadingMedia.setShortUrl(addVideoResponse.getShortUrl());
            uploadingMedia.setVideoId(addVideoResponse.getVideoId());
            uploadingMedia.setShareType(mVideoInfo.getSmileId() > 0 ? ShareDialog.TYPE_EMOTICON : ShareDialog.TYPE_SHOT);

            // post event with video uploaded data.  this event will be caught in current running activity (based on BaseVideoManageActivity)
            if (mAvatarType == VideoInfo.NONE || mAvatarType == VideoInfo.PET_AVATAR) {
                EventBus.getDefault().post(addVideoResponse);
            }


            App.getInstance().getContentResolver().delete(ContentDescriptor.VideosUpload.URI, "_id = " + mVideoInfo.getLocalId(), null);

            EventBus.getDefault().post(uploadingMedia);

            try {
                FileUtils.forceDelete(new File(mVideoInfo.getVideoPath()));
                FileUtils.forceDelete(new File(mVideoInfo.getImagePath()));
            } catch (IOException ignore) {
                // ignore IOException
            }

            return uploadingMedia;

        } catch (Exception ex) {
            changeVideoPostingStatus(VideoInfo.STATUS_UPLOAD_REQUIRED);

            if (mNotifyManager != null) {
                mNotifyManager.cancel(NOTIFICATION_ID);
            }

            throw ex;
        }
    }

    private void uploadAdditionalDataForVideo(UploadingMedia uploadingMedia) throws Exception {
        if (mAvatarType == VideoInfo.AVATAR && AppUser.getInstance().get() != null) {
            AppUser.getInstance().get().setAvatar(uploadingMedia.getThumb());

            getService().editUserInfo(App.API_APP_NAME, EditUserInfoRequest.userInfoToArgsMap(AppUser.getInstance().get()));
        }

        if (sAdditionalUploadingActionDelegate != null)
            sAdditionalUploadingActionDelegate.additionalAction(mVideoInfo, uploadingMedia);
    }


    private void changeVideoPostingStatus(@VideoInfo.UploadingStatus int status) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(ContentDescriptor.VideosUpload.Cols.UPLOADING_STATUS, status);
        saveVideoData(contentValues);
    }

    private void saveVideoUploadingData(String videoUrl, String imageUrl) {
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(ContentDescriptor.VideosUpload.Cols.UPLOADED_VIDEO, videoUrl);
        contentValues.put(ContentDescriptor.VideosUpload.Cols.UPLOADED_IMAGE, imageUrl);
        saveVideoData(contentValues);
    }

    private void saveVideoData(ContentValues contentValues) {
        App.getInstance().getContentResolver().update(ContentDescriptor.VideosUpload.URI, contentValues, "_id = " + mVideoInfo.getLocalId(), null);
    }

    private AddVideoResponse saveVideo(UploadingMedia uploadingMedia) {
        ArgsMap params = new ArgsMap(true);

        VideoInfo videoInfo = mVideoInfo;
        int audioId = 0;

        if (videoInfo.getAudioPath() != null) {
            String[] audioFiles = FilePathHelper.getAudioDirectory().list();
            for (int i = 0; i < audioFiles.length; i++) {
                if (audioFiles[i].contains(videoInfo.getAudioPath())) {
                    audioId = i;
                    break;
                }
            }
        }

        float duration = 0;
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(new File(videoInfo.getVideoPath()).getAbsolutePath());
            duration = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            duration /= 1000.f;
            duration = duration == 0 ? 1 : duration;
            mediaMetadataRetriever.release();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (mAvatarType == VideoInfo.AVATAR || mAvatarType == VideoInfo.PET_AVATAR) {
            params.put("avatar", 1);
        }

        if (mPetId != 0) {
            params.put("id_pet", mPetId);
        }

        params.put("media", uploadingMedia.getMedia());
        params.put("thumb", uploadingMedia.getThumb());

        params.put("id_audio", String.valueOf(audioId));
        params.put("id_effect", 0 + "");
        params.put("id_color", 0 + "");
        params.put("length", duration);
        params.put("id_smile", videoInfo.getSmileId());
        params.put("id_reply", videoInfo.getReplyToVideoId());

        params.put("location_x", videoInfo.getLatitude());
        params.put("location_y", videoInfo.getLongitude());

        if (mAvatarType == VideoInfo.NONE || mAvatarType == VideoInfo.PET_AVATAR) {
            if (videoInfo.getCategory() != 0) {
                params.put("id_category", videoInfo.getCategory());
            }
        } else {
            params.put("id_category", 1);
        }

        if (!TextUtils.isEmpty(videoInfo.getTags())) {
            params.put("title", videoInfo.getTags());
        }

        if (videoInfo.getVideoType() != null) {
            params.put("type", videoInfo.getVideoType() == VideoType.PET_MY ? 20 : videoInfo.getVideoType().getRequestParam()); //TODO: Dirty hack to put undocumented value in request
            if (videoInfo.getVideoType().getAdditionParam() != null)
                params.put("params", videoInfo.getVideoType().getAdditionParam());
        } else {
            params.put("type", VideoType.EMOTICON_ALL.getRequestParam());
        }

        if (videoInfo.getCost() != 0) {
            params.put("cost", videoInfo.getCost());
        }

        Log.d(TAG, "params:" + params.toString());
        return getService().addVideo(App.API_APP_NAME, params);
    }

    private UploadingMedia uploadVideo() throws Exception {
        UploadingMedia uploadingMedia;
        if (mVideoInfo.getUploadedVideoPath() != null || mVideoInfo.getUploadedImagePath() != null) {
            uploadingMedia = new UploadingMedia();
            uploadingMedia.setMedia(mVideoInfo.getUploadedVideoPath());
            uploadingMedia.setThumb(mVideoInfo.getUploadedImagePath());
        } else {
            File videoFile = new File(mVideoInfo.getVideoPath());
            File imgFile = new File(mVideoInfo.getImagePath());

            Log.i(TAG, "start uploading:" + new Date());

            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
            final MediaType MEDIA_TYPE_MP4 = MediaType.parse("video/mp4");

            final OkHttpClient client = new OkHttpClient();

            MultipartBuilder multipartBuilder = new MultipartBuilder();
            multipartBuilder.type(MultipartBuilder.FORM);
            multipartBuilder.addPart(Headers.of("Content-Disposition", "form-data; name=\"thumb\"; filename=\"" + imgFile.getName() + "\""),
                    RequestBody.create(MEDIA_TYPE_PNG, imgFile));

            multipartBuilder.addPart(
                    Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"" + videoFile.getName() + "\""),
                    new CountingFileRequestBody(RequestBody.create(MEDIA_TYPE_MP4, videoFile), new CountingFileRequestBody.Listener() {
                        int mLastProgress = -1;

                        @Override
                        public void onRequestProgress(long bytesWritten, long contentLength) {
                            int progress = (int) ((bytesWritten / (float) contentLength) * 100);
                            if (progress != mLastProgress && progress > mLastProgress) {
                                publishProgress(progress);
                                Log.d(TAG, "uploaded:" + progress + "%");
                                mLastProgress = progress;
                            }
                        }
                    })
            );

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(App.getInstance().getServerEndpoint() + "/media/")
                    .post(requestBody)
                    .build();

            Response requestResponse = client.newCall(request).execute();

            String responseString = IOUtils.toString(requestResponse.body().byteStream());

            uploadingMedia = new Gson().fromJson(responseString, UploadingMedia.class);

            saveVideoUploadingData(uploadingMedia.getMedia(), uploadingMedia.getThumb());


            Log.d(TAG, "response:" + responseString);
        }
        return uploadingMedia;
    }

    @Override
    protected void publishProgress(float progress) {
        super.publishProgress(progress);

        if (progress < 100) {
            mBuilder.setProgress(100, (int) progress, false);
            mBuilder.setContentText("Progress: " + progress + "%");
            mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
        } else {
            mNotifyManager.cancel(NOTIFICATION_ID);
        }
    }


    private VideoInfo getVideoInfo() {
        Cursor videoCursor = App.getInstance().getContentResolver().query(ContentDescriptor.VideosUpload.URI,
                null,
                ContentDescriptor.VideosUpload.Cols.UPLOADING_STATUS + " = " + VideoInfo.STATUS_UPLOAD_REQUIRED + " AND _id=" + mVideoId,
                null,
                null);

        VideoInfo videoInfo = null;

        if (videoCursor != null && videoCursor.moveToFirst()) {
            videoInfo = VideoInfo.fromCursor(videoCursor);

            videoCursor.close();
        }

        return videoInfo;
    }

    public interface AdditionalUploadingActionDelegate {
        void additionalAction(VideoInfo videoInfo, UploadingMedia uploadingMedia);
    }
}
