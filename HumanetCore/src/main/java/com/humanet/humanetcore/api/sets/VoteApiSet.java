package com.humanet.humanetcore.api.sets;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.vote.AddVoteResponse;
import com.humanet.humanetcore.api.response.vote.VoteListsResponse;
import com.humanet.humanetcore.api.response.vote.VoteResponse;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public interface VoteApiSet {

    @POST(Constants.BASE_API_URL + "/vote.add")
    AddVoteResponse addVote(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/vote.vote")
    BaseResponse sendVote(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/vote.getList")
    VoteListsResponse getVoteList(@Path("appName") String appName, @Body ArgsMap options);


    @POST(Constants.BASE_API_URL + "/vote.my")
    VoteListsResponse getMyVoteList(@Path("appName") String appName, @Body ArgsMap options);

    @POST(Constants.BASE_API_URL + "/vote.get")
    VoteResponse getVote(@Path("appName") String appName, @Body ArgsMap options);
}