package com.humanet.humanetcore.api.response.video;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.Comment;

/**
 * Created by ovi on 12/23/15.
 */
public class CommentsResponse extends BaseResponse {

    @SerializedName("comment")
    private Comment[] mComments;

    public Comment[] getComments() {
        return mComments;
    }

}
