package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.model.Rating;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovitali on 13.10.2015.
 */
public class RatingRequest extends RetrofitSpiceRequest<Rating, UserApiSet> {

    private ArgsMap argsMap;

    public RatingRequest(String ratingType) {
        super(Rating.class, UserApiSet.class);
        argsMap = new ArgsMap(true);

        if (ratingType != null)
            argsMap.put("type", ratingType);
    }

    @Override
    public Rating loadDataFromNetwork() throws Exception {
        Log.i("RatingRequest", argsMap.toString());

        return getService().getRating(App.API_APP_NAME, argsMap);
    }
}
