package com.humanet.humanetcore.api.response.video;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.model.Video;

import java.util.ArrayList;

/**
 * Created by ovitali on 15.10.2015.
 */
public class FlowResponse extends BaseResponse{

    @SerializedName("flow")
    ArrayList<Video> mFlow;

    public ArrayList<Video> getFlow() {
        return mFlow;
    }

}
