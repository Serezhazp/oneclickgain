package com.humanet.humanetcore.api.requests.user;

import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovi on 1/5/16.
 */
public class LogoutRequest extends RetrofitSpiceRequest<BaseResponse, UserApiSet> {

    private ArgsMap mParams = new ArgsMap();

    public LogoutRequest() {
        super(BaseResponse.class, UserApiSet.class);
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        Log.i("API: LogoutRequest", mParams.toString());
        return getService().logout(App.API_APP_NAME, mParams);
    }
}
