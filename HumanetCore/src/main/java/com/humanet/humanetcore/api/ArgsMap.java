package com.humanet.humanetcore.api;

import com.google.gson.Gson;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.utils.PrefHelper;

import java.util.HashMap;

/**
 * Created by ovitali on 12.10.2015.
 */
public class ArgsMap extends HashMap<String, Object> {

    public ArgsMap() {
        this(true);
    }

    public ArgsMap(boolean withAuthToken) {
        if (withAuthToken) {
            put("auth_token", PrefHelper.getStringPref(Constants.PREF.TOKEN));
        }
    }

    public String put(String key, int value) {
        return (String) super.put(key, String.valueOf(value));
    }

    public String put(String key, long value) {
        return (String)super.put(key, String.valueOf(value));
    }




    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
