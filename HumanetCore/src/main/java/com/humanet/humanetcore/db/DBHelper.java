package com.humanet.humanetcore.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.humanet.humanetcore.App;

/**
 * Created by michael on 07.11.14.
 * Changed by Denis on 25.02.15
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "oneclickgain.db";

    private static DBHelper instance;

    private static Delegate sDelegate;

    public static void init(Delegate delegate) {
        sDelegate = delegate;
    }

    public static DBHelper getInstance() {
        if (instance == null) {
            instance = new DBHelper(App.getInstance());
        }
        return instance;
    }

    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    private DBHelper(Context c) {
        super(c, DB_NAME, null, App.getDatabaseVersion());
    }

    private void createTables(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE " + ContentDescriptor.Videos.TABLE_NAME
                + "(  "
                + ContentDescriptor.Videos.Cols.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ContentDescriptor.Videos.Cols.VIDEO_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.VIDEO_URL + " TEXT,"
                + ContentDescriptor.Videos.Cols.VIDEO_SHORT_URL + " TEXT,"
                + ContentDescriptor.Videos.Cols.THUMB_URL + " TEXT,"
                + ContentDescriptor.Videos.Cols.CREATED_AT + " INTEGER,"
                + ContentDescriptor.Videos.Cols.AUTHOR_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.AUTHOR_NAME + " TEXT,"
                + ContentDescriptor.Videos.Cols.TITLE + " TEXT,"
                + ContentDescriptor.Videos.Cols.VIEWS + " INTEGER,"
                + ContentDescriptor.Videos.Cols.LIKES + " INTEGER,"
                + ContentDescriptor.Videos.Cols.REPLIES + " INTEGER,"
                + ContentDescriptor.Videos.Cols.REPLAY_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.PET_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.COMMENTS_COUNT + " INTEGER,"
                + ContentDescriptor.Videos.Cols.SMILE_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.LENGTH + " INTEGER,"
                + ContentDescriptor.Videos.Cols.MY_MARK + " INTEGER,"
                + ContentDescriptor.Videos.Cols.TAGS + " TEXT, "
                + ContentDescriptor.Videos.Cols.GUESSED + " INTEGER,"
                + ContentDescriptor.Videos.Cols.CITY + " TEXT,"
                + ContentDescriptor.Videos.Cols.COUNTRY + " TEXT,"
                + ContentDescriptor.Videos.Cols.COST + " INTEGER,"
                + ContentDescriptor.Videos.Cols.AVAILABLE + " INTEGER,"
                + ContentDescriptor.Videos.Cols.EARN + " INTEGER,"
                + ContentDescriptor.Videos.Cols.GAME_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Videos.Cols.PARAMS + " TEXT,"
                + ContentDescriptor.Videos.Cols.CATEGORY_ID + " INTEGER,"
                + ContentDescriptor.Videos.Cols.SCREEN + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.VideosUpload.TABLE_NAME
                + "(  "
                + ContentDescriptor.VideosUpload.Cols.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ContentDescriptor.VideosUpload.Cols.VIDEO_URL + " TEXT,"
                + ContentDescriptor.VideosUpload.Cols.VIDEO_SHORT_URL + " TEXT,"
                + ContentDescriptor.VideosUpload.Cols.THUMB_URL + " TEXT,"
                + ContentDescriptor.VideosUpload.Cols.CREATED_AT + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.REPLAY_ID + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.PET_ID + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.SMILE_ID + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.TAGS + " TEXT, "
                + ContentDescriptor.VideosUpload.Cols.COST + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.AVATAR_TYPE + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.UPLOADING_STATUS + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.UPLOADED_VIDEO + " TEXT,"
                + ContentDescriptor.VideosUpload.Cols.UPLOADED_IMAGE + " TEXT,"
                + ContentDescriptor.VideosUpload.Cols.ADDITIONAL_PARAMS + " TEXT,"
                + ContentDescriptor.VideosUpload.Cols.PRODUCT_TYPE + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.CATEGORY_ID + " INTEGER,"
                + ContentDescriptor.VideosUpload.Cols.LATITUDE + " REAL,"
                + ContentDescriptor.VideosUpload.Cols.LONGITUDE + " REAL"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Categories.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Categories.Cols.ID + " INTEGER,"
                + ContentDescriptor.Categories.Cols.NAME_ENG + " TEXT, "
                + ContentDescriptor.Categories.Cols.NAME_ESP + " TEXT, "
                + ContentDescriptor.Categories.Cols.NAME_RUS + " TEXT, "
                + ContentDescriptor.Categories.Cols.VIDEO_TYPE + " INTEGER, "
                + ContentDescriptor.Categories.Cols.TECH + " INTEGER, "
                + ContentDescriptor.Categories.Cols.PARAMS + " TEXT, "
                + ContentDescriptor.Categories.Cols.THUMBNAIL + " TEXT, "
                + ContentDescriptor.Categories.Cols.MAIN + " INTEGER, "
                + ContentDescriptor.Categories.Cols.VIDEO_COUNT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Balances.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Balances.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Balances.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Balances.Cols.CHANGE + " FLOAT,"
                + ContentDescriptor.Balances.Cols.VIDEO_ID + " INTEGER,"
                + ContentDescriptor.Balances.Cols.VIDEO_IMG + " TEXT,"
                + ContentDescriptor.Balances.Cols.TAB_POSITION + " INTEGER,"
                + ContentDescriptor.Balances.Cols.CREATED_AT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Countries.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Countries.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Countries.Cols.NAME_ENG + " TEXT,"
                + ContentDescriptor.Countries.Cols.NAME_RUS + " TEXT,"
                + ContentDescriptor.Countries.Cols.NAME_ESP + " TEXT,"
                + ContentDescriptor.Countries.Cols.COUNTRY_CODE + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Cities.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Cities.Cols.ID + " INTEGER UNIQUE,"
                + ContentDescriptor.Cities.Cols.COUNTRY_ID + " INTEGER,"
                + ContentDescriptor.Cities.Cols.NAME_ENG + " TEXT,"
                + ContentDescriptor.Cities.Cols.NAME_RUS + " TEXT,"
                + ContentDescriptor.Cities.Cols.NAME_ESP + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Grimaces.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Grimaces.Cols.ID + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.ID_AUTHOR + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.ID_SMILE + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.BOUGHT + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.SELLING + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.URL + " TEXT,"
                + ContentDescriptor.Grimaces.Cols.EFFECT + " INTEGER,"
                + ContentDescriptor.Grimaces.Cols.TYPE + " INTEGER, "
                + ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " INTEGER "
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.GrimaceStatistic.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.GrimaceStatistic.Cols.GRIMACE_ID + " INTEGER,"
                + ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);


        createTableQuery = "CREATE TABLE " + ContentDescriptor.Votes.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Votes.Cols.TITLE_ENG + " TEXT,"
                + ContentDescriptor.Votes.Cols.TITLE_RUS + " TEXT,"
                + ContentDescriptor.Votes.Cols.TITLE_ESP + " TEXT,"
                + ContentDescriptor.Votes.Cols.DESCR_ENG + " TEXT,"
                + ContentDescriptor.Votes.Cols.DESCR_RUS + " TEXT,"
                + ContentDescriptor.Votes.Cols.DESCR_ESP + " TEXT,"
                + ContentDescriptor.Votes.Cols.STATUS + " INTEGER,"
                + ContentDescriptor.Votes.Cols.VOTED + " INTEGER,"
                + ContentDescriptor.Votes.Cols.MY_VOTE + " INTEGER,"
                + ContentDescriptor.Votes.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Votes.Cols.VOTES_TOTAL + " INTEGER,"
                + ContentDescriptor.Votes.Cols.MY_INITIATION + " INTEGER DEFAULT 0"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.VoteOptions.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.VoteOptions.Cols.OPTION_ID + " INTEGER,"
                + ContentDescriptor.VoteOptions.Cols.VOTE_ID + " INTEGER,"
                + ContentDescriptor.VoteOptions.Cols.VOTES + " INTEGER,"
                + ContentDescriptor.VoteOptions.Cols.TITLE_ENG + " TEXT,"
                + ContentDescriptor.VoteOptions.Cols.TITLE_RUS + " TEXT,"
                + ContentDescriptor.VoteOptions.Cols.TITLE_ESP + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.UserSearchResult.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.UserSearchResult.Cols.ID + " INTEGER,"
                + ContentDescriptor.UserSearchResult.Cols.FIRST_NAME + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.LAST_NAME + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.AVATAR + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.CITY + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.COUNTRY + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.CITY_ID + " INTEGER,"
                + ContentDescriptor.UserSearchResult.Cols.COUNTRY_ID + " INTEGER,"
                + ContentDescriptor.UserSearchResult.Cols.LANG + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.HOBBIE + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.SPORT + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.PETS + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.INTEREST + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.RELIGION + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.CRAFT + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.TAGS + " TEXT,"

                + ContentDescriptor.UserSearchResult.Cols.GAME + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.NON_GAME + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.EXTREME + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.VIRTUAL + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK + " TEXT,"
                + ContentDescriptor.UserSearchResult.Cols.PART_GAMES + " TEXT,"

                + ContentDescriptor.UserSearchResult.Cols.REQUIRED + " TEXT,"

                + ContentDescriptor.UserSearchResult.Cols.RATING + " FLOAT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Feedback.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Feedback.Cols.ID + " INTEGER,"
                + ContentDescriptor.Feedback.Cols.TYPE + " INTEGER,"
                + ContentDescriptor.Feedback.Cols.LAST_MESSAGE + " TEXT,"
                + ContentDescriptor.Feedback.Cols.NEW + " INTEGER,"
                + ContentDescriptor.Feedback.Cols.CREATED + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.FeedbackMessages.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.FeedbackMessages.Cols.AUTHOR_ID + " INTEGER,"
                + ContentDescriptor.FeedbackMessages.Cols.ID_FEEDBACK + " INTEGER,"
                + ContentDescriptor.FeedbackMessages.Cols.MESSAGE + " TEXT,"
                + ContentDescriptor.FeedbackMessages.Cols.TIME + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.Question.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.Question.Cols.GAME_KEY + " TEXT,"
                + ContentDescriptor.Question.Cols.QUESTION_ID + " INTEGER,"
                + ContentDescriptor.Question.Cols.ANSWERED + " INTEGER DEFAULT 0,"
                + ContentDescriptor.Question.Cols.OPENED + " INTEGER DEFAULT 0,"
                + ContentDescriptor.Question.Cols.COVER + " TEXT,"
                + ContentDescriptor.Question.Cols.URL_ANSWER + " TEXT"
                + ");";
        db.execSQL(createTableQuery);

        createTableQuery = "CREATE TABLE " + ContentDescriptor.QuestionMedia.TABLE_NAME
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ContentDescriptor.QuestionMedia.Cols.QUESTION_ID + " INTEGER,"
                + ContentDescriptor.QuestionMedia.Cols.ORDER + " INTEGER, "
                + ContentDescriptor.QuestionMedia.Cols.GAME_KEY + " TEXT,"
                + ContentDescriptor.QuestionMedia.Cols.MEDIA_ID + " INTEGER,"
                + ContentDescriptor.QuestionMedia.Cols.MEDIA + " TEXT,"
                + ContentDescriptor.QuestionMedia.Cols.URL_ANSWER + " TEXT,"
                + ContentDescriptor.QuestionMedia.Cols.DOWNLOADED + " INTEGER"
                + ");";
        db.execSQL(createTableQuery);

        if (sDelegate != null) {
            sDelegate.createTables(db);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    private void dropTables(SQLiteDatabase db) {
        for (String table : getTables())
            dropTable(db, table);

        if (sDelegate != null) {
            sDelegate.dropTable(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public static void dropTable(SQLiteDatabase db, String table) {
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

    public void trancateTables(SQLiteDatabase db) {
        trancateTables(db, getTables());
    }

    public static void trancateTables(SQLiteDatabase db, String[] tables) {
        for (String table : tables)
            db.delete(table, null, null);
    }

    private String[] getTables() {
        return new String[]{
                ContentDescriptor.Videos.TABLE_NAME,
                ContentDescriptor.VideosUpload.TABLE_NAME,
                ContentDescriptor.Categories.TABLE_NAME,
                ContentDescriptor.Countries.TABLE_NAME,
                ContentDescriptor.Cities.TABLE_NAME,
                ContentDescriptor.Balances.TABLE_NAME,
                ContentDescriptor.Votes.TABLE_NAME,
                ContentDescriptor.VoteOptions.TABLE_NAME,
                ContentDescriptor.UserSearchResult.TABLE_NAME,
                ContentDescriptor.Feedback.TABLE_NAME,
                ContentDescriptor.FeedbackMessages.TABLE_NAME,
                ContentDescriptor.Question.TABLE_NAME,
                ContentDescriptor.QuestionMedia.TABLE_NAME,
                ContentDescriptor.Grimaces.TABLE_NAME,
                ContentDescriptor.GrimaceStatistic.TABLE_NAME
        };
    }

    public interface Delegate {
        void createTables(SQLiteDatabase db);

        void dropTable(SQLiteDatabase db);
    }

}
