package com.humanet.humanetcore.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by ovitali on 26.01.2015.
 * Changed by Denis on 25.02.2015
 */
public class DataProvider extends ContentProvider {
    private DBHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        long id = -1;

        try {
            id = db.insert(tableName, null, values);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        if (id != -1) {
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.withAppendedPath(uri, Long.toString(id));
        }

        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int count = db.delete(tableName, selection, selectionArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @NonNull ContentValues values, String selection, String[] selectionArgs) {
        String tableName = getTableName(uri);

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int count = db.update(tableName, values, selection, selectionArgs);

        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int numInserted = 0;

        String tableName = getTableName(uri);

        SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
        sqlDB.beginTransaction();
        try {
            for (ContentValues cv : values) {
                long newID = sqlDB.insertWithOnConflict(tableName, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                if (newID <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            sqlDB.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);

            numInserted = values.length;
        } catch (Exception e) {
            Log.e("bulkInsert", e.getMessage());
        } finally {
            sqlDB.endTransaction();
        }
        return numInserted;
    }

    private String getTableName(Uri uri) {
        String tableName;
        switch (ContentDescriptor.URI_MATCHER.match(uri)) {
            case ContentDescriptor.Videos.CODE_ALL:
            case ContentDescriptor.Videos.CODE_ID:
                tableName = ContentDescriptor.Videos.TABLE_NAME;
                break;
            case ContentDescriptor.VideosUpload.CODE_ALL:
            case ContentDescriptor.VideosUpload.CODE_ID:
                tableName = ContentDescriptor.VideosUpload.TABLE_NAME;
                break;
            case ContentDescriptor.Categories.CODE_ALL:
            case ContentDescriptor.Categories.CODE_ID:
                tableName = ContentDescriptor.Categories.TABLE_NAME;
                break;
            case ContentDescriptor.Balances.CODE_ALL:
            case ContentDescriptor.Balances.CODE_ID:
                tableName = ContentDescriptor.Balances.TABLE_NAME;
                break;
            case ContentDescriptor.Cities.CODE_ALL:
            case ContentDescriptor.Cities.CODE_ID:
                tableName = ContentDescriptor.Cities.TABLE_NAME;
                break;
            case ContentDescriptor.Countries.CODE_ALL:
            case ContentDescriptor.Countries.CODE_ID:
                tableName = ContentDescriptor.Countries.TABLE_NAME;
                break;
            case ContentDescriptor.Votes.CODE_ALL:
            case ContentDescriptor.Votes.CODE_ID:
                tableName = ContentDescriptor.Votes.TABLE_NAME;
                break;
            case ContentDescriptor.VoteOptions.CODE_ALL:
            case ContentDescriptor.VoteOptions.CODE_ID:
                tableName = ContentDescriptor.VoteOptions.TABLE_NAME;
                break;
            case ContentDescriptor.UserSearchResult.CODE_ALL:
            case ContentDescriptor.UserSearchResult.CODE_ID:
                tableName = ContentDescriptor.UserSearchResult.TABLE_NAME;
                break;
            case ContentDescriptor.Feedback.CODE_ID:
            case ContentDescriptor.Feedback.CODE_ALL:
                tableName = ContentDescriptor.Feedback.TABLE_NAME;
                break;
            case ContentDescriptor.FeedbackMessages.CODE_ID:
            case ContentDescriptor.FeedbackMessages.CODE_ALL:
                tableName = ContentDescriptor.FeedbackMessages.TABLE_NAME;
                break;

            case ContentDescriptor.Question.CODE_ID:
            case ContentDescriptor.Question.CODE_ALL:
                tableName = ContentDescriptor.Question.TABLE_NAME;
                break;
            case ContentDescriptor.QuestionMedia.CODE_ID:
            case ContentDescriptor.QuestionMedia.CODE_ALL:
                tableName = ContentDescriptor.QuestionMedia.TABLE_NAME;
                break;

            case ContentDescriptor.GrimaceStatistic.CODE_ALL:
            case ContentDescriptor.GrimaceStatistic.CODE_ID:
                tableName = ContentDescriptor.GrimaceStatistic.TABLE_NAME;
                break;
            case ContentDescriptor.Grimaces.CODE_ALL:
            case ContentDescriptor.Grimaces.CODE_ID:
                tableName = ContentDescriptor.Grimaces.TABLE_NAME;
                break;
            case ContentDescriptor.Nursling.CODE_ALL:
            case ContentDescriptor.Nursling.CODE_ID:
                tableName = ContentDescriptor.Nursling.TABLE_NAME;
                break;
            case ContentDescriptor.Skill.CODE_ALL:
            case ContentDescriptor.Skill.CODE_ID:
                tableName = ContentDescriptor.Skill.TABLE_NAME;
                break;

            default:
                throw new IllegalArgumentException("Can't recognize uri!");
        }

        return tableName;
    }
}
