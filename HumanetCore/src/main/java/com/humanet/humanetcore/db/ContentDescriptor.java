package com.humanet.humanetcore.db;

import android.content.UriMatcher;
import android.net.Uri;

import com.humanet.humanetcore.App;

import java.util.Locale;

/**
 * Created by Denis on 25.02.2015.
 */
public class ContentDescriptor {
    public static final String AUTHORITY = App.getInstance().getPackageName() + ".db.provider";
    public static final UriMatcher URI_MATCHER = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, Videos.TABLE_NAME, Videos.CODE_ALL);
        matcher.addURI(AUTHORITY, Videos.TABLE_NAME + "/#", Videos.CODE_ID);
        matcher.addURI(AUTHORITY, VideosUpload.TABLE_NAME, VideosUpload.CODE_ALL);
        matcher.addURI(AUTHORITY, VideosUpload.TABLE_NAME + "/#", VideosUpload.CODE_ID);
        matcher.addURI(AUTHORITY, Categories.TABLE_NAME, Categories.CODE_ALL);
        matcher.addURI(AUTHORITY, Categories.TABLE_NAME + "/#", Categories.CODE_ID);
        matcher.addURI(AUTHORITY, Balances.TABLE_NAME, Balances.CODE_ALL);
        matcher.addURI(AUTHORITY, Balances.TABLE_NAME + "/#", Balances.CODE_ID);
        matcher.addURI(AUTHORITY, Grimaces.TABLE_NAME, Grimaces.CODE_ALL);
        matcher.addURI(AUTHORITY, Grimaces.TABLE_NAME + "/#", Grimaces.CODE_ID);
        matcher.addURI(AUTHORITY, GrimaceStatistic.TABLE_NAME, GrimaceStatistic.CODE_ALL);
        matcher.addURI(AUTHORITY, GrimaceStatistic.TABLE_NAME + "/#", GrimaceStatistic.CODE_ID);
        matcher.addURI(AUTHORITY, Votes.TABLE_NAME, Votes.CODE_ALL);
        matcher.addURI(AUTHORITY, Votes.TABLE_NAME + "/#", Votes.CODE_ID);
        matcher.addURI(AUTHORITY, VoteOptions.TABLE_NAME, VoteOptions.CODE_ALL);
        matcher.addURI(AUTHORITY, VoteOptions.TABLE_NAME + "/#", VoteOptions.CODE_ID);
        matcher.addURI(AUTHORITY, Cities.TABLE_NAME, Cities.CODE_ALL);
        matcher.addURI(AUTHORITY, Cities.TABLE_NAME + "/#", Cities.CODE_ID);
        matcher.addURI(AUTHORITY, Countries.TABLE_NAME, Countries.CODE_ALL);
        matcher.addURI(AUTHORITY, Countries.TABLE_NAME + "/#", Countries.CODE_ID);
        matcher.addURI(AUTHORITY, UserSearchResult.TABLE_NAME, UserSearchResult.CODE_ALL);
        matcher.addURI(AUTHORITY, UserSearchResult.TABLE_NAME + "/#", UserSearchResult.CODE_ID);
        matcher.addURI(AUTHORITY, Skill.TABLE_NAME, Skill.CODE_ALL);
        matcher.addURI(AUTHORITY, Skill.TABLE_NAME + "/#", Skill.CODE_ID);
        matcher.addURI(AUTHORITY, Nursling.TABLE_NAME, Nursling.CODE_ALL);
        matcher.addURI(AUTHORITY, Nursling.TABLE_NAME + "/#", Nursling.CODE_ID);
        matcher.addURI(AUTHORITY, Feedback.TABLE_NAME, Feedback.CODE_ALL);
        matcher.addURI(AUTHORITY, Feedback.TABLE_NAME + "/#", Feedback.CODE_ID);
        matcher.addURI(AUTHORITY, FeedbackMessages.TABLE_NAME, FeedbackMessages.CODE_ALL);
        matcher.addURI(AUTHORITY, FeedbackMessages.TABLE_NAME + "/#", FeedbackMessages.CODE_ID);
        matcher.addURI(AUTHORITY, Question.TABLE_NAME, Question.CODE_ALL);
        matcher.addURI(AUTHORITY, Question.TABLE_NAME + "/#", Question.CODE_ID);
        matcher.addURI(AUTHORITY, QuestionMedia.TABLE_NAME, QuestionMedia.CODE_ALL);
        matcher.addURI(AUTHORITY, QuestionMedia.TABLE_NAME + "/#", QuestionMedia.CODE_ID);
        return matcher;
    }

    public static class Videos {
        public static final String TABLE_NAME = "videos";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 100;
        public static final int CODE_ID = 200;

        public static class Cols {
            public static final String ID = "_id";
            public static final String VIDEO_ID = "video_id";
            public static final String VIDEO_URL = "video_url";
            public static final String VIDEO_SHORT_URL = "video_short_url";
            public static final String THUMB_URL = "thumb_url";
            public static final String CREATED_AT = "created_at";
            public static final String AUTHOR_ID = "author_id";
            public static final String TITLE = "title";
            public static final String AUTHOR_NAME = "author_name";
            public static final String VIEWS = "views";
            public static final String LIKES = "likes";
            public static final String REPLIES = "replies";
            public static final String COMMENTS_COUNT = "comments_count";
            public static final String SMILE_ID = "smile_id";
            public static final String TAGS = "tags";
            public static final String LENGTH = "length";
            public static final String MY_MARK = "my_mark";
            public static final String GUESSED = "guessed";
            public static final String CITY = "id_city";
            public static final String COUNTRY = "id_country";
            public static final String COST = "cost";
            public static final String AVAILABLE = "paid";
            public static final String GAME_ID = "game_id";
            public static final String REPLAY_ID = "replay_id";
            public static final String PET_ID = "id_pet";
            public static final String EARN = "earn";
            public static final String TYPE = "type";
            public static final String PARAMS = "params";
            public static final String CATEGORY_ID = "category_id";
            public static final String SCREEN = "screen";
        }
    }

    public static class VideosUpload {
        public static final String TABLE_NAME = "videosUpload";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 101;
        public static final int CODE_ID = 201;

        public static class Cols {
            public static final String ID = "_id";
            public static final String VIDEO_URL = "video_url";
            public static final String VIDEO_SHORT_URL = "video_short_url";
            public static final String THUMB_URL = "thumb_url";
            public static final String CREATED_AT = "created_at";
            public static final String SMILE_ID = "smile_id";
            public static final String TAGS = "tags";
            public static final String COST = "cost";
            public static final String REPLAY_ID = "replay_id";
            public static final String PET_ID = "id_pet";
            public static final String UPLOADING_STATUS = "to_upload";
            public static final String UPLOADED_VIDEO = "uploaded_video";
            public static final String UPLOADED_IMAGE = "uploaded_image";
            public static final String PRODUCT_TYPE = "product_type";
            public static final String TYPE = "type";
            public static final String CATEGORY_ID = "category_id";
            public static final String AVATAR_TYPE = "avatar_type";
            public static final String LATITUDE = "latitude";
            public static final String LONGITUDE = "longitude";
            public static final String ADDITIONAL_PARAMS = "additional_params";
        }
    }


    public static class Categories {
        public static final String TABLE_NAME = "categories";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 500;
        public static final int CODE_ID = 600;

        public static class Cols {
            public static final String ID = "id";
            public static final String NAME_RUS = "name_rus";
            public static final String NAME_ENG = "name_eng";
            public static final String NAME_ESP = "name_esp";
            public static final String VIDEO_TYPE = "video_type";
            public static final String MAIN = "main";
            public static final String TECH = "tech";
            public static final String PARAMS = "params";
            public static final String THUMBNAIL = "thumb";
            public static final String VIDEO_COUNT = "video_count";
        }
    }

    public static class Balances {
        public static final String TABLE_NAME = "balance";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 900;
        public static final int CODE_ID = 1000;

        public static class Cols {
            public static final String ID = "id";
            public static final String TYPE = "type";
            public static final String TAB_POSITION = "tab_posistion";
            public static final String CHANGE = "change";
            public static final String VIDEO_ID = "video_id";
            public static final String VIDEO_IMG = "video_img";
            public static final String CREATED_AT = "created_at";
        }

        public static String whereByTabType(int position) {
            return String.format(Locale.getDefault(), "%s='%d'", Cols.TAB_POSITION, position);
        }
    }

    public static class Countries {
        public static final String TABLE_NAME = "countries";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1100;
        public static final int CODE_ID = 1200;

        public static class Cols {
            public static final String ID = "id";
            public static final String NAME_ENG = "name_eng";
            public static final String NAME_ESP = "name_esp";
            public static final String NAME_RUS = "name_rus";
            public static final String COUNTRY_CODE = "country_code";
        }
    }

    public static class Grimaces {
        public static final String TABLE_NAME = "grimaces";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1500;
        public static final int CODE_ID = 1600;

        public static class Cols {
            public static final String ID = "id";
            public static final String ID_AUTHOR = "author_id";
            public static final String ID_SMILE = "smile_id";
            public static final String SELLING = "selling";
            public static final String BOUGHT = "bought";
            public static final String URL = "url";
            public static final String EFFECT = "effect";
            public static final String TYPE = "type";
            public static final String TO_UPLOAD = "to_upload";
        }
    }

    public static class GrimaceStatistic {
        public static final String TABLE_NAME = "grimace_statistic";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2300;
        public static final int CODE_ID = 2400;

        public static class Cols {
            public static final String GRIMACE_ID = "grimace_id";
            public static final String USE_COUNT = "use_count";
        }
    }

    public static class Cities {
        public static final String TABLE_NAME = "cities";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1300;
        public static final int CODE_ID = 1400;

        public static class Cols {
            public static final String ID = "id";
            public static final String COUNTRY_ID = "country_id";
            public static final String NAME_ENG = "name_eng";
            public static final String NAME_ESP = "name_esp";
            public static final String NAME_RUS = "name_rus";
        }
    }

    public static class Votes {
        public static final String TABLE_NAME = "votes";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1700;
        public static final int CODE_ID = 1800;

        public static class Cols {
            public static final String TITLE_ENG = "title_eng";
            public static final String TITLE_ESP = "title_esp";
            public static final String TITLE_RUS = "title_rus";
            public static final String STATUS = "status";
            public static final String VOTED = "voted";
            public static final String TYPE = "type";
            public static final String MY_VOTE = "my_vote";
            public static final String DESCR_ENG = "descr_eng";
            public static final String DESCR_ESP = "descr_esp";
            public static final String DESCR_RUS = "descr_rus";
            public static final String VOTES_TOTAL = "votes";
            public static final String MY_INITIATION = "IS_MY_INITIATED";
        }
    }

    public static class VoteOptions {
        public static final String TABLE_NAME = "options";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1900;
        public static final int CODE_ID = 2000;

        public static class Cols {
            public static final String OPTION_ID = "id";
            public static final String VOTE_ID = "vote_id";
            public static final String VOTES = "votes";
            public static final String TITLE_ENG = "title_eng";
            public static final String TITLE_ESP = "title_esp";
            public static final String TITLE_RUS = "title_rus";
        }
    }


    public static class UserSearchResult {
        public static final String TABLE_NAME = "user_search";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2500;
        public static final int CODE_ID = 2600;

        public static class Cols {
            public static final String ID = "id";
            public static final String FIRST_NAME = "first_name";
            public static final String LAST_NAME = "last_name";
            public static final String AVATAR = "avatar";
            public static final String CITY = "city";
            public static final String COUNTRY = "country";
            public static final String CITY_ID = "id_city";
            public static final String COUNTRY_ID = "id_country";
            public static final String LANG = "lang";
            public static final String HOBBIE = "hobbie";
            public static final String SPORT = "sport";
            public static final String PETS = "pets";
            public static final String INTEREST = "interest";
            public static final String RELIGION = "religion";
            public static final String CRAFT = "craft";
            public static final String TAGS = "tags";
            public static final String RATING = "rating";

            public static final String GAME = "game";
            public static final String NON_GAME = "non_game";
            public static final String VIRTUAL = "virtual";
            public static final String EXTREME = "extreme";
            public static final String HOW_I_LOOK = "how_i_look";
            public static final String PART_GAMES = "part_games";

            public static final String REQUIRED = "REQUIRED";
        }
    }

    public static class Nursling {
        public static final String TABLE_NAME = "pets";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2520;
        public static final int CODE_ID = 2620;
    }

    public static class Skill {
        public static final String TABLE_NAME = "skills";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2540;
        public static final int CODE_ID = 2640;
    }

    public static class Feedback {
        public static final String TABLE_NAME = "feedback";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2700;
        public static final int CODE_ID = 2800;

        public static class Cols {
            public static final String ID = "id";
            public static final String TYPE = "type";
            public static final String LAST_MESSAGE = "last_message";
            public static final String NEW = "_new";
            public static final String CREATED = "created_at";
        }
    }

    public static class FeedbackMessages {
        public static final String TABLE_NAME = "feedback_messages";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2900;
        public static final int CODE_ID = 3000;

        public static class Cols {
            public static final String AUTHOR_ID = "author_id";
            public static final String MESSAGE = "message";
            public static final String ID_FEEDBACK = "id_feedback";
            public static final String TIME = "time";
        }
    }

    public static class Question {
        public static final String TABLE_NAME = "question";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 3100;
        public static final int CODE_ID = 3200;

        public static class Cols {
            public static final String GAME_KEY = "game_key";
            public static final String QUESTION_ID = "q_id";
            public static final String ANSWERED = "answered";
            public static final String OPENED = "opened";
            public static final String COVER = "cover";
            public static final String URL_ANSWER = "url_answer";
        }
    }

    public static class QuestionMedia {
        public static final String TABLE_NAME = "question_media";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 3300;
        public static final int CODE_ID = 3400;

        public static class Cols {
            public static final String QUESTION_ID = "q_id";
            public static final String ORDER = "qOrder";
            public static final String GAME_KEY = "game_key";
            public static final String MEDIA_ID = "media_id";
            public static final String MEDIA = "url";
            public static final String URL_ANSWER = "url_answer";
            public static final String DOWNLOADED = "downloaded";
        }
    }

}