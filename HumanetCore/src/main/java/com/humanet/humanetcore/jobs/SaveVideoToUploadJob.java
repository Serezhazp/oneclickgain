package com.humanet.humanetcore.jobs;

import android.content.ContentValues;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.VideoUploadRequest;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.utils.LocationWatcher;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.octo.android.robospice.persistence.DurationInMillis;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by ovitali on 13.11.2015.
 */
public final class SaveVideoToUploadJob extends Thread implements LocationWatcher.GetLocationListener {

    @NonNull
    private final VideoInfo mVideoInfo;

    private int mPetId;

    public SaveVideoToUploadJob(@NonNull VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    public SaveVideoToUploadJob(@NonNull VideoInfo videoInfo, int petId) {
        this(videoInfo);
        mPetId = petId;
    }

    /**
     * if value does not equals null that mean app did not get location yet.
     */
    @Nullable
    private LocationWatcher mLocationWatcher = null;

    @Override
    public void run() {

        mVideoInfo.setCreated(System.currentTimeMillis());

        final File srcFileVideo = new File(mVideoInfo.getVideoPath());
        final File srcFileImage = new File(mVideoInfo.getImagePath());

        final File outFileVideo = new File(FilePathHelper.getUploadDirectory(), mVideoInfo.getCreated() + "." + Constants.VIDEO_EXTENTION);
        final File outFileImage = new File(FilePathHelper.getUploadDirectory(), mVideoInfo.getCreated() + "." + Constants.IMAGE_EXTENTION);

        try {
            FileUtils.copyFile(srcFileVideo, outFileVideo);
            FileUtils.copyFile(srcFileImage, outFileImage);
        } catch (IOException e) {
            Log.e("SaveVideoToUploadJob", "IOException", e);
        }

        mVideoInfo.setVideoPath(outFileVideo.getAbsolutePath());
        mVideoInfo.setImagePath(outFileImage.getAbsolutePath());

        final ContentValues contentValues = mVideoInfo.toContentValues();
        final Uri uri = App.getInstance().getContentResolver().insert(ContentDescriptor.VideosUpload.URI, contentValues);

        if (!NetworkUtil.isOfflineMode(App.getInstance())) {
            final int id = Integer.parseInt(uri.getLastPathSegment());

            if(mPetId != 0)
                mVideoInfo.setPetId(mPetId);

            if (mLocationWatcher != null) {
                mVideoInfo.setLocalId(id);
                mLocationWatcher.setVideoInfo(mVideoInfo);

                waitForLocation();
            }

            VideoUploadRequest videoUploadRequest = new VideoUploadRequest(id, mVideoInfo.getPetId());

            App.getSpiceManager().execute(videoUploadRequest, String.valueOf(id), DurationInMillis.ONE_SECOND, new SimpleRequestListener<UploadingMedia>());
        }
    }

    /**
     * wait for location if it was not received.
     */
    private void waitForLocation() {
        final int seconds = 60;
        int currentSeconds = 0;
        while (mLocationWatcher != null && seconds > currentSeconds++) {
            try {
                sleep(1000);
            } catch (InterruptedException ignore) {
            }
        }

        if (mLocationWatcher != null) {
            mLocationWatcher.dispose();
        }
    }

    public void setLocationWatcher(@Nullable LocationWatcher locationWatcher) {
        mLocationWatcher = locationWatcher;
        if (locationWatcher != null) {
            locationWatcher.setGetLocationListener(this);
        }
    }

    @Override
    public final void onGotLocation() {
        mLocationWatcher = null;
    }
}
