package com.humanet.humanetcore.jobs;

import android.database.Cursor;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.VideoUploadRequest;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.VideoInfo;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by ovi on 4/22/16.
 */
public class UploadVideoJob extends Thread {

    public static void startUploader() {
        UploadVideoJob uploadVideoJob = new UploadVideoJob();
        uploadVideoJob.setPriority(MIN_PRIORITY);
        uploadVideoJob.run();
    }

    @Override
    public void run() {
        Cursor videoCursor = App.getInstance().getContentResolver().query(ContentDescriptor.VideosUpload.URI,
                null,
                ContentDescriptor.VideosUpload.Cols.UPLOADING_STATUS + " = " + VideoInfo.STATUS_UPLOAD_REQUIRED,
                null,
                ContentDescriptor.VideosUpload.Cols.CREATED_AT + " ASC");

        if (videoCursor != null && videoCursor.moveToFirst()) {
            do {
                int id = videoCursor.getInt(videoCursor.getColumnIndex("_id"));
                int petId = videoCursor.getInt(videoCursor.getColumnIndex("id_pet"));
                VideoUploadRequest videoUploadRequest = new VideoUploadRequest(id, petId);
                videoUploadRequest.setPriority(SpiceRequest.PRIORITY_LOW);
                App.getSpiceManager().execute(videoUploadRequest, String.valueOf(id), DurationInMillis.ALWAYS_EXPIRED, new SimpleRequestListener<UploadingMedia>());
            } while (videoCursor.moveToNext());

            videoCursor.close();
        }
    }
}
