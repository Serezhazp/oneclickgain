package com.humanet.humanetcore.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.humanet.humanetcore.Constants;

/**
 * Created by ovitali on 23.12.2014.
 */
public class PrefHelper {

    private static SharedPreferences sSharedPreferences;

    public static void init(Context context) {
        sSharedPreferences = context.getSharedPreferences("imon_app", Context.MODE_PRIVATE);
    }

    public static SharedPreferences getPreferences() {
        return sSharedPreferences;
    }

    public static String getStringPref(String key) {
        return sSharedPreferences.getString(key, null);
    }

    public static void setStringPref(String key, String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void removeStringPref(String key) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public static long getLongPref(String key) {
        return sSharedPreferences.getLong(key, -1);
    }

    public static void setLongPref(String key, long value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static int getIntPref(String key) {
        return getIntPref(key, -1);
    }

    public static int getIntPref(String key, int def) {
        return sSharedPreferences.getInt(key, def);
    }

    public static void setIntPref(String key, int value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static boolean getBooleanPref(String key) {
        return sSharedPreferences.getBoolean(key, false);
    }

    public static void setBooleanPref(String key, boolean value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static String getToken() {
        return getStringPref(Constants.PREF.TOKEN);
    }

    public static void clear() {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.clear().apply();
    }
}
