package com.humanet.humanetcore.utils;

import android.content.Context;
import android.support.annotation.StringDef;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.UserInfo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Deni on 15.10.2015.
 */
public final class AnalyticsHelper {
    private static final String TAG = "AnalyticsHelper";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            ENTER_IN_APP,
            EMOTICON_CREATE,
            CODE_REQUEST,
            NUMBER_VERIFICATION,
            FILL_PROFILE_BASE,
            FILL_PROFILE_EXTENDED,
            VIDEO_RECORDING,
            TOKEN_SEND,
            SHARE,

            GAME_PROMO,
            GAME_ASTRA,
            GAME_CREDO,
            GAME_GENERAL_GAME,
            GAME_ELSE,

            MARKET_ADD_ITEM,
            MARKET_BUY_ITEM,
            FEEDBACK_SEND,

            POLL_CREATE,
            POLL_PARTICIPATE,


            BUY_WIDGET,
    })
    public @interface TrackEventTitle {
    }

    private static Analytics sAnalytics;

    public static void setAnalytics(Analytics analytics) {
        sAnalytics = analytics;
    }

    public static void init(Context context) {
        if (!App.DEBUG &&  sAnalytics != null)
            sAnalytics.init(context);
    }

    public static void identifyUser(UserInfo userInfo) {
        if (!App.DEBUG && sAnalytics != null)
            sAnalytics.identifyUser(userInfo);
    }

    public static void trackEvent(Context context, @TrackEventTitle String event) {
        if (!App.DEBUG && sAnalytics != null)
            sAnalytics.trackEvent(context, event);
    }

    public static final String ENTER_IN_APP = "Вход в приложение";
    public static final String EMOTICON_CREATE = "Creation an emoticon";
    public static final String CODE_REQUEST = "Запрос кода";
    public static final String NUMBER_VERIFICATION = "Подтверждение номера";
    public static final String FILL_PROFILE_BASE = "Заполнение базовой анкеты";
    public static final String FILL_PROFILE_EXTENDED = "Заполнение расширенной анкеты";
    public static final String VIDEO_RECORDING = "Съемка видео";
    public static final String TOKEN_SEND = "Отправка токена";
    public static final String SHARE = "Шер видео/гримасы";

    public static final String GAME_PROMO = "Promo игра";
    public static final String GAME_ASTRA = "Игра Astra";
    public static final String GAME_CREDO = "Игра Credo";
    public static final String GAME_GENERAL_GAME = "Игра Classic";
    public static final String GAME_ELSE = "Игра ELSE";

    public static final String MARKET_ADD_ITEM = "Добавление товара в маркет";
    public static final String MARKET_BUY_ITEM = "Покупка в маркете";
    public static final String CROWD_CREATE = "Создание крауд видео";
    public static final String CROWD_DONATE = "Пожертвование на крауд видео";
    public static final String FEEDBACK_SEND = "Отправка сообщения в обратной связи";

    public static final String POLL_CREATE = "Создание голосования";
    public static final String POLL_PARTICIPATE = "Участие в голосовании";

    public static final String BUY_WIDGET = "Покупка виджета";


    public interface Analytics {
        void init(Context context);

        void identifyUser(UserInfo userInfo);

        void trackEvent(Context context, @TrackEventTitle String event);
    }

}
