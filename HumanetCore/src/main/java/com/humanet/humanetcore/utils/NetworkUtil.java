package com.humanet.humanetcore.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;

import com.humanet.humanetcore.App;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * Created by Deni on 03.08.2015.
 */
public class NetworkUtil {
    public static final int NOT_CONNECTED = 0;
    public static final int WIFI = 1;
    public static final int MOBILE = 2;

    public static boolean isOfflineMode() {
        return isOfflineMode(App.getInstance());
    }

    public static boolean isOfflineMode(Context context) {
        return getConnectionStatus(context) == NOT_CONNECTED;
    }

    public static int getConnectionStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return WIFI;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return MOBILE;
        }
        return NOT_CONNECTED;
    }

    public static String getConnectionStatusString(Context context) {
        int connectionStatus = NetworkUtil.getConnectionStatus(context);
        if (connectionStatus == NetworkUtil.WIFI)
            return "Connected to Wifi";
        if (connectionStatus == NetworkUtil.MOBILE)
            return "Connected to Mobile Data";
        return "No internet connection";
    }

    public static String getMobileNetworkName(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String networkName = tm.getNetworkOperatorName();
        if (networkName != null) {
            return networkName;
        }
        return "";
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @return mac address or empty string
     */
    @Nullable
    public static String getMACAddress() {
        String interfaceName = "wlan0";
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ignore) {
        }
        return null;
    }

    public static String intToIp(int i) {
        return ((i >> 24) & 0xFF) + "." +
                ((i >> 16) & 0xFF) + "." +
                ((i >> 8) & 0xFF) + "." +
                (i & 0xFF);
    }
}
