package com.humanet.humanetcore.utils;

import android.content.Context;
import android.content.res.Resources;

import com.humanet.humanetcore.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Uran on 10.06.2016.
 */
public class TimeIntervalConverter {

    public static String convert(Context context, long date) {
        return convert(context.getResources(), date);
    }

    public static String convert(Resources resources, long date) {

        Date d = new Date(System.currentTimeMillis() - date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);

        int years = calendar.get(Calendar.YEAR) - 1970;
        if (years > 0)
            return resources.getQuantityString(R.plurals.interval_years, years, years);

        int months = calendar.get(Calendar.MONTH);
        if (months > 0)
            return resources.getQuantityString(R.plurals.interval_month, months, months);

        int days = calendar.get(Calendar.DAY_OF_YEAR);
        days = Math.max(1, days);
        return resources.getQuantityString(R.plurals.interval_days, days, days);
    }

    public static String convert(Context context, int years) {
        return convert(context.getResources(), years);
    }

    public static String convert(Resources resources, int years) {
        return resources.getQuantityString(R.plurals.interval_years, years, years);
    }

}
