package com.humanet.humanetcore.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.humanet.humanetcore.model.enums.BalanceChangeType;

import java.lang.reflect.Type;

/**
 * Created by ovi on 2/5/16.
 */
public class BalanceHelper {

    private static BalanceTypeArray sImpl;

    public static void setsImpl(BalanceTypeArray impl) {
        sImpl = impl;
    }

    public static BalanceChangeType getById(int id) {
        return sImpl.getById(id);
    }


    public static class Deserializer implements JsonDeserializer<BalanceChangeType> {
        @Override
        public BalanceChangeType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            return getById(element.getAsInt());
        }
    }

   public interface BalanceTypeArray {
        BalanceChangeType getById(int id);
    }

}
