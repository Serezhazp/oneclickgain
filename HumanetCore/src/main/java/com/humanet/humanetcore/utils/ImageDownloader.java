package com.humanet.humanetcore.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by ovitali on 17.09.2015.
 */
public class ImageDownloader extends BaseImageDownloader {
    public ImageDownloader(Context context) {
        super(context);
    }

    public ImageDownloader(Context context, int connectTimeout, int readTimeout) {
        super(context, connectTimeout, readTimeout);
    }

    @Override
    protected InputStream getStreamFromFile(final String imageUri, final Object extra) throws IOException {
        if (imageUri.endsWith(".mp4")) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(imageUri, new HashMap<String, String>());

            Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            return new ByteArrayInputStream(bitmapdata);
        }
        return super.getStreamFromFile(imageUri, extra);
    }

    @Override
    protected synchronized InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {
        if (imageUri.endsWith(".mp4")) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(imageUri, new HashMap<String, String>());

            Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            return new ByteArrayInputStream(bitmapdata);
        } else {
            return super.getStreamFromNetwork(imageUri, extra);
        }
    }
}
