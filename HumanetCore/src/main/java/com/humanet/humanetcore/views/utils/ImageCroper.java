package com.humanet.humanetcore.views.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

/**
 * Created by Владимир on 28.10.2014.
 */
public class ImageCroper {
    /**
     * Take a bitmap and make from it circular bitmap.
     * Add border around image
     *
     * @param bitmap      image to make circular
     * @param borderSize  size of border around image
     * @param borderColor color of border
     * @return circular bitmap with border
     */
    public static synchronized Bitmap getCircularBitmap(Bitmap bitmap, int borderSize, int borderColor) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int halfBorderSize = borderSize / 2;

        float size = Math.min(width, height);
        Bitmap result = Bitmap.createBitmap((int) size, (int) size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);

        BitmapShader bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        RectF mBitmapRect = new RectF((width - size) / 2f, (height - size) / 2f, width - (width - size) / 2, height - (height - size) / 2);

        RectF mRect = new RectF(halfBorderSize, halfBorderSize, size - halfBorderSize, size - halfBorderSize);

        Paint paint;

        Matrix shaderMatrix = new Matrix();
        shaderMatrix.setRectToRect(mBitmapRect, mRect, Matrix.ScaleToFit.FILL);
        bitmapShader.setLocalMatrix(shaderMatrix);


        //Create new bitmap with the same size like input bitmap
        int radius = (int) (size / 2);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(bitmapShader);
        canvas.drawRoundRect(mRect, radius, radius, paint);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(borderSize);
        paint.setColor(borderColor);
        //paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawOval(mRect, paint);


      /*  int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        //Create new bitmap with the same size like input bitmap
        Bitmap result;

        if(width > height) {
            result = Bitmap.createBitmap(height, height, Bitmap.Config.ARGB_8888);
        } else {
            result = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(result);

        final int color = 0xff424242;
//        final int whiteColor = Color.parseColor("#FFFFFF");
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);

        float radius ;

        if(width > height) {
            radius = height / 2;
        } else {
            radius = width / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);

        paint.setColor(color);
        canvas.drawCircle(radius, radius, radius - borderSize, paint);

        //draw image
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        if(borderSize > 0) {
            //draw while circle for fon
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
            paint.setColor(borderColor);
            canvas.drawCircle(radius, radius, radius, paint);
        }
*/
        return result;
    }

    /**
     * Take a bitmap and make from it circular bitmap.
     *
     * @param bitmap image to make circular
     * @return circular bitmap
     */
    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        return getCircularBitmap(bitmap, 0, 0);
    }

    /**
     * Take a bitmap and make from it circular bitmap.
     * Add white border around image.
     *
     * @param bitmap     image to make circular
     * @param borderSize size of border around image
     * @return
     */
    public static Bitmap getCircularBitmap(Bitmap bitmap, int borderSize) {
        return getCircularBitmap(bitmap, borderSize, android.R.color.white);
    }
}
