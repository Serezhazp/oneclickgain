package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.humanet.humanetcore.views.utils.OnCacheUpdateListener;
import com.humanet.humanetcore.views.widgets.items.FrameItemView;

import java.util.ArrayList;

/**
 * Created by Владимир on 29.10.2014.
 */
public class FrameListAdapter extends BaseAdapter implements OnCacheUpdateListener {

    private final int MAX_MEMORY = (int) Runtime.getRuntime().maxMemory();
    private final int CACHE_MEMORY = MAX_MEMORY / 4;

    private Context mContext;
    private ArrayList<String> mData;
    private int mSelected = 0;
    private LruCache<String, Bitmap> mBitmapCache;

    public FrameListAdapter(Context context) {
        mContext = context;
        mData = new ArrayList<>();

        //initialize bitmap cache
        mBitmapCache = new LruCache<String, Bitmap>(CACHE_MEMORY) {

            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FrameItemView view = (FrameItemView) convertView;
        if(view == null) {
            view = new FrameItemView(mContext);
        }
        view.setOnCacheUpdateListener(this);
        String frame = getItem(position);
        view.setData(frame);
        return view;
    }

    public void setData(ArrayList<String> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setSelected(int selected) {
        mSelected = selected;
        notifyDataSetChanged();
    }

    public int getSelected() {
        return mSelected;
    }

    @Override
    public void onCacheUpdated(Object key, Bitmap image) {
       /* if (image == null)
            mBitmapCache.remove((String) key);
        else
            mBitmapCache.put((String) key, image);*/
    }

    @Override
    public Bitmap getCachedBitmap(Object key) {
        if(key == null) {
            return  null;
        }
        return mBitmapCache.get((String) key);
    }

    public void clearCache(){
        mBitmapCache.evictAll();
    }
}
