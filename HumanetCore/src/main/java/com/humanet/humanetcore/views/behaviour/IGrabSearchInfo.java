package com.humanet.humanetcore.views.behaviour;

import com.humanet.humanetcore.api.ArgsMap;

/**
 * Created by ovitali on 26.10.2015.
 */
public interface IGrabSearchInfo {

    void grabSearchInfo(ArgsMap argsMap);

}
