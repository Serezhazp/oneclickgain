package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.CheckBox;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.ContactItem;

public class ContactItemView extends CardView {

    private CheckBox check;
    private ContactItem contact;


    public ContactItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_item, this);
        check = (CheckBox) findViewById(R.id.checkBox);
    }

    public void setData(ContactItem fr) {
        contact = fr;
        check.setText(contact.getName());
        check.setChecked(fr.isCheck());
    }

    public void setCheck(boolean val) {
        contact.setCheck(val);
        check.setChecked(val);
    }

    public boolean getCheck() {
        return contact.isCheck();
    }
}
