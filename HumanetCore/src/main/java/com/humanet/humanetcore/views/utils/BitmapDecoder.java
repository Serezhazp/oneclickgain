package com.humanet.humanetcore.views.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;

/**
 * Created by Владимир on 30.10.2014.
 */
public class BitmapDecoder {

    /**
     * Decode bitmap from file with required width and height.
     *
     * @param path    path to image file
     * @param width   required width
     * @param height  required height
     * @param isAlpha are we require alpha in out bitmap
     * @return decoded bitmap
     */
    public static Bitmap decodeFile(String path, int width, int height, boolean isAlpha) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        //read only bitmap sizes
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        //calculate size
        options.inSampleSize = calculateSize(options, width, height);

        //now we will read bitmap
        options.inJustDecodeBounds = false;

        //if we don't need bitmap with alpha
        if (!isAlpha) {
            options.inPreferredConfig = Bitmap.Config.RGB_565;
        }
        return BitmapFactory.decodeFile(path, options);

    }

    public static Bitmap decodeResources(Resources res, int id, int width, int height, boolean isAlpha) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        //read only bitmap sizes
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, id, options);

        //calculate size
        options.inSampleSize = calculateSize(options, width, height);

        //now we will read bitmap
        options.inJustDecodeBounds = false;

        //if we don't need bitmap with alpha
        if (!isAlpha) {
            options.inPreferredConfig = Bitmap.Config.RGB_565;
        } else {
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        }
        return BitmapFactory.decodeResource(res, id, options);

    }

    /**
     * Calculate required size for decoding bitmap
     *
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return required size
     */
    private static int calculateSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int width = options.outWidth;
        final int heigth = options.outHeight;

        int inSize = 1;

        if (heigth > reqHeight || width > reqWidth) {
            final int halfWidth = width / 2;
            final int halfHeight = heigth / 2;

            while ((halfWidth / inSize) > reqWidth
                    && (halfHeight / inSize) > reqHeight) {
                inSize *= 2;
            }
        }
        return inSize;
    }

    public static Bitmap createSquareBitmap(String path, int size, boolean isAlpha) {
        return createSquareBitmap(path, size, 0, isAlpha);
    }

    public static Bitmap createSquareBitmap(String path, int size, int camId, boolean isAlpha) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        //read only bitmap sizes
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        final int w = options.outWidth;
        final int h = options.outHeight;

        int startX = 0;
        int startY = 0;
        int rawSquareSize = 0;

        //prepare values to make square
        if (h > w) { //if bitmap is portrait
            startY = (h - w) / 2;
            rawSquareSize = w;
        } else if (w > h) { //if bitmap is landscape (or already square)
            startX = (w - h) / 2;
            rawSquareSize = h;
        }
        options.inJustDecodeBounds = false;
        if (!isAlpha) {
            options.inPreferredConfig = Bitmap.Config.RGB_565;
        } else {
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        }

        //decode raw bitmap and crop square in a center (with rotation for portrait bitmaps)
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        if (startY != 0 && startX == 0) { //if bitmap is portrait
            bmp = Bitmap.createBitmap(bmp, startX, startY, rawSquareSize, rawSquareSize);
        } else if (startY == 0 && startX != 0) { //if bitmap is landscape
            switch (camId) {
                case 1: //frame from front-cam
                    Camera.CameraInfo info = new Camera.CameraInfo();
                    Camera.getCameraInfo(camId, info);
                    float rotation = info.orientation;
                    Matrix m = new Matrix();
                    m.setRotate(rotation, rawSquareSize / 2, rawSquareSize / 2);
                    bmp = Bitmap.createBitmap(bmp, startX, startY, rawSquareSize, rawSquareSize, m, false);
                    break;
                default: //no rotation needed
                    bmp = Bitmap.createBitmap(bmp, startX, startY, rawSquareSize, rawSquareSize);
                    break;
            }
        }

        //scale new bitmap to preferred size
        if (bmp != null) {
            bmp = Bitmap.createScaledBitmap(bmp, size, size, false);
        }
        return bmp;
    }

}
