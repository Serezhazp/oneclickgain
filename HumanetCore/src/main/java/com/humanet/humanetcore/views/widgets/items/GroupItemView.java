package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.ContactsActivity;

public class GroupItemView extends LinearLayout {

    private CheckBox check;

    private ContactsActivity.Group mGroup;


    public GroupItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_group, this);

        check = (CheckBox) findViewById(R.id.checkBox);
    }

    public void setData(ContactsActivity.Group group) {
        mGroup = group;
        check.setText(group.name);
        check.setChecked(mGroup.checked);
    }

    public void setCheck(boolean val) {
        mGroup.checked = val;
        check.setChecked(val);
    }
}
