package com.humanet.humanetcore.views.behaviour;

import android.support.design.widget.TabLayout;

/**
 * Created by ovi on 2/19/16.
 */
public class SimpleOnTabSelectedListener implements TabLayout.OnTabSelectedListener {
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
