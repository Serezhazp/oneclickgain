package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.humanet.humanetcore.R;

/**
 * Created by ovitali on 09.01.2015.
 */
public class CircularProgressBar extends ProgressBar {

    private final RectF mCircleBounds = new RectF();
    private int STROKE_WIDTH = 0;
    private Paint mProgressColorPaint;

    public CircularProgressBar(Context context) {
        super(context);
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected synchronized void onDraw(Canvas canvas) {
        int prog = getProgress();
        float scale = getMax() > 0 ? (float) prog / getMax() * 360 : 0;
        canvas.drawArc(mCircleBounds, 90, scale, false, mProgressColorPaint);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (mProgressColorPaint == null) {
            STROKE_WIDTH = (int) getResources().getDimension(R.dimen.image_border_size);

            mProgressColorPaint = new Paint();
            mProgressColorPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
            mProgressColorPaint.setColor(getResources().getColor(R.color.colorPrimary));
            mProgressColorPaint.setAntiAlias(true);
            mProgressColorPaint.setStyle(Paint.Style.STROKE);
            mProgressColorPaint.setStrokeWidth(STROKE_WIDTH * 4);
        }

        final int height = MeasureSpec.getSize(heightMeasureSpec);
        final int width = MeasureSpec.getSize(widthMeasureSpec);
        int min = Math.min(width, height) - STROKE_WIDTH * 4;

        int x = (width - min) / 2;
        int y = (height - min) / 2;

        mCircleBounds.set(
                x,
                y,
                x + min,
                y + min);

        super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
    }
}
