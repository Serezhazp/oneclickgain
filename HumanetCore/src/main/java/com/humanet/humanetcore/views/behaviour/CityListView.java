package com.humanet.humanetcore.views.behaviour;

import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.modules.LocationDataLoader;

import java.util.List;

/**
 * Created by ovitali on 15.12.2015.
 */
public interface CityListView {

    void setCityList(List<City> cities);

    void setSelectedPosition(int selectedPosition);

    void setLocationDataLoader(LocationDataLoader locationDataLoader);

    void clear();

}
