package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.humanetcore.model.Balance;
import com.humanet.humanetcore.views.widgets.items.BalanceItemView;

public class BalanceListAdapter extends CursorAdapter {

    private Context mContext;

    public BalanceListAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new BalanceItemView(mContext);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((BalanceItemView) view).setData(Balance.fromCursor(cursor));
    }
}
