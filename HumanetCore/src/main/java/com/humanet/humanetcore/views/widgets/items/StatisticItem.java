package com.humanet.humanetcore.views.widgets.items;

import com.humanet.humanetcore.interfaces.CirclePickerItem;

/**
 * Created by ovitali on 09.02.2015.
 */
public class StatisticItem implements CirclePickerItem {
    private int mId;
    private String mTitle;
    private String mDrawable;

    public StatisticItem(int id, CirclePickerItem item) {
        mTitle = item.getTitle();
        mId = id;
        mDrawable = item.getDrawable();
    }

    public StatisticItem(int id, String title) {
        mTitle = title;
        mId = id;
    }

    public StatisticItem(int id, String title, String drawable) {
        mTitle = title;
        mId = id;
        mDrawable = drawable;
    }

    public int getId() {
        return mId;
    }

    public String getDrawable() {
        return mDrawable;
    }

    public String getTitle() {
        return mTitle;
    }


    public void setTitle(String title) {
        mTitle = title;
    }

}
