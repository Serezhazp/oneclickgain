package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;

import com.humanet.humanetcore.activities.ContactsActivity;
import com.humanet.humanetcore.model.ContactItem;
import com.humanet.humanetcore.views.widgets.items.ContactItemView;
import com.humanet.humanetcore.views.widgets.items.GroupItemView;

import java.util.ArrayList;

public class ContactsListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private ArrayList<ContactsActivity.Group> arList;
    private ExpandableListView mListView;

    public ContactsListAdapter(Context context, ArrayList<ContactsActivity.Group> list, ExpandableListView listView) {
        this.context = context;
        arList = list;
        mListView = listView;
    }

    @Override
    public int getGroupCount() {
        return arList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return arList.get(groupPosition).list.size();
    }

    @Override
    public ContactsActivity.Group getGroup(int groupPosition) {
        return arList.get(groupPosition);
    }

    @Override
    public ContactItem getChild(int groupPosition, int childPosition) {
        return arList.get(groupPosition).list.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new GroupItemView(context);
        }
        ((GroupItemView) convertView).setData(arList.get(groupPosition));
        mListView.expandGroup(groupPosition);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new ContactItemView(context);
        }
        ((ContactItemView) convertView).setData(arList.get(groupPosition).list.get(childPosition));
        return convertView;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void replace(ArrayList<ContactsActivity.Group> list) {
        arList.clear();
        arList.addAll(list);
        notifyDataSetChanged();
    }
}
