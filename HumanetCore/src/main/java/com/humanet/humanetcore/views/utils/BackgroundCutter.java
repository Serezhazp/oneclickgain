package com.humanet.humanetcore.views.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;

/**
 * Created by Владимир on 12.11.2014.
 */
public class BackgroundCutter {

    public static Bitmap createCuttedBitmap(Context context, Drawable background, int viewWidth, int viewHeight, int left, int top) {
        if (background instanceof BitmapDrawable) {
            return createCuttedBitmap(context, ((BitmapDrawable) background).getBitmap(), viewWidth, viewHeight, left, top);
        } else if (background instanceof ColorDrawable) {
            return createCuttedBitmap(context, ((ColorDrawable) background).getColor(), viewWidth, viewHeight, left, top);
        } else if (background instanceof GradientDrawable) {
            return null; //createCuttedBitmap(context, ((GradientDrawable) background), viewWidth, viewHeight, left, top);
        } else if (background != null) {
            throw new RuntimeException("Unexpected Drawable type");
        }
        return null;
    }


    public static Bitmap createCuttedBitmap(Context context, int color, int viewWidth, int viewHeight, int left, int top) {
        int borderSize = context.getResources().getDimensionPixelSize(R.dimen.image_border_size);

        int radius = (Math.min(viewWidth, viewHeight)) / 2 - borderSize;

        Bitmap bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        Canvas bitmapCanvas = new Canvas(bitmap);

        bitmapCanvas.drawColor(color);

        //
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        paint = new Paint();
        paint.setStrokeWidth(borderSize);
        paint.setColor(context.getResources().getColor(R.color.colorPrimary));
        paint.setStyle(Paint.Style.STROKE);
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        return bitmap;
    }

    public static Bitmap createCuttedBitmap(Context context, Bitmap background, int viewWidth, int viewHeight, int left, int top) {
        int borderSize = context.getResources().getDimensionPixelSize(R.dimen.image_border_size);

        int radius = (Math.min(viewWidth, viewHeight)) / 2 - borderSize;

        Bitmap bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        Canvas bitmapCanvas = new Canvas(bitmap);

        float scale = Math.max((float) viewWidth / (float) background.getWidth(), (float) viewHeight / (float) background.getHeight());

        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        matrix.setTranslate(-left, -top);

        bitmapCanvas.drawBitmap(background, matrix, new Paint());
        //
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        paint = new Paint();
        paint.setStrokeWidth(borderSize);
        paint.setColor(context.getResources().getColor(R.color.colorPrimary));
        paint.setStyle(Paint.Style.STROKE);
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        return bitmap;
    }

    public static Bitmap createCuttedBitmapFromGraddient(Context context, int parentHeight, int viewWidth, int viewHeight, int top, int padding) {
        int borderSize = context.getResources().getDimensionPixelSize(R.dimen.image_border_size);

        int radius = (Math.min(viewWidth, viewHeight)) / 2 - borderSize - padding;

        Bitmap bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        Canvas bitmapCanvas = new Canvas(bitmap);

        /* Create your gradient. */
        LinearGradient grad = new LinearGradient(0, -top,
                0, parentHeight - top,
                Color.BLACK, Color.parseColor("#6563a4")/* фиолетовый */,
                Shader.TileMode.CLAMP);

        /* Draw your gradient to the top of your bitmap. */
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setShader(grad);
        bitmapCanvas.drawRect(0, 0, viewWidth, viewHeight, p);

        //
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        paint = new Paint();
        paint.setStrokeWidth(borderSize);
        paint.setColor(context.getResources().getColor(R.color.colorPrimary));
        paint.setStyle(Paint.Style.STROKE);
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        return bitmap;
    }


    public static Bitmap blurImage(View view, Bitmap bitmap) {
        float w = view.getMeasuredWidth();
        float h = view.getMeasuredHeight();
        if (w == 0) {
            w = App.WIDTH;
        }
        if (h == 0) {
            h = App.HEIGHT;
        }

        float scale = Math.max(w / (float) bitmap.getWidth(), h / (float) bitmap.getHeight());
        float dx = ((float) bitmap.getWidth() * scale - w) / 2;
        float dy = ((float) bitmap.getHeight() * scale - h) / 2;
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        matrix.postTranslate(-dx, -dy);

        Bitmap b = Bitmap.createBitmap((int) w, (int) h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);

        c.drawBitmap(bitmap, matrix, new Paint());

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setAlpha(180);
        paint.setStyle(Paint.Style.FILL);

        c.drawRect(0, 0, (int) w, (int) h, paint);

        return b;
    }

}
