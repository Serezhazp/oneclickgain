package com.humanet.humanetcore.views.dialogs.dialogViews;

import android.content.Context;
import android.widget.LinearLayout;

import com.humanet.humanetcore.R;

/**
 * Created by michael on 25.11.14.
 */
public class MoreDialogView extends LinearLayout {

    public MoreDialogView(Context context) {
        super(context);
        inflate(context, R.layout.dialog_more, this);
    }
}
