package com.humanet.humanetcore.views.widgets.items;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.OnUpdateVideoListener;
import com.humanet.humanetcore.events.OnViewProfileListener;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.humanet.humanetcore.views.widgets.BubbleView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * View to dislay main video info
 * <p>
 * clickclap uses TextView to display likes count, that is why mLikesButton should be null for non-clickclap apps
 * ClickclapVideoItemDelegator - implementing setting correct smile(when need) and likes count
 */
@SuppressLint("ViewConstructor")
public class VideoItemView extends RelativeLayout implements View.OnClickListener {

    private static final int TODAY = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);

    private static final int CROWD_SINGLE_AMOUNT = 10;

    private final static int BASTOGRAM_LIKE_BRONZE = 100;
    private final static int BASTOGRAM_LIKE_SILVER = 500;
    private final static int BASTOGRAM_LIKE_GOLD = 1000;
    private final static int BASTOGRAM_LIKE_RUBY = 5000;
    private final static int BASTOGRAM_LIKE_BRILLIANT = 10000;

    private static ClickclapVideoItemDelegator sClickclapVideoItemDelegator;

    private static BastogramVideoItemDelegator sBastogramVideoItemDelegator;

    private int mCurrentEarned;

    private TextView mTagsTextView;
    private TextView mUserNameTextView;
    private TextView mLocalityTextView;
    private TextView mWatchesTextView;
    private BubbleView mCommentsButton;
    private ImageButton mLikesButton;

    private TextView mCreatedTextView;

    private ImageView mPreviewImage;

    private Video mData;
    private Video mVideoParent;
    private VideoType mVideoType;
    private Category mCategory;

    private TextView mBuyPriceView;
    private ImageView mBuyPriceButtonView;

    private OnVideoChooseListener mOnVideoChooseListener;

    public static void init(ClickclapVideoItemDelegator delegator) {
        sClickclapVideoItemDelegator = delegator;
    }

    public static void init(BastogramVideoItemDelegator delegator) {
        sBastogramVideoItemDelegator = delegator;
    }

    public VideoItemView(Context context, int currentUserId, VideoType videoType, Category category) {
        super(context);
        inflate(context, R.layout.item_video_list, this);
        mVideoType = videoType;
        mCategory = category;

        mPreviewImage = (ImageView) findViewById(R.id.preview_image);

        findViewById(R.id.play_button).setOnClickListener(this);

        mTagsTextView = (TextView) findViewById(R.id.stream_item_tags);

        mUserNameTextView = (TextView) findViewById(R.id.stream_item_user_name);
        mUserNameTextView.setOnClickListener(this);
        mLocalityTextView = (TextView) findViewById(R.id.stream_item_locality);
        mWatchesTextView = (TextView) findViewById(R.id.stream_item_watches);

        mCommentsButton = (BubbleView) findViewById(R.id.stream_item_comment);
        mCommentsButton.setOnClickListener(this);

        View v = findViewById(R.id.stream_item_likes);
        v.setOnClickListener(this);
        if (v instanceof ImageButton) {
            mLikesButton = (ImageButton) v;
        }

        mCreatedTextView = (TextView) findViewById(R.id.stream_item_created);

        findViewById(R.id.stream_item_more).setOnClickListener(this);

        if (mVideoType != null && mVideoType.isForSale()) {
            mBuyPriceView = (TextView) findViewById(R.id.buy_price);
            mBuyPriceButtonView = (ImageView) findViewById(R.id.buy_btn);
            mBuyPriceButtonView.setOnClickListener(this);
            mBuyPriceView.setOnClickListener(this);
        } else {
            findViewById(R.id.buy_container).setVisibility(INVISIBLE);
        }
    }

    public void setData(Video data, Video videoParent) {
        mData = data;
        mVideoParent = videoParent;

        //setting image
        mPreviewImage.setImageDrawable(null);
        ImageLoader.getInstance().displayImage(data.getThumbUrl(), mPreviewImage);

        String userName = mData.getAuthorName();
        if (userName != null) {
            userName = userName.replace(" ", "\n");
            mUserNameTextView.setText(userName);
        }
        mWatchesTextView.setText(String.valueOf(mData.getViews()));


        if (sClickclapVideoItemDelegator == null)
            setMark(mData.getMyMark(), mData.getLikes());

        if (!TextUtils.isEmpty(data.getTitle())) {
            mTagsTextView.setText(data.getTitle());
            mTagsTextView.setVisibility(VISIBLE);
        } else {
            mTagsTextView.setVisibility(GONE);
        }

        long created = data.getCreatedAt() * 1000;
        Calendar videoCalender = Calendar.getInstance();
        videoCalender.setTimeInMillis(created);

        if (mVideoType != null && ((mVideoType.equals(VideoType.CROWD_ASK)) || (mVideoType.equals(VideoType.CROWD_PET_ASK)))) {
            String crowned = String.format(Locale.getDefault(), "%d/<b>%d</b> %s", data.getEarn(), data.getCost(), App.COINS_CODE.toLowerCase(Locale.getDefault()));
            mCreatedTextView.setText(Html.fromHtml(crowned));
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                    videoCalender.get(Calendar.DAY_OF_YEAR) < TODAY && created < System.currentTimeMillis()
                            ? "dd.MM.yy"
                            : "HH:mm",
                    Locale.getDefault());
            mCreatedTextView.setText(simpleDateFormat.format(videoCalender.getTime()));
        }

        mCreatedTextView.setVisibility(VISIBLE);

        mCommentsButton.setCount(mData.getCommentsCount());

        setLocation(data.getCountry(), data.getCity());


        if (mVideoType != null && mVideoType.isForSale()) {
            boolean available = isBuyButtonAvailable();
            mBuyPriceButtonView.setEnabled(available);
            mBuyPriceView.setEnabled(available);
            mBuyPriceView.setText(String.valueOf(getBuyCost()));
        }

        if (sClickclapVideoItemDelegator != null)
            sClickclapVideoItemDelegator.displayEmoticon(this, mVideoType, mCategory, mData);
    }

    private void updateCrowdStatus(Video data) {
        data.setEarn(data.getEarn() + getBuyCost());
        mCurrentEarned = data.getEarn();
        String crowned = String.format(Locale.getDefault(), "%d/<b>%d</b> %s", data.getEarn(), data.getCost(), App.COINS_CODE.toLowerCase(Locale.getDefault()));
        mCreatedTextView.setText(Html.fromHtml(crowned));
    }

    private boolean isBuyButtonAvailable() {
        boolean available = mData.isAvailable();
        if (available && mVideoType != null) {
            if (mVideoType.equals(VideoType.CROWD_ASK) || mVideoType.equals(VideoType.CROWD_PET_ASK)) {
                if (mData.getEarn() >= mData.getCost()) {
                    available = false;
                }
            }
        }

        if(mData.getAuthorId() == AppUser.getInstance().get().getId())
            available = false;

        return available;
    }

    /*private int getBuyCost() {
        *//*if (mVideoType != null && ((mVideoType.equals(VideoType.CROWD_GIVE)) || (mVideoType.equals(VideoType.CROWD_PET_GIVE)))) {
            return CROWD_SINGLE_AMOUNT;
        }*//*
        //return mData.getCost();
        if(mData != null)
            return mData.getCost();
        else
            return CROWD_SINGLE_AMOUNT;
    }*/
    private int getBuyCost() {
        if (mVideoType != null && ((mVideoType.equals(VideoType.CROWD_ASK)) || (mVideoType.equals(VideoType.CROWD_PET_ASK)))) {
            return CROWD_SINGLE_AMOUNT;
        }
        return mData.getCost();
    }

    private void setLocation(String countryTitle, String cityTitle) {
        String locality = "";
        if (!TextUtils.isEmpty(countryTitle)) {
            locality += countryTitle;
        }
        if (!TextUtils.isEmpty(countryTitle) && !TextUtils.isEmpty(cityTitle)) {
            locality += "\n";
        }
        if (!TextUtils.isEmpty(cityTitle))
            locality += cityTitle;

        mLocalityTextView.setText(locality);
    }

    private void setMark(int mark) {
        setMark(mark, 0);
    }

    private void setMark(int mark, int likes) {
        int markRes;

        if(sBastogramVideoItemDelegator == null) {
            if (mark <= 0)
                markRes = R.drawable.like_empty;
            else
                markRes = R.drawable.like_filled;
        } else {
            if (mark <= 0)
                markRes = R.drawable.ic_pet_paw_green_light;
            else if(likes < BASTOGRAM_LIKE_BRONZE)
                markRes = R.drawable.ic_pet_paw_green;
            else if(likes < BASTOGRAM_LIKE_SILVER)
                markRes = R.drawable.ic_pet_paw_bronze;
            else if(likes < BASTOGRAM_LIKE_GOLD)
                markRes = R.drawable.ic_pet_paw_silver;
            else if(likes < BASTOGRAM_LIKE_RUBY)
                markRes = R.drawable.ic_pet_paw_gold;
            else if(likes < BASTOGRAM_LIKE_BRILLIANT)
                markRes = R.drawable.ic_pet_paw_ruby;
            else
                markRes = R.drawable.ic_pet_paw_brilliant;
        }

        mLikesButton.setImageResource(markRes);
    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.play_button) {
            if (mOnVideoChooseListener != null)
                mOnVideoChooseListener.onVideoChosen(mData);
            else
                ((OnVideoChooseListener) getContext()).onVideoChosen(mData);

        } else if (viewId == R.id.stream_item_comment) {
            ((OnCommentListener) getContext()).onShowComment(mData, mCategory, mVideoType);

        } else if (viewId == R.id.stream_item_user_name) {
            ((OnViewProfileListener) getContext()).onViewProfile(mData.getAuthorId());

        } else if (viewId == R.id.stream_item_more) {
            ((OnMoreListener) getContext()).onMore(mData, mVideoParent);

        } else if (viewId == R.id.stream_item_likes) {
            like();

        } else if ((viewId == R.id.buy_btn || viewId == R.id.buy_price)) {
            if (mVideoType.isForSale())
                buy();
        } else if (sClickclapVideoItemDelegator != null) {
            sClickclapVideoItemDelegator.emoticonClicked(v, getContext(), mData);
        }
    }

    public void buy() {
        if (!mData.isAvailable())
            return;

        if (mVideoType.equals(VideoType.MARKET_SHARE)
                || !mVideoType.equals(VideoType.MARKET_SELL) && BalanceUtil.isAllowedTransaction(AppUser.getInstance().get().getBalance(), getContext(), mData.getCost())
                || mVideoType.equals(VideoType.MARKET_SELL) && BalanceUtil.isAllowedTransaction(AppUser.getInstance().get().getGameBalanceValue(), getContext(), mData.getCost())
                ) {

            ((OnUpdateVideoListener) getContext()).onVideoPay(mVideoType, mData.getVideoId(), new ButtonStateChangeDisable(), new ButtonStateChangeEnable());
            updateCrowdStatus(mData);
        }
    }

    public void like() {
        int currentMark = mData.getMyMark();

        @OnUpdateVideoListener.MarkType final int mark = currentMark <= OnUpdateVideoListener.UN_LIKED ? OnUpdateVideoListener.LIKED : OnUpdateVideoListener.UN_LIKED;

        int likes = mData.getLikes();
        if (mark == OnUpdateVideoListener.LIKED) {
            mData.setLikes(++likes);
        } else {
            mData.setLikes(--likes);
        }

        ContentValues contentValues = new ContentValues(2);
        contentValues.put(ContentDescriptor.Videos.Cols.LIKES, mData.getLikes());
        contentValues.put(ContentDescriptor.Videos.Cols.MY_MARK, mark);

        getContext().getContentResolver().update(
                ContentDescriptor.Videos.URI,
                contentValues,
                ContentDescriptor.Videos.Cols.ID + " = " + mData.getVideoId(),
                null);

        mData.setMyMark(mark);

        if (sClickclapVideoItemDelegator == null)
            setMark(mark);
        else
            sClickclapVideoItemDelegator.displayEmoticon(this, mVideoType, mCategory, mData);


        //  mLikesButton.setText(String.valueOf(mData.getLikes()));

        ((OnUpdateVideoListener) getContext()).onVideoMark(mData.getVideoId(), mark);
    }

    public void setOnVideoChooseListener(OnVideoChooseListener onVideoChooseListener) {
        mOnVideoChooseListener = onVideoChooseListener;
    }

    public interface OnVideoChooseListener {
        void onVideoChosen(Video video);
    }

    public interface OnCommentListener {
        void onShowComment(Video video, Category category, VideoType videoType);
    }

    public interface OnMoreListener {
        void onMore(Video video, Video videoParent);
    }


    public interface ClickclapVideoItemDelegator {
        void displayEmoticon(VideoItemView imageView, VideoType videoType, Category category, Video videoData);

        void emoticonClicked(View view,  Context context,Video data);
    }

    public interface BastogramVideoItemDelegator {

    }

    private class ButtonStateChangeDisable implements Runnable {

        @Override
        public void run() {
            mBuyPriceButtonView.setEnabled(false);
            mBuyPriceView.setEnabled(false);
            //mData.setAvailable(false);
        }
    }

    private class ButtonStateChangeEnable implements Runnable {

        @Override
        public void run() {
            mBuyPriceButtonView.setEnabled(isBuyButtonAvailable());
            mBuyPriceView.setEnabled(isBuyButtonAvailable());
            //mData.setAvailable(false);
        }
    }


}
