package com.humanet.humanetcore.views.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.RoundedVignetteBitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

public class CircleBitmapDisplayer extends RoundedVignetteBitmapDisplayer {
    public CircleBitmapDisplayer() {
        super(0, 0);
    }

    @Override
    public void display(Bitmap bitmap, ImageAware imageAware, LoadedFrom loadedFrom) {
        if (imageAware instanceof ImageViewAware){
            imageAware.setImageDrawable(new CircleDrawable(bitmap, margin));
        }
    }

    public static class CircleDrawable extends RoundedDrawable {

        private Bitmap mBitmap;

        public CircleDrawable(Bitmap bitmap, int margin) {
            super(bitmap, 0, margin);
            this.mBitmap = bitmap;
        }

        @Override
        public void draw(Canvas canvas) {
            int radius = Math.min(canvas.getWidth(), canvas.getHeight()) / 2;

            canvas.drawRoundRect(mRect, radius, radius, paint);
        }
    }
}
