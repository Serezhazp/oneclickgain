package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.IInputTextView;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;

import java.util.Locale;

/**
 * Created by ovitali on 24.09.2015.
 */
public class EditWithCounterView extends LinearLayout implements IInputTextView {

    public EditWithCounterView(Context context) {
        this(context, null);
    }

    TextView mNewMessageLength;
    EditText mNewMessageView;

    private int mMaxEditTextLength = 120;

    public EditWithCounterView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(VERTICAL);

        inflate(context, R.layout.layout_edit_with_counter, this);

        if (isInEditMode())
            return;

        mNewMessageLength = (TextView) findViewById(R.id.new_message_length_view);
        mNewMessageView = (EditText) findViewById(R.id.new_message_view);
        mNewMessageView.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);

                setRemainedCharsCount();
            }
        });
        setRemainedCharsCount();

    }

    private void setRemainedCharsCount() {
        mNewMessageLength.setText(String.format(Locale.getDefault(), "%d/%d", mNewMessageView.getText().length(), mMaxEditTextLength));
    }

    public void setMaxEditTextLength(int length) {
        mMaxEditTextLength = length;
    }

    public Editable getText() {
        return mNewMessageView.getText();
    }

    public EditText getEditView() {
        return mNewMessageView;
    }

    public void setText(CharSequence text) {
        mNewMessageView.setText(text);
        setRemainedCharsCount();
    }

    public void setHint(CharSequence hint) {
        mNewMessageView.setHint(hint);
    }
}
