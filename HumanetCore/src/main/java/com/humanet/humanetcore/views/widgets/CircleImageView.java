package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.utils.ImageCroper;

/**
 * Created by Владимир on 28.10.2014.
 */
public class CircleImageView extends ImageView {

    private int mBorderSize;
    private int mBorderColor;

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //get attrs
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageViewOptions, 0, 0);

        //get border color
        mBorderColor = a.getColor(R.styleable.CircleImageViewOptions_borderColor, Color.parseColor("#FFFFFF"));

        //get border size
        mBorderSize = a.getDimensionPixelSize(R.styleable.CircleImageViewOptions_borderSize, 0);

        //free attrs
        a.recycle();


        if (isInEditMode()) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) getDrawable();
            if (bitmapDrawable != null) {
                setImageBitmap(bitmapDrawable.getBitmap());
            }
        }
    }

    public CircleImageView(Context context) {
        super(context);
    }

    public void setBorderSize(int borderSize) {
        if (mBorderSize != borderSize) {
            mBorderSize = borderSize;
        }
    }

    public void setBorderColor(int borderColor) {
        mBorderColor = borderColor;
    }

    public int getBorderSize() {
        return mBorderSize;
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            Bitmap outputBitmap = ImageCroper.getCircularBitmap(((BitmapDrawable) drawable).getBitmap(), mBorderSize, mBorderColor);
            super.setImageDrawable(new BitmapDrawable(getResources(), outputBitmap));
        } else {
            super.setImageDrawable(drawable);
        }
    }


    public void setImageBitmapWithoutRounding(Bitmap bm) {
        super.setImageBitmap(bm);
    }

    public void setImageResourceWithoutRounding(int resId) {
        Bitmap outputBitmap = BitmapFactory.decodeResource(getResources(), resId);
        setImageBitmapWithoutRounding(outputBitmap);
    }
}
