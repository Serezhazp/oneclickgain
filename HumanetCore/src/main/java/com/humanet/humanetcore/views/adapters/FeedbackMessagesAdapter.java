package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.FeedbackMessage;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Unneeded in this application adapter
 * Must be removed on code cleanup
 */

public class FeedbackMessagesAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;

    public FeedbackMessagesAdapter(Context context, Cursor c) {
        super(context, c, 0);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return FeedbackMessage.fromCursor(cursor).isReply()
                ? mLayoutInflater.inflate(R.layout.item_feedback_admin_message, parent, false)
                : mLayoutInflater.inflate(R.layout.item_feedback_my_message, parent, false);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        Cursor cursor = getCursor();
        cursor.moveToPosition(position);
        return FeedbackMessage.fromCursor(cursor).isReply() ? 1 : 0;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        FeedbackMessage message = FeedbackMessage.fromCursor(cursor);
        TextView messageView = (TextView) view.findViewById(R.id.message);
        messageView.setText(message.getMessage());

        ImageView imageView = (ImageView) view.findViewById(R.id.author_image);
        // TextView authorView = (TextView) view.findViewById(R.id.author_name);
        if (message.isReply()) {
            imageView.setImageResource(R.mipmap.ic_launcher);
            //     authorView.setText(R.string.feedback_answer_author);
        } else {
            ImageLoader.getInstance().displayImage(AppUser.getInstance().get().getAvatarSmall(), imageView);
            //     authorView.setText(AppUser.get().getFullName());
        }

        TextView timeView = (TextView) view.findViewById(R.id.time);
        timeView.setText(Constants.TIME_FORMAT.format(message.getTime()));

    }

}
