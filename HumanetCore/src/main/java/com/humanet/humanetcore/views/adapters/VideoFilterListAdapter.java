package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.humanet.filters.videofilter.IFilter;
import com.humanet.humanetcore.views.widgets.items.VideoFilterItemView;

import java.util.ArrayList;

/**
 * Created by Владимир on 31.10.2014.
 */
public class VideoFilterListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<IFilter> mData;

    private String mImagePath;

    public VideoFilterListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public IFilter getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VideoFilterItemView view = (VideoFilterItemView) convertView;
        if (view == null) {
            view = new VideoFilterItemView(mContext);
        }
        IFilter filter = getItem(position);
        view.setData(mImagePath, filter);
        return view;
    }


    public void setData(ArrayList<IFilter> data) {
        mData = data;
        notifyDataSetChanged();
    }

}
