package com.humanet.humanetcore.views.behaviour;

import android.widget.AdapterView;

/**
 * Created by ovitali on 08.10.2015.
 */
public abstract class SimpleOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
