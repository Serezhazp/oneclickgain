package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.Balance;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 13.11.2014.
 */
public class BalanceItemView extends CardView {

    private ImageView mImageView;
    private TextView mTitleTextView;
    private TextView mCoinsView;
    private TextView mDateView;

    public BalanceItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_balance, this);
        mImageView = (ImageView) findViewById(R.id.item_balance_image);
        mTitleTextView = (TextView) findViewById(R.id.item_balance_title);
        mCoinsView = (TextView) findViewById(R.id.item_balance_mon_coin);
        mDateView = (TextView) findViewById(R.id.item_balance_date);
    }

    public void setData(Balance data) {
        mImageView.setImageDrawable(null);

        if (data.getBalanceChangeType() == null)
            return;

        if (data.getBalanceChangeType().getDrawable() == null) {
            if (data.getVideoImage() != null)
                ImageLoader.getInstance().displayImage(data.getVideoImage(), mImageView);
        } else {
            ImageLoader.getInstance().displayImage(data.getBalanceChangeType().getDrawable(), mImageView, DisplayImageOptions.createSimple());
        }

        mTitleTextView.setText(data.getBalanceChangeType().getTitle());
        String symbol = "";

        if (data.getChange() != 0)
            symbol = data.getChange() > 0 ? "+ " : "- ";
        String balance = symbol + BalanceUtil.balanceToString(Math.abs(data.getChange())) + " " + App.COINS_CODE;
        mCoinsView.setText(balance);

        mDateView.setText(Constants.USER_DATE_FORMAT.format(data.getCreatedAt()));
    }
}
