package com.humanet.humanetcore.views.dialogs;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.ContactsActivity;

import java.util.ArrayList;

public class SelectContactsTypeDialog extends DialogFragment {
    public static String TAG = "SelectContactsTypeDialog";

    private AsyncTask<Integer, Void, ArrayList<ContactsActivity.Group>> mTask;

    private int mSelectedGroup;

    public static SelectContactsTypeDialog newInstance(AsyncTask<Integer, Void, ArrayList<ContactsActivity.Group>> task) {
        SelectContactsTypeDialog selectContactsTypeDialog = new SelectContactsTypeDialog();
        selectContactsTypeDialog.mTask = task;
        return selectContactsTypeDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.Dialog_No_Border);
    }

    @Override
    public Dialog getDialog() {
        Dialog dialog = super.getDialog();
        dialog.setCancelable(false);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_select_contact_group, container, false);
        RadioGroup rg = (RadioGroup) view.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedGroup = checkedId;
            }
        });
        view.findViewById(R.id.btn_next).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTask.execute(mSelectedGroup);
                dismiss();
            }
        });
        rg.check(R.id.rb_fav);

        setCancelable(false);

        return view;
    }


}
