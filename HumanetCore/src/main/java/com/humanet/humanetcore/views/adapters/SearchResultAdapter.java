package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.UserFinded;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.modules.BalanceUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Deni on 22.06.2015.
 */
public class SearchResultAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;
    Context mContext;

    public SearchResultAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.item_search, parent, false);
        new ViewHolder(view);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        UserFinded user = UserFinded.fromCursor(cursor);

        if (user.getId() == AppUser.getInstance().getUid())
            viewHolder.itemView.setCardBackgroundColor(context.getResources().getColor(R.color.listItemSelected));
        else
            viewHolder.itemView.setCardBackgroundColor(context.getResources().getColor(R.color.listItemNotSelected));


        String[] required = user.getRequiredFields();
        if (required == null) {
            required = new String[0];
        }

        StringBuilder userName = new StringBuilder();
        String value = validateRequired(
                ContentDescriptor.UserSearchResult.Cols.FIRST_NAME,
                user.getFirstName(),
                required
        );

        userName.append(value);
        userName.append(" ");

        value = validateRequired(
                ContentDescriptor.UserSearchResult.Cols.LAST_NAME,
                user.getLastName(),
                required
        );
        userName.append(value);
        viewHolder.nameView.setText(Html.fromHtml(userName.toString()));

        StringBuilder location = new StringBuilder();

        if (user.getCountry() != null) {
            value = validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.COUNTRY_ID,
                    user.getCountry(),
                    user.getCountryId(),
                    required);

            location.append(value);
            location.append(" ");
        }

        if (user.getCity() != null) {
            value = validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.CITY_ID,
                    user.getCity(),
                    user.getCityId(),
                    required);
            location.append(value);
        }
        viewHolder.location.setText(Html.fromHtml(location.toString()));

        StringBuilder details = new StringBuilder();
        if (!TextUtils.isEmpty(user.getLang())) {
            Language lang = Language.getFromString(user.getLang());

            String requiredLang = getPropertyOfRequiredPair(ContentDescriptor.UserSearchResult.Cols.LANG, required);

            value = lang.getTitle();
            if (requiredLang != null && requiredLang.equals(lang.getRequestParam())) {
                value = "<b>" + value + "</b>";
            }

            details.append(value);
            details.append(", ");
        }

        if (!TextUtils.isEmpty(user.getCraft())) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.CRAFT,
                    user.getCraft(),
                    required));

            details.append(", ");
        }

        if (user.getHobby() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.HOBBIE,
                    user.getHobby().getTitle(),
                    user.getHobby().getId(),
                    required));

            details.append(", ");
        }

        if (user.getInterest() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.INTEREST,
                    user.getInterest().getTitle(),
                    user.getInterest().getId(),
                    required));
            details.append(", ");
        }

        if (user.getSport() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.SPORT,
                    user.getSport().getTitle(),
                    user.getSport().getId(),
                    required));
            details.append(", ");
        }

        if (user.getPets() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.PETS,
                    user.getPets().getTitle(),
                    user.getPets().getId(),
                    required));
            details.append(", ");
        }
       /*
        if (user.getPets() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.PETS,
                    user.getPets().getTitle(), user.getPets().getId(),
                    required));
            details.append(", ");
        }
*/
        if (user.getReligion() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.RELIGION,
                    user.getReligion().getTitle(),
                    user.getReligion().getId(),
                    required));
            details.append(", ");
        }
        if (user.getGames() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.GAME,
                    user.getGames().getTitle(),
                    user.getGames().getId(),
                    required));
            details.append(", ");
        }
        if (user.getNonGame() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.NON_GAME,
                    user.getNonGame().getTitle(),
                    user.getNonGame().getId(),
                    required));
            details.append(", ");
        }
        if (user.getExtreme() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.EXTREME,
                    user.getExtreme().getTitle(),
                    user.getExtreme().getId(),
                    required));
            details.append(", ");
        }
        if (user.getVirtual() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.VIRTUAL,
                    user.getVirtual().getTitle(),
                    user.getVirtual().getId(),
                    required));
            details.append(", ");
        }
        if (user.getHowILook() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK,
                    user.getHowILook().getTitle(),
                    user.getHowILook().getId(),
                    required));
            details.append(", ");
        }
        if (user.getPartGames() != null) {
            details.append(validateRequired(
                    ContentDescriptor.UserSearchResult.Cols.PART_GAMES,
                    user.getPartGames().getTitle(),
                    user.getPartGames().getId(),
                    required));
            details.append(", ");
        }

        if (details.length() > 1) {
            viewHolder.detailsView.setText(Html.fromHtml(details.substring(0, details.length() - 2)));
        }

        viewHolder.ratingView.setText(BalanceUtil.balanceToString(user.getRating()));

        viewHolder.avatarView.setImageDrawable(null);
        if (user.getAvatarSmall() != null)
            ImageLoader.getInstance().displayImage(user.getAvatarSmall(), viewHolder.avatarView);

    }

    private class ViewHolder {
        CardView itemView;
        TextView nameView;
        TextView location;
        TextView ratingView;
        TextView detailsView;
        ImageView avatarView;

        public ViewHolder(View view) {
            itemView = (CardView) view;
            nameView = (TextView) view.findViewById(R.id.name);
            location = (TextView) view.findViewById(R.id.location);
            detailsView = (TextView) view.findViewById(R.id.details);
            ratingView = (TextView) view.findViewById(R.id.rating);
            avatarView = (ImageView) view.findViewById(R.id.avatar);

            view.setTag(this);
        }
    }

    private String validateRequired(String propertyName, String propertyValue, String[] requiredFields) {
        if (propertyValue == null)
            return null;
        for (String requiredField : requiredFields) {
            String[] pair = requiredField.split("~");
            if (pair[0].equals(propertyName) && pair[1].equals(propertyValue)) {
                return "<b>" + propertyValue + "</b>";
            }
        }
        return propertyValue;
    }

    private String validateRequired(String propertyName, String propertyValue, int propertyValueId, String[] requiredFields) {
        if (propertyValue == null)
            return null;
        for (String requiredField : requiredFields) {
            String[] pair = requiredField.split("~");
            if (pair[0].equals(propertyName) && pair[1].equals(String.valueOf(propertyValueId))) {
                return "<b>" + propertyValue + "</b>";
            }
        }
        return propertyValue;
    }

    private String getPropertyOfRequiredPair(String requiredKey, String[] requiredFields) {
        for (String requiredField : requiredFields) {
            String[] pair = requiredField.split("~");
            if (pair[0].equals(requiredKey))
                return pair[1];
        }
        return null;
    }
}
