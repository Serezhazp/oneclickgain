package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.humanet.humanetcore.model.Country;
import com.humanet.humanetcore.views.adapters.location.SimpleSpinnerAdapter;
import com.humanet.humanetcore.views.behaviour.CountryListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovi on 12/29/15.
 */
public class CountrySpinnerView extends Spinner implements CountryListView {

    private SimpleSpinnerAdapter<Country> mAdapter;

    public CountrySpinnerView(Context context) {
        this(context, null);
    }

    public CountrySpinnerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnItemSelectedListener(null);
        mAdapter = new SimpleSpinnerAdapter<>(new ArrayList<Country>(0));
        mAdapter.setHasHitItem(true);
        setAdapter(mAdapter);
    }

    @Override
    public void setCountryList(List<Country> countries) {
        mAdapter.set(countries);
    }


    public void setItemColor(int color) {
        mAdapter.setItemColor(color);
    }

    @Override
    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        super.setOnItemSelectedListener(new WrappedOnItemSelectedListener(listener));
    }

    private class WrappedOnItemSelectedListener implements OnItemSelectedListener {

        private OnItemSelectedListener mListener;

        public WrappedOnItemSelectedListener(OnItemSelectedListener listener) {
            mListener = listener;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (mListener != null)
                mListener.onItemSelected(parent, view, position, id);

        }


        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (mListener != null)
                mListener.onNothingSelected(parent);
        }
    }
}
