package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.Feedback;

import java.util.Date;

/**
 * Created by Denis on 28.04.2015.
 */
public class FeedbackAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;

    public FeedbackAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return mLayoutInflater.inflate(R.layout.item_feedback, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Feedback feedback = Feedback.fromCursor(cursor);

        TextView textView = (TextView) view.findViewById(R.id.message);
        textView.setText(String.valueOf(feedback.getLastMessage()));

        textView = (TextView) view.findViewById(R.id.time);
        textView.setText(Constants.DATE_FORMAT.format(new Date(feedback.getCreated()*1000)));

        TextView newCountView = (TextView) view.findViewById(R.id.new_count);
        if (feedback.getNew() > 0) {
            newCountView.setText(String.valueOf(feedback.getNew()));
            newCountView.setVisibility(View.VISIBLE);
        } else {
            newCountView.setVisibility(View.GONE);
        }

    }
}