package com.humanet.humanetcore.views.behaviour;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by ovitali on 18.08.2015.
 */
public class SimpleTextListener implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
