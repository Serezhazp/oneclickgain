package com.humanet.humanetcore.views.adapters.location;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.ILocationModel;

import java.util.Collection;
import java.util.List;

/**
 * Created by ovitali on 28.08.2015.
 */
public final class SimpleSpinnerAdapter<T extends ILocationModel> extends BaseAdapter {

    private List<T> mList;

    private int mHintItemColor;
    private boolean mHasHitItem;
    private int mItemColor;
    private int mDropdownItemColor;

    public SimpleSpinnerAdapter(List<T> list) {
        this.mList = list;
        mHintItemColor = App.getInstance().getResources().getColor(R.color.light_gray);
        mItemColor = App.getInstance().getResources().getColor(R.color.infoTextColor);
        mDropdownItemColor = Color.BLACK;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public T getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;

        if (convertView != null) {
            textView = (TextView) convertView;
        } else {
            textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        textView.setText(getItem(position).getTitle());

        if (mHasHitItem && position == 0)
            textView.setTextColor(mHintItemColor);
        else
            textView.setTextColor(mItemColor);


        return textView;
    }

    public void setHintItemColor(int hintItemColor) {
        mHintItemColor = hintItemColor;
    }

    public void setHasHitItem(boolean hasHitItem) {
        mHasHitItem = hasHitItem;
    }

    public void setItemColor(int itemColor) {
        mItemColor = itemColor;
    }



    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView;

        if (convertView != null) {
            textView = (TextView) convertView;
        } else {
            textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_dropdown_item, parent, false);
        }

        if (mHasHitItem && position == 0)
            textView.setTextColor(mHintItemColor);
        else
            textView.setTextColor(mDropdownItemColor);

        textView.setText(getItem(position).getTitle());

        return textView;
    }

    @SuppressWarnings("Unchecked")
    public void set(Collection<T> list) {
        if (mList.size() > 0) {
            mList.clear();
        }
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void add(Collection<T> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        if (mList != null && mList.size() > 0) {
            mList.clear();
            notifyDataSetChanged();
        }
    }
}
