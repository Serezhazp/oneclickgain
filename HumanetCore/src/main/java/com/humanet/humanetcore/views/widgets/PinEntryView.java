package com.humanet.humanetcore.views.widgets;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;
import com.humanet.humanetcore.views.utils.ConverterUtil;

public class PinEntryView extends ViewGroup implements View.OnTouchListener {

    private static final int DIGITS_COUNT = 4;

    private int mDigitWidth;
    private int mDigitHeight;
    private int mDigitSpacing;

    private int mCursorWidth;
    private int mCursorHeight;

    private View mCursor;

    private EditText mEntryView;
    private DigitView[] mDigits;

    public PinEntryView(Context context) {
        this(context, null);
    }

    public PinEntryView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode())
            return;

        setOnTouchListener(this);

        mDigitWidth = (int) ConverterUtil.dpToPix(context, 50);
        mDigitHeight = (int) ConverterUtil.dpToPix(context, 41);
        mDigitSpacing = (int) ConverterUtil.dpToPix(context, 5);


        mEntryView = new EditText(context);
        mEntryView.setCursorVisible(false);
        mEntryView.setHeight(mDigitHeight);
        addViewInLayout(mEntryView, -1, new LayoutParams(1, 1));
        mEntryView.setInputType(InputType.TYPE_CLASS_NUMBER);
        mEntryView.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        mDigits = new DigitView[DIGITS_COUNT];
        for (int i = 0; i < DIGITS_COUNT; i++) {
            DigitView digitView = new DigitView(context);
            mDigits[i] = digitView;

            addViewInLayout(digitView, -1, new LayoutParams(mDigitWidth, mDigitHeight));
        }


        mEntryView.addTextChangedListener(new SimpleTextListener() {

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (s.length() <= DIGITS_COUNT) {
                    int i = 0;
                    if (string.length() == 0) {
                        setCursor();
                    }
                    for (; i < string.length(); i++) {
                        mDigits[i].setText(String.valueOf(string.charAt(i)));
                        setCursor();
                    }
                    for (; i < DIGITS_COUNT; i++) {
                        mDigits[i].setText("");
                    }
                } else {
                    mEntryView.setText(string.substring(0, DIGITS_COUNT));
                }
            }
        });

        mCursorWidth = (int) ConverterUtil.dpToPix(context, 2);
        mCursorHeight = (int) (mEntryView.getTextSize() * 1.2f);

        mCursor = new View(context);

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(250);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        mCursor.startAnimation(anim);

        mCursor.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        addViewInLayout(mCursor, -1, new LayoutParams(mCursorWidth, mCursorHeight));

        post(new Runnable() {
            @Override
            public void run() {
                focus();
            }
        });
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        for (int i = 0; i < DIGITS_COUNT; i++) {
            DigitView digitView = mDigits[i];
            int x = i * (mDigitWidth + mDigitSpacing) + getPaddingLeft();
            digitView.layout(x, getPaddingTop(), x + mDigitWidth, getPaddingTop() + mDigitHeight);
        }
        mEntryView.layout(0, 0, 1, getMeasuredHeight());

        float cursorY = (getMeasuredHeight() - mCursorHeight) / 2f + getPaddingTop();

        mCursor.layout(
                0,
                (int) cursorY,
                mCursorWidth,
                (int) (cursorY + mCursorHeight)
        );

        setCursor();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (isInEditMode()) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        for (int i = 0; i < getChildCount() - 1; i++) {
            getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
        }
        mCursor.measure(mCursorWidth, mCursorHeight);

        int width = (mDigitWidth * DIGITS_COUNT) + (mDigitSpacing * (DIGITS_COUNT - 1));
        setMeasuredDimension(
                width + getPaddingLeft() + getPaddingRight(),
                mDigitHeight + getPaddingTop() + getPaddingBottom());
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public void setCursor() {
        int count = getText().length();

        float x = getPaddingLeft() + mDigitWidth / 2 + count * (mDigitWidth + mDigitSpacing);
        mCursor.setX(x);
    }

    private class DigitView extends TextView {
        public DigitView(Context context) {
            super(context);
            setTextColor(context.getResources().getColor(R.color.white_transparent));
            setBackgroundResource(R.drawable.edit_view_bg);
            setGravity(Gravity.CENTER);
            setWidth(mDigitWidth);
            setHeight(mDigitHeight);
            setTextSize(TypedValue.COMPLEX_UNIT_PX, mEntryView.getTextSize());
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // Make sure this view is focused
            focus();
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void focus() {
        postDelayed(new FocusRequestRunnable(3), 100);
    }

    public void addTextChangedListener(TextWatcher textWatcher) {
        mEntryView.addTextChangedListener(textWatcher);
    }

    public String getText() {
        return mEntryView.getText().toString();
    }

    private class FocusRequestRunnable implements Runnable {

        int counts;

        public FocusRequestRunnable(int counts) {
            this.counts = counts;
        }

        @Override
        public void run() {
            Activity activity = (Activity) getContext();

            if (activity.isFinishing()) {
                return;
            }
            counts--;

            mEntryView.requestFocus();
            // Show keyboard
            InputMethodManager inputMethodManager = (InputMethodManager) getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(mEntryView, 0);

            View focused = activity.getCurrentFocus();
            if (focused == null || !focused.equals(mEntryView) && counts > 0) {
                postDelayed(this, 100);
            }
        }
    }
}
