package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.utils.BackgroundCutter;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.utils.FlowMediaFacade;
import com.humanet.humanetcore.views.utils.UiUtil;

/**
 * Created by ovitali on 19.08.2015.
 */
public class CircleView extends FrameLayout {

    protected VideoSurfaceView mSurface;
    protected TextureView mTextureView;
    protected ImageView mPreviewImage;
    protected View mOverlayView;

    protected YellowProgressBar mProgressBar;
    protected ImageButton mPlayButton;

    private int mOverlayPadding;

    private boolean mBackgroundRequired;

    private FlowMediaFacade mFlowMediaFacade;

    public CircleView(Context context) {
        super(context);

        mOverlayView = new View(context);
        addView(mOverlayView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    public CircleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mBackgroundRequired = true;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleView);

        if (a.getBoolean(R.styleable.CircleView_surface, false)) {
            addSurfaceView(this);
        }
        if (a.getBoolean(R.styleable.CircleView_texture, false)) {
            addTextureView(this);
        }

        if (a.getBoolean(R.styleable.CircleView_preview, false)) {
            addImageView(this);
        }

        mOverlayPadding = a.getDimensionPixelSize(R.styleable.CircleView_overlayPadding, 0);

        mOverlayView = new View(context);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        // layoutParams.setMargins(mOverlayPadding, mOverlayPadding, mOverlayPadding, mOverlayPadding);
        addViewInLayout(mOverlayView, -1, layoutParams);

        if (a.getBoolean(R.styleable.CircleView_progress, false)) {
            addProgressbar(this);
        }

        if (a.getBoolean(R.styleable.CircleView_playButton, false)) {
            addPlayButton(this);
        }

        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        int size = Math.max(Math.min(width, height), 1);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY));


        /*if (mSurface != null && mSurfaceWidth > 0) {
            mSurface.getLayoutParams().width = mSurfaceWidth;
            mSurface.getLayoutParams().height = mSurfaceHeight;

            mSurface.setX(mSurfaceX);
            mSurface.setY(mSurfaceY);
        }*/
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        int size = Math.min(right - left, bottom - top);

        if (size < 10) {
            return;
        }

        if (size > 0) {
            Drawable background = getBackground();
            if (background == null) {
                background = ((View) getParent()).getBackground();

                left = (int) getX();
                top = (int) getY();
            } else {
                left = 0;
                top = 0;
            }

            if (mBackgroundRequired && isRedrawRequired(left, top, size)) {
                Bitmap bitmap;
                if (background instanceof GradientDrawable) {
                    bitmap = BackgroundCutter.createCuttedBitmapFromGraddient(getContext(),
                            ((View) getParent()).getMeasuredHeight(),
                            size,
                            size,
                            top,
                            mOverlayPadding);

                    UiUtil.setBackgroundDrawable(mOverlayView, bitmap);

                } else if (background instanceof ColorDrawable){
                    bitmap = BackgroundCutter.createCuttedBitmap(getContext(),
                            background,
                            size,
                            size,
                            left, top);

                    UiUtil.setBackgroundDrawable(mOverlayView, bitmap);
                } else if (background instanceof BitmapDrawable){
                    View parent = (View) getParent();
                    bitmap = ((BitmapDrawable) background).getBitmap();
                    bitmap = Bitmap.createScaledBitmap(bitmap, parent.getWidth(), parent.getHeight(), false);
                    bitmap = BackgroundCutter.createCuttedBitmap(getContext(),
                            bitmap,
                            size,
                            size,
                            left, top);

                    UiUtil.setBackgroundDrawable(mOverlayView, bitmap);
                }


               /* mOverlayView.setAlpha(0.2f);*/
            }
            // mOverlayView.setPadding(-getPaddingLeft(), -getPaddingTop(), -getPaddingRight(), -getPaddingBottom());
        }

    }

    public void setImage(String url) {
        getFlowMediaFacade().setImage(url);
    }

    public void resizeSurfaceView(float width, float height) {
        UiUtil.resizedToFitParent(mSurface, this, width, height);
        requestLayout();
    }

    public void setPlayButtonClickListener(View.OnClickListener clickListener) {
        mPlayButton.setOnClickListener(clickListener);
    }

    public VideoSurfaceView getSurfaceView() {
        return mSurface;
    }

    public TextureView getTextureView() {
        return mTextureView;
    }

    public ImageButton getPlayButton() {
        return mPlayButton;
    }

    public YellowProgressBar getProgressBar() {
        return mProgressBar;
    }

    public FlowMediaFacade getFlowMediaFacade() {
        if (mFlowMediaFacade == null) {
            mFlowMediaFacade = new FlowMediaFacade(mSurface, mPreviewImage, mProgressBar, mPlayButton);
        }
        return mFlowMediaFacade;
    }

    public ImageView getPreviewView() {
        return mPreviewImage;
    }

    public void disableVideoPlayback() {
        if (mFlowMediaFacade != null) mFlowMediaFacade.closeMediaPlayer();
        if (mPlayButton != null)
            mPlayButton.setVisibility(GONE);
        if (mProgressBar != null)
            mProgressBar.setVisibility(GONE);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mFlowMediaFacade != null) mFlowMediaFacade.closeMediaPlayer();
    }

    public void refresh() {
        mLastSizeValues = 0;
        requestLayout();
    }

    public View getOverlayView() {
        return mOverlayView;
    }

    public void insertView(View view) {
        int index = -1;
        if (mOverlayView != null) {
            index = indexOfChild(mOverlayView);
        }
        addViewInLayout(view, index, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    //-------------

    //----
    private long mLastSizeValues;

    private boolean isRedrawRequired(int left, int top, int size) {
        long newValues = left | top << 12 | size << 24;
        boolean required = mLastSizeValues != newValues;
        mLastSizeValues = newValues;
        return required;
    }

    //----

    public static void addSurfaceView(CircleView circleView) {
        circleView.mSurface = new VideoSurfaceView(circleView.getContext());
        circleView.mSurface.setId(R.id.video_displaying_surface_view);

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        circleView.addViewInLayout(circleView.mSurface, 0, layoutParams);
    }
    public static void addTextureView(CircleView circleView) {
        circleView.mTextureView = new VideoTextureView(circleView.getContext());
        circleView.mTextureView.setId(R.id.video_displaying_surface_view);

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        circleView.addViewInLayout(circleView.mTextureView, 0, layoutParams);
    }

    public static void addImageView(CircleView circleView) {
        circleView.mPreviewImage = new ImageView(circleView.getContext());
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        circleView.addViewInLayout(circleView.mPreviewImage, circleView.mSurface == null ? 0 : 1, layoutParams);
    }

    public static void addProgressbar(CircleView circleView) {
        circleView.mProgressBar = new YellowProgressBar(circleView.getContext());
        int size = (int) ConverterUtil.dpToPix(circleView.getContext(), 70);
        LayoutParams layoutParams = new LayoutParams(size, size);
        layoutParams.gravity = Gravity.CENTER;
        circleView.addViewInLayout(circleView.mProgressBar, -1, layoutParams);
    }

    public static void addPlayButton(CircleView circleView) {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;

        circleView.mPlayButton = new ImageButton(circleView.getContext());
        circleView.mPlayButton.setId(R.id.play_button);
        circleView.mPlayButton.setOnClickListener(null);
        circleView.mPlayButton.setVisibility(GONE);
        circleView.mPlayButton.setBackgroundResource(R.drawable.start_button);
        circleView.addViewInLayout(circleView.mPlayButton, -1, layoutParams);
    }

}
