package com.humanet.humanetcore.views.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.ViewGroup;

import com.humanet.humanetcore.R;

/**
 * Created by Владимир on 06.10.2014.
 */
public class DialogBuilder {

    private Context mContext;
    private String mTitle;
    private String mMessage;
    private String[] mItems;
    private DialogInterface.OnClickListener mItemsClickListener;
    private DialogInterface.OnClickListener mPositiveClickListener;
    private DialogInterface.OnClickListener mNegativeClickListener;
    private int mPositiveButtonId;
    private int mNegativeButtonId;
    private boolean isCancelable = false;
    private boolean isTransparentBackground = true;

    public DialogBuilder(Context context) {
        mContext = context;
    }

    public DialogBuilder setTitle(int titleId) {
        String title = mContext.getResources().getString(titleId);
        return setTitle(title);
    }

    public DialogBuilder setTitle(String title) {
        mTitle = title;
        return this;
    }

    public DialogBuilder setMessage(int messageId) {
        String message = mContext.getResources().getString(messageId);
        return setMessage(message);
    }

    public DialogBuilder setMessage(String message) {
        mMessage = message;
        return this;
    }

    public DialogBuilder setItems(String[] items, DialogInterface.OnClickListener listener) {
        mItems = items;
        mItemsClickListener = listener;
        return this;
    }

    public DialogBuilder setPositiveButton(int id, DialogInterface.OnClickListener listener) {
        mPositiveClickListener = listener;
        mPositiveButtonId = id;
        return this;
    }

    public DialogBuilder setPositiveButton(DialogInterface.OnClickListener listener) {
        mPositiveClickListener = listener;
        return this;
    }

    public DialogBuilder setNegativeButton(int id, DialogInterface.OnClickListener listener) {
        mNegativeClickListener = listener;
        mNegativeButtonId = id;
        return this;
    }

    public DialogBuilder setNegativeButton(DialogInterface.OnClickListener listener) {
        mNegativeClickListener = listener;
        return this;
    }

    public DialogBuilder setNotTransparentBackground(boolean isNotTransparent) {
        isTransparentBackground = !isNotTransparent;
        return this;
    }

    public Dialog create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, android.R.style.Theme_DeviceDefault_Dialog);
        if (mTitle != null) {
            builder.setTitle(mTitle);
            mTitle = null;
        }

        if (mMessage != null) {
            builder.setMessage(mMessage);
            mMessage = null;
        } else if (mItems != null) {
            builder.setItems(mItems, mItemsClickListener);
            mItems = null;
        }

        if (mPositiveClickListener != null) {
            if(mPositiveButtonId != 0) {
                builder.setPositiveButton(mPositiveButtonId, mPositiveClickListener);
            } else {
                builder.setPositiveButton(R.string.ok, mPositiveClickListener);
            }
            mPositiveClickListener = null;
        } else {
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }

        if (mNegativeClickListener != null) {
            if(mNegativeButtonId != 0) {
                builder.setNegativeButton(mNegativeButtonId, mNegativeClickListener);
            } else {
                builder.setNegativeButton(R.string.cancel, mNegativeClickListener);
            }
            mNegativeClickListener = null;
        }

        builder.setCancelable(isCancelable);

        Dialog dialog = builder.create();
        if(isTransparentBackground)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        else
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    public DialogBuilder setCancelable(boolean val) {
        isCancelable = val;
        return this;
    }
}
