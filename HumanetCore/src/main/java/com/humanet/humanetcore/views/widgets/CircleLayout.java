package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.humanet.humanetcore.App;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CircleLayout extends FrameLayout implements View.OnTouchListener {

    enum LayoutMode {
        LEFT, RIGHT;

        public int toInt() {
            if (this.equals(LEFT))
                return 0;
            else
                return -1;
        }
    }

    // Event listeners
    private OnItemSelectedListener mOnItemSelectedListener = null;

    private Scroller mScroller;
    private GestureDetector mGestureDetector;


//    String tag = CircleLayout.class.getSimpleName();

    private ItemWrapper mSelectedItem;

    // Child sizes
    private static int childWidth = -1;
    private static int childHeight = -1;
    private int halfOfCircleWidth = -1;
    private int halfOfChildWidth = -1;

    // Sizes of the ViewGroup
    private int radius = 0;

    private BaseAdapter mAdapter;

    private LinkedList<View> mCache = new LinkedList<>();

    private int mScrollMinValue;
    private int mScrollMaxValue;

    private ItemWrapper mTouchedItem;
    private float mStartViewTouchX;

    public CircleLayout(Context context) {
        this(context, null);
    }

    public CircleLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setAdapter(BaseAdapter adapter) {
        mAdapter = adapter;
        update();
    }

    public void update() {
        mScrollMinValue = -childWidth;
        mScrollMaxValue = mAdapter.getCount() * childWidth;

        mScroller = new Scroller(getContext(), new DecelerateInterpolator());
        mGestureDetector = new GestureDetector(getContext(), mSimpleOnGestureListener);
    }

    public void scrollToItem(ItemWrapper itemWrapper) {
        if (itemWrapper != null) {
            int scrollerCurrentX = mScroller.getCurrX();
            int itemX = itemWrapper.getPosition() * childWidth;

            int deltaX = itemX - scrollerCurrentX;

            if (deltaX != 0) {
                mScroller.startScroll(scrollerCurrentX, 0, deltaX, 0, 500);
                post(new FlyingRunnable());
            } else {
                selectItem(itemWrapper);
            }
        }
    }

    private GestureDetector.SimpleOnGestureListener mSimpleOnGestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            mScroller.forceFinished(true);
            if (mTouchedItem != null) {
                scrollToItem(mTouchedItem);
                return true;
            }/* else {
                Log.e("onSingleTapUp", "mTouchedItem == null");
            }*/
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            mScroller.forceFinished(true);
            mTouchedItem = null;
            mScroller.computeScrollOffset();
            mScroller.fling(mScroller.getCurrX(), 0, -(int) velocityX, 0,
                    mScrollMinValue, mScrollMaxValue, 0, 0);
            post(new FlyingRunnable());
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mScroller.forceFinished(true);
            if (mScroller.getCurrX() > mScrollMinValue - halfOfChildWidth * 3 && mScroller.getCurrX() < mScrollMaxValue + childWidth) {
                mScroller.startScroll(mScroller.getCurrX(), 0, (int) distanceX, 0, 0);
                if (mScroller.computeScrollOffset()) {
                    rotateButtons();
                }
            }

            return false;
        }
    };

    private class FlyingRunnable implements Runnable {
        @Override
        public void run() {
            if (mScroller.computeScrollOffset()) {
                rotateButtons();
                postDelayed(this, 30);
            } else {
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToCloserItem();
                    }
                }, 30);
            }
        }
    }

    private void scrollToCloserItem() {
        int closestItemPosition = (int) Math.round((double) mScroller.getCurrX() / (double) childWidth);
        closestItemPosition = Math.max(closestItemPosition, 0);
        closestItemPosition = Math.min(closestItemPosition, mAdapter.getCount() - 1);

        int closedDistance = closestItemPosition * childWidth - mScroller.getCurrX();

        ItemWrapper closerItem = null;

        if (mSelectedItem != null && mSelectedItem.getPosition() == closestItemPosition) {
            closerItem = mSelectedItem;
        } else {
            for (int i = 0; i < getChildCount(); i++) {
                closerItem = (ItemWrapper) getChildAt(i);
                if (closerItem.getPosition() == closestItemPosition) {
                    break;
                }
            }
        }

        if (Math.abs(closedDistance) > 5) {
            scrollToItem(closerItem);
        } else {
            mScroller.startScroll(mScroller.getCurrX(), 0, closedDistance, 0, 0);
            mScroller.computeScrollOffset();
            rotateButtons();
        }

        selectItem(closerItem);
    }


    private void selectItem(ItemWrapper closerItem) {
        if (closerItem == null) {
            return;
        }

        if (mSelectedItem != null && !mSelectedItem.equals(closerItem)) {
            mSelectedItem.setSelected(false);
        }

        if (mSelectedItem == null || !mSelectedItem.equals(closerItem)) {
            if (mOnItemSelectedListener != null) {
                mOnItemSelectedListener.onItemSelected(mAdapter.getItem(closerItem.getPosition()));
            }
            closerItem.setSelected(true);
            mSelectedItem = closerItem;
        }
    }

    protected void init() {
        radius = (int) (App.WIDTH * 1.2f / 2);

        childWidth = App.IMAGE_SMALL_SIDE;
        childHeight = (int) (childWidth * 2f);

        halfOfCircleWidth = radius;

        halfOfChildWidth = childWidth / 2;
    }

    public void setSelectedItem(int position) {
        removeAllViews();

        ItemWrapper selectedItem = null;

        int itemCount = mAdapter.getCount();

        int itemPosition = Math.max(position - 2, 0);

        int addViewCount = 0;

        int scrollX = position * childWidth;

        mScroller.startScroll(0, 0, scrollX, 0, 0);
        mScroller.computeScrollOffset();

        while (itemPosition < itemCount && addViewCount <= 5) {
            final ItemWrapper child = (ItemWrapper) mAdapter.getView(itemPosition, null, CircleLayout.this);

            if (itemPosition == position) {
                selectedItem = child;
            }

            child.setPosition(itemPosition);

            double d = setChildPosition(scrollX, child);
            updateItemScale(child, d);

            add(child, LayoutMode.RIGHT);

            itemPosition++;

            addViewCount++;
        }

        invalidateAll();

        selectItem(selectedItem);

        postDelayed(new FlyingRunnable(), 30);
    }

    public void clear() {
        removeAllViews();
    }


    public void invalidateAll() {
        invalidate();
        requestLayout();
        for (int i = 0; i < getChildCount(); i++) {
            ItemWrapper view = (ItemWrapper) getChildAt(i);
            for (int j = 0; j < view.getChildCount(); j++) {
                View v = view.getChildAt(j);
                v.invalidate();
            }
        }
    }

    private void updateScale() {
        int childCount = getChildCount();
        int scrollerX = mScroller.getCurrX();
        for (int i = 0; i < childCount; i++) {
            final ItemWrapper child = (ItemWrapper) getChildAt(i);
            if (child != null) {
                double d = setChildPosition(scrollerX, child);

                updateItemScale(child, d);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, childHeight);
    }

    private void updateItemScale(ItemWrapper item, double d) {
        float alpha = (float) Math.sin(d);

        alpha *= alpha;
        item.setAlpha(alpha);
        item.setScaleX(alpha);
        item.setScaleY(alpha);
    }

    public int rotateButtons() {
        long started = System.currentTimeMillis();

        int childCount = getChildCount();

        if (childCount == 0) {
            return 0;
        }

        boolean changed = false;

        int scrollerX = mScroller.getCurrX();

        updateScale();
        //  invalidateAll();

        List<ItemWrapper> listToRemove = new ArrayList<>(5);
        for (int i = 0; i < childCount; i++) {
            final ItemWrapper child = (ItemWrapper) getChildAt(i);
            if (child != null) {
                if (child.getX() > App.WIDTH) {
                    listToRemove.add(child);
                }
                if (child.getX() < -childWidth) {
                    listToRemove.add(child);
                }
            }
        }


        ItemWrapper firstItem = (ItemWrapper) getChildAt(0);
        if (firstItem != null) {
            while (firstItem.getX() > -childWidth && firstItem.getPosition() > 0) {
                ItemWrapper newItem = (ItemWrapper) mAdapter.getView(firstItem.getPosition() - 1, getCachedView(), this);
                newItem.setPosition(firstItem.getPosition() - 1);
                double d = setChildPosition(scrollerX, newItem);
                updateItemScale(newItem, d);
                add(newItem, LayoutMode.LEFT);
                changed = true;
                firstItem = newItem;
            }
        }

        ItemWrapper lastItem = (ItemWrapper) getChildAt(childCount - 1);
        if (lastItem != null) {
            while (lastItem.getX() < App.WIDTH && lastItem.getPosition() < mAdapter.getCount() - 1) {
                ItemWrapper newItem = (ItemWrapper) mAdapter.getView(lastItem.getPosition() + 1, getCachedView(), this);
                newItem.setPosition(lastItem.getPosition() + 1);
                double d = setChildPosition(scrollerX, newItem);
                updateItemScale(newItem, d);
                add(newItem, LayoutMode.RIGHT);
                changed = true;
                lastItem = newItem;
            }
        }

        if (listToRemove.size() > 0 && childCount == 1) {
            int ignorePosition = -1;
            if (listToRemove.size() >= childCount) {
                if (scrollerX < 0) {
                    ignorePosition = mAdapter.getCount() - 1;
                } else {
                    ignorePosition = 0;
                }
            }

            for (int i = listToRemove.size() - 1; i >= 0; i--) {
                ItemWrapper view = listToRemove.remove(i);
                if (view.getPosition() != ignorePosition) {
                    removeViewInLayout(view);
                    mCache.push(view);
                    changed = true;
                }
            }
        }

        if (changed) {
            invalidateAll();
        }
        long ended = System.currentTimeMillis();
        // Log.d(tag, "rotateButtons " + (ended - started) + " mCacheSize" + mCache.size() + "  child count:" + getChildCount());
        return (int) (ended - started);
    }

    public double setChildPosition(int scrollerX, ItemWrapper item) {
        int itemRealPositionX = getChildPosition(scrollerX, item.getPosition());

        float dx = item.getPosition() * childWidth - scrollerX;
        double d = Math.atan(dx / (float) radius) + Math.PI / 2;
        float dy = halfOfCircleWidth - (float) (halfOfCircleWidth * Math.sin(d));
        item.setY(childWidth / 2 - dy);
        item.setX(itemRealPositionX);

        return d;
    }

    private int getChildPosition(int scrollerX, int position) {
        int centerOfZeroItem = App.WIDTH / 2 - halfOfChildWidth;
        int currentItemRelativePosition = position * childWidth;
        int itemRealPositionX = currentItemRelativePosition + centerOfZeroItem - scrollerX;
        return itemRealPositionX;
    }

    public void add(View child, LayoutMode mode) {
        ViewGroup.LayoutParams params = child.getLayoutParams();
        if (params == null) {
            params = new ViewGroup.LayoutParams(childWidth, childHeight);
        }
        addViewInLayout(child, mode.toInt(), params, true);
        child.setOnTouchListener(this);
    }

    private View getCachedView() {
        if (mCache.size() > 0) {
            return mCache.remove();
        }

        return null;
    }

    public interface OnItemSelectedListener {
        void onItemSelected(Object data);
    }

    public void setOnItemSelectedListener(
            OnItemSelectedListener onItemSelectedListener) {
        this.mOnItemSelectedListener = onItemSelectedListener;
    }


    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (mGestureDetector != null) {
            mGestureDetector.onTouchEvent(event);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStartViewTouchX = event.getX();
                    requestDisallowInterceptTouchEvent(true);
                    break;

                case MotionEvent.ACTION_MOVE:
                    if (Math.abs(mStartViewTouchX - event.getX()) > halfOfChildWidth / 2) {
                        mTouchedItem = null;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    requestDisallowInterceptTouchEvent(false);

                    if (mScroller.isFinished()) {
                        if (mTouchedItem != null) {
                            scrollToItem(mTouchedItem);
                        } else {
                            scrollToCloserItem();
                        }
                    }

                    break;
            }
            return true;
        }
        return false;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (v instanceof ItemWrapper) {
                    mTouchedItem = (ItemWrapper) v;
                }
        }

        return false;
    }

    public static class ItemWrapper extends LinearLayout {
        public ItemWrapper(Context context) {
            super(context);
            setGravity(Gravity.CENTER_HORIZONTAL);
        }

        // Position represents the index of this view in the viewgroups children array
        private int position = Integer.MIN_VALUE;

        /**
         * Return the position of the view.
         *
         * @return Returns the position of the view.
         */
        public int getPosition() {
            return position;
        }

        /**
         * Set the position of the view.
         *
         * @param position The position to be set for the view.
         */
        public void setPosition(int position) {
            this.position = position;
        }


        private Object mData;

        public void setData(Object data) {
            mData = data;
        }

        public Object getData() {
            return mData;
        }

        private boolean mSelected;

        public void setSelected(boolean selected) {
            super.setSelected(selected);
            if (mSelected == selected)
                return;

            mSelected = selected;
            postInvalidate();
        }


        public static int getW() {
            return childWidth;
        }
    }
}