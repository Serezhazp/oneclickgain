package com.humanet.humanetcore.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.vote.VoteOption;
import com.humanet.humanetcore.views.widgets.VoteProgressBar;

import java.util.Locale;

/**
 * Created by ovitali on 17.11.2015.
 */
public class VoteOptionsAdapter extends CursorAdapter {

    private int mVoted;
    private boolean mFinished;
    private int mVotedCount;


    public VoteOptionsAdapter(Context context, Cursor cursor, int voted) {
        super(context, cursor, 0);
        mVoted = voted;
    }

    public void setVoted(int voted) {
        mVoted = voted;
    }

    public void setFinished(boolean finished) {
        mFinished = finished;
    }

    public void setVotedCount(int votedCount) {
        mVotedCount = votedCount;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        ViewHolder viewHolder;
        if (mVoted != 0 || mFinished) {
            viewHolder = new VotedViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_voted_option, parent, false));
        } else {
            viewHolder = new VoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vote_option, parent, false));
        }

        return viewHolder.itemView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (mVoted != 0 || mFinished) ? 1 : 0;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.setData(VoteOption.fromCursor(cursor));
    }


    // -------------- internal classes ------------
    abstract class ViewHolder {
        View itemView;
        TextView titleView;

        private ViewHolder(View itemView) {
            this.itemView = itemView;
            this.itemView.setTag(this);
        }

        abstract void setData(VoteOption voteOption);
    }

    class VoteViewHolder extends ViewHolder {
        TextView titleView;

        private VoteViewHolder(View itemView) {
            super(itemView);
            titleView = (TextView) itemView.findViewById(R.id.title);
        }

        @Override
        void setData(VoteOption voteOption) {
            titleView.setText(voteOption.getTitle());
        }
    }

    class VotedViewHolder extends ViewHolder {
        VoteProgressBar progressView;
        TextView countView;

        private VotedViewHolder(View itemView) {
            super(itemView);
            titleView = (TextView) itemView.findViewById(R.id.title);
            progressView = (VoteProgressBar) itemView.findViewById(R.id.statistic);
            countView = (TextView) itemView.findViewById(R.id.count);
        }

        @Override
        void setData(VoteOption option) {
            String title = option.getTitle() + (mVoted == option.getId() ? " (" + itemView.getResources().getString(R.string.vote_voted_your_choice) + ")" : "");
            titleView.setText(title);

            if (mVotedCount > 0) {
                progressView.setProgress((float) option.getVotes() / (float) mVotedCount);

                int progress = Math.round(((float) option.getVotes() / (float) mVotedCount) * 100f);

                countView.setText(String.format(Locale.getDefault(), "%d (%d%%)", option.getVotes(), progress));
            }
        }
    }


}
