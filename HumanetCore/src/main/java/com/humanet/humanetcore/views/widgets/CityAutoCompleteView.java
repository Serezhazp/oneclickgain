package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.modules.LocationDataLoader;
import com.humanet.humanetcore.views.adapters.location.CityAutoCompleteAdapter;
import com.humanet.humanetcore.views.behaviour.CityListView;
import com.humanet.humanetcore.views.behaviour.SimpleTextListener;

import java.util.List;
import java.util.Locale;

/**
 * Created by ovi on 12/23/15.
 */
public class CityAutoCompleteView extends AutoCompleteTextView implements CityListView {

    private CityAutoCompleteAdapter mAdapter;

    private City mSelectedCity;

    private LocationDataLoader mLocationDataLoader;

    private AdapterView.OnItemSelectedListener mListener;

    public CityAutoCompleteView(Context context) {
        this(context, null);
    }

    public CityAutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSaveEnabled(false);
        setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        setTextSize(18);
        setThreshold(0);

        setHintTextColor(getResources().getColor(R.color.light_gray));

        setHint(R.string.profile_private_info_choose_city);

        mAdapter = new CityAutoCompleteAdapter();
        setAdapter(mAdapter);

        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setSelectedPosition(position);
                if (mListener != null)
                    mListener.onItemSelected(parent, view, position, id);
            }
        });

        setOnItemSelectedListener(null);

        addTextChangedListener(new SimpleTextListener() {
            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString().toLowerCase();
                if (mSelectedCity != null && mSelectedCity.getTitle().toLowerCase(Locale.getDefault()).equals(text) || text.contains("@")) {
                    return;
                }
                mSelectedCity = null;
                for (int i = 0; i < mAdapter.getCount(); i++) {
                    City c = mAdapter.getItem(i);
                    if (c.getTitle().toLowerCase(Locale.getDefault()).equals(text)) {
                        mSelectedCity = c;
                    }
                }

                if (mSelectedCity == null && text.length() > 1)
                    mLocationDataLoader.loadCities(text);
            }
        });

        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showDropDown();
            }
        });

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (hasFocus())
                    if (!isPopupShowing())
                        showDropDown();
                    else
                        requestFocus();

                return false;
            }
        });

    }

    public void setLocationDataLoader(LocationDataLoader locationDataLoader) {
        mLocationDataLoader = locationDataLoader;
    }

    public int getSelectedItemId() {
        return mSelectedCity != null ? mSelectedCity.getId() : -1;
    }


    public void setSelectedPosition(int position) {
        mSelectedCity = mAdapter.getItem(position);
        String cityName = mSelectedCity.getTitle();
        setText(cityName);
        setSelection(cityName.length());
    }

    public City getSelectedItem() {
        return mSelectedCity;
    }

    public void clear() {
        mSelectedCity = null;
        setText("");
        mAdapter.clear();
    }

    @Override
    public void setCityList(List<City> cities) {
        mAdapter.set(cities);
        setVisibility(VISIBLE);
    }

    @Override
    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener l) {
        mListener = l;
        super.setOnItemSelectedListener(new WrappedOnItemSelectedListener());
    }

    private class WrappedOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            setSelectedPosition(position);

            if (mListener != null)
                mListener.onItemSelected(parent, view, position, id);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (mListener != null)
                mListener.onNothingSelected(parent);
        }
    }

}
