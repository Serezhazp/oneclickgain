package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;

/**
 * Created by ovitali on 17.09.2015.
 */
public class VideoTextureView extends TextureView {
    public VideoTextureView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoTextureView(Context context) {
        this(context, null);
    }

    private int calculatedX;
    private int calculatedY;
    private int calculatedWidth;
    private int calculatedHeight;


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (calculatedWidth != 0 && calculatedHeight != 0) {
            getLayoutParams().width = calculatedWidth;
            getLayoutParams().height = calculatedHeight;

            setX(calculatedX);
            setY(calculatedY);

            left = calculatedX;
            top = calculatedY;

            bottom = top + calculatedHeight;
            right = left + calculatedWidth;
        }

        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (calculatedWidth == 0) {
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        } else {
            setMeasuredDimension(calculatedWidth, calculatedHeight);
        }
    }


    public void setCalculatedX(int calculatedX) {
        setX(calculatedX);
        this.calculatedX = calculatedX;
    }

    public void setCalculatedY(int calculatedY) {
        setY(calculatedY);
        this.calculatedY = calculatedY;
    }

    public boolean setCalculatedSizes(int calculatedWidth, int calculatedHeight) {
        if (this.calculatedWidth == calculatedWidth && this.calculatedHeight == calculatedHeight) {
            return false;
        }
        setCalculatedWidth(calculatedWidth);
        setCalculatedHeight(calculatedHeight);

        return true;
    }

    public void setCalculatedWidth(int calculatedWidth) {
        getLayoutParams().width = calculatedWidth;
        this.calculatedWidth = calculatedWidth;
    }

    public void setCalculatedHeight(int calculatedHeight) {
        getLayoutParams().height = calculatedHeight;
        this.calculatedHeight = calculatedHeight;
    }


}
