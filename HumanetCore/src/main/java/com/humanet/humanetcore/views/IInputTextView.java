package com.humanet.humanetcore.views;

import android.text.Editable;
import android.widget.EditText;

/**
 * Created by ovi on 23.05.2016.
 */
public interface IInputTextView {

    Editable getText();

    void setText(CharSequence text);

    void setHint(CharSequence text);

    EditText getEditView();

}
