package com.humanet.humanetcore.views.utils;

import android.graphics.Bitmap;

/**
 * Created by Владимир on 12.12.2014.
 *
 * Interface for list adapters for listener
 * updating event in images cache.
 */
public interface OnCacheUpdateListener {

    /**
     * Call when you have already load and process bitmap.
     * This method puts image in cache.
     *
     * @param key   The key for image. Normally it is absolute path to image file.
     * @param image The bitmap that we going to put in cache.
     */
    void onCacheUpdated(Object key, Bitmap image);

    /**
     * Returns bitmap from cache.
     *
     * @param key The key for image. Normally it is absolute path to image file.
     * @return The cached bitmap, if cache consist it, or null otherwise.
     */
    Bitmap getCachedBitmap(Object key);
}
