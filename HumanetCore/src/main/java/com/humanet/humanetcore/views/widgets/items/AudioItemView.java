package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.humanet.audio.AudioTrack;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.humanet.humanetcore.views.widgets.CircleLayout;

/**
 * Created by Владимир on 03.12.2014.
 */
public class AudioItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTextView;
    private AudioTrack mData;

    public AudioItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.item_circle, this);
        setOrientation(LinearLayout.VERTICAL);


        mImageView = (CircleImageView) findViewById(R.id.item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));

        mTextView = (TextView) findViewById(R.id.item_title);

        mImageView.setImageResource(R.drawable.icon_select_track);
    }

    public void setData(AudioTrack data) {
        mData = data;
        String title = mData.getTitle();
        if (title.lastIndexOf(".") > 0)
            title = title.substring(0, title.lastIndexOf("."));
        mTextView.setText(title);
    }
}
