package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by ovitali on 07.10.2015.
 */
public class ShadowImageButton extends ImageButton {

    int size = 0;
    float iconPadding = 0;

    private Drawable mIcon;

    private int mColor;
    private int mDisabledColor;
    private int mPressedColor;

    /**
     * Чтобы не перекрашивать белые картинки в нужный цвет с помощью фотошопа, применем
     * фильтр, тем самым получил нужный вид. Согласен, решение, в некотором роде, паршивое.
     * Но сейчас нет выбора.
     */
    public static int iconFilterColor = -1;

    public ShadowImageButton(Context context) {
        this(context, null);
        updateBackground();
    }

    public ShadowImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ShadowImageButton);
        size = (int) a.getDimension(R.styleable.ShadowImageButton_size, getResources().getDimension(R.dimen.round_button_size));
        iconPadding = (int) a.getDimension(R.styleable.ShadowImageButton_iconPadding, ConverterUtil.dpToPix(context, 10));
        mColor = a.getColor(R.styleable.ShadowImageButton_mainColor, getResources().getColor(R.color.colorPrimary));
        mPressedColor = a.getColor(R.styleable.ShadowImageButton_pressedColor, getResources().getColor(R.color.colorPrimaryDark));
        mDisabledColor = a.getColor(R.styleable.ShadowImageButton_disabledColor, getResources().getColor(R.color.colorPrimaryDisabled));
        a.recycle();


        if (isInEditMode()) {
            return;
        }

        updateBackground();
    }

    public void setIconPadding(float iconPadding) {
        this.iconPadding = iconPadding;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(size, size);
    }

    private void updateBackground() {
        final float strokeWidth = ConverterUtil.dpToPix(getResources(), 1);
        final float halfStrokeWidth = strokeWidth / 2f;


        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{-android.R.attr.state_enabled}, createCircleDrawable(mDisabledColor));
        stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, createCircleDrawable(mPressedColor));
        stateListDrawable.addState(new int[]{}, createCircleDrawable(mColor));


        Bitmap bitmap = ImageLoader.getInstance().loadImageSync("drawable://" + R.drawable.shadow_bg_normal, DisplayImageOptions.createSimple());

        float scale = (float) size / (float) bitmap.getWidth();

        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, size, size, true));

        Drawable[] layers = new Drawable[mIcon != null ? 4 : 3];

        layers[0] = bitmapDrawable;
        layers[1] = stateListDrawable;
        layers[2] = createOuterStrokeDrawable(strokeWidth);

        if (mIcon != null) {

            if (iconFilterColor != -1) {
                mIcon = mIcon.mutate();
                mIcon.setColorFilter(iconFilterColor, PorterDuff.Mode.SRC_IN);
            }

            layers[3] = mIcon;
        }

        LayerDrawable layerDrawable = new LayerDrawable(layers);

        layerDrawable.setLayerInset(1,
                (int) (27f * scale),
                (int) (17f * scale),
                (int) (27f * scale),
                (int) (35f * scale));

        layerDrawable.setLayerInset(2,
                (int) (27f * scale - halfStrokeWidth),
                (int) (17f * scale - halfStrokeWidth),
                (int) (27f * scale - halfStrokeWidth),
                (int) (35f * scale - halfStrokeWidth));

        if (mIcon != null) {
            layerDrawable.setLayerInset(3,
                    (int) (28f * scale + iconPadding),
                    (int) (18f * scale + iconPadding),
                    (int) (28f * scale + iconPadding),
                    (int) (36f * scale + iconPadding));
        }

        UiUtil.setBackgroundDrawable(this, layerDrawable);
    }

    private Drawable createOuterStrokeDrawable(float strokeWidth) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());

        final Paint paint = shapeDrawable.getPaint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLACK);
        paint.setAlpha(opacityToAlpha(0.02f));

        return shapeDrawable;
        //return new ColorDrawable(Color.TRANSPARENT);
    }

    private int opacityToAlpha(float opacity) {
        return (int) (255f * opacity);
    }

    private Drawable createCircleDrawable(int color) {
        ShapeDrawable fillDrawable = new ShapeDrawable(new OvalShape());

        final Paint paint = fillDrawable.getPaint();
        paint.setAntiAlias(true);
        paint.setColor(color);

        return fillDrawable;
    }

    public void setImageDrawable(Drawable drawable) {
        if (isInEditMode()) {
            super.setImageDrawable(drawable);
            return;
        }

        mIcon = drawable;
        if (size != 0)
            updateBackground();
    }

    public void setImageResource(int drawableId) {
        if (isInEditMode()) {
            super.setImageResource(drawableId);
            return;
        }

        mIcon = ResourcesCompat.getDrawable(getResources(), drawableId, null);
        if (size != 0)
            updateBackground();

    }


}
