package com.humanet.humanetcore.views.widgets.items;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.model.SimpleListItem;
import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by ovitali on 02.02.2015.
 */
public class BaseItemView extends CircleLayout.ItemWrapper {

    protected CircleImageView mImageView;
    private TextView mTextView;

    public BaseItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.item_circle, this);
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);

        mImageView = (CircleImageView) findViewById(R.id.item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mTextView = (TextView) findViewById(R.id.item_title);

        mImageView.getLayoutParams().width = App.IMAGE_SMALL_SIDE;
        mImageView.getLayoutParams().height = App.IMAGE_SMALL_SIDE;

        // mImageView.setPadding(padding, padding, padding, padding);
        mImageView.setBorderSize(App.IMAGE_BORDER);
    }

    public void setData(SimpleListItem data) {
        if (data != null) {
            String title = data.getTitle();
            mTextView.setText(title);

            ImageLoader.getInstance().displayImage(data.getImage(), mImageView, new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build());
        }
    }

    //-------------
}
