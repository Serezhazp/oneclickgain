package com.humanet.humanetcore.views.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.interfaces.EditableCirclePickerItem;
import com.humanet.humanetcore.views.widgets.square.SquareFrameLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Denis on 05.03.2015.
 */
public class CirclePickerView<T extends CirclePickerItem> extends SquareFrameLayout implements View.OnClickListener, TextWatcher {

    private static boolean sResizeItemOnSelection = false;

    public static void init(boolean resizeItemOnSelection) {
        sResizeItemOnSelection = resizeItemOnSelection;
    }

    public static boolean isResizeItemOnSelection() {
        return sResizeItemOnSelection;
    }

    private TextView mLabel;
    private Context mContext;

    private int mImageSize = Integer.MIN_VALUE;
    private int mCustomSize = Integer.MIN_VALUE;
    private double mRadius = Double.MIN_VALUE;
    private double mCustomRadius = Double.MIN_VALUE;
    private int mCurrentSelectedElement;

    private ArrayList<T> mOwnElements;

    private SparseArray<CirclePickerItem> mElements = new SparseArray<>();
    private OnPickListener mListener;

    public CirclePickerView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public CirclePickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public CirclePickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init() {
        mLabel = new EditText(mContext);
        mLabel.setBackgroundResource(android.R.color.transparent);
        mLabel.setTextColor(mContext.getResources().getColor(R.color.circlePickerColor));
        mLabel.setTypeface(mLabel.getTypeface(), Typeface.BOLD);
        mLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        mLabel.setLayoutParams(params);
        mLabel.setGravity(Gravity.CENTER);
        addView(mLabel);
        mLabel.setEnabled(false);
    }

    public void fill(ArrayList<T> elements, ArrayList<T> ownElements, int currentId, OnPickListener listener) {
        fill(elements, currentId, listener);

        mOwnElements = ownElements;
        highlightOwnElements(ownElements);
    }

    public void fill(ArrayList<T> elements, int currentId, OnPickListener listener) {
        fill(elements, 90, currentId, listener);
    }

    public void fill(ArrayList<T> elements, int startAngleOffset, int currentId, OnPickListener listener) {
        fill(elements, startAngleOffset, 360d / elements.size(), currentId, listener);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);
        int size = Math.min(w, h);
        super.onMeasure(size, size);
    }

    public void fill(
            final ArrayList<T> elements,
            final int startAngleOffset,
            final double angleOffset,
            final int currentId,
            final OnPickListener listener) {


        if (getWidth() == 0) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (getWidth() == 0)
                        return;

                    ViewTreeObserver obs = getViewTreeObserver();
                    obs.removeOnGlobalLayoutListener(this);

                    internalFill(elements, startAngleOffset, angleOffset, currentId, listener);
                }
            });
        } else {
            internalFill(elements, startAngleOffset, angleOffset, currentId, listener);
        }
    }

    public void internalFill(
            final ArrayList<T> elements,
            final int startAngleOffset,
            final double angleOffset,
            final int currentId,
            final OnPickListener listener) {

        mListener = listener;
        mCurrentSelectedElement = currentId;

        int w = getWidth();
        while (getChildCount() > 1) {
            removeViewAt(1);
        }

        mElements.clear();

        //resize smile bg, set size and mRadius according to orientation
        View imageView = getChildAt(0);
        FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) imageView.getLayoutParams();

        flp.width = FrameLayout.LayoutParams.MATCH_PARENT;
        flp.height = FrameLayout.LayoutParams.MATCH_PARENT;

        mRadius = sResizeItemOnSelection ? w * 3d / 8d : (w / 3d);

        mImageSize = w / (sResizeItemOnSelection ? 8 : 4);
        if (mRadius * Math.PI * 2 < (mImageSize) * elements.size()) {
            double newImageSize = (int) (mRadius * Math.PI * 2d / elements.size());
            mRadius += (mImageSize - newImageSize) / 2;
            mImageSize = (int) (newImageSize + (mImageSize - newImageSize) / 4);

            mImageSize *= 0.8d;
        }

        if (mCustomSize != Integer.MIN_VALUE)
            mImageSize = mCustomSize;
        if (mCustomRadius != Double.MIN_VALUE)
            mRadius = mCustomRadius;

        imageView.setLayoutParams(flp);

        //center X offset in smiles container
        int x0 = w / 2;
        //center Y offset in smiles container
        int y0 = w / 2;
        for (int i = 0; i < elements.size(); i++) {
            CirclePickerItem element = (CirclePickerItem) elements.get(i);

            double angle = (startAngleOffset + angleOffset * (i)) * Math.PI / 180;
            //top/left offset of smile center (relative to container center)
            double x = (double) Math.round(Math.cos(angle) * mRadius);
            double y = (double) Math.round(Math.sin(angle) * mRadius);

            // Inflater used to implement style with background
            ImageView elementImageView = (ImageView) LayoutInflater.from(mContext).inflate(R.layout.item_circle_picker, CirclePickerView.this, false);

            elementImageView.setOnClickListener(CirclePickerView.this);
            elementImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);


            //load smile either from file or from resources
            ImageLoader.getInstance().displayImage(element.getDrawable(), elementImageView, DisplayImageOptions.createSimple());


            flp = new FrameLayout.LayoutParams(mImageSize, mImageSize);
            flp.setMargins((int) (x0 + x - mImageSize / 2), (int) (y0 + y - mImageSize / 2), 0, 0);
            addView(elementImageView, flp);
            elementImageView.setId(element.getId());
            mElements.put(element.getId(), element);
        }

        if (mCurrentSelectedElement >= 0) {
            highlightElement(mCurrentSelectedElement);
        } else {
            mLabel.setText("");
        }
    }

    public CirclePickerItem getCurrentElement() {
        if (mCurrentSelectedElement >= 0) {
            return mElements.get(mCurrentSelectedElement);
        }
        return null;
    }

    private EditableCirclePickerItem getOtherOptionElement() {
        if (mCurrentSelectedElement >= 0) {
            CirclePickerItem item = mElements.get(mElements.size() - 1);
            if (item instanceof EditableCirclePickerItem) {
                return (EditableCirclePickerItem) item;
            }
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        highlightElement(id);
        if(mOwnElements != null) {
            highlightOwnElements(mOwnElements);
        }
        if (mListener != null) {
            mListener.onPick(this, mElements.get(id));
        }
    }

    private void highlightOwnElement(int elementId) {
        ImageView smileView = (ImageView) findViewById(elementId);
        smileView.setSelected(true);

        if (sResizeItemOnSelection) {
            FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) smileView.getLayoutParams();
            if (flp.width < mImageSize * 3 / 2) {
                flp.width = mImageSize * 3 / 2;
                flp.height = mImageSize * 3 / 2;
                flp.leftMargin -= mImageSize / 3 * 2;
                flp.topMargin -= mImageSize / 3 * 2;
                smileView.setLayoutParams(flp);
            }
        }
    }

    private void highlightElement(int elementId) {
        ImageView smileView;

        CirclePickerItem element = mElements.get(elementId);
        if (element instanceof EditableCirclePickerItem && ((EditableCirclePickerItem) element).isOtherOption()) {
            mLabel.setHint(R.string.other_option);
            mLabel.setEnabled(true);
            mLabel.setText(element.getTitle());
            mLabel.requestFocus();
            mLabel.addTextChangedListener(this);
        } else {
            mLabel.removeTextChangedListener(this);
            mLabel.setEnabled(false);
            mLabel.setText(element.getTitle());
        }

        //shrink prev smile
        if (mCurrentSelectedElement >= 0) {
            smileView = (ImageView) findViewById(mCurrentSelectedElement);
            smileView.setSelected(false);

            if (sResizeItemOnSelection) {
                FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) smileView.getLayoutParams();
                if (flp.width > mImageSize) {
                    flp.width = mImageSize;
                    flp.height = mImageSize;
                    flp.leftMargin += mImageSize / 2;
                    flp.topMargin += mImageSize / 2;
                    smileView.setLayoutParams(flp);
                }
            }
        }
        //
        smileView = (ImageView) findViewById(elementId);
        smileView.setSelected(true);

        if (sResizeItemOnSelection) {
            FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) smileView.getLayoutParams();
            if (flp.width < mImageSize * 2) {
                flp.width = mImageSize * 2;
                flp.height = mImageSize * 2;
                flp.leftMargin -= mImageSize / 2;
                flp.topMargin -= mImageSize / 2;
                smileView.setLayoutParams(flp);
                mCurrentSelectedElement = elementId;
            }
        }

        mCurrentSelectedElement = elementId;
    }

    private void highlightOwnElements(ArrayList<T> ownElements) {

        for(T element : ownElements) {
            highlightOwnElement(element.getId());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        EditableCirclePickerItem element = getOtherOptionElement();
        if (element != null) {
            element.setTitle(s.toString());
            if (mListener != null) {
                mListener.onPick(this, element);
            }
        }
    }

    public void setImageSize(final int imageSize) {
        mCustomSize = imageSize;
        mImageSize = imageSize;
    }

    public void setRadius(final double radius) {
        mCustomRadius = radius;
        mRadius = radius;
    }

    public interface OnPickListener {
        void onPick(View view, CirclePickerItem element);
    }
}