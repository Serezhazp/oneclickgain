package com.humanet.humanetcore.views.behaviour;

import android.content.Context;

import com.humanet.humanetcore.model.Category;

import java.util.List;

/**
 * oneClickGain
 * Created by ovitali on 17.12.2015.
 */
public interface CategoriesListView {

    void setCategories(List<Category> categoryList);

    Context getActivity();

}
