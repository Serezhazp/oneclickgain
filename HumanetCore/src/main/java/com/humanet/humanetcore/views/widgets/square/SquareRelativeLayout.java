package com.humanet.humanetcore.views.widgets.square;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by ovitali on 08.01.2015.
 */
public class SquareRelativeLayout extends RelativeLayout {

    private final double mScale = 1.0;


    public SquareRelativeLayout(Context context) {
        super(context);
    }

    public SquareRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float width = MeasureSpec.getSize(widthMeasureSpec);
        float height = (int) (width * mScale);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec((int) width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec((int) height, MeasureSpec.EXACTLY)
        );
    }

}
