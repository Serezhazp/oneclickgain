package com.humanet.humanetcore.modules;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.INavigatableActivity;
import com.humanet.humanetcore.views.behaviour.TextViewLinkMovementMethod;
import com.humanet.humanetcore.views.utils.ConverterUtil;

import java.util.Locale;

/**
 * Created by ovi on 12/30/15.
 */
public class BalanceUtil {

    public static boolean checkBalance = false;

    public static boolean isAllowedTransaction(final float balance, final Context context, int sum) {
        if (!checkBalance)
            return true;

        if (balance - sum < 0) {
            final TextView textView = new TextView(context);
            textView.setText(Html.fromHtml(context.getString(R.string.error_balance_not_enough)));
            int padding = (int) ConverterUtil.dpToPix(context, 10);
            textView.setPadding(padding, padding, padding, padding);

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setView(textView);

            final AlertDialog dialog = builder.show();
            textView.setMovementMethod(new TextViewLinkMovementMethod() {
                @Override
                public void onLinkClick(String url) {
                    ((INavigatableActivity) context).onNavigateByViewId(R.id.currency);
                    dialog.dismiss();
                }
            });


            return false;
        } else {
            return true;
        }
    }

    public static String balanceToString(float balance) {
        int h = (int) (balance * 100f - (float) ((int) balance * 100));
        if (Math.abs(h) == 0.0) {
            return String.valueOf((int) balance);
        } else {
            return String.format(Locale.getDefault(), "%.2f", balance);
        }
    }

}
