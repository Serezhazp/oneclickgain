package com.humanet.humanetcore.modules;

import android.view.View;
import android.widget.AdapterView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.database.CitiesRequest;
import com.humanet.humanetcore.api.requests.database.GetCountryRequest;
import com.humanet.humanetcore.model.City;
import com.humanet.humanetcore.model.Country;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.views.behaviour.SimpleOnItemSelectedListener;
import com.humanet.humanetcore.views.widgets.CityAutoCompleteView;
import com.humanet.humanetcore.views.widgets.CountrySpinnerView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LocationDataLoader {

    private SpiceManager mSpiceManager;
    private CityAutoCompleteView mCityAutoCompleteView;
    private CountrySpinnerView mCountrySpinnerView;

    private CitiesRequest mCitiesRequest;

    private int mUserCountryId = -1;
    private int mUserCityId = -1;

    private int mCountryId = -1;

    public LocationDataLoader(SpiceManager spiceManager, CityAutoCompleteView cityListView, CountrySpinnerView countryListView, int cityId) {
        mSpiceManager = spiceManager;
        mCityAutoCompleteView = cityListView;
        mCityAutoCompleteView.setLocationDataLoader(this);
        mCountrySpinnerView = countryListView;


        mCountrySpinnerView.setOnItemSelectedListener(new SimpleOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mCityAutoCompleteView.setVisibility(View.GONE);
                } else {
                    if (mUserCityId > 0) {
                        mCountryId = (int) id;
                        loadCities(AppUser.getInstance().get().getCity());
                    } else {
                        loadCities((int) id);
                    }
                }
            }
        });

        if (AppUser.getInstance().get() != null) { // on new profile User is null
            mUserCountryId = AppUser.getInstance().get().getCountryId();
        }

        mUserCityId = cityId;

        loadCountries();
    }

    private void loadCities(final int countryId) {
        if (countryId != mCountryId) {
            mCityAutoCompleteView.clear();
        }
        mCountryId = countryId;
        loadCities(countryId, null);
    }

    public void loadCities(String typed) {
        loadCities(mCountryId, typed);
    }

    private void loadCities(final int countryId, String typed) {
        if (mCitiesRequest != null) {
            mSpiceManager.cancel(mCitiesRequest);
        }
        mCitiesRequest = new CitiesRequest(countryId, typed);

        mSpiceManager.execute(mCitiesRequest, new SimpleRequestListener<City[]>() {
            @Override
            public void onRequestSuccess(City[] cities) {
                if (cities == null) {
                    return;
                }
                mCityAutoCompleteView.setCityList(Arrays.asList(cities));
                if (mUserCityId > -1) {
                    for (int i = 0; i < cities.length; i++) {
                        if (mUserCityId == cities[i].getId()) {
                            mCityAutoCompleteView.setSelectedPosition(i);
                            break;
                        }
                    }
                    mUserCityId = -1;
                }

                mCitiesRequest = null;
            }
        });


    }

    public void loadCountries() {
        mSpiceManager.execute(new GetCountryRequest(), null, DurationInMillis.ALWAYS_EXPIRED, new SimpleRequestListener<Country[]>() {
            @Override
            public void onRequestSuccess(final Country[] countries) {
                List<Country> countryList = new ArrayList<>(countries.length + 1);
                countryList.add(new Country(App.getInstance().getString(R.string.profile_private_info_choose_country)));
                Collections.addAll(countryList, countries);
                mCountrySpinnerView.setCountryList(countryList);

                if (mUserCountryId >= 0) {
                    for (int i = 0; i < countryList.size(); i++) {
                        int id = countryList.get(i).getId();
                        if (id == mUserCountryId) {
                            mCountrySpinnerView.setSelection(i);
                            break;
                        }
                    }
                } else {
                    String countryCode = PrefHelper.getStringPref(Constants.PREF.COUNTRY_CODE);
                    if (countryCode != null) {
                        for (int i = 0; i < countryList.size(); i++) {
                            Country country = countryList.get(i);
                            if (countryCode.equalsIgnoreCase(country.getCode())) {
                                mCountrySpinnerView.setSelection(i);
                                break;
                            }
                        }
                    }
                }

            }
        });

    }

    public void setItemColor(int color) {
        mCountrySpinnerView.setItemColor(color);
    }

    public int getSelectedCountryId() {
        return (int) mCountrySpinnerView.getSelectedItemId();
    }

    public String getSelectedCountryName() {
        return ((Country) mCountrySpinnerView.getSelectedItem()).getTitle();
    }

    public int getSelectedCityId() {
        return mCityAutoCompleteView.getSelectedItemId();
    }

    public String getSelectedCityName() {
        return mCityAutoCompleteView.getSelectedItem().getTitle();
    }

}
