package com.humanet.humanetcore.modules.media;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.humanet.humanetcore.LeakCannaryHelper;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;

import java.io.IOException;

/**
 * Created by ovitali on 25.11.2015.
 */
public class MediaFacade {

    private final VideoSurfaceView mSurfaceView;

    private IMediaEventListener mMediaEventListener;

    protected MediaPlayer mediaPlayer;

    protected String videoUrl;

    private boolean mLooping;

    private boolean mIsNeedToStart;

    private volatile boolean mIsPreparing;

    public MediaFacade(final VideoSurfaceView surfaceView) {
        mSurfaceView = surfaceView;
    }

    public void setMediaEventListener(final IMediaEventListener mediaEventListener) {
        mMediaEventListener = mediaEventListener;
    }

    public void closeMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.setOnPreparedListener(null);
            mediaPlayer.setOnCompletionListener(null);

            mediaPlayer.reset();
            mediaPlayer.release();

            mediaPlayer = null;
        }

        mIsPreparing = false;
    }

    public void replay() {
        play(videoUrl);
    }

    public void play(String videoUrl, boolean isNeedToStart) {
        mIsNeedToStart = isNeedToStart;

        closeMediaPlayer();

        if (mSurfaceView.isDestroyed())
            return;

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setLooping(mLooping);
        this.videoUrl = videoUrl;

        mediaPlayer.setSurface(mSurfaceView.getHolder().getSurface());
        try {
            mediaPlayer.setDataSource(videoUrl);
            mediaPlayer.setOnPreparedListener(new OnPreparedListener());
            mediaPlayer.setOnCompletionListener(new OnCompletionListener());
            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    Crashlytics.logException(new Throwable("MediaPlayer: Couldn't play video is url " + videoUrl));
                    return false;
                }
            });
            mediaPlayer.prepareAsync();

            mIsPreparing = true;
        } catch (IOException e) {
            Log.e(MediaFacade.class.getName(), videoUrl, e);
        }

        LeakCannaryHelper.watch(this);
    }

    public void play(String videoUrl) {
        play(videoUrl, true);
    }

    private class OnPreparedListener implements MediaPlayer.OnPreparedListener {

        @Override
        public void onPrepared(final MediaPlayer mp) {
            View parent = (View) mSurfaceView.getParent();
            UiUtil.resizedToFitParent(
                    mSurfaceView,
                    parent,
                    mp.getVideoWidth(),
                    mp.getVideoHeight()
            );

            if(mIsNeedToStart) {
                mp.start();
                if (mMediaEventListener != null) {
                    mMediaEventListener.onVideoPlayingStarted(MediaFacade.this, mp);
                }
            }

            mIsPreparing = false;

        }
    }

    public void setLooping(boolean looping) {
        mLooping = looping;
    }

    private class OnCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            if (!mp.isLooping()) {
                mp.setOnCompletionListener(null);
                closeMediaPlayer();

                if (mMediaEventListener != null) {
                    mMediaEventListener.onVideoPlayingCompleted(MediaFacade.this, mp);
                }

                mIsPreparing = false;
            }
        }
    }

    public boolean isPlaying() {
        return mediaPlayer != null && (mediaPlayer.isPlaying() || mIsPreparing);
    }


}
