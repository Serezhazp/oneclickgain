package com.humanet.humanetcore;

import android.support.v4.app.Fragment;

import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.octo.android.robospice.SpiceService;

/**
 * Created by ovi on 24.05.2016.
 */
public class NavigationManager {


    private static Delegate sDelegate;

    public static void init(Delegate delegate) {
        sDelegate = delegate;
    }

    public static Class getPlayFlowActivityClass() {
        return sDelegate.getPlayFlowActivityClass();
    }

    public static Class getCommentsActivityClass() {
        return sDelegate.getCommentsActivityClass();
    }

    public static Class getAgreementActivityClass() {
        return sDelegate.getAgreementActivityClass();
    }

    public static Class getSearchActivityClass() {
        return sDelegate.getSearchActivityClass();
    }

    public static Class getInputActivityClass() {
        return sDelegate.getInputActivityClass();
    }

    public static Class getTokenActivityClass() {
        return sDelegate.getTokenActivityClass();
    }

    public static Class getContactsActivityClass() {
        return sDelegate.getContactsActivityClass();
    }

    public static Class getEditProfileActivityClass() {
        return sDelegate.getEditProfileActivityClass();
    }

    public static Class getCaptureActivityClass() {
        return sDelegate.getCaptureActivityClass();
    }

    public static Class getPushActivityClass() {
        return sDelegate.getPushActivityClass();
    }

    public static Class<? extends SpiceService> getSpiceServiceClass() {
        return sDelegate.getSpiceServiceClass();
    }

    public static Class<? extends BaseViewProfileFragment> getViewProfileFragmentClass() {
        return sDelegate.getViewProfileFragmentClass();
    }

    public static Class<? extends Fragment> getVideoDescriptionFragmentClass() {
        return sDelegate.getVideoDescriptionFragmentClass();
    }

    public interface Delegate {
        Class getPlayFlowActivityClass();

        Class getCommentsActivityClass();

        Class getAgreementActivityClass();

        Class getSearchActivityClass();

        Class getInputActivityClass();

        Class getTokenActivityClass();

        Class getContactsActivityClass();

        Class getEditProfileActivityClass();

        Class getCaptureActivityClass();

        Class getPushActivityClass();

        Class<? extends SpiceService> getSpiceServiceClass();

        Class<? extends BaseViewProfileFragment> getViewProfileFragmentClass();

        Class<? extends Fragment> getVideoDescriptionFragmentClass();

    }

}
