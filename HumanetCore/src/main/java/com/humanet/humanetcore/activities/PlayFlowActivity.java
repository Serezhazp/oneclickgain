package com.humanet.humanetcore.activities;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.ImageView;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.Constants.ACTION;
import com.humanet.humanetcore.Constants.PARAMS;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.presenters.video.VideoCacherPresenter;
import com.humanet.humanetcore.presenters.video.VideoListLoaderPresenter;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.IVideoCacheView;
import com.humanet.humanetcore.views.IVideoListView;
import com.humanet.humanetcore.views.behaviour.SimpleSurfaceHolderCallback;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.VideoSurfaceView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class PlayFlowActivity extends BaseActivity implements View.OnClickListener, IVideoListView, IVideoCacheView {

    public static void startNewInstance(Context context, Category category, int currentVideoPosition, int replyId, int petId, int userId, String title, ArrayList<Video> list, long screenId) {
        Intent intent = new Intent(context, NavigationManager.getPlayFlowActivityClass());
        intent.putExtra(PARAMS.VIDEO_LIST, list);
        intent.putExtra(PARAMS.CATEGORY, category);
        intent.putExtra(PARAMS.REPLY_ID, replyId);
        intent.putExtra(PARAMS.NURSLING_ID, petId);
        intent.putExtra(PARAMS.USER_ID, userId);
        intent.putExtra(PARAMS.TITLE, title);
        intent.putExtra(PARAMS.ACTION, ACTION.PLAY_FLOW);
        intent.putExtra(PARAMS.CURRENT_VIDEO, currentVideoPosition);
        intent.putExtra(PARAMS.TYPE, screenId);
        context.startActivity(intent);
    }

    public static void startNewInstance(Context context, Video video) {
        Intent intent = new Intent(context, NavigationManager.getPlayFlowActivityClass());
        intent.putExtra(PARAMS.VIDEO_LIST, video);
        intent.putExtra(PARAMS.ACTION, ACTION.PLAY_FLOW_SINGLE_VIDEO);
        context.startActivity(intent);
    }

    private ArrayList<Video> mVideos;
    private int mCurrentPosition = 0;

    private VideoSurfaceView mSurfaceView;
    private ImageView mVideoPreview;
    private YellowProgressBar mProgressBar;

    private MediaPlayer mPlayer;

    private int mCachedPosition;
    private boolean mIsPlaying;

    private View mFadingView;

    private int mCurrentAction;

    private boolean mIsLastPage;

    private VideoCacherPresenter mVideoCacherPresenter;
    private VideoListLoaderPresenter mVideoListLoaderPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_flow);

        mVideoCacherPresenter = new VideoCacherPresenter();
        mVideoCacherPresenter.setVideoCacheView(this);
        mVideoListLoaderPresenter = new VideoListLoaderPresenter();
        mVideoListLoaderPresenter.setVideoListView(this);

        CircleView circleView = (CircleView) findViewById(R.id.video_view);

        mSurfaceView = circleView.getSurfaceView();
        mVideoPreview = circleView.getPreviewView();
        mProgressBar = circleView.getProgressBar();

        //mFadingView = findViewById(R.id.fading_view);
        //TODO create this view in layout directly
        mFadingView = new View(this);
        mFadingView.setVisibility(View.INVISIBLE);
        mFadingView.setBackgroundColor(getResources().getColor(R.color.screen_background));
        circleView.insertView(mFadingView);


        mCurrentAction = getIntent().getIntExtra(PARAMS.ACTION, 0);
        if (mCurrentAction == ACTION.PLAY_FLOW) {
            Category category = (Category) getIntent().getSerializableExtra(PARAMS.CATEGORY);
            if (category != null)
                mVideoListLoaderPresenter.setFlowRequestParams(category.getParams());
            mVideoListLoaderPresenter.setReplayId(getIntent().getIntExtra(PARAMS.REPLY_ID, 0));

            mVideos = (ArrayList<Video>) getIntent().getSerializableExtra(PARAMS.VIDEO_LIST);

            mCurrentPosition = mCachedPosition = getIntent().getIntExtra(PARAMS.CURRENT_VIDEO, 0);
        } else {
            Video video = (Video) getIntent().getSerializableExtra(PARAMS.VIDEO_LIST);
            mVideos = new ArrayList<>(1);
            mVideos.add(video);
        }

        mFadingView.setOnClickListener(this);
        mSurfaceView.setOnClickListener(this);

    }


    public void nextStep() {
        if (mCurrentAction == ACTION.PLAY_FLOW) {
            play();
        } else {
            finish();
        }
    }

    private void loadNextData() {
        if (!mIsLastPage && mCurrentPosition >= mVideos.size() - 2) {
            if (mCurrentAction == ACTION.PLAY_FLOW)
                loadNextPage();
        }

        mCachedPosition++;
        if (mCachedPosition < mVideos.size() - 1)
            mVideoCacherPresenter.runCacher(mVideos.get(mCachedPosition).getUrl());
    }

    private boolean play() {
        if (!mSurfaceView.isCreated() || !mSurfaceView.getHolder().getSurface().isValid()) {
            mIsPlaying = false;
            Log.e("PLAY FLOW", "surface isn't alive");
            return false;
        }

        if (mCurrentPosition >= mVideos.size()) {
            onBackPressed();
            mIsPlaying = false;
            return false;
        }

        String videoUrl = mVideos.get(mCurrentPosition).getUrl();
        videoUrl = FilenameUtils.getName(videoUrl);
        File file = new File(FilePathHelper.getVideoCacheDirectory(), videoUrl);

        if (file.exists() && startPlaying(file)) {
            mIsPlaying = true;
        } else {
            reloadVideo();
            mIsPlaying = false;
        }

        return mIsPlaying;
    }

    private void reloadVideo() {
        Video video = mVideos.get(mCurrentPosition);
        ImageLoader.getInstance().displayImage(video.getThumbUrl(), mVideoPreview);
        mProgressBar.setVisibility(View.VISIBLE);
        mVideoPreview.setVisibility(View.VISIBLE);
        mVideoCacherPresenter.runCacher(video.getUrl());
        mFadingView.setVisibility(View.INVISIBLE);
    }

    private boolean startPlaying(File file) {
        closePlayer();

        mPlayer = new MediaPlayer();
        mPlayer.setSurface(mSurfaceView.getHolder().getSurface());

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mCurrentPosition++;
                mIsPlaying = false;
                closePlayer();
                nextStep();
            }
        });

        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(final MediaPlayer mp, int what, int extra) {
                Log.e("PLAY FLOW", "error " + what + ", extra:" + extra);

                mIsPlaying = false;
                closePlayer();

                mCurrentPosition++;
                nextStep();

                return true;
            }
        });

        try {
            mPlayer.setDataSource(file.getAbsolutePath());
            mPlayer.prepare();
            mPlayer.start();

            mFadingView.setVisibility(View.VISIBLE);
            createFadingAnimation();

            mVideoPreview.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);

            loadNextData();
        } catch (Exception ex) {
            Log.e("PLAY FLOW", "exception " + ex.toString());
            Log.e("PLAY FLOW", "exception " + file.getAbsolutePath());
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        mVideoCacherPresenter.setSpiceManager(getSpiceManager());
        mVideoListLoaderPresenter.setSpiceManager(getSpiceManager());

        Video video = mVideos.get(mCachedPosition);
        ImageLoader.getInstance().displayImage(video.getThumbUrl(), mVideoPreview);

        if (mSurfaceView.isCreated()) {
            if (!play()) {
                openFlowScreen();
            }
        } else {
            mSurfaceView.getHolder().addCallback(new SimpleSurfaceHolderCallback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {
                    mSurfaceView.getHolder().removeCallback(this);
                    play();
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        mIsPlaying = false;
        closePlayer();
    }


    private void closePlayer() {
        if (mPlayer != null) {
            mPlayer.setOnPreparedListener(null);
            mPlayer.setOnCompletionListener(null);

            mPlayer.reset();
            mPlayer.release();

            mPlayer = null;
        }
    }


    @Override
    public void publishCachingProgress(float progress) {
        if (!mIsPlaying) {
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setProgress((int) progress);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void publishCachingComplete(boolean success) {
        if (success && !mIsPlaying) {
            Log.i("PLAY FLOW", "publishCachingComplete play:" + play());
            // play();
        }
    }

    public void loadNextPage() {
        mVideoListLoaderPresenter.loadPage();
    }

    @Override
    public long getScreenId() {
        return getIntent().getLongExtra(PARAMS.TYPE, 0);
    }

    @Override
    public long getLastVideoId() {
        if (mVideos != null && mVideos.size() > 0)
            return mVideos.get(mVideos.size() - 1).getVideoId();
        return 0;
    }

    @Override
    public void addVideos(List<Video> videoList) {
        int count = videoList == null ? 0 : videoList.size();

        mIsLastPage = count < Constants.LIMIT;

        if (videoList != null)
            mVideos.addAll(videoList);
    }

    private void openFlowScreen() {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVideoCacherPresenter.stopCache();
    }

    @Override
    public void onClick(View v) {
        openFlowScreen();
    }


    private void createFadingAnimation() {
        int duration = mPlayer.getDuration();

        int stepDuration;
        if (duration <= 2000) {
            stepDuration = 200;
        } else if (duration <= 4000) {
            stepDuration = 400;
        } else {
            stepDuration = 600;
        }

        Handler handler = new Handler();
        int steps = stepDuration / 40;
        for (int i = 1; i < steps + 1; i++) {
            float delta = 1.0f / ((float) steps) * i;
            handler.postDelayed(new VolumeChangeRunnable(delta), i * 40);
        }

        for (int i = 1; i < steps + 1; i++) {
            float delta = 1.0f / ((float) steps) * ((float) (steps - i));
            int d = duration - stepDuration + i * 40;
            handler.postDelayed(new VolumeChangeRunnable(delta), d);
        }
    }

    private class VolumeChangeRunnable implements Runnable {

        private float mVolume;


        public VolumeChangeRunnable(float volume) {
            mVolume = volume;
        }

        @Override
        public void run() {
            if (mPlayer == null)
                return;

            mPlayer.setVolume(mVolume, mVolume);
            mFadingView.setAlpha(1.0f - mVolume);
        }
    }
}
