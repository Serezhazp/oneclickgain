package com.humanet.humanetcore.activities;

import android.os.Bundle;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.profile.TokenFragment;
import com.humanet.humanetcore.utils.ToolbarHelper;

/**
 * Created by ovitali on 24.09.2015.
 */
public abstract class TokenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base_fragment_container);
        toolbar = ToolbarHelper.createToolbar(this);

        startFragment(new TokenFragment(), false);

    }
}
