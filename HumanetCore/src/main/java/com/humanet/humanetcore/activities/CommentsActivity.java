package com.humanet.humanetcore.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseVideoManageActivity;
import com.humanet.humanetcore.events.OnViewProfileListener;
import com.humanet.humanetcore.fragments.CommentsFragment;
import com.humanet.humanetcore.fragments.profile.ProfileFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.ToolbarHelper;
import com.humanet.humanetcore.views.dialogs.MoreDialog;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;

public abstract class CommentsActivity extends BaseVideoManageActivity implements
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnCommentListener,
        VideoItemView.OnMoreListener, OnViewProfileListener {

    public static void startNewInstance(Context context, Video video, Category category, VideoType videoType) {
        Intent intent = new Intent(context, NavigationManager.getCommentsActivityClass());
        intent.putExtra("video", video);
        intent.putExtra("category", category);
        intent.putExtra("videoType", videoType);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);
        toolbar = ToolbarHelper.createToolbar(this);

        Video video = (Video) getIntent().getSerializableExtra("video");
        Category category = (Category) getIntent().getSerializableExtra("category");
        VideoType videoType = (VideoType) getIntent().getSerializableExtra("videoType");
        startFragment(CommentsFragment.newInstance(video, category, videoType), false);
    }


    public void onMore(Video video, Video videoParent) {
        new MoreDialog(this, video, videoParent).show();
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }


    @Override
    public void onViewProfile(int uid) {
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    @Override
    public void onShowComment(Video video, Category category, VideoType videoType) {
        startFragment(CommentsFragment.newInstance(video, category, videoType), false);
    }
}
