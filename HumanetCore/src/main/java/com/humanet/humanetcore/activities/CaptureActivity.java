package com.humanet.humanetcore.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.github.hiteshsondhi88.libffmpeg.FFmpegSync;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.humanet.audio.MoveFilesJob;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.BaseFilter;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.GrabFramesEvent;
import com.humanet.humanetcore.fragments.capture.AudioFragment;
import com.humanet.humanetcore.fragments.capture.FilterFragment;
import com.humanet.humanetcore.fragments.capture.FramePreviewFragment;
import com.humanet.humanetcore.fragments.capture.VideoDescriptionFragment;
import com.humanet.humanetcore.fragments.capture.VideoPreviewFragment;
import com.humanet.humanetcore.fragments.capture.VideoRecordFragment;
import com.humanet.humanetcore.jobs.SaveVideoToUploadJob;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.utils.LocationWatcher;
import com.humanet.humanetcore.utils.ToolbarHelper;
import com.humanet.humanetcore.views.dialogs.MissedPermissionDialog;
import com.humanet.video.ProcessCapturedVideoRunnable;
import com.tbruyelle.rxpermissions.RxPermissions;

import org.apache.commons.io.IOUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ovitali on 20.08.2015.
 */
public class CaptureActivity extends BaseActivity implements
        LocationWatcher.GetLocationListener,
        VideoRecordFragment.OnVideoRecordCompleteListener,
        VideoPreviewFragment.OnVideoCommandSelected,
        VideoDescriptionFragment.OnEnterDescriptionListener {

    private boolean mIsPetAvatar;

    public static void startNewInstance(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, NavigationManager.getCaptureActivityClass());
        activity.startActivityForResult(intent, requestCode);
    }

    public static Intent createNewInstance(Context context, VideoType videoType) {
        Intent intent = new Intent(context, NavigationManager.getCaptureActivityClass());
        intent.putExtra("type", videoType);

        return intent;
    }

    public static void startNewInstance(Context context, Video replyForVideo) {
        Intent intent = new Intent(context, NavigationManager.getCaptureActivityClass());
        intent.putExtra("reply_video", replyForVideo);
        context.startActivity(intent);
    }

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1972;

    private LocationWatcher mLocationWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            NewVideoInfo.init();
        } else {
            VideoInfo video = (VideoInfo) savedInstanceState.get("video");
            NewVideoInfo.set(video);
        }

        setContentView(R.layout.activity_base_fragment_container);

        toolbar = ToolbarHelper.createToolbar(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Bundle args = getIntent().getExtras();
        if (args != null) {
            if (args.containsKey("reply_video")) {
                Video replyVideo = (Video) args.getSerializable("reply_video");
                assert replyVideo != null;
                NewVideoInfo.get().setReplyToVideoId(replyVideo.getVideoId());
                NewVideoInfo.get().setCategoryId(replyVideo.getCategoryId());
                if (replyVideo.getType() > 0) {
                    NewVideoInfo.get().setVideoType(VideoType.getByCode(replyVideo.getType(), replyVideo.getParams()));
                } else {
                    NewVideoInfo.get().setVideoType(VideoType.VLOG);
                }
            }

            if(args.containsKey("update_avatar")) {
                if(args.getInt("update_avatar") == VideoInfo.PET_AVATAR) {
                    NewVideoInfo.get().setVideoType(VideoType.PET_ALL);
                    //NewVideoInfo.get().setPetId(args.getInt("pet_id"));
                    //NewVideoInfo.get().setAvatarType(VideoInfo.PET_AVATAR);
                    mIsPetAvatar = true;
                }
            }

            if (args.containsKey("type")) {
                VideoType type = (VideoType) args.getSerializable("type");
                assert type != null;
                NewVideoInfo.get().setVideoType(type);
            }
        }


        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result == ConnectionResult.SUCCESS) {
            getLocation();
        } else {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            startRecording();
        }

        checkAndCopyAudioFiles();
    }

    private void startRecording() {
        if (!isAndroidEmulator())
            launchRecording();
        else
            skipRecordingOnEmulator();
    }

    private void getLocation() {
        Handler handler = new Handler(getMainLooper());
        RxPermissions.getInstance(this)
                .request(Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) {
                        mLocationWatcher = LocationWatcher.create(this, NewVideoInfo.get());
                        mLocationWatcher.setGetLocationListener(this);
                    }

                    handler.postDelayed(() -> {
                        RxPermissions.getInstance(CaptureActivity.this)
                                .request(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
                                .subscribe(cameraGranted -> {
                                            if (cameraGranted) { // Always true pre-M
                                                handler.postDelayed(this::startRecording, 100);
                                            } else {
                                                MissedPermissionDialog.show(this, ((dialogInterface, i) -> finish()));
                                            }
                                        }
                                );
                    }, 100);

                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLAY_SERVICES_RESOLUTION_REQUEST && resultCode == Activity.RESULT_OK)
            getLocation();
    }

    protected void launchRecording() {
        startFragment(new VideoRecordFragment(), false);
    }

    @Override
    public void onVideoRecordComplete(String videoPath, int cameraId) {
        NewVideoInfo.get().setAudioPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        NewVideoInfo.get().setFilter(new BaseFilter());
        NewVideoInfo.get().setAudioApplied(NewVideoInfo.get().getAudioPath());
        NewVideoInfo.get().setImageFilterApplied(null);
        NewVideoInfo.get().setVideoFilterApplied(null);
        NewVideoInfo.get().setCameraId(cameraId);

        int framesCount = FilePathHelper.getVideoFrameFolder().listFiles().length;
        NewVideoInfo.get().setOriginalImagePath(FilePathHelper.getVideoFrameImagePath(Math.max(framesCount / 2 - 1, 0)));
        NewVideoInfo.get().setOriginalVideoPath(videoPath);
        VideoPreviewFragment.VideoProcessTask.getInstance().mPreviewBitmap = null;

        NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());
        NewVideoInfo.grabFramesPack();

        startFragment(VideoPreviewFragment.newInstance(0), true);
    }

    @Override
    public void onOpenRecorder(int step) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.container, new VideoRecordFragment(), VideoRecordFragment.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onOpenFramePreview() {
        startFragment(FramePreviewFragment.newInstance(), true);
    }

    @Override
    public void onOpenFilter() {
        startFragment(FilterFragment.newInstance(), true);
    }

    @Override
    public void onOpenTrack() {
        startFragment(AudioFragment.newInstance(), true);
    }

    @Override
    public boolean onComplete(int action) {
        if (NewVideoInfo.get().getVideoType() == null || NewVideoInfo.get().getSmileId() > 0 || mIsPetAvatar) {
            saveVideoAndBack();
            return true;
        } else {
            try {
                Fragment fragment = NavigationManager.getVideoDescriptionFragmentClass().getConstructor().newInstance();
                Bundle bundle = new Bundle();
                if(!(NewVideoInfo.get().getVideoType() == VideoType.CROWD_PET_ASK || NewVideoInfo.get().getVideoType() == VideoType.CROWD_PET_GIVE)) {
                    bundle.putInt("is_need_pet_description", 1);
                } else {
                    bundle.putInt("is_need_pet_description", 0);
                }
                fragment.setArguments(bundle);
                startFragment(fragment, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }


    protected void trackAnalytics(VideoType videoType) {
        if (videoType != null) {
            switch (videoType) {
                case MARKET_SHARE:
                case MARKET_SELL:
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.MARKET_ADD_ITEM);
                    break;

                default:
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.VIDEO_RECORDING);
            }
        } else {
            AnalyticsHelper.trackEvent(this, AnalyticsHelper.VIDEO_RECORDING);
        }
    }

    protected void saveVideoDataLocal(VideoInfo videoInfo) {
        @VideoInfo.AvatarType
        int isNewAvatar = getIntent() != null && getIntent().hasExtra("update_avatar")
                ? getIntent().getIntExtra("update_avatar", VideoInfo.NONE)
                : VideoInfo.NONE;
        videoInfo.setAvatarType(isNewAvatar);

        getSpiceManager().removeAllDataFromCache();

        SaveVideoToUploadJob saveVideoToUploadJob = new SaveVideoToUploadJob(videoInfo);
        saveVideoToUploadJob.setLocationWatcher(mLocationWatcher);
        saveVideoToUploadJob.start();
    }

    protected void successfulBack(VideoInfo videoInfo) {
        Intent intent = new Intent();
        intent.setData(Uri.fromFile(new File(NewVideoInfo.get().getImagePath())));
        if (videoInfo.getCategory() != 0)
            intent.putExtra(Constants.PARAMS.CATEGORY, videoInfo.getCategory());
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void saveVideoAndBack() {
        VideoInfo videoInfo = NewVideoInfo.get().clone();

        trackAnalytics(videoInfo.getVideoType());
        saveVideoDataLocal(videoInfo);
        successfulBack(videoInfo);
    }

    @Override
    public void onDescriptionEntered() {
        saveVideoAndBack();
    }


    private void checkAndCopyAudioFiles() {
        MoveFilesJob.run(this, FilePathHelper.getAudioDirectory());
    }

    @Override
    protected void onStop() {
        super.onStop();
        FFmpegSync.getInstance(getApplicationContext()).close();
    }

    @Override
    public final void onGotLocation() {
        mLocationWatcher = null;
    }

    /**
     * skip recording on emulator. Frames will grab from tmp.mp4.
     * <u>be sure you have tmp.mp4 in your assets folder</u>
     */
    private void skipRecordingOnEmulator() {
        NewVideoInfo.get().setSizes(480, 480);

        FilterController.init(getApplicationContext(), 1);
        EventBus.getDefault().register(CaptureActivity.this);
        new Thread() {
            @Override
            public void run() {
                try {
                    IOUtils.copy(getAssets().open("tmp.mp4"), new FileOutputStream(FilePathHelper.getVideoTmpFile()));

                    new Thread(new ProcessCapturedVideoRunnable.Builder(App.getInstance())
                            .setCameraId(1)
                            .setCameraSideSize(480)
                            .setFramesFolder(FilePathHelper.getVideoFrameFolder())
                            .setSourceVideoFilePath(FilePathHelper.getVideoTmpFile())
                            .setTmpVideoFilePath(FilePathHelper.getVideoTmpFile2())
                            .setAudioStreamPath(FilePathHelper.getAudioStreamFile())
                            .setListener(new ProcessCapturedVideoRunnable.OnVideoProcessedListener() {
                                @Override
                                public void onVideoProcessed(boolean success) {
                                    EventBus.getDefault().post(new GrabFramesEvent(success));
                                }
                            })
                            .build())
                            .start();


                } catch (IOException ex) {
                    throw new RuntimeException("be sure you have tmp.mp4 in your assets folder");
                }
            }
        }.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("video", NewVideoInfo.get());
    }

    // event throws from skipRecordingOnEmulator()
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GrabFramesEvent event) {
        onVideoRecordComplete(FilePathHelper.getVideoTmpFile().getAbsolutePath(), 1);

        EventBus.getDefault().unregister(this);
    }

    public static boolean isAndroidEmulator() {
        String model = Build.MODEL;
        String product = Build.PRODUCT;
        boolean isEmulator = false;
        if (product != null) {
            isEmulator = product.equals("sdk") || product.contains("vbox") || product.contains("sdk_");
        }
        Log.d(CaptureActivity.class.getSimpleName(), "isEmulator=" + isEmulator);
        return isEmulator;
    }
}
