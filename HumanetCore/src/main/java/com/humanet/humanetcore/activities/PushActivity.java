package com.humanet.humanetcore.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.GetFlowRequest;
import com.humanet.humanetcore.api.requests.vote.GetVotesListRequest;
import com.humanet.humanetcore.api.response.video.FlowResponse;
import com.humanet.humanetcore.api.response.vote.VoteListsResponse;
import com.humanet.humanetcore.fragments.vote.VoteFragment;
import com.humanet.humanetcore.fragments.vote.VoteTabFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.NotificationType;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.model.vote.Vote;

/**
 * Created by serezha on 06.09.16.
 */
public class PushActivity extends BaseActivity {

	private final String TAG = "PushActivity";

	public final static String TYPE = "type";
	public final static String OBJECT_ID = "object_id";

	private int mType;
	protected int mObjectId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.i(TAG, "onCreate");

		setContentView(R.layout.activity_base_fragment_container);

		if(getIntent().getExtras().containsKey(TYPE))
			mType = getIntent().getExtras().getInt(TYPE);

		if(getIntent().getExtras().containsKey(OBJECT_ID))
			mObjectId = getIntent().getExtras().getInt(OBJECT_ID);

		startNeededScreen(NotificationType.fromInt(mType));

	}

	private void startNeededScreen(NotificationType type) {

		Log.i(TAG, "startNeededScreen");

		switch (type) {
			case NOT_FILLED_INFO:
				showProfile();
				break;
			case NEW_COMMENT:
				showComments();
				break;
			case NEW_VIDEO_ANSWER:
				showVideoAnswers();
				break;
			case ADD_YOUR_VIDEO:
				showCaptureActivity();
				break;
			case NEW_VOTE:
				showVote();
				break;
			case NEW_CONTENT:
				//TODO: Only for OneClickGain
				showGame();
				break;
			default:
				Toast.makeText(this, TAG, Toast.LENGTH_SHORT).show();
				break;
		}
	}

	private void showProfile() {
		Intent intent = new Intent(this, NavigationManager.getEditProfileActivityClass());
		startActivity(intent);
	}

	private void showComments() {
		GetFlowRequest flowRequest = new GetFlowRequest();
		flowRequest.setUserId(AppUser.getInstance().getUid());
		getSpiceManager().execute(flowRequest, new SimpleRequestListener<FlowResponse>() {
			@Override
			public void onRequestSuccess(FlowResponse flowResponse) {
				if(flowResponse.getFlow() != null) {
					for (Video video : flowResponse.getFlow()) {
						//TODO: Implement getting video by id method on server side
						CommentsActivity.startNewInstance(PushActivity.this, video, new Category(video.getCategoryId()), VideoType.getById(video.getType()));
						return;
						/*if(video.getVideoId() == mObjectId) {
							CommentsActivity.startNewInstance(PushActivity.this, video, new Category(video.getCategoryId()), VideoType.getById(video.getType()));
							return;
						}*/
					}
				}
			}
		});
	}

	private void showVideoAnswers() {
		GetFlowRequest flowRequest = new GetFlowRequest();
		flowRequest.setReplyId(mObjectId);
		getSpiceManager().execute(flowRequest, new SimpleRequestListener<FlowResponse>() {
			@Override
			public void onRequestSuccess(FlowResponse flowResponse) {
				if(flowResponse.getFlow() != null) {
					Intent intent = new Intent(PushActivity.this, NavigationManager.getPlayFlowActivityClass());
					intent.putExtra(Constants.PARAMS.VIDEO_LIST, flowResponse.getFlow().get(0));
					intent.putExtra(Constants.PARAMS.ACTION, Constants.ACTION.PLAY_FLOW_SINGLE_VIDEO);
					startActivity(intent);
				}
			}
		});
	}

	private void showVote() {
		GetVotesListRequest request = new GetVotesListRequest(VoteTabFragment.NOW, VoteTabFragment.getVoteTypeId(VoteTabFragment.NOW));
		getSpiceManager().execute(request, new SimpleRequestListener<VoteListsResponse>() {
			@Override
			public void onRequestSuccess(VoteListsResponse voteListsResponse) {
				if (voteListsResponse.getVotes() != null) {
					for (Vote vote : voteListsResponse.getVotes()) {
						if (vote.getId() == mObjectId) {
							VoteFragment fragment = VoteFragment.newInstance(vote, false);
							startFragment(fragment, false);
						}
					}
				}
			}
		});
	}

	private void showCaptureActivity() {
		Intent intent = new Intent(this, NavigationManager.getCaptureActivityClass());
		startActivity(intent);
	}

	protected void showGame() {
		Log.i("GAME:", "Parent show game, id: " + mObjectId);
	}
}
