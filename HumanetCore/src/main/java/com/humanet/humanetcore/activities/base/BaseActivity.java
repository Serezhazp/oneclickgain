package com.humanet.humanetcore.activities.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.LeakCannaryHelper;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.events.ServerErrorEvent;
import com.humanet.humanetcore.fcm.AppFirebaseInstanceIdService;
import com.humanet.humanetcore.interfaces.SpiceContext;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.humanet.humanetcore.views.dialogs.ErrorDialog;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * Created by ovitaliy on 08.01.2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements SpiceContext {
    ErrorDialog mErrorDialog;

    protected Toolbar toolbar;


    private SpiceManager mSpiceManager = new SpiceManager(NavigationManager.getSpiceServiceClass());
    private NetworkChangeReceiver mNetworkChangeReceiver = new NetworkChangeReceiver();

    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    public void startFragment(Fragment f, boolean addToBackStack, boolean popBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        String tag = f.getClass().getSimpleName();

        if (popBackStack) {
            manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            if (manager.findFragmentByTag(tag) != null) {
                return;
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_NONE);
        ft.replace(R.id.container, f, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }

        ft.commit();
    }

    public void setActionBarTitle(String title) {
        if (toolbar == null)
            return;

        toolbar.setTitle(title);
        ((TextView) toolbar.findViewById(R.id.title)).setText(title);
    }

    public void setActionBarOfflineMode(boolean offlineMode) {
        if (toolbar == null)
            return;
        TextView offlineView = (TextView) toolbar.findViewById(R.id.offline_mode);
        if (offlineMode) {
            offlineView.setVisibility(View.VISIBLE);
        } else {
            offlineView.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ServerErrorEvent event) {
        if (mErrorDialog != null && mErrorDialog.isVisible()) {
            mErrorDialog.dismiss();
        }
        mErrorDialog = ErrorDialog.newInstance(event.code, event.message);
        mErrorDialog.show(getSupportFragmentManager(), "error");
    }

    public void startFragment(Fragment f, boolean addToBackStack) {
        startFragment(f, addToBackStack, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNetworkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(mNetworkChangeReceiver);
        } catch (Exception ignore) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LeakCannaryHelper.watch(this);

        if (App.WIDTH == 0) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            App.WIDTH = size.x;
            App.HEIGHT = size.y;

            App.MARGIN = getResources().getDimensionPixelSize(R.dimen.mrg_normal);
            App.WIDTH_WITHOUT_MARGINS = App.WIDTH - App.MARGIN * 2;

            App.IMAGE_BORDER = getResources().getDimensionPixelSize(R.dimen.stream_item_small_border);
            App.IMAGE_SMALL_SIDE = (int) (App.WIDTH / 4.4 - App.IMAGE_BORDER * 4f);
        }

        Log.d("FCM", "Token: " + AppFirebaseInstanceIdService.getFcmToken());

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);
    }

    @Override
    protected void onStop() {
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }

        super.onStop();
    }

    //--------------------- network receiver ---------------------
    private class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean offlineMode = NetworkUtil.isOfflineMode();
            setActionBarOfflineMode(offlineMode);
        }
    }

    //------------------ end network receiver ---------------------
}
