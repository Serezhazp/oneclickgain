package com.humanet.humanetcore.activities.base;

import android.content.Context;
import android.widget.Toast;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.GameBalanceValue;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.CommentsActivity;
import com.humanet.humanetcore.activities.PlayFlowActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.MarkVideoRequest;
import com.humanet.humanetcore.api.requests.video.PayForVideoRequest;
import com.humanet.humanetcore.api.response.BaseCoinsResponse;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.events.OnUpdateVideoListener;
import com.humanet.humanetcore.fragments.profile.ProfileFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.views.dialogs.MoreDialog;
import com.humanet.humanetcore.views.dialogs.ShareDialog;
import com.humanet.humanetcore.views.dialogs.dialogViews.ConfirmationDialog;
import com.humanet.humanetcore.views.utils.UiUtil;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public abstract class BaseVideoManageActivity extends BaseActivity implements
        VideoItemView.OnCommentListener,
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnMoreListener,
        OnUpdateVideoListener {

    @Override
    public void onShowComment(Video video, Category category, VideoType videoType) {
        CommentsActivity.startNewInstance(this, video, category, videoType);
    }

    @Override
    public void onMore(Video video, Video videoParent) {
        new MoreDialog(this, video, videoParent).show();
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }

    @Override
    public final void onVideoPay(final VideoType videoType, final int videoId, final Runnable runnable, final Runnable anotherRunnable) {

        final PayRunnable payRunnable = new PayRunnable(videoType, videoId, this, getSpiceManager(), anotherRunnable);

        if(videoType == VideoType.MARKET_SELL) {
            showConfirmationDialog(runnable, payRunnable);
        } else {
            payRunnable.run();
            runnable.run();
        }

    }

    @Override
    public void onVideoMark(int videoId, @MarkType int mark) {
        getSpiceManager().execute(
                new MarkVideoRequest(videoId, mark),
                new SimpleRequestListener<BaseResponse>());
    }

    public void openUserProfile(int uid) {
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UploadingMedia addVideoResponse) {
        new ShareDialog.Builder()
                .setShareType(addVideoResponse.getShareType())
                .setLink(addVideoResponse.getShortUrl())
                .setGifLink(ShareDialog.getGifUrl(addVideoResponse.getVideoId()))
                .setVideoId(addVideoResponse.getVideoId())
                .setCanBeClosed(true)
                .build()
                .show(getSupportFragmentManager(), ShareDialog.TAG);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        UiUtil.hideKeyboard(this);
    }

    private static class PayRunnable implements Runnable {

        private VideoType mVideoType;
        private int mVideoId;
        private Context mContext;
        private SpiceManager mSpiceManager;
        private Runnable mRunnable;

        public PayRunnable(VideoType videoType, int videoId, Context context, SpiceManager spiceManager, Runnable runnable) {
            mVideoType = videoType;
            mVideoId = videoId;
            mContext = context;
            mSpiceManager = spiceManager;
            mRunnable = runnable;
        }

        @Override
        public void run() {
            mSpiceManager.execute(
                    new PayForVideoRequest(mVideoId),
                    new SimpleRequestListener<BaseCoinsResponse>() {
                        @Override
                        public void onRequestSuccess(BaseCoinsResponse baseCoinsResponse) {

                            String text;
                            switch (mVideoType) {
                                case MARKET_SELL:
                                    text = mContext.getString(R.string.market_sold, String.valueOf(Math.abs(baseCoinsResponse.getCoinsChange())), App.COINS_CODE);
                                    break;

                                case MARKET_SHARE:
                                    text = mContext.getString(R.string.market_shared);
                                    break;

                                case CROWD_GIVE:
                                    text = mContext.getString(R.string.crowd_shared);
                                    break;

                                case CROWD_ASK:
                                    text = mContext.getString(R.string.crowd_donated, String.valueOf(Math.abs(baseCoinsResponse.getCoinsChange())), App.COINS_CODE);
                                    break;

                                case CROWD_PET_GIVE:
                                    text = mContext.getString(R.string.crowd_shared);
                                    break;

                                case CROWD_PET_ASK:
                                    text = mContext.getString(R.string.crowd_donated, String.valueOf(Math.abs(baseCoinsResponse.getCoinsChange())), App.COINS_CODE);
                                    break;

                                default:
                                    text = null;
                            }
                            if (text != null)
                                Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();

                            AnalyticsHelper.trackEvent(mContext, AnalyticsHelper.MARKET_BUY_ITEM);

                            if (mVideoType.equals(VideoType.MARKET_SELL)) {
                                AppUser.getInstance().get().setBalance(AppUser.getInstance().get().getGameBalanceValue() + baseCoinsResponse.getCoinsChange());
                                GameBalanceValue.getInstance().setBalance(AppUser.getInstance().get().getGameBalanceValue());
                            } else {
                                AppUser.getInstance().get().setBalance(AppUser.getInstance().get().getBalance() + baseCoinsResponse.getCoinsChange());
                            }

                            mRunnable.run();
                        }
                    }
            );
        }
    }

    private void showConfirmationDialog(Runnable buttonStateRunnable, Runnable payRunnable) {

        ConfirmationDialog dialog = new ConfirmationDialog.Builder().build();
        dialog.setButtonStateRunnable(buttonStateRunnable);
        dialog.setPayRunnable(payRunnable);
        dialog.show(getSupportFragmentManager(), dialog.getClass().getSimpleName());

    }
}