package com.humanet.humanetcore.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.info.FeedbackChatFragment;
import com.humanet.humanetcore.fragments.info.NewFeedbackFragment;
import com.humanet.humanetcore.fragments.vote.NewVoteFragment;
import com.humanet.humanetcore.utils.ToolbarHelper;
import com.humanet.humanetcore.views.utils.UiUtil;

import static com.humanet.humanetcore.model.Feedback.Type;

/**
 * Created by Deni on 06.07.2015.
 */
public abstract class InputActivity extends BaseActivity {

    public static Intent newFeedbackIntent(Context context, int feedbackId, @Type int type) {
        Intent intent = new Intent(context, NavigationManager.getInputActivityClass());
        intent.putExtra(Constants.EXTRA_ACTION, Constants.ACTION.NEW_FEED_BACK);
        intent.putExtra("feedback_id", feedbackId);
        intent.putExtra("type", type);
        return intent;
    }

    public static Intent newPollIntent(Context context) {
        Intent intent = new Intent(context, NavigationManager.getInputActivityClass());
        intent.putExtra(Constants.EXTRA_ACTION, Constants.ACTION.NEW_POLL);
        return intent;
    }

    @Override
    protected void onPause() {
        UiUtil.hideKeyboard(this);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);

        toolbar = ToolbarHelper.createToolbar(this);

        final int action = getIntent().getIntExtra(Constants.EXTRA_ACTION, 0);

        switch (action) {
            case Constants.ACTION.NEW_FEED_BACK:
                int id = getIntent().getIntExtra("feedback_id", -1);
                @Type int type = getIntent().getIntExtra("type", 0);
                if (id == -1) {
                    startFragment(NewFeedbackFragment.newInstance(id, type), false);
                } else {
                    startFragment(FeedbackChatFragment.newInstance(id, type), false);
                }
                break;

            case Constants.ACTION.NEW_POLL:
                startFragment(new NewVoteFragment(), false);

        }

    }
}
