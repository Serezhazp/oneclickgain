package com.humanet.humanetcore.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimplePendingRequestListener;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.events.ShowProgressDialogEvent;
import com.humanet.humanetcore.fragments.profile.BaseProfileFragment;
import com.humanet.humanetcore.fragments.profile.EditProfileFragment;
import com.humanet.humanetcore.fragments.profile.ProfilePrivateInfoFragment;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.humanet.humanetcore.utils.ToolbarHelper;

import org.greenrobot.eventbus.EventBus;

public abstract class EditProfileActivity extends BaseActivity implements BaseProfileFragment.OnProfileFillListener, ProfilePrivateInfoFragment.OnFillMainProfileListener {

    private UserInfo mUser;

    public static void startNewInstance(Context context, int tabPosition) {
        Intent intent = new Intent(context, NavigationManager.getEditProfileActivityClass());
        intent.putExtra("tab_to_open", tabPosition);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static void startNewInstance(Context context) {
        startNewInstance(context, 0);
    }

    private Fragment mProfilePrivateInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment_container);

        mUser = AppUser.getInstance().get();

        toolbar = ToolbarHelper.createToolbar(this);

        setActionBarOfflineMode(NetworkUtil.isOfflineMode());

        launchStartFragment();
    }

    protected void launchStartFragment() {
        startFragment(new EditProfileFragment(), false);
    }


    @Override
    public void onFillProfile(UserInfo userInfo) {
        saveProfile(mUser);
    }

    @Override
    public UserInfo getUser() {
        return mUser;
    }

    public void saveProfile(UserInfo userInfo) {
        AppUser.getInstance().set(userInfo);

        EventBus.getDefault().post(new ShowProgressDialogEvent(true));

        getSpiceManager().execute(new EditUserInfoRequest(EditUserInfoRequest.userInfoToArgsMap(userInfo)), new SimplePendingRequestListener<BaseResponse>() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                onBackPressed();
            }
        });
    }

    public void setProfilePrivateInfoFragment(Fragment profilePrivateInfoFragment) {
        mProfilePrivateInfoFragment = profilePrivateInfoFragment;
    }

    @Override
    public void onFillMainProfileInfo(UserInfo userInfo) {
        saveProfile(userInfo);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mProfilePrivateInfoFragment != null) {
            mProfilePrivateInfoFragment.onActivityResult(requestCode & 0xffff, resultCode, data);
        }
    }
}
