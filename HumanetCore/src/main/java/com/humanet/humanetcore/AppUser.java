package com.humanet.humanetcore;

import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.GsonHelper;
import com.humanet.humanetcore.utils.PrefHelper;

/**
 * Created by Deni on 03.08.2015.
 */
public final class AppUser<T extends UserInfo> {
    private T mUserInfoCached;
    private Class<T> mUserClass;
    private static volatile AppUser sInstance;

    public static AppUser getInstance() {
        if(sInstance == null) {
            sInstance = new AppUser();
            sInstance.mUserClass = App.getInstance().getUserClass();
        }
        return sInstance;
    }

    public synchronized void set(String userJson) {
        if (!TextUtils.isEmpty(userJson)) {
            PrefHelper.setStringPref("app_user", userJson);
        }
        mUserInfoCached = null;
        mUserInfoCached = get();
    }

    public synchronized void set(T userInfo) {
        if (userInfo != null) {
            PrefHelper.setStringPref("app_user", GsonHelper.getGson().toJson(userInfo));
            if (!App.DEBUG) {
                Crashlytics.getInstance().core.setUserIdentifier(String.valueOf(userInfo.getId()));
                if (userInfo.getFName() != null) {
                    Crashlytics.getInstance().core.setUserName(String.valueOf(userInfo.getFName()));
                }
            }
            AnalyticsHelper.identifyUser(userInfo);
        }
        mUserInfoCached = userInfo;
    }

    public Language getLanguage() {
        UserInfo user = get();
        if (user == null) {
            return Language.getSystem();
        }
        return user.getLanguage();
    }

    public synchronized T get() {
        if (mUserInfoCached == null) {
            String userString = PrefHelper.getStringPref("app_user");
            if (userString != null) {
                mUserInfoCached = GsonHelper.getGson().fromJson(userString, mUserClass);
            }
        }
        return mUserInfoCached;
    }

    public synchronized void clear() {
        mUserInfoCached = null;
        PrefHelper.setStringPref("app_user", null);
    }

    public int getUid() {
        return get() != null ? get().getId() : 0;
    }
}
