package com.humanet.humanetcore.model;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.utils.BitmapDecoder;
import com.humanet.video.ProcessFrameGrabbing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by Denis on 15.05.2015.
 */
public class NewVideoInfo {


    private static VideoInfo sVideoInfo;

    public static void init() {
        sVideoInfo = new VideoInfo();
    }

    public static VideoInfo get() {
        return sVideoInfo;
    }

    public static void set(VideoInfo info) {
        sVideoInfo = info;
    }

    public static boolean isVideoRecorded() {
        return sVideoInfo != null && !TextUtils.isEmpty(get().getOriginalVideoPath()) && new File(get().getOriginalVideoPath()).exists();
    }


    private static boolean sIsFilterBuilding = false;
    private volatile static boolean sIsFrameGrabbing = false;

    public static boolean isIsFilterBuilding() {
        return sIsFilterBuilding;
    }

    private static ScheduledExecutorService sFilterExecutor;

    public synchronized static void rebuildFilterPack(final String newImagePath) {
        sIsFilterBuilding = true;

        if (sFilterExecutor != null) {
            sFilterExecutor.shutdownNow();
        }
        sFilterExecutor = Executors.newSingleThreadScheduledExecutor();

        final File filtersDir = FilePathHelper.getVideoFilteredImagePreviewFolder();

        List<Runnable> tasks = new ArrayList<>();

        Runnable prepareRunnable = new Runnable() {
            @Override
            public void run() {


                Bitmap bitmap = BitmapDecoder.createSquareBitmap(newImagePath, App.IMAGE_SMALL_SIDE, 0, false);
                FileOutputStream fileOutputStream = null;
                try {
                    fileOutputStream = new FileOutputStream(FilePathHelper.getVideoPreviewImageSmallPath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);


                for (File f : filtersDir.listFiles()) {
                    f.delete();
                }
            }
        };

        tasks.add(prepareRunnable);

        IFilter[] filters = FilterController.getFilters();


        for (IFilter filter : filters) {
            tasks.addAll(FilterController.getFilterToImageTasks(
                            filtersDir.getAbsolutePath(),
                            FilePathHelper.getVideoPreviewImageSmallPath(),
                            FilePathHelper.getFilteredImagePreview(filter.getTitle()).getAbsolutePath(),
                            filter,
                            100, 100,
                            null

                    )
            );
        }

        Runnable complete = new Runnable() {
            @Override
            public void run() {
                sIsFilterBuilding = false;
            }
        };
        tasks.add(complete);

        for (Runnable runnable : tasks)
            sFilterExecutor.execute(runnable);

    }

    public synchronized static void grabFramesPack() {
        sIsFrameGrabbing = true;

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.execute(ProcessFrameGrabbing.getRunnable(FilePathHelper.getVideoFrameFolder(), FilePathHelper.getVideoTmpFile(), ProcessFrameGrabbing.MODE_ALL_EXCLUDING_FIRST_FRAME));
        executor.execute(new Runnable() {
            @Override
            public void run() {
                sIsFrameGrabbing = false;
            }
        });

    }
}
