package com.humanet.humanetcore.model.enums.selectable;

import com.humanet.humanetcore.R;

import java.util.List;

/**
 * Created by Denis on 05.03.2015.
 */
public class Religion extends BaseItem {

    private static final String[] VALUES = new String[]{
            "christianity",
            "islam",
            "judaism",
            "hinduism",
            "buddhism",
            "atheism",
            "agnosticism",
            "other"};


    public Religion(int id, String item) {
        super(id, item);
    }

    public static Religion getById(String id) {
        return BaseItem.getById(Religion.class, VALUES, id);
    }

    @Override
    public String getDrawable() {
        if (getId() == VALUES.length)
            return "drawable://" + R.drawable.other_option;
        return super.getDrawable();
    }

    public static List<Religion> values() {
        return BaseItem.values(Religion.class, VALUES);
    }
}
