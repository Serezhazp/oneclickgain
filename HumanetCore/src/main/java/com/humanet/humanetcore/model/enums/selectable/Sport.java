package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Sport extends BaseItem {
    private static final String[] VALUES = new String[]{
            "football",
            "basketball",
            "hockey",
            "volleyball",
            "baseball",
            "fitness",
            "yoga",
            "cycling",
            "swimming",
            "martial",
            "winter",
            "tennis",
            "extreme",
            "rugby",
            "national",
            OTHER_OPTION
    };

    public Sport(int id, String item) {
        super(id, item);
    }

    public static Sport getById(String id) {
        return BaseItem.getById(Sport.class, VALUES, id);
    }


    public static List<Sport> values() {
        return BaseItem.values(Sport.class, VALUES);
    }
}
