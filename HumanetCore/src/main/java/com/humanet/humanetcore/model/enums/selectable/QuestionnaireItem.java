package com.humanet.humanetcore.model.enums.selectable;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;
import com.humanet.humanetcore.interfaces.CirclePickerItem;

/**
 * Created by ovitali on 29.10.2015.
 */
public enum QuestionnaireItem implements CirclePickerItem {

    HOBBIES {
        @Override
        public String getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_hobby);
        }

        public String getSearchTitle() {
            return App.getInstance().getString(R.string.search_questionnaire_hobby);
        }
    },
    SPORT {
        @Override
        public String getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_sport);
        }

        public String getSearchTitle() {
            return App.getInstance().getString(R.string.search_questionnaire_sport);
        }
    },
    PET {
        @Override
        public String getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_pet);
        }

        public String getSearchTitle() {
            return App.getInstance().getString(R.string.search_questionnaire_pets);
        }
    },
    INTERESTS {
        @Override
        public String getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_interests);
        }

        public String getSearchTitle() {
            return App.getInstance().getString(R.string.search_questionnaire_interests);
        }
    },

    RELIGION {
        @Override
        public String getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_religion);
        }

        public String getSearchTitle() {
            return App.getInstance().getString(R.string.search_questionnaire_religion);
        }
    };

    public abstract String getSearchTitle();
  /*  NON_GAME {
        @Override
        public Drawable getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_non_game);
        }
    },
    GAME {
        @Override
        public Drawable getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_game);
        }
    },
    EXTREME {
        @Override
        public Drawable getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_extreme);
        }
    },
    VIRTUAL {
        @Override
        public Drawable getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_virtual);
        }
    },

    PART_GAMES {
        @Override
        public Drawable getDrawable() {
            return null;
        }

        @Override
        public int getId() {
            return ordinal();
        }

        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.profile_picker_part_games);
        }
    },*/




}
