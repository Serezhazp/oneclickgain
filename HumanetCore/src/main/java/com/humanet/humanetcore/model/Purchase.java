package com.humanet.humanetcore.model;

import android.content.Context;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.R;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by ovi on 01.06.2016.
 */
public enum Purchase implements Serializable{

    P1_1(1, 0, 1,"p1_1"),
    P5_5(5, 0, 5, "p5_5"),
    P10_10(10.5, 5, 10, "p10_10"),
    P27_25(27.5, 10, 25, "p27_25"),
    P57_50(57.5, 15, 50, "p57_50"),
    P120_100(120, 20, 100, "p120_100"),
    P390_300(390, 30, 300, "p390_300");


    private final double mAmount;

    private final double mAmountFree;

    private final int mPrice;

    private final String mCode;


    public double getAmount() {
        return mAmount;
    }

    public double getAmountFree() {
        return mAmountFree;
    }

    public int getPrice() {
        return mPrice;
    }

    public String getCode() {
        return mCode;
    }

    public String title() {
        DecimalFormat format = new DecimalFormat("#.##");
        Context context = App.getInstance();
        if (mAmountFree > 0)
            return context.getString(
                    R.string.balance_currency_external_format_with_free,
                    format.format(mAmount),
                    format.format(mAmountFree) + "%",
                    App.COINS_CODE,
                    format.format(mPrice)
            );
        else
            return context.getString(
                    R.string.balance_currency_external_format,
                    format.format(mAmount),
                    App.COINS_CODE,
                    format.format(mPrice)
            );
    }

    public String simpleTitle() {
        DecimalFormat format = new DecimalFormat("#.##");
        Context context = App.getInstance();
        return context.getString(
                R.string.balance_currency_external_format_give_simple,
                format.format(mPrice)
        );
    }

    Purchase(double amount, double amountFree, int price, String code) {
        mAmount = amount;
        mAmountFree = amountFree;
        mPrice = price;
        mCode = code;
    }

    public static Purchase findByCode(String code) {
        for (Purchase purchase : values())
            if (purchase.getCode().equals(code))
                return purchase;

        throw new IllegalArgumentException("can not find purchase with passed code");
    }
}
