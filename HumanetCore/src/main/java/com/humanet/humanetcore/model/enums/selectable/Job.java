package com.humanet.humanetcore.model.enums.selectable;

import com.humanet.humanetcore.views.utils.ConverterUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis on 27.05.2015.
 */
public class Job extends BaseItem {

    private static final String[] VALUES = new String[]{
            "none",
            "unemployed",
            "wage_earner",
            "selfemployed",
            "freelance"
    };

    public Job(int id, String item) {
        super(id, item);
    }

    public static Job getById(String sid) {
        try {
            int id = -1;
            String item = VALUES[0];
            if (ConverterUtil.isInteger(sid)) {
                id = Integer.parseInt(sid);
                item = VALUES[id + 1];
            }

            return new Job(id, item);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    @Override
    public String getRequestParam() {
        return String.valueOf(getId());
    }

    public static List<Job> values() {
        List<Job> jobs = new ArrayList<>(VALUES.length);
        for (int i = 0; i < VALUES.length; i++) {
            jobs.add(new Job(i - 1, VALUES[i]));
        }

        return jobs;
    }
}
