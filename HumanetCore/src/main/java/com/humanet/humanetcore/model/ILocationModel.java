package com.humanet.humanetcore.model;

/**
 * Created by ovitali on 28.08.2015.
 */
public interface ILocationModel{
    int getId();
    String getTitle();
}
