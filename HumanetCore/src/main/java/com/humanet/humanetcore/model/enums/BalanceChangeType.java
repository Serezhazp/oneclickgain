package com.humanet.humanetcore.model.enums;

/**
 * Created by ovitali on 03.02.2015.
 */
public interface BalanceChangeType {

    int getId();
    String getTitle();

    String getDrawable();

}
