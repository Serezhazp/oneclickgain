package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Virtual extends BaseItem {

    private static String VALUES[] = new String[]{
            "action",
            "shooters",
            "fighting",
            "slasher",
            "arcade",
            "stealth_action",
            "technical_simulators",
            "simulations",
            "strategy",
            "adventure",
            "quests",
            "puzzle",
            "musical_games",
            "role_playing",
            "multiplayer_games",
            OTHER_OPTION};


    public Virtual(int id, String item) {
        super(id, item);
    }

    public static Virtual getById(String id) {
        return BaseItem.getById(Virtual.class, VALUES, id);
    }


    public static List<Virtual> values() {
        return BaseItem.values(Virtual.class, VALUES);
    }
}
