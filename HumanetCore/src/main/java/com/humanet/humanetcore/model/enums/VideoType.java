package com.humanet.humanetcore.model.enums;

import android.text.TextUtils;

import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.interfaces.IServerRequestParams;

/**
 * Created by Denis on 06.04.2015.
 */
public enum VideoType implements IServerRequestParams {

    ALL {
        @Override
        public String getRequestParam() {
            return String.valueOf(0);
        }

        public String getAdditionParam() {
            return "all";
        }
    },

    VLOG {
        @Override
        public String getRequestParam() {
            return String.valueOf(2);
        }

        @Override
        public String getAdditionParam() {
            return null;
        }
    },

    PET_MY {
        @Override
        public String getRequestParam() {
            return String.valueOf(18);
        }

        @Override
        public String getUserId() {
            return AppUser.getInstance() != null ? String.valueOf(AppUser.getInstance().getUid()) : null;
        }

        /*@Override
        public String getAdditionParam() {
            return "my";
        }*/
    },

    PET_ALL {
        @Override
        public String getRequestParam() {
            return String.valueOf(18);
        }

        @Override
        public String getAdditionParam() {
            return "all";
        }
    },

    PET_MY_CHOICE {
        @Override
        public String getRequestParam() {
            return String.valueOf(18);
        }

        @Override
        public String getAdditionParam() {
            return "my";
        }

        @Override
        public String getUserId() {
            return AppUser.getInstance() != null ? String.valueOf(AppUser.getInstance().getUid()) : null;
        }
    },

    MY_CHOICE {
        @Override
        public String getRequestParam() {
            return String.valueOf(0);
        }

        public String getAdditionParam() {
            return "my";
        }
    },

    NEWS {
        @Override
        public String getRequestParam() {
            return String.valueOf(10);
        }

        @Override
        public String getAdditionParam() {
            return null;
        }
    },

    CROWD_PET_ASK {
        @Override
        public String getRequestParam() {
            return String.valueOf(19);
        }

        @Override
        public String getAdditionParam() {
            return "please";
        }

        public boolean isForSale() {
            return true;
        }
    },

    CROWD_PET_GIVE {
        @Override
        public String getRequestParam() {
            return String.valueOf(19);
        }

        @Override
        public String getAdditionParam() {
            return "give";
        }

        public boolean isForSale() {
            return true;
        }
    },

    CROWD_ASK {
        public int getCode() {
            return 6;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "please";
        }

        public boolean isForSale() {
            return true;
        }
    },

    CROWD_GIVE {
        public int getCode() {
            return 6;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "give";
        }

        public boolean isForSale() {
            return true;
        }


    },

    DIY_SHARE_SKILL {
        @Override
        public String getRequestParam() {
            return String.valueOf(14);
        }

        @Override
        public String getAdditionParam() {
            return "skill";
        }
    },
    DIY_SHARE_ITEM {
        @Override
        public String getRequestParam() {
            return String.valueOf(14);
        }

        @Override
        public String getAdditionParam() {
            return "item";
        }
    },
    DIY_ASK_SKILL {
        @Override
        public String getRequestParam() {
            return String.valueOf(15);
        }

        @Override
        public String getAdditionParam() {
            return "skill";
        }
    },
    DIY_ASK_ITEM {
        @Override
        public String getRequestParam() {
            return String.valueOf(15);
        }

        @Override
        public String getAdditionParam() {
            return "item";
        }
    },

    MARKET_SHARE {
        @Override
        public String getRequestParam() {
            return String.valueOf(17);
        }

        @Override
        public String getAdditionParam() {
            return "share";
        }

        public boolean isForSale() {
            return true;
        }
    },
    MARKET_SELL {
        @Override
        public String getRequestParam() {
            return String.valueOf(17);
        }

        @Override
        public String getAdditionParam() {
            return "sell";
        }

        public boolean isForSale() {
            return true;
        }
    },
    ALIEN_LOOK_TASK {
        public int getCode() {
            return 16;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "look";
        }
    },

    ALIEN_LOOK_CHALLENGE {
        public int getCode() {
            return 16;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "challenge";
        }
    },

    EMOTICON_ALL {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "all";
        }

        public boolean isEmoticon() {
            return true;
        }
    },

    EMOTICON_BEST {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "best";
        }

        public boolean isEmoticon() {
            return true;
        }

    },

    EMOTICON_MY_CHOICE {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "my";
        }

        public boolean isEmoticon() {
            return true;
        }

    },;


    public boolean isForSale() {
        return false;
    }

    public boolean isEmoticon() {
        return false;
    }

    public int getId() {
        return ordinal() + 1;
    }

    public static VideoType getById(int id) {
        for (VideoType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public static VideoType getByCode(int id) {
        for (VideoType type : values()) {
            if (Integer.parseInt(type.getRequestParam()) == id) {
                return type;
            }
        }
        return null;
    }

    public static VideoType getByCode(int id, String params) {
        for (VideoType type : values()) {
            if (Integer.parseInt(type.getRequestParam()) == id) {
                if (TextUtils.isEmpty(params))
                    return type;
                else if (!TextUtils.isEmpty(type.getAdditionParam()) && params.equals(type.getAdditionParam()))
                    return type;
            }
        }
        return null;
    }

    @Override
    public String getRequestParam() {
        return String.valueOf(ordinal());
    }

    public String getAdditionParam() {
        return null;
    }

    public String getUserId() {
        return null;
    }

    public static VideoType getByRequestParam(String requestParam) {
        for (VideoType type : values()) {
            if (type.getRequestParam().equals(requestParam)) {
                return type;
            }
        }
        return null;
    }


}
