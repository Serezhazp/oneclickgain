package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Interest extends BaseItem {
    private static final String[] VALUES = new String[]{
            "movies",
            "fashion",
            "games",
            "history",
            "music",
            "news",
            "humor",
            "weapons",
            "science",
            "gadgets",
            "tourism",
            "health",
            "food",
            "celebrities",
            "entertainment",
            OTHER_OPTION
    };

    public Interest(int id, String item) {
        super(id, item);
    }

    public static Interest getById(String id) {
        return BaseItem.getById(Interest.class, VALUES, id);
    }


    public static List<Interest> values() {
        return BaseItem.values(Interest.class, VALUES);
    }
}
