package com.humanet.humanetcore.model;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.model.enums.NotificationType;

import java.io.Serializable;

/**
 * Created by serezha on 31.08.16.
 */
public class PushNotification implements ILocationModel, Serializable {

	@SerializedName("type")
	private int mType;

	@SerializedName("title_ru")
	private String mRu;

	@SerializedName("title_en")
	private String mEn;

	@SerializedName("title_es")
	private String mEs;

	@SerializedName("id_object")
	private int mId;

	@Override
	public int getId() {
		return mId;
	}

	@Override
	public String getTitle() {
		Language lang = Language.getSystem();
		switch (lang) {
			case RUS:
				return mRu;
			case ESP:
				return mEs;
			case ENG:
				return mEn;
			default:
				return mEn;
		}
	}

	public NotificationType getType() {
		return NotificationType.fromInt(mType);
	}
}
