package com.humanet.humanetcore.model.enums;

/**
 * Created by Denis on 22.04.2015.
 */
public enum Side {
    LEFT, RIGHT
}
