package com.humanet.humanetcore.model;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.views.dialogs.ShareDialog;

/**
 * Created by ovi on 12/22/15.
 */
public class UploadingMedia {

    @SerializedName("media")
    private String mMedia;
    @SerializedName("thumb")
    private String mThumb;

    private String mShortUrl;

    private int mVideoId;

    @ShareDialog.ShareType
    public int mShareType;

    public String getMedia() {
        return mMedia;
    }

    public String getThumb() {
        return mThumb;
    }

    public void setMedia(String media) {
        mMedia = media;
    }

    public void setThumb(String thumb) {
        mThumb = thumb;
    }

    public String getShortUrl() {
        return mShortUrl;
    }

    public void setShortUrl(String shortUrl) {
        mShortUrl = shortUrl;
    }

    public int getVideoId() {
        return mVideoId;
    }

    public void setVideoId(int videoId) {
        mVideoId = videoId;
    }

    @ShareDialog.ShareType
    public int getShareType() {
        return mShareType;
    }

    public void setShareType(@ShareDialog.ShareType int shareType) {
        mShareType = shareType;
    }
}
