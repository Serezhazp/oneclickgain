package com.humanet.humanetcore.model.enums;

/**
 * Created by Denis on 18.05.2015.
 */
public enum VideoFilterType {
    NEGATIVE,
    GRAY,
    COOL,
    SEPIA,
    CINEMATIC,
    CINEMATIC2,
    CINEMATIC3,
    GRITTY,
    WARM
}
