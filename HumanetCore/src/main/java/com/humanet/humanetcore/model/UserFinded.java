package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.model.enums.selectable.Extreme;
import com.humanet.humanetcore.model.enums.selectable.Game;
import com.humanet.humanetcore.model.enums.selectable.Hobby;
import com.humanet.humanetcore.model.enums.selectable.HowILook;
import com.humanet.humanetcore.model.enums.selectable.Interest;
import com.humanet.humanetcore.model.enums.selectable.NonGame;
import com.humanet.humanetcore.model.enums.selectable.PartGames;
import com.humanet.humanetcore.model.enums.selectable.Pet;
import com.humanet.humanetcore.model.enums.selectable.Religion;
import com.humanet.humanetcore.model.enums.selectable.Sport;
import com.humanet.humanetcore.model.enums.selectable.Virtual;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Deni on 22.06.2015.
 */
public class UserFinded extends BaseUser implements Comparable<UserFinded> {

    @SerializedName("id")
    private int mId;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.FIRST_NAME)
    private String mFirstName;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.LAST_NAME)
    private String mLastName;

    @SerializedName("id_city")
    protected int mCityId;

    private String mCity;

    @SerializedName("city_rus")
    private String mCityRu;
    @SerializedName("city_eng")
    private String mCityEng;
    @SerializedName("city_esp")
    private String mCityEsp;

    @SerializedName("id_country")
    protected int mCountryId;

    private String mCountry;

    @SerializedName("country_rus")
    private String mCountryRu;
    @SerializedName("country_eng")
    private String mCountryEng;
    @SerializedName("country_esp")
    private String mCountryEsp;


    @SerializedName(ContentDescriptor.UserSearchResult.Cols.LANG)
    private String mLang;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.HOBBIE)
    private Hobby mHobby;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.PETS)
    private Pet mPets;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.INTEREST)
    private Interest mInterest;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.RELIGION)
    private Religion mReligion;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.CRAFT)
    private String mCraft;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.SPORT)
    private Sport mSport;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.EXTREME)
    private Extreme mExtreme;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.GAME)
    private Game mGames;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.NON_GAME)
    private NonGame mNonGame;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.VIRTUAL)
    private Virtual mVirtual;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK)
    private HowILook mHowILook;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.PART_GAMES)
    private PartGames mPartGames;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.TAGS)
    private List<String> mTags;

    @SerializedName(ContentDescriptor.UserSearchResult.Cols.RATING)
    private float mRating;

    private String[] mRequiredFields;


    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }


    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }


    public void setCity(String city) {
        mCity = city;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public void setLang(String lang) {
        mLang = lang;
    }

    public void setHobby(Hobby hobby) {
        mHobby = hobby;
    }

    public void setPets(Pet pets) {
        mPets = pets;
    }

    public void setInterest(Interest interest) {
        mInterest = interest;
    }

    public void setReligion(Religion religion) {
        mReligion = religion;
    }

    public void setCraft(String craft) {
        mCraft = craft;
    }

    public void setSport(Sport sport) {
        mSport = sport;
    }

    public void setExtreme(Extreme extreme) {
        mExtreme = extreme;
    }

    public void setGames(Game games) {
        mGames = games;
    }

    public void setNonGame(NonGame nonGame) {
        mNonGame = nonGame;
    }

    public void setVirtual(Virtual virtual) {
        mVirtual = virtual;
    }

    public void setHowILook(HowILook howILook) {
        mHowILook = howILook;
    }

    public void setPartGames(PartGames partGames) {
        mPartGames = partGames;
    }

    public void setTags(List<String> tags) {
        mTags = tags;
    }

    public void setRating(float rating) {
        mRating = rating;
    }

    public String[] getRequiredFields() {
        return mRequiredFields;
    }

    public void setRequiredFields(String[] requiredFields) {
        mRequiredFields = requiredFields;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public int getCityId() {
        return mCityId;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public String getCity() {
        if (mCity == null) {
            switch (Language.getSystem()) {
                case ENG:
                    mCity = mCityEng;
                    break;
                case RUS:
                    mCity = mCityRu;
                    break;
                case ESP:
                    mCity = mCityEsp;
                    break;
            }
        }

        return mCity;
    }

    public String getCountry() {
        if (mCountry == null) {
            switch (Language.getSystem()) {
                case ENG:
                    mCountry = mCountryEng;
                    break;
                case RUS:
                    mCountry = mCountryRu;
                    break;
                case ESP:
                    mCountry = mCountryEsp;
                    break;
            }
        }

        return mCountry;
    }

    public String getLang() {
        return mLang;
    }

    public Hobby getHobby() {
        return mHobby;
    }

    public Pet getPets() {
        return mPets;
    }

    public Interest getInterest() {
        return mInterest;
    }

    public Religion getReligion() {
        return mReligion;
    }

    public String getCraft() {
        return mCraft;
    }

    public Sport getSport() {
        return mSport;
    }

    public Extreme getExtreme() {
        return mExtreme;
    }

    public Game getGames() {
        return mGames;
    }

    public NonGame getNonGame() {
        return mNonGame;
    }

    public Virtual getVirtual() {
        return mVirtual;
    }

    public HowILook getHowILook() {
        return mHowILook;
    }

    public PartGames getPartGames() {
        return mPartGames;
    }

    public List<String> getTags() {
        return mTags;
    }

    public float getRating() {
        return mRating;
    }

    @Override
    public int compareTo(@NonNull UserFinded another) {
        if (mRating > another.mRating) {
            return -1;
        } else if (mRating < another.mRating) {
            return 1;
        } else {
            return 0;
        }
    }


    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues(15);
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.ID, mId);
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.AVATAR, getAvatarSmall());
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.FIRST_NAME, mFirstName);
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.LAST_NAME, mLastName);
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.CRAFT, mCraft);

        contentValues.put(ContentDescriptor.UserSearchResult.Cols.CITY, getCity());
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.CITY_ID, mCityId);

        contentValues.put(ContentDescriptor.UserSearchResult.Cols.COUNTRY, getCountry());
        contentValues.put(ContentDescriptor.UserSearchResult.Cols.COUNTRY_ID, mCountryId);

        contentValues.put(ContentDescriptor.UserSearchResult.Cols.LANG, mLang);

        if (mHobby != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.HOBBIE, mHobby.getRequestParam());
        if (mInterest != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.INTEREST, mInterest.getRequestParam());
        if (mPets != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.PETS, mPets.getRequestParam());
        if (mSport != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.SPORT, mSport.getRequestParam());
        if (mReligion != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.RELIGION, mReligion.getRequestParam());
        if (mGames != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.GAME, mGames.getRequestParam());
        if (mNonGame != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.NON_GAME, mNonGame.getRequestParam());
        if (mExtreme != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.EXTREME, mExtreme.getRequestParam());
        if (mVirtual != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.VIRTUAL, mVirtual.getRequestParam());
        if (mHowILook != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK, mHowILook.getRequestParam());
        if (mPartGames != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.PART_GAMES, mPartGames.getRequestParam());

        contentValues.put(ContentDescriptor.UserSearchResult.Cols.REQUIRED, TextUtils.join(",", mRequiredFields));

        contentValues.put(ContentDescriptor.UserSearchResult.Cols.RATING, mRating);
        if (mTags != null)
            contentValues.put(ContentDescriptor.UserSearchResult.Cols.TAGS, TextUtils.join(",", mTags));

        return contentValues;
    }

    public static UserFinded fromCursor(Cursor cursor) {
        UserFinded user = new UserFinded();
        user.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.ID)));
        user.setAvatar(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.AVATAR)));
        user.setCraft(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.CRAFT)));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.FIRST_NAME)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.LAST_NAME)));

        user.setCity(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.CITY)));
        user.mCityId = (cursor.getInt(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.CITY_ID)));

        user.setCountry(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.COUNTRY)));
        user.mCountryId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.COUNTRY_ID));

        user.setHobby(Hobby.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.HOBBIE))));
        user.setInterest(Interest.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.INTEREST))));
        user.setSport(Sport.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.SPORT))));
        user.setPets(Pet.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.PETS))));
        user.setReligion(Religion.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.RELIGION))));

        user.setGames(Game.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.GAME))));
        user.setNonGame(NonGame.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.NON_GAME))));
        user.setExtreme(Extreme.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.EXTREME))));
        user.setVirtual(Virtual.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.VIRTUAL))));
        user.setHowILook(HowILook.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.HOW_I_LOOK))));
        user.setPartGames(PartGames.getById(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.PART_GAMES))));

        String tags = cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.TAGS));
        if (tags != null) {
            user.setTags(Arrays.asList(tags.split(",")));
        }
        String requiredFields = cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.REQUIRED));
        if (requiredFields != null) {
            user.setRequiredFields(requiredFields.split(","));
        }

        user.setLang(cursor.getString(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.LANG)));
        user.setRating(cursor.getFloat(cursor.getColumnIndex(ContentDescriptor.UserSearchResult.Cols.RATING)));
        return user;
    }


}
