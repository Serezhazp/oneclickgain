package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.IntDef;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.db.ContentDescriptor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Denis on 28.04.2015.
 */
public class Feedback {

    public static final int MOTION = 0;
    public static final int ERROR = 1;
    public static final int COMPLAIN = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({MOTION, ERROR, COMPLAIN})
    public @interface Type {
    }

    @SerializedName("id")
    private int mId;

    @SerializedName("type")
    @Type
    private int mType;

    @SerializedName("last_msg")
    private String mLastMessage;

    @SerializedName("new")
    private int mNew;

    @SerializedName("created_at")
    private long created;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(5);
        values.put(ContentDescriptor.Feedback.Cols.ID, mId);
        values.put(ContentDescriptor.Feedback.Cols.TYPE, mType);
        values.put(ContentDescriptor.Feedback.Cols.LAST_MESSAGE, mLastMessage);
        values.put(ContentDescriptor.Feedback.Cols.NEW, mNew);
        values.put(ContentDescriptor.Feedback.Cols.CREATED, created);
        return values;
    }

    public static Feedback fromCursor(Cursor cursor) {
        Feedback feeback = new Feedback();
        feeback.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.ID)));
        @Type int status = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.TYPE));
        feeback.setType(status);
        feeback.setNew(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.NEW)));
        feeback.setLastMessage(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.LAST_MESSAGE)));
        feeback.created = cursor.getLong(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.CREATED));
        return feeback;
    }

    @Type
    public int getType() {
        return mType;
    }

    public void setType(@Type int type) {
        mType = type;
    }

    public String getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
    }

    public int getNew() {
        return mNew;
    }

    public void setNew(int aNew) {
        mNew = aNew;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getCreated() {
        return created;
    }
}
