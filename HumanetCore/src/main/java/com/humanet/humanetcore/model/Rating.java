package com.humanet.humanetcore.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 09.10.2015.
 */
public class Rating {

    @SerializedName("rating")
    float mRate;

    @SerializedName("my_percent")
    int mPosition;

    @SerializedName("users")
    RatingUser[] mTopUsers;

    public float getRating() {
        return mRate;
    }

    public int getPosition() {
        return mPosition;
    }

    public RatingUser[] getTopUsers() {
        return mTopUsers;
    }
}
