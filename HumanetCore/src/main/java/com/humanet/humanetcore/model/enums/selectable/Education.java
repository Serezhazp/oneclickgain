package com.humanet.humanetcore.model.enums.selectable;

import com.humanet.humanetcore.views.utils.ConverterUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis on 27.05.2015.
 */
public class Education extends BaseItem {

    private static final String[] VALUES = new String[]{
            "none",
            "student",
            "secondary",
            "higher"
    };

    public Education(int id, String item) {
        super(id, item);
    }

    public static Education getById(String sid) {
        try {
            int id = -1;
            String item = VALUES[0];
            if (ConverterUtil.isInteger(sid)) {
                id = Integer.parseInt(sid);
                item = VALUES[id + 1];
            }

            return new Education(id, item);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    @Override
    public String getRequestParam() {
        return String.valueOf(getId());
    }

    public static List<Education> values() {
        List<Education> list = new ArrayList<>(VALUES.length);
        for (int i = 0; i < VALUES.length; i++) {
            list.add(new Education(i - 1, VALUES[i]));
        }

        return list;
    }
}
