package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by Denis on 05.03.2015.
 */
public class Hobby extends BaseItem {

    private static String VALUES[] = new String[]{
            "gardering",
            "foto",
            "dancing",
            "design",
            "painting",
            "books",
            "travel",
            "collecting",
            "modelling",
            "auto",
            "cooking",
            "pottery",
            "dolls",
            "hunting_and_fishing",
            "knitting",
            OTHER_OPTION};

    public Hobby(int id, String item) {
        super(id, item);
    }

    public static Hobby getById(String id) {
        return BaseItem.getById(Hobby.class, VALUES, id);
    }


    public static List<Hobby> values() {
        return BaseItem.values(Hobby.class, VALUES);
    }
}
