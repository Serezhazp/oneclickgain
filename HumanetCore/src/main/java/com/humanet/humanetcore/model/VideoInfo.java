package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.IntDef;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.VideoType;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class VideoInfo implements Serializable, Cloneable {


    public static final int NONE = 0;
    public static final int AVATAR = 1;
    public static final int PET_AVATAR = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NONE, AVATAR, PET_AVATAR})
    public @interface AvatarType {
    }


    public static final int STATUS_UPLOAD_REQUIRED = 1;
    public static final int STATUS_UPLOADING = -1;
    public static final int STATUS_DONE = 0;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATUS_UPLOAD_REQUIRED, STATUS_UPLOADING, STATUS_DONE})
    public @interface UploadingStatus {
    }


    private int mLocalId = -1;
    private String mVideoPath;
    private String mImagePath;
    private String mAudioPath;
    private int mSmileId = -1;
    private IFilter mFilter;
    private int mCameraId;
    private int mReplyToVideoId;
    private int mCategoryId;
    private int mPetId;
    private String mTags;

    @AvatarType
    private int mAvatarType = NONE;

    private int mCost;

    private VideoType mVideoType;

    private String mOriginalVideoPath;
    private String mOriginalImagePath;

    private IFilter mVideoFilterApplied;
    private IFilter mImageFilterApplied;
    private String mAudioAppliedPath;

    private int mWidth;
    private int mHeight;

    private double mLatitude;
    private double mLongitude;

    private long mCreated;

    @VideoInfo.UploadingStatus
    private int mUploadingStatus = STATUS_UPLOAD_REQUIRED;
    private String mUploadedVideoPath;
    private String mUploadedImagePath;

    private Object mAdditionalParams;


    public String getOriginalVideoPath() {
        return mOriginalVideoPath;
    }

    public void setOriginalVideoPath(String originalVideoPath) {
        mOriginalVideoPath = originalVideoPath;
        mVideoPath = mOriginalVideoPath;
    }

    public String getOriginalImagePath() {
        return mOriginalImagePath;
    }

    public void setOriginalImagePath(String originalImagePath) {
        mOriginalImagePath = originalImagePath;
        mImagePath = mOriginalImagePath;
    }

    public IFilter getVideoFilterApplied() {
        return mVideoFilterApplied;
    }

    public void setVideoFilterApplied(IFilter videoFilterApplied) {
        mVideoFilterApplied = videoFilterApplied;
    }

    public IFilter getImageFilterApplied() {
        return mImageFilterApplied;
    }

    public void setImageFilterApplied(IFilter videoFilterApplied) {
        mImageFilterApplied = videoFilterApplied;
    }

    public void setAudioApplied(String audioApplied) {
        mAudioAppliedPath = audioApplied;
    }

    public boolean isVideoFilterApplied() {
        return FilterController.areFiltersSame(mFilter, mVideoFilterApplied);
    }

    public boolean isImageFilterApplied() {
        return FilterController.areFiltersSame(mFilter, mImageFilterApplied);
    }

    public boolean isAudioApplied() {
        return mAudioPath.equals(mAudioAppliedPath);
    }

    public int getCameraId() {
        return mCameraId;
    }

    public void setCameraId(int mCameraId) {
        this.mCameraId = mCameraId;
    }

    public IFilter getFilter() {
        return mFilter;
    }

    public void setFilter(IFilter filter) {
        mFilter = filter;
    }

    public void setPetId(int petId) {
        mPetId = petId;
    }

    public VideoInfo() {
    }

    public String getAudioPath() {
        return mAudioPath;
    }

    public void setAudioPath(String mAudioPath) {
        this.mAudioPath = mAudioPath;
    }

    public String getVideoPath() {
        if (TextUtils.isEmpty(mVideoPath)) {
            mVideoPath = mOriginalVideoPath;
        }
        return mVideoPath;
    }

    public void setVideoPath(String videoPath) {
        mVideoPath = videoPath;
    }

    public String getImagePath() {
        if (TextUtils.isEmpty(mImagePath)) {
            mImagePath = mOriginalImagePath;
        }
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }


    public int getSmileId() {
        return mSmileId;
    }

    public void setSmileId(int smileId) {
        mSmileId = smileId;
    }

    public int getReplyToVideoId() {
        return mReplyToVideoId;
    }

    public void setReplyToVideoId(int replyToVideoId) {
        mReplyToVideoId = replyToVideoId;
    }

    public int getCategory() {
        return mCategoryId;
    }

    public void setCategoryId(int category) {
        mCategoryId = category;
    }

    public String getTags() {
        return mTags;
    }

    public void setTags(String tags) {
        mTags = tags;
    }

    public VideoType getVideoType() {
        return mVideoType;
    }

    public int getCost() {
        return mCost;
    }

    public void setCost(int cost) {
        mCost = cost;
    }

    public void setVideoType(VideoType videoType) {
        mVideoType = videoType;
    }


    public int getLocalId() {
        return mLocalId;
    }

    public void setLocalId(int localId) {
        mLocalId = localId;
    }


    @AvatarType
    public int isAvatar() {
        return mAvatarType;
    }

    public void setAvatarType(@AvatarType int avatarType) {
        mAvatarType = avatarType;
    }

    public String getUploadedVideoPath() {
        return mUploadedVideoPath;
    }

    public void setUploadedVideoPath(String uploadedVideoPath) {
        mUploadedVideoPath = uploadedVideoPath;
    }

    public String getUploadedImagePath() {
        return mUploadedImagePath;
    }

    public void setUploadedImagePath(String uploadedImagePath) {
        mUploadedImagePath = uploadedImagePath;
    }

    public void setSizes(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setLocation(double latitude, double longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public long getCreated() {
        return mCreated;
    }

    public void setCreated(long created) {
        mCreated = created;
    }

    public Object getAdditionalParams() {
        return mAdditionalParams;
    }

    public void setAdditionalParams(Object additionalParams) {
        mAdditionalParams = additionalParams;
    }

    public int getPetId() {
        return mPetId;
    }

    @Override
    public VideoInfo clone() {
        try {
            Object clone = super.clone();
            return (VideoInfo) clone;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        if (mLocalId != -1)
            values.put(ContentDescriptor.VideosUpload.Cols.ID, mLocalId);
        values.put(ContentDescriptor.VideosUpload.Cols.VIDEO_URL, mVideoPath);
        values.put(ContentDescriptor.VideosUpload.Cols.THUMB_URL, mImagePath);
        values.put(ContentDescriptor.VideosUpload.Cols.CREATED_AT, mCreated);
        values.put(ContentDescriptor.VideosUpload.Cols.SMILE_ID, mSmileId);
        values.put(ContentDescriptor.VideosUpload.Cols.COST, getCost());
        values.put(ContentDescriptor.VideosUpload.Cols.REPLAY_ID, mReplyToVideoId);
        if (mVideoType != null)
            values.put(ContentDescriptor.VideosUpload.Cols.TYPE, mVideoType.getId());
        values.put(ContentDescriptor.VideosUpload.Cols.CATEGORY_ID, mCategoryId);

        values.put(ContentDescriptor.VideosUpload.Cols.LATITUDE, mLatitude);
        values.put(ContentDescriptor.VideosUpload.Cols.LONGITUDE, mLongitude);

        values.put(ContentDescriptor.VideosUpload.Cols.TAGS, mTags);

        values.put(ContentDescriptor.VideosUpload.Cols.AVATAR_TYPE, mAvatarType);
        values.put(ContentDescriptor.VideosUpload.Cols.UPLOADING_STATUS, mUploadingStatus);

        if (mAdditionalParams != null)
            values.put(ContentDescriptor.VideosUpload.Cols.ADDITIONAL_PARAMS, new Gson().toJson(mAdditionalParams));


        return values;
    }

    public static VideoInfo fromCursor(Cursor cursor) {
        VideoInfo videoInfo = new VideoInfo();

        videoInfo.mLocalId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.ID));
        videoInfo.mVideoPath = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.VIDEO_URL));
        videoInfo.mImagePath = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.THUMB_URL));
        videoInfo.mCreated = cursor.getLong(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.CREATED_AT));
        videoInfo.mSmileId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.SMILE_ID));
        videoInfo.mTags = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.TAGS));
        videoInfo.mReplyToVideoId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.REPLAY_ID));
        videoInfo.mCost = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.COST));

        @AvatarType
        int avatarType = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.AVATAR_TYPE));
        videoInfo.mAvatarType = avatarType;

        @UploadingStatus int uploadingStatus = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.UPLOADING_STATUS));
        videoInfo.mUploadingStatus = uploadingStatus;
        videoInfo.mUploadedVideoPath = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.UPLOADED_VIDEO));
        videoInfo.mUploadedImagePath = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.UPLOADED_IMAGE));

        videoInfo.mLatitude = cursor.getDouble(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.LATITUDE));
        videoInfo.mLongitude = cursor.getDouble(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.LONGITUDE));


        videoInfo.mVideoType = VideoType.getById(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.TYPE)));
        videoInfo.mCategoryId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.CATEGORY_ID));

        String additinalParams = cursor.getString(cursor.getColumnIndex(ContentDescriptor.VideosUpload.Cols.ADDITIONAL_PARAMS));
        if (additinalParams != null)
            videoInfo.mAdditionalParams = new Gson().fromJson(additinalParams, Object.class);

        return videoInfo;
    }
}
