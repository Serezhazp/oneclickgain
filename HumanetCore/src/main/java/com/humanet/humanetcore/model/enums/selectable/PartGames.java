package com.humanet.humanetcore.model.enums.selectable;

import java.util.List;

import static com.humanet.humanetcore.Constants.OTHER_OPTION;

/**
 * Created by ovitali on 19.11.2015.
 */
public class PartGames extends BaseItem {

    private static String VALUES[] = new String[]{
            "professional_sports",
            "amateur_sports",
            "extreme",
            "computer_games",
            "gambling",
            OTHER_OPTION};

    public PartGames(int id, String item) {
        super(id, item);
        suffix = "part_games";
    }

    public static PartGames getById(String id) {
        return BaseItem.getById(PartGames.class, VALUES, id);
    }

    public static List<PartGames> values() {
        return PartGames.values(PartGames.class, VALUES);
    }

}
