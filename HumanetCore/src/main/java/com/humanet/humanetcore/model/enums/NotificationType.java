package com.humanet.humanetcore.model.enums;

/**
 * Created by serezha on 31.08.16.
 */
public enum NotificationType {
	NOT_FILLED_INFO, NEW_COMMENT, NEW_VIDEO_ANSWER, ADD_YOUR_VIDEO, NEW_VOTE, NEW_CONTENT;

	public static NotificationType fromInt(int number) {
		switch (number) {
			case 1:
				return NOT_FILLED_INFO;
			case 2:
				return NEW_COMMENT;
			case 3:
				return NEW_VIDEO_ANSWER;
			case 4:
				return ADD_YOUR_VIDEO;
			case 5:
				return NEW_VOTE;
			case 6:
				return NEW_CONTENT;
			default:
				return null;
		}
	}

	public static int toInt(NotificationType type) {
		switch (type) {
			case NOT_FILLED_INFO:
				return 1;
			case NEW_COMMENT:
				return 2;
			case NEW_VIDEO_ANSWER:
				return 3;
			case ADD_YOUR_VIDEO:
				return 4;
			case NEW_VOTE:
				return 5;
			case NEW_CONTENT:
				return 6;
			default:
				return 0;
		}
	}

}
