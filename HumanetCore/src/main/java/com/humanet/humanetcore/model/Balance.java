package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.BalanceChangeType;
import com.humanet.humanetcore.utils.BalanceHelper;

import java.util.Date;

/**
 * Created by Владимир on 13.11.2014.
 */
public class Balance {

    @SerializedName("id")
    private long mId;

    @SerializedName("type")
    private BalanceChangeType mBalanceChangeType;

    @SerializedName("change")
    private float mChange;

    @SerializedName("id_video")
    private int mVideoId;

    @SerializedName("img_object")
    private String mVideoImage;

    @SerializedName("created_at")
    private Date createdAt;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public BalanceChangeType getBalanceChangeType() {
        return mBalanceChangeType;
    }

    public void setBalanceChangeType(BalanceChangeType balanceChangeType) {
        this.mBalanceChangeType = balanceChangeType;
    }

    public float getChange() {
        return mChange;
    }

    public void setChange(float change) {
        this.mChange = change;
    }

    public int getVideoId() {
        return mVideoId;
    }

    public void setVideoId(int videoId) {
        this.mVideoId = videoId;
    }

    public String getVideoImage() {
        return mVideoImage;
    }

    public void setVideoImage(String videoImage) {
        this.mVideoImage = videoImage;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Balance() {
    }

    public ContentValues toContentValues(int position) {
        ContentValues contentValues = new ContentValues(7);
        contentValues.put("_id", ((long) position << 32) + getId());
        if (getBalanceChangeType() != null) {
            contentValues.put(ContentDescriptor.Balances.Cols.TYPE, getBalanceChangeType().getId());
        }
        contentValues.put(ContentDescriptor.Balances.Cols.TAB_POSITION, position);
        contentValues.put(ContentDescriptor.Balances.Cols.CHANGE, getChange());
        contentValues.put(ContentDescriptor.Balances.Cols.VIDEO_ID, getVideoId());
        contentValues.put(ContentDescriptor.Balances.Cols.VIDEO_IMG, getVideoImage());
        if (getCreatedAt() != null) {
            contentValues.put(ContentDescriptor.Balances.Cols.CREATED_AT, getCreatedAt().getTime());
        }
        return contentValues;
    }

    public static Balance fromCursor(Cursor cursor) {
        Balance balance = new Balance();
        balance.setId(cursor.getInt(0));
        balance.setBalanceChangeType(BalanceHelper.getById(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.TYPE))));
        balance.setChange(cursor.getFloat(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.CHANGE)));
        balance.setVideoId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.VIDEO_ID)));
        balance.setVideoImage(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.VIDEO_IMG)));

        balance.setCreatedAt(new Date(cursor.getLong(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.CREATED_AT))));

        return balance;
    }

}
