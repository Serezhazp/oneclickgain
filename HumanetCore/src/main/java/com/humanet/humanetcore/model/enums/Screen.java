package com.humanet.humanetcore.model.enums;

/**
 * Created by Denis on 26.02.2015.
 */
public enum Screen {
    MY_PROFILE, USER_PROFILE, CATEGORY, POPULAR, SUBSCRIPTIONS, LIST_FLOW, PLAY_FLOW
}
