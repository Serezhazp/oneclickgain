package com.humanet.humanetcore.model;

/**
 * Created by Denis on 27.03.2015.
 */
public class ContactItem {
    private long mId;
    private String mName;

    transient private boolean mCheck = false;
    transient private boolean mFavorites = false;
    transient private boolean mAll = false;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public ContactItem(long id, String name) {
        mId = id;
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public boolean isCheck() {
        return mCheck;
    }

    public void setCheck(boolean check) {
        this.mCheck = check;
    }

    public boolean isFavorites() {
        return mFavorites;
    }

    public void setFavorites(boolean favorites) {
        this.mFavorites = favorites;
    }

    public boolean isAll() {
        return mAll;
    }

    public void setAll(boolean all) {
        this.mAll = all;
    }
}
