package com.humanet.humanetcore.model;

import java.io.Serializable;

/**
 * Created by Владимир on 19.11.2014.
 */
public interface SimpleListItem extends Serializable{
    String getTitle();

    String getImage();
}
