package com.humanet.humanetcore.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.utils.GsonHelper;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by Владимир on 24.11.2014.
 */
public class Video implements Serializable {

    @SerializedName("id")
    private int mVideoId;

    @SerializedName("author_id")
    private int mAuthorId;

    @SerializedName("id_smile")
    private int mSmileId;

    @SerializedName("views")
    private int mViews;

    @SerializedName("comments")
    private int mCommentsCount;

    @SerializedName("likes")
    private int mLikes;

    @SerializedName("replays")
    private int mReplays;


    @SerializedName("id_category")
    private int mCategoryId;

    @SerializedName("id_reply")
    private int mReplyId;

    @SerializedName("id_pet")
    private int mPetId;

    @SerializedName("length")
    private float mLength;

    @SerializedName("created_at")
    private long mCreatedAt;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("author_nickname")
    private String mAuthorName;

    @SerializedName("author_photo")
    private String mAuthorPhotoUrl;

    @SerializedName("media")
    private String mUrl;

    @SerializedName("short_url")
    private String mShortUrl;

    @SerializedName("thumb")
    private String mThumbUrl;


    @SerializedName("my_mark")
    private int mMyMark;

    @SerializedName("tags")
    private List<String> tags;

    private String mCity;
    private String mCountry;


    @SerializedName("cost")
    private int mCost;

    @SerializedName("available")
    private Boolean mAvailable;

    @SerializedName("earn")
    private int mEarn;

    @SerializedName("type")
    private int mType;

    @SerializedName("params")
    private String mParams;

    @SerializedName("guess")
    private Boolean mSmileGuessed;


    @SerializedName("comment")
    private List<Comment> mComments;

    public Video() {
    }

    public float getLength() {
        return mLength;
    }

    public void setLength(float length) {
        mLength = length;
    }

    public List<Comment> getComments() {
        return mComments;
    }


    public void setTags(String[] tags) {
        List<String> tagList = new ArrayList<String>(tags.length);
        Collections.addAll(tagList, tags);

        this.tags = tagList;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public int getVideoId() {
        return mVideoId;
    }

    public void setVideoId(int videoId) {
        mVideoId = videoId;
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(int authorId) {
        mAuthorId = authorId;
    }

    public int getSmileId() {
        return mSmileId;
    }

    public void setSmileId(int smileId) {
        mSmileId = smileId;
    }

    public int getViews() {
        return mViews;
    }

    public void setViews(int views) {
        mViews = views;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        mCommentsCount = commentsCount;
    }

    public int getLikes() {
        return mLikes;
    }

    public void setLikes(int likes) {
        mLikes = likes;
    }

    public int getReplays() {
        return mReplays;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setReplays(int replays) {
        mReplays = replays;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;
    }


    public int getReplyId() {
        return mReplyId;
    }

    public void setReplyId(int replyId) {
        mReplyId = replyId;
    }

    public int getPetId() {
        return mPetId;
    }

    public void setPetId(int petId) {
        mPetId = petId;
    }

    public long getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(long createdAt) {
        mCreatedAt = createdAt;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAuthorName() {
        return mAuthorName;
    }

    public void setAuthorName(String authorName) {
        mAuthorName = authorName;
    }

    public String getAuthorPhotoUrl() {
        return mAuthorPhotoUrl;
    }

    public void setAuthorPhotoUrl(String authorPhotoUrl) {
        mAuthorPhotoUrl = authorPhotoUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        mThumbUrl = thumbUrl;
    }

    public int getMyMark() {
        return mMyMark;
    }

    public void setMyMark(int myMark) {
        mMyMark = myMark;
    }

    public boolean isSmileGuessed() {
        return mSmileGuessed == null ? false : mSmileGuessed;
    }

    public void setSmileGuessed(boolean smileGuessed) {
        mSmileGuessed = smileGuessed;
    }

    public String getParams() {
        return mParams;
    }

    public void setParams(String params) {
        mParams = params;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ContentDescriptor.Videos.Cols.VIDEO_ID, getVideoId());
        values.put(ContentDescriptor.Videos.Cols.VIDEO_URL, getUrl());
        values.put(ContentDescriptor.Videos.Cols.VIDEO_SHORT_URL, getShortUrl());
        values.put(ContentDescriptor.Videos.Cols.THUMB_URL, getThumbUrl());
        values.put(ContentDescriptor.Videos.Cols.CREATED_AT, getCreatedAt());
        values.put(ContentDescriptor.Videos.Cols.AUTHOR_ID, getAuthorId());
        values.put(ContentDescriptor.Videos.Cols.AUTHOR_NAME, getAuthorName());
        values.put(ContentDescriptor.Videos.Cols.TITLE, getTitle());
        values.put(ContentDescriptor.Videos.Cols.VIEWS, getViews());
        values.put(ContentDescriptor.Videos.Cols.LIKES, getLikes());
        values.put(ContentDescriptor.Videos.Cols.REPLIES, getReplays());
        values.put(ContentDescriptor.Videos.Cols.COMMENTS_COUNT, getCommentsCount());
        values.put(ContentDescriptor.Videos.Cols.SMILE_ID, getSmileId());
        values.put(ContentDescriptor.Videos.Cols.LENGTH, getLength());
        values.put(ContentDescriptor.Videos.Cols.MY_MARK, getMyMark());
        values.put(ContentDescriptor.Videos.Cols.GUESSED, isSmileGuessed() ? 1 : 0);
        values.put(ContentDescriptor.Videos.Cols.CITY, getCity());
        values.put(ContentDescriptor.Videos.Cols.COUNTRY, getCountry());
        values.put(ContentDescriptor.Videos.Cols.COST, getCost());
        values.put(ContentDescriptor.Videos.Cols.EARN, getEarn());
        values.put(ContentDescriptor.Videos.Cols.REPLAY_ID, getReplyId());
        values.put(ContentDescriptor.Videos.Cols.PET_ID, getPetId());
        values.put(ContentDescriptor.Videos.Cols.TYPE, getType());
        values.put(ContentDescriptor.Videos.Cols.PARAMS, getParams());
        values.put(ContentDescriptor.Videos.Cols.CATEGORY_ID, getCategoryId());
        values.put(ContentDescriptor.Videos.Cols.AVAILABLE, isAvailable() ? 1 : 0);
        if (getTags() != null) {
            values.put(ContentDescriptor.Videos.Cols.TAGS, TextUtils.join(",", getTags()));
        }
        return values;
    }

    public static Video fromCursor(Cursor cursor) {
        int videoId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIDEO_ID));
        String videoUrl = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIDEO_URL));
        String videoShortUrl = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIDEO_SHORT_URL));
        String imageUrl = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.THUMB_URL));
        long createdAt = cursor.getLong(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CREATED_AT));
        int authorId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.AUTHOR_ID));
        String authorName = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.AUTHOR_NAME));
        String title = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.TITLE));
        int views = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.VIEWS));
        int likes = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.LIKES));
        int replies = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.REPLIES));
        int smileId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.SMILE_ID));
        int commentsCount = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COMMENTS_COUNT));
        int myMark = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.MY_MARK));
        int smileGuessed = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.GUESSED));
        String tags = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.TAGS));
        String city = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CITY));
        String country = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COUNTRY));
        int cost = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.COST));
        int earn = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.EARN));
        int paid = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.AVAILABLE));

        int length = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.LENGTH));
        int gameId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.GAME_ID));
        int replayId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.REPLAY_ID));
        int petId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.PET_ID));

        int type = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.TYPE));
        String params = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.PARAMS));
        int categoryId = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Videos.Cols.CATEGORY_ID));

        Video video = new Video();
        video.setVideoId(videoId);
        video.setUrl(videoUrl);
        video.setThumbUrl(imageUrl);
        video.setCreatedAt(createdAt);
        video.setAuthorId(authorId);
        video.setAuthorName(authorName);
        video.setTitle(title);
        video.setViews(views);
        video.setLikes(likes);
        video.setReplays(replies);
        video.setCommentsCount(commentsCount);
        video.setSmileId(smileId);
        video.setLength(length);
        video.setMyMark(myMark);
        video.setSmileGuessed(smileGuessed > 0);
        if (tags != null) {
            video.setTags(tags.split(","));
        }
        video.setShortUrl(videoShortUrl);
        video.setCity(city);
        video.setCountry(country);
        video.setCost(cost);
        video.setAvailable(paid == 1);
        video.setEarn(earn);
        video.setReplyId(replayId);
        video.setPetId(petId);
        video.setType(type);
        video.setParams(params);
        video.setCategoryId(categoryId);

        return video;
    }

    public String getShortUrl() {
        return mShortUrl;
    }

    public void setShortUrl(String shortUrl) {
        mShortUrl = shortUrl;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public int getCost() {
        return mCost;
    }

    public void setCost(int cost) {
        mCost = cost;
    }

    public Boolean isAvailable() {
        if (mAvailable == null)
            return false;
        return mAvailable;
    }

    public void setAvailable(Boolean available) {
        mAvailable = available;
    }

    public int getEarn() {
        return mEarn;
    }

    public void setEarn(int earn) {
        mEarn = earn;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }

    public static class Deserializer implements JsonDeserializer<Video> {
        @Override
        public Video deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            Video video = new GsonBuilder().registerTypeAdapter(Boolean.class, new GsonHelper.BooleanDeserializer())
                    .create().fromJson(json, Video.class);

            try {

                Language lang = Language.getSystem();
                String slang = lang.toString().toLowerCase(Locale.getDefault());

                String key;

                key = "country_" + slang;
                video.mCountry = ((JsonObject) json).get(key).getAsString();

                key = "city_" + slang;
                video.mCity = ((JsonObject) json).get(key).getAsString();
            } catch (Exception ignore) {

            }

            return video;
        }
    }

}