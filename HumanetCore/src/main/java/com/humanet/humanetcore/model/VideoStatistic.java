package com.humanet.humanetcore.model;


import com.humanet.humanetcore.model.enums.VideoType;

public class VideoStatistic {

    private static VideoStatisticDelegator sVideoStatisticDelegator;

    private String mImageResId;
    private int mTitleResId;
    private int mId;
    private int mDuration;

    public VideoStatistic() {

    }

    public String getImageResId() {
        return mImageResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getId() {
        return mId;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public void setImageResId(String imageResId) {
        mImageResId = imageResId;
    }

    public void setTitleResId(int titleResId) {
        mTitleResId = titleResId;
    }

    public static VideoStatistic create(int videoTypeCode, int duration) {
        VideoStatistic videoStatistic = null;

        if (duration > 0) {
            videoStatistic = new VideoStatistic();
            videoStatistic.mDuration = duration;
            videoStatistic.mId = videoTypeCode;
            VideoType type = VideoType.getByCode(videoTypeCode);
            if (type != null && sVideoStatisticDelegator != null)
                sVideoStatisticDelegator.create(videoStatistic, type, duration);
            else
                videoStatistic = null;
        }

        return videoStatistic;
    }


    public static void setVideoStatisticDelegator(VideoStatisticDelegator videoStatisticDelegator) {
        sVideoStatisticDelegator = videoStatisticDelegator;
    }

    public interface VideoStatisticDelegator {
        VideoStatistic create(VideoStatistic videoStatistic, VideoType videoType, int duration);
    }
}
