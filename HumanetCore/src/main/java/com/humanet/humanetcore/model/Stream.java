package com.humanet.humanetcore.model;

import java.io.Serializable;

/**
 * Created by Владимир on 24.11.2014.
 */
public class Stream implements Serializable{
    private Category mCategory;

    public Stream() {
    }

    public Stream(Category category) {
        mCategory = category;
    }

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    public void setId(int id) {
        mCategory.setId(id);
    }

    public void setTitle(String title) {
        mCategory.setTitle(title);
    }
}
