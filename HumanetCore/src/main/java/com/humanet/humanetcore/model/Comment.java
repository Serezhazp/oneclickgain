package com.humanet.humanetcore.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Владимир on 26.11.2014.
 */
public class Comment implements Serializable {

    @SerializedName("author_id")
    private int mAuthorId;

    @SerializedName("author")
    private String mAuthor;

    @SerializedName("comment")
    private String mText;

    @SerializedName("author_photo")
    private String mAvatarUrl;

    @SerializedName("created_at")
    private long mTime;

    public Comment() {
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }
}
