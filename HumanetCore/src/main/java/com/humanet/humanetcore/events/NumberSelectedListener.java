package com.humanet.humanetcore.events;

/**
 * Created by Deni on 26.08.2015.
 */
public interface NumberSelectedListener {
    void onNumberSelected(String userName, String number);
}