package com.humanet.humanetcore.events;

import android.support.annotation.IntDef;

import com.humanet.humanetcore.model.enums.VideoType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by denisvasilenko on 21.09.2015.
 */
public interface OnUpdateVideoListener {

    int LIKED = 1;
    int UN_LIKED = -1;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LIKED, UN_LIKED})
    @interface MarkType {
    }


    void onVideoPay(VideoType videoType, int videoId, Runnable runnable, Runnable anotherRunnable);

    void onVideoMark(int videoId, @MarkType int mark);
}
