package com.humanet.humanetcore.events;

/**
 * Created by Deni on 21-Jun-16.
 */
public class CategoriesLoadedEvent {
    private int mCount;

    public CategoriesLoadedEvent(int count) {
        mCount = count;
    }

    public int getCount() {
        return mCount;
    }
}
