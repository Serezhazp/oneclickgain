package com.humanet.humanetcore.events;

/**
 * Created by ovitali on 09.12.2015.
 */
public interface IMenuBindActivity {

    void selectMenuItem(int itemId);

}
