package com.humanet.humanetcore.events;

/**
 * Created by ovi on 1/25/16.
 */
public interface INavigatableActivity {
    void onNavigateByViewId(int viewId);
}
