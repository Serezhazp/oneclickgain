package com.humanet.humanetcore.events;

/**
 * Created by ovitali on 09.12.2015.
 */
public interface IMenuBindFragment {

    int getSelectedMenuItem();

}
