package com.humanet.humanetcore.events;

import android.graphics.Bitmap;
import android.view.View;

import com.humanet.humanetcore.views.widgets.YellowProgressBar;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by ovitali on 18.02.2015.
 */
public class ProgressedImageLoadingListener implements ImageLoadingListener {

    private YellowProgressBar mProgressBar;

    public ProgressedImageLoadingListener(YellowProgressBar progressBar) {
        mProgressBar = progressBar;
    }


    @Override
    public void onLoadingStarted(String imageUri, View view) {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {

    }
}
