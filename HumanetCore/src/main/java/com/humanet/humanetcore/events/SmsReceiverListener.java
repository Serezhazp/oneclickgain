package com.humanet.humanetcore.events;

/**
 * Created by ovi on 2/10/16.
 */
public interface SmsReceiverListener {
    void sendVerificationCode(CharSequence code);
}
