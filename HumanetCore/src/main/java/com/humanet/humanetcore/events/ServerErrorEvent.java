package com.humanet.humanetcore.events;

/**
 * Created by Denis on 24.05.2015.
 */
public class ServerErrorEvent {
    public int code;
    public String message;

    public ServerErrorEvent(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
