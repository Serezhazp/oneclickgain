package com.humanet.humanetcore;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

/**
 * Created by ovi on 4/1/16.
 */
public class LeakCannaryHelper {

    public static final boolean ENABLED = true && App.DEBUG;

    private static RefWatcher sRefWatcher;

    public static void init(Application application) {
        if (ENABLED)
            sRefWatcher = LeakCanary.install(application);
    }

    public static void watch(Object target) {
        if (ENABLED)
            sRefWatcher.watch(target);
    }
}
