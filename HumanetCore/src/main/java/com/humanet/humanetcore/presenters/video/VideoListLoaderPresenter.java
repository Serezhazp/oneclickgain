package com.humanet.humanetcore.presenters.video;

import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.video.GetFlowRequest;
import com.humanet.humanetcore.api.response.video.FlowResponse;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.IVideoListView;
import com.octo.android.robospice.SpiceManager;

import java.util.HashMap;

/**
 * Created by ovi on 16.05.2016.
 */
public class VideoListLoaderPresenter {

    private SpiceManager mSpiceManager;

    private IVideoListView mVideoListView;

    private boolean mIsLastPage = false;

    private VideoType mVideoType;
    private int mReplayId;
    private int mUserId;
    private int mPetId;
    private HashMap<String, Object> mFlowRequestParams;


    public void setSpiceManager(SpiceManager spiceManager) {
        mSpiceManager = spiceManager;
        spiceManager.cancel(String[].class, "VideoPreloadRequest");
    }

    public void setVideoListView(IVideoListView videoListView) {
        mVideoListView = videoListView;
    }

    public void setVideoType(VideoType videoType) {
        mVideoType = videoType;
    }

    public void setReplayId(int replayId) {
        mReplayId = replayId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public void setPetId(int petId) {
        mPetId = petId;
    }

    public void setFlowRequestParams(HashMap<String, Object> flowRequestParams) {
        mFlowRequestParams = flowRequestParams;
    }


    public void loadPage() {
        loadPage(mVideoListView.getLastVideoId());
    }

    public void loadPage(long lastId) {
        GetFlowRequest request = new GetFlowRequest(mFlowRequestParams);
        request.saveData(mVideoListView.getScreenId(), lastId > 0);
        request.setLast(lastId);
        request.setReplyId(mReplayId);
        request.setUserId(mUserId);
        request.setPetId(mPetId);

        mSpiceManager.execute(request, new GetFlowRequestListener());
    }

    private class GetFlowRequestListener extends SimpleRequestListener<FlowResponse> {
        @Override
        public void onRequestSuccess(FlowResponse response) {
            int count = response.getFlow() == null ? 0 : response.getFlow().size();

            mIsLastPage = count < Constants.LIMIT;

            if (response.getFlow() != null && mVideoListView != null)
                mVideoListView.addVideos(response.getFlow());
        }
    }


}
