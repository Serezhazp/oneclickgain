package com.humanet.humanetcore;

import android.app.Application;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.humanet.humanetcore.api.sets.UserApiSet;
import com.humanet.humanetcore.db.DBHelper;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.model.enums.Language;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.views.utils.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.octo.android.robospice.SpiceManager;

import io.fabric.sdk.android.Fabric;

public abstract class App extends Application {

    public static String API_APP_NAME;
    public static boolean DEBUG = true;
    protected static int DATABASE_VERSION = 50;

    public static String COINS_CODE = "GC";

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static int WIDTH;
    public static int WIDTH_WITHOUT_MARGINS;
    public static int HEIGHT;
    public static int MARGIN;
    public static int IMAGE_BORDER;
    public static int IMAGE_SMALL_SIDE;

    private void initLang() {
        if (TextUtils.isEmpty(PrefHelper.getStringPref(Constants.PREF.SUFFIX))) {
            PrefHelper.setStringPref(Constants.PREF.SUFFIX, Language.getSystem().getSuffix());
        }
    }

    private static SpiceManager sSpiceManager;

    public static SpiceManager getSpiceManager() {
        if (sSpiceManager == null || !sSpiceManager.isStarted()) {
            sSpiceManager = new SpiceManager(NavigationManager.getSpiceServiceClass());
            sSpiceManager.start(instance);
        }
        return sSpiceManager;
    }

    //---

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        LeakCannaryHelper.init(this);

        if (!DEBUG) {
            final Fabric fabric = new Fabric.Builder(this)
                    .kits(new Crashlytics())
                    .debuggable(true)
                    .build();
            Fabric.with(fabric);
        }


        PrefHelper.init(this);

        DBHelper.getInstance(this);

        initImageLoader();
        initLang();


        //Ubertesters.initialize(this);
    }

    private void initImageLoader() {
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .displayer(new CircleBitmapDisplayer())
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(2)
                .threadPriority(Thread.MIN_PRIORITY + 1)
                .defaultDisplayImageOptions(displayImageOptions)
                .imageDownloader(new com.humanet.humanetcore.utils.ImageDownloader(this))
                .build();
        ImageLoader.getInstance().init(config);
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        ImageLoader.getInstance().clearMemoryCache();
        System.gc();
    }

    public static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    public abstract String getServerEndpoint();

    public Class<? extends UserInfo> getUserClass(){
        return UserInfo.class;
    }

    public Class getUserApiSetClass(){
        return UserApiSet.class;
    }

    //--- navigation


}
