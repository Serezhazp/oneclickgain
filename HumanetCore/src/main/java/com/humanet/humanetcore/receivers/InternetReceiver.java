package com.humanet.humanetcore.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.humanet.humanetcore.jobs.UploadVideoJob;
import com.humanet.humanetcore.utils.NetworkUtil;

public abstract class InternetReceiver extends BroadcastReceiver {
    public InternetReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean offlineMode = NetworkUtil.isOfflineMode(context);

        if (!offlineMode) {
            UploadVideoJob.startUploader();
        }
    }

}
