package com.humanet.humanetcore.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.humanet.humanetcore.events.SmsReceiverListener;

import java.util.Locale;

/**
 * Created by ovi on 2/10/16.
 */
public class SmsReceiver extends WakefulBroadcastReceiver {

    public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";


    private SmsReceiverListener mBaseRegistrationActivity;

    public SmsReceiver(SmsReceiverListener baseRegistrationActivity) {
        mBaseRegistrationActivity = baseRegistrationActivity;
    }


    private String getCodeFromSms(String body) {
        String[] words = body.split("\\s+");
        if (words.length > 0) {
            for (String word : words) {
                if (TextUtils.isDigitsOnly(word)) {
                    return word;
                }
            }
        }
        return null;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SMS_RECEIVED)) {
            SmsMessage[] msgs = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            } else {
                Bundle data = intent.getExtras();
                if (data != null) {
                    Object[] pdus = (Object[]) data.get("pdus");
                    if (pdus != null) {
                        msgs = new SmsMessage[pdus.length];
                        for (int i = 0; i < msgs.length; i++) {
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }
                    }
                } else {
                    return;
                }
            }

            String code = "";
            if (msgs != null) {
                String body = msgs[0].getMessageBody().toLowerCase(Locale.getDefault());
                boolean contains = body.contains("thehumanet") || body.contains("1clickgain");
                if (contains) {
                    code = getCodeFromSms(body);
                    if (!TextUtils.isEmpty(code)) {
                        mBaseRegistrationActivity.sendVerificationCode(code);
                    }
                }
            }
        }
    }
}