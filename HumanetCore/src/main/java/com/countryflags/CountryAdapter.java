/*
 * Copyright (c) 2014-2015 Amberfog.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.countryflags;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.humanet.humanetcore.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CountryAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater mLayoutInflater;

    private List<Country> mFilteredList = new ArrayList<>();
    private List<Country> mList = new ArrayList<>();

    public CountryAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
    }

    public <T extends Comparable<? super T>> void addAll(List<Country> collection) {
        Collections.sort(collection);
        mList.addAll(collection);
        mFilteredList.addAll(collection);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mFilteredList.size();
    }

    @Override
    public Country getItem(int position) {
        return mFilteredList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_country_drop, parent, false);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) convertView.findViewById(R.id.image);
            holder.mNameView = (TextView) convertView.findViewById(R.id.country_name);
            holder.mCodeView = (TextView) convertView.findViewById(R.id.country_code);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Country country = getItem(position);
        if (country != null) {
            holder.mNameView.setText(country.getName());
            holder.mCodeView.setText(country.getCountryCodeStr());

            ImageLoader.getInstance().displayImage(country.getResId(), holder.mImageView, new DisplayImageOptions.Builder().build());
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                List<Country> filteredList = new ArrayList<>();
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;

                if (TextUtils.isEmpty(constraint)) {
                    for (int i = 0; i < mList.size(); i++)
                        filteredList.add(mList.get(i));
                } else {
                    String s = constraint.toString();
                    for (int i = 0; i < mList.size(); i++) {
                        Country country = mList.get(i);
                        if (country.getCountryCodeStr().startsWith(s))
                            filteredList.add(country);
                    }
                }

                filterResults.count = filteredList.size();

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredList = (List<Country>) results.values;
                notifyDataSetChanged();
                //notifyDataSetInvalidated();
            }
        };
    }

    private static class ViewHolder {
        public ImageView mImageView;
        public TextView mNameView;
        public TextView mCodeView;
    }
}
