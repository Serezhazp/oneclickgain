package com.henesiz.fragments.registration;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.henesiz.R;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;

public class OkFragment extends BaseTitledFragment implements View.OnClickListener {

    private int mAction;

    private String mShareUrl;
    private int mVideoId;

    public static OkFragment newInstance(int action, String shareUrl, int videoId) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_ACTION, action);
        args.putString("shareUrl", shareUrl);
        args.putInt("videoId", videoId);
        fragment.setArguments(args);
        return fragment;
    }

    public static OkFragment newInstance(int action) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }

    private OnCongratulationListener mOpenVerificationListener;

    public OkFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ok, container, false);

        mAction = getArguments().getInt(Constants.EXTRA_ACTION);
        switch (mAction) {
            case Constants.CongratulationsType.VERIFY_OK:
                ((TextView) view.findViewById(R.id.congratulationTextView)).setText(R.string.ok_verification);
                ((TextView) view.findViewById(R.id.title)).setText("");
                break;
        }
        view.findViewById(R.id.btn_next).setOnClickListener(this);

        return view;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_next) {
            nextScreen();
        }
    }

    private void nextScreen() {
        mOpenVerificationListener.onCongratulationViewed(mAction);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOpenVerificationListener = (OnCongratulationListener) activity;
    }

    public interface OnCongratulationListener {
        void onCongratulationViewed(int action);
    }


}
