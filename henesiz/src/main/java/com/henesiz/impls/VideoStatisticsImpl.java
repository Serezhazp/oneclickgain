package com.henesiz.impls;

import com.henesiz.R;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovi on 2/23/16.
 */
public class VideoStatisticsImpl implements VideoStatistic.VideoStatisticDelegator {

    @Override
    public VideoStatistic create(VideoStatistic videoStatistic, VideoType videoType, int duration) {
        switch (videoType) {
            case DIY_SHARE_ITEM:
            case DIY_SHARE_SKILL:
            case DIY_ASK_ITEM:
            case DIY_ASK_SKILL:
                videoStatistic.setImageResId("assets://statistics/diy.png");
                videoStatistic.setTitleResId(R.string.diy_title);
                break;
            case VLOG:
                videoStatistic.setImageResId("assets://statistics/vlog.png");
                videoStatistic.setTitleResId(R.string.online_tab_vlog);
                break;
            case NEWS:
                videoStatistic.setImageResId("assets://statistics/news.png");
                videoStatistic.setTitleResId(R.string.online_tab_vlog);
                break;
            case CROWD_ASK:
            case CROWD_GIVE:
                videoStatistic.setImageResId("assets://statistics/crowd.png");
                videoStatistic.setTitleResId(R.string.crowd);
                break;
            case ALIEN_LOOK_TASK:
            case ALIEN_LOOK_CHALLENGE:
                videoStatistic.setImageResId("assets://statistics/alien_look.png");
                videoStatistic.setTitleResId(R.string.menu_alien_look);
                break;
        }

        return videoStatistic;
    }
}
