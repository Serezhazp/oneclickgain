package com.henesiz.impls;

import android.content.Context;

import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;

/**
 * Created by ovi on 3/31/16.
 */
public class RatingImpl implements RatingsTabFragment.RatingTabView {

    public static final String LOCAL = "local";


    private Context mContext;

    public RatingImpl(Context context) {
        mContext = context;
    }

    @Override
    public String[] getTabNames() {
        return new String[]{
                mContext.getString(com.humanet.humanetcore.R.string.rating_type_local),
                mContext.getString(com.humanet.humanetcore.R.string.rating_type_global),
        };
    }

    @Override
    public int getDefaultTabPosition() {
        return 0;
    }

    @Override
    public String getRatingType(int tabPosition) {
        switch (tabPosition) {
            case 0:
                return LOCAL;
        }
        return null;
    }
}
