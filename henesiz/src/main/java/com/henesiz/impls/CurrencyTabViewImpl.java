package com.henesiz.impls;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.InternalCurrencyFragment;
import com.humanet.humanetcore.interfaces.OnBalanceChanged;

/**
 * Created by ovi on 4/1/16.
 */
public final class CurrencyTabViewImpl implements CurrencyFragment.CurrencyTabView {

    private Context mContext;

    private OnBalanceChanged mOnBalanceChanged;


    public CurrencyTabViewImpl(Context context) {
        mContext = context;
    }

    public void setOnBalanceChangedListener(OnBalanceChanged onBalanceChanged) {
        mOnBalanceChanged = onBalanceChanged;
    }

    @Override
    public String[] getTabNames() {
        return new String[]{
                mContext.getString(com.humanet.humanetcore.R.string.balance_currency_tab_internal),
                //   mContext.getString(com.humanet.humanetcore.R.string.balance_currency_tab_external),
        };
    }

    @Override
    public int getDefaultTabPosition() {
        return 0;
    }

    @Override
    public int getAddBalanceTabPosition() {
        return 1;
    }

    @Override
    public Fragment getFragmentAtPosition(int position) {
        switch (position) {
            case 0:
                InternalCurrencyFragment fragment = new InternalCurrencyFragment();
                Bundle bundle = new Bundle(2);
                bundle.putInt("position", position);
                bundle.putString("type", "local");
                fragment.setArguments(bundle);
                return fragment;

            default:
                throw new RuntimeException("undefined balance tab position");
        }
    }
}
