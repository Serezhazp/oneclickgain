package com.henesiz;

import android.support.v4.app.Fragment;

import com.henesiz.activities.AgreementActivity;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.activities.CommentsActivity;
import com.henesiz.activities.ContactsActivity;
import com.henesiz.activities.EditProfileActivity;
import com.henesiz.activities.InputActivity;
import com.henesiz.activities.PlayFlowActivity;
import com.henesiz.activities.SearchActivity;
import com.henesiz.activities.TokenActivity;
import com.henesiz.service.AppService;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.activities.PushActivity;
import com.humanet.humanetcore.fragments.capture.VideoDescriptionFragment;
import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.humanet.humanetcore.fragments.profile.ProfileFragment;
import com.octo.android.robospice.SpiceService;

/**
 * Created by ovi on 24.05.2016.
 */
public class NavigationManagerImpl implements NavigationManager.Delegate {
    @Override
    public Class getPlayFlowActivityClass() {
        return PlayFlowActivity.class;
    }

    @Override
    public Class getCommentsActivityClass() {
        return CommentsActivity.class;
    }

    @Override
    public Class getAgreementActivityClass() {
        return AgreementActivity.class;
    }

    @Override
    public Class getSearchActivityClass() {
        return SearchActivity.class;
    }

    @Override
    public Class getInputActivityClass() {
        return InputActivity.class;
    }

    @Override
    public Class getTokenActivityClass() {
        return TokenActivity.class;
    }

    @Override
    public Class getContactsActivityClass() {
        return ContactsActivity.class;
    }

    @Override
    public Class getEditProfileActivityClass() {
        return EditProfileActivity.class;
    }

    @Override
    public Class getCaptureActivityClass() {
        return CaptureActivity.class;
    }

    @Override
    public Class getPushActivityClass() {
        return PushActivity.class;
    }

    @Override
    public Class<? extends SpiceService> getSpiceServiceClass() {
        return AppService.class;
    }

    @Override
    public Class<? extends BaseViewProfileFragment> getViewProfileFragmentClass() {
        return ProfileFragment.class;
    }

    @Override
    public Class<? extends Fragment> getVideoDescriptionFragmentClass() {
        return VideoDescriptionFragment.class;
    }
}
