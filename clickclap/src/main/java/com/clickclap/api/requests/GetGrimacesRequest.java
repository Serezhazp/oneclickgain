package com.clickclap.api.requests;

import android.content.ContentValues;

import com.clickclap.ClickclapApp;
import com.clickclap.api.responses.GrimacesListResponse;
import com.clickclap.api.sets.GrimaceRestApi;
import com.clickclap.models.Grimace;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GetGrimacesRequest extends RetrofitSpiceRequest<GrimacesListResponse, GrimaceRestApi> {
    private int mType;
    private int mUserId;

    public GetGrimacesRequest(int type, int userId) {
        super(GrimacesListResponse.class, GrimaceRestApi.class);
        mType = type;
        mUserId = userId;
    }

    public GetGrimacesRequest(int type) {
        super(GrimacesListResponse.class, GrimaceRestApi.class);
        mType = type;
        mUserId = 0;
    }

    @Override
    public GrimacesListResponse loadDataFromNetwork() throws Exception {
        ArgsMap args = new ArgsMap(true);
        switch (mType) {
            case Grimace.TYPE_COLLECTION:
                args.put("type", "collection");
                break;
            case Grimace.TYPE_USER:
                args.put("type", "user");
                break;
            case Grimace.TYPE_MARKET:
                args.put("type", "market");
                break;
        }

        if (mUserId != 0 && mUserId != AppUser.getInstance().getUid()) {
            args.put("id_user", mUserId);
        }

        GrimacesListResponse response = getService().getGrimaces(ClickclapApp.API_APP_NAME,args);

        if (response != null && response.isSuccess()) {
            ClickclapApp.getInstance().getContentResolver().delete(ContentDescriptor.Grimaces.URI,
                    ContentDescriptor.Grimaces.Cols.TYPE + " = " + mType, null);

            List<Grimace> list = response.getGrimaces();

            if (list != null) {
                int count = list.size();
                ContentValues[] contentValueses = new ContentValues[count];

                for (int i = 0; i < count; i++) {
                    ContentValues values = list.get(i).toContentValues();
                    values.put(ContentDescriptor.Grimaces.Cols.TYPE, mType);
                    contentValueses[i] = values;
                }

                ClickclapApp.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Grimaces.URI, contentValueses);
            }
        }

        return response;
    }
}
