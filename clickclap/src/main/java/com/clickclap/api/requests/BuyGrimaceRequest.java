package com.clickclap.api.requests;

import android.content.ContentValues;

import com.clickclap.ClickclapApp;
import com.clickclap.api.sets.GrimaceRestApi;
import com.humanet.humanetcore.api.ArgsMap;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class BuyGrimaceRequest extends RetrofitSpiceRequest<BaseResponse, GrimaceRestApi> {
    int mGrimaceId;

    public BuyGrimaceRequest(int grimaceId) {
        super(BaseResponse.class, GrimaceRestApi.class);
        mGrimaceId = grimaceId;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_grimace", mGrimaceId);

        BaseResponse response = getService().buyGrimace(ClickclapApp.API_APP_NAME,map);

        ContentValues values = new ContentValues(1);
        if (response != null && response.isSuccess()) {
            values.put(ContentDescriptor.Grimaces.Cols.BOUGHT, 1);
        } else {
            values.put(ContentDescriptor.Grimaces.Cols.BOUGHT, 0);
        }
        ClickclapApp.getInstance().getContentResolver().update(ContentDescriptor.Grimaces.URI,
                values,
                ContentDescriptor.Grimaces.Cols.ID + " = " + mGrimaceId,
                null);

        return response;
    }
}
