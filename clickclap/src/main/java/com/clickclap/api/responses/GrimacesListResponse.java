package com.clickclap.api.responses;

import com.clickclap.models.Grimace;
import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

import java.util.List;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GrimacesListResponse extends BaseResponse {
    @SerializedName("grimace")
    List<Grimace> mGrimaces;

    public List<Grimace> getGrimaces() {
        return mGrimaces;
    }
}