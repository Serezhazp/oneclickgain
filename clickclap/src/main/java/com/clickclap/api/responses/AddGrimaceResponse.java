package com.clickclap.api.responses;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.api.response.BaseResponse;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddGrimaceResponse extends BaseResponse {
    @SerializedName("id")
    int mId;

    public int getId() {
        return mId;
    }
}