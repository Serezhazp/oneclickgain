package com.clickclap.job;

import android.database.Cursor;

import com.clickclap.ClickclapApp;
import com.clickclap.api.requests.GrimaceUploadRequest;
import com.clickclap.api.responses.AddGrimaceResponse;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by ovi on 4/25/16.
 */
public class UploadGrimaceJob extends Thread {

    public static void startUploader() {
        UploadGrimaceJob uploadVideoJob = new UploadGrimaceJob();
        uploadVideoJob.setPriority(MIN_PRIORITY);
        uploadVideoJob.run();
    }

    @Override
    public void run() {
        Cursor cursor = ClickclapApp.getInstance().getContentResolver().query(ContentDescriptor.Grimaces.URI,
                null,
                ContentDescriptor.Grimaces.Cols.TO_UPLOAD + " = 1",
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                GrimaceUploadRequest uploadRequest = new GrimaceUploadRequest(id);
                uploadRequest.setPriority(SpiceRequest.PRIORITY_LOW);
                ClickclapApp.getSpiceManager().execute(uploadRequest, String.valueOf(id), DurationInMillis.ALWAYS_EXPIRED, new SimpleRequestListener<AddGrimaceResponse>());
            } while (cursor.moveToNext());

            cursor.close();
        }
    }

}
