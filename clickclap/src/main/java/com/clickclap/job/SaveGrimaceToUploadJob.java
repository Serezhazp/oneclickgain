package com.clickclap.job;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.api.requests.GrimaceUploadRequest;
import com.clickclap.api.responses.AddGrimaceResponse;
import com.clickclap.models.Grimace;
import com.humanet.filters.FilterController;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.utils.NetworkUtil;
import com.humanet.humanetcore.views.utils.ImageCroper;
import com.octo.android.robospice.persistence.DurationInMillis;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ovitali on 13.11.2015.
 */
public final class SaveGrimaceToUploadJob extends Thread {

    private VideoInfo mVideoInfo;

    public static void run(VideoInfo videoInfo) {
        new SaveGrimaceToUploadJob(videoInfo).start();
    }

    private SaveGrimaceToUploadJob(VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    @Override
    public void run() {
        long createdAt = System.currentTimeMillis();
        Grimace grimace = new Grimace();
        grimace.setAuthorId(AppUser.getInstance().getUid());

        grimace.setSmileId(mVideoInfo.getSmileId());
        grimace.setEffect(FilterController.getFilterId(mVideoInfo.getFilter()));

        grimace.setToUpload(1);

        File srcFileImage = new File(mVideoInfo.getImagePath());

        File outFileImage = new File(FilePathHelper.getUploadDirectory(), createdAt + "." + Constants.IMAGE_EXTENTION);

        Bitmap bitmap = BitmapFactory.decodeFile(srcFileImage.getAbsolutePath());
        Context context = ClickclapApp.getInstance();

        int color = context.getResources().getColor(R.color.colorPrimary);

        bitmap = ImageCroper.getCircularBitmap(bitmap,
                context.getResources().getDimensionPixelSize(R.dimen.image_border_size),
                color
        );

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outFileImage);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        } catch (IOException e) {
            Log.e("SaveVideoToUploadJob", "IOException", e);
        }

        grimace.setUrl(outFileImage.getAbsolutePath());

        ContentValues contentValues = grimace.toContentValues();
        contentValues.put(ContentDescriptor.Grimaces.Cols.TYPE, Grimace.TYPE_COLLECTION);
        Uri uri = ClickclapApp.getInstance().getContentResolver().insert(ContentDescriptor.Grimaces.URI, contentValues);

        if (!NetworkUtil.isOfflineMode()) {
            int id = Integer.parseInt(uri.getLastPathSegment());
            ClickclapApp.getSpiceManager().execute(new GrimaceUploadRequest(id), String.valueOf(id), DurationInMillis.ONE_SECOND, new SimpleRequestListener<AddGrimaceResponse>());
        }
    }
}
