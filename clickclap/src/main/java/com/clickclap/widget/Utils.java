package com.clickclap.widget;

import android.app.ActivityManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;

import com.clickclap.ClickclapApp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Denis on 28.05.2015.
 */
public class Utils {

    /**
     * first app user
     */
    public static final int AID_APP = 10000;

    /**
     * offset for uid ranges for each user
     */
    public static final int AID_USER = 100000;

    static Calendar calendar = Calendar.getInstance();

    public static String getForegroundAppFromLog() {
        File[] files = new File("/proc").listFiles();
        int lowestOomScore = Integer.MAX_VALUE;
        String foregroundProcess = null;

        for (File file : files) {
            if (!file.isDirectory()) {
                continue;
            }

            int pid;
            try {
                pid = Integer.parseInt(file.getName());
            } catch (NumberFormatException e) {
                continue;
            }

            try {
                String cgroup = read(String.format("/proc/%d/cgroup", pid));

                String[] lines = cgroup.split("\n");

                if (lines.length != 2) {
                    continue;
                }

                String cpuSubsystem = lines[0];
                String cpuaccctSubsystem = lines[1];

                if (!cpuaccctSubsystem.endsWith(Integer.toString(pid))) {
                    // not an application process
                    continue;
                }

                if (cpuSubsystem.endsWith("bg_non_interactive")) {
                    // background policy
                    continue;
                }

                String cmdline = read(String.format("/proc/%d/cmdline", pid));

                if (cmdline.contains("com.android.systemui")) {
                    continue;
                }

                int uid = Integer.parseInt(
                        cpuaccctSubsystem.split(":")[2].split("/")[1].replace("uid_", ""));
                if (uid >= 1000 && uid <= 1038) {
                    // system process
                    continue;
                }

                int appId = uid - AID_APP;
                int userId = 0;
                // loop until we get the correct user id.
                // 100000 is the offset for each user.
                while (appId > AID_USER) {
                    appId -= AID_USER;
                    userId++;
                }

                if (appId < 0) {
                    continue;
                }

                // u{user_id}_a{app_id} is used on API 17+ for multiple user account support.
                // String uidName = String.format("u%d_a%d", userId, appId);

                File oomScoreAdj = new File(String.format("/proc/%d/oom_score_adj", pid));
                if (oomScoreAdj.canRead()) {
                    int oomAdj = Integer.parseInt(read(oomScoreAdj.getAbsolutePath()));
                    if (oomAdj != 0) {
                        continue;
                    }
                }

                String score = read(String.format("/proc/%d/oom_score", pid));
                score = score.replaceAll("\\s+", "");
                int oomscore = Integer.parseInt(score);
                if (oomscore < lowestOomScore) {
                    lowestOomScore = oomscore;
                    foregroundProcess = cmdline;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return foregroundProcess;
    }

    private static String read(String path) throws IOException {
        StringBuilder output = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(path));
        output.append(reader.readLine());
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            output.append('\n').append(line);
        }
        reader.close();
        return output.toString();
    }

    public static String getRunningTask(Context context) {
        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> processes = mActivityManager.getRunningTasks(1);
        if (processes != null && processes.size() > 0) {
            return processes.get(0).baseActivity.getPackageName();
        }
        return null;
    }

    public static String getCurLaunchedAppPackageName(Context context) {
        String mPackageName = "";
        if (Build.VERSION.SDK_INT < 21) {
            mPackageName = getRunningTask(context);
        } else {
            mPackageName = getForegroundAppFromLog();
        }

        if (mPackageName != null) {
            mPackageName = mPackageName.replaceAll("[^\\x20-\\x7E]", "");
        }

//        getPackageNameFromStatistics();
        return mPackageName;
    }

    public static void getPackageNameFromStatistics() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager usm = (UsageStatsManager) ClickclapApp.getInstance().getSystemService(Context.USAGE_STATS_SERVICE);
            long endTime = calendar.getTimeInMillis();
            calendar.add(Calendar.SECOND, -1);
            long startTime = calendar.getTimeInMillis();

            UsageEvents uEvents;
            uEvents = usm.queryEvents(startTime, endTime);

            if (uEvents != null && uEvents.hasNextEvent()) {
                UsageEvents.Event e = new UsageEvents.Event();
                uEvents.getNextEvent(e);
                Log.d("usage", "Event: " + e.getPackageName() + "\t" + e.getTimeStamp());
            }
        }
    }

    public static boolean isScreenOn() {
        Context context = ClickclapApp.getInstance();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            DisplayManager dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
            for (Display display : dm.getDisplays()) {
                if (display.getState() != Display.STATE_OFF) {
                    return true;
                }
            }
            return false;
        } else {
            // If you use less than API20:
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            return powerManager.isScreenOn();
        }
    }
}
