package com.clickclap.widget.helper;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.humanet.humanetcore.App;

public class PrefHelper {

    public static synchronized String getStringPreference(String name) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        String uid = settings.getString(name, null);
        return uid;
    }

    public static synchronized void setStringPreference(String name, String value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static synchronized float getFloatPreference(String name) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        float coord = settings.getFloat(name, 0);
        return coord;
    }

    public static synchronized void setFloatPreference(String name, float value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        Editor editor = settings.edit();
        editor.putFloat(name, value);
        editor.apply();
    }

    public static synchronized void setDoublePreference(String name, double value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        Editor editor = settings.edit();
        editor.putFloat(name, (float) value);
        editor.apply();
    }

    public static synchronized int getIntPreference(String name) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        int result = settings.getInt(name, 0);
        return result;
    }

    public static synchronized void setIntPreference(String name, int value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        Editor editor = settings.edit();
        editor.putInt(name, value);
        editor.apply();
    }

    public static synchronized long getLongPreference(String name) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        long result = settings.getLong(name, 0);
        return result;
    }

    public static synchronized void setLongPreference(String name, long value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        Editor editor = settings.edit();
        editor.putLong(name, value);
        editor.apply();
    }

    public static synchronized void setBooleanPreference(String name, boolean value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        Editor editor = settings.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    public static synchronized boolean getBooleanPreference(String name) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        return preferences.getBoolean(name, false);
    }

    public static void setHided(boolean hided) {
        setBooleanPreference("hided", hided);
    }

    public static boolean isHided() {
        return getBooleanPreference("hided");
    }
}
