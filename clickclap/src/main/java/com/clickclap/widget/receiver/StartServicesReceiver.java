package com.clickclap.widget.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.clickclap.widget.service.BackgroundService;

public class StartServicesReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent appDetectionService = new Intent(context, BackgroundService.class);
        context.startService(appDetectionService);
    }
}