package com.clickclap.widget.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.widget.helper.PrefHelper;

/**
 * Created by ovi on 31.05.2016.
 */
class WidgetButton extends ImageView implements View.OnTouchListener {

    public static final String POSITION_X = "x";
    public static final String POSITION_Y = "y";

    private final int mHideAreaHeight;
    private final int mButtonSize;

    private final int mMoveStep;

    private boolean mVisible;

    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;

    boolean mMoved;

    private int mAvailableAreaWidth;
    private int mAvailableAreaHeight;


    private OpenWidgetListener mOpenWidgetListener;

    private OnWidgetButtonMoveListener mOnWidgetButtonMoveListener;


    public WidgetButton(Context context) {
        this(context, null);
    }

    public WidgetButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        setImageResource(R.drawable.widget_button);
        setScaleType(ScaleType.FIT_XY);

        mAvailableAreaWidth = Widget.getSize(context).x;
        mAvailableAreaHeight = Widget.getSize(context).y;

        mButtonSize = context.getResources().getDimensionPixelSize(R.dimen.button_size);
        mHideAreaHeight = (int) (mButtonSize * 1.1);

        setOnTouchListener(this);

        mMoveStep = mAvailableAreaWidth / 7;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(mButtonSize, mButtonSize);
    }

    public void hide() {
        mVisible = false;
        setVisibility(GONE);
    }

    public void show() {
        mVisible = true;
        setVisibility(VISIBLE);
    }

    /**
     * set initial button position.
     * make sure #setOnWidgetButtonMoveListener had called first
     */
    public void setInitialPosition() {
        int mInitialPositionX = (int) PrefHelper.getFloatPreference(POSITION_X);
        int mInitialPositionY = (int) PrefHelper.getFloatPreference(POSITION_Y);

        if (mInitialPositionY == 0) {
            mInitialPositionY = (int) (mAvailableAreaHeight - mButtonSize / 2 - ClickclapApp.getInstance().getResources().getDimension(R.dimen.button_size_bottom_offset));
        }

        mOnWidgetButtonMoveListener.onWidgetButtonMoved(mInitialPositionX, mInitialPositionY);
    }

    private void checkHideArea() {
        // check out screen position
       /* if (mCurrentY + mButtonSize < mHideAreaHeight) {
            PrefHelper.setFloatPreference(POSITION_X, 0);
            PrefHelper.setFloatPreference(POSITION_Y, 0);
            hide();
            PrefHelper.setHided(true);
        }*/
    }

    public int getButtonSize() {
        return mButtonSize;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            mMoved = false;
            initialX = mOnWidgetButtonMoveListener.getWidgetXPosition();
            initialY = mOnWidgetButtonMoveListener.getWidgetYPosition();
            initialTouchX = event.getRawX();
            initialTouchY = event.getRawY();
        } else if (action == MotionEvent.ACTION_MOVE) {

            int dX = (int) (event.getRawX() - initialTouchX);
            int dY = (int) (event.getRawY() - initialTouchY);

            int newX = initialX + dX;
            int newY = initialY + dY;

            if (Math.abs(dX) > mMoveStep / 2 || Math.abs(dY) > mMoveStep / 2)
                mMoved = true;

            mOnWidgetButtonMoveListener.onWidgetButtonMoved(newX, newY);
        } else if (action == MotionEvent.ACTION_UP) {
            if (!mMoved) {
                mOpenWidgetListener.onOpenWidget();
            }

            int newX = mOnWidgetButtonMoveListener.getWidgetXPosition();
            int newY = mOnWidgetButtonMoveListener.getWidgetYPosition();

            newX = (int) (Math.ceil((double) newX / (double) mMoveStep) * (double) mMoveStep);
            newY = (int) (Math.ceil((double) newY / (double) mMoveStep) * (double) mMoveStep);

            mOnWidgetButtonMoveListener.onWidgetButtonMoved(newX, newY);

            PrefHelper.setFloatPreference(POSITION_X, newX);
            PrefHelper.setFloatPreference(POSITION_Y, newY);
        }
        return true;
    }


    public void setOpenWidgetListener(OpenWidgetListener openWidgetListener) {
        mOpenWidgetListener = openWidgetListener;
    }

    public void setOnWidgetButtonMoveListener(OnWidgetButtonMoveListener onWidgetButtonMoveListener) {
        mOnWidgetButtonMoveListener = onWidgetButtonMoveListener;
    }

    public boolean isVisible() {
        return mVisible;
    }
}
