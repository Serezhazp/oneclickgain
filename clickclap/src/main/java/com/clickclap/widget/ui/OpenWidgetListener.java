package com.clickclap.widget.ui;

/**
 * Created by ovi on 31.05.2016.
 */
interface OpenWidgetListener {
    void onOpenWidget();
}
