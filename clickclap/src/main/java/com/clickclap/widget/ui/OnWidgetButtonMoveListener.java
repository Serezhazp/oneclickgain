package com.clickclap.widget.ui;

/**
 * Created by ovi on 31.05.2016.
 */
interface OnWidgetButtonMoveListener {

    void onWidgetButtonMoved(int x, int y);

    int getWidgetXPosition();
    int getWidgetYPosition();

}
