package com.clickclap.widgets.grimaces;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.models.Grimace;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

/**
 * Created by Denis on 01.04.2015.
 */
public class GrimaceCircleView extends FrameLayout {
    private static final int BLACKOUT_COLOR = Color.parseColor("#88000000");
    private static final int BORDER_SIZE = (int) ConverterUtil.dpToPix(ClickclapApp.getInstance(), 3);

    private Grimace mGrimace;
    private int mType;

    private View mShadeView;
    private CircleImageView mImageView;

    public GrimaceCircleView(Context context) {
        super(context);
        init();
    }

    public GrimaceCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GrimaceCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mImageView = new CircleImageView(getContext());
        mImageView.setBackgroundResource(R.drawable.yellow_ring);
        mImageView.setPadding(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE);
        addView(mImageView, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mShadeView = new View(getContext());
        mShadeView.setBackgroundColor(BLACKOUT_COLOR);
        addView(mShadeView, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void setGrimace(Grimace grimace) {
        mGrimace = grimace;
        update();
    }

    public void setType(int type) {
        mType = type;
        update();
    }

    private void update() {
        if (mGrimace != null) {
            mImageView.setVisibility(View.VISIBLE);
            new DisplayImageTask().execute();
        } else {
            mImageView.setVisibility(View.INVISIBLE);
        }
        if (mGrimace != null) {
            if (mType == Grimace.TYPE_USER && !mGrimace.isSelling()) {
                setBlackout(true);
            } else if (mType == Grimace.TYPE_MARKET && !mGrimace.isBought()) {
                setBlackout(true);
            } else {
                setBlackout(false);
            }
        }
    }

    public void setBlackout(boolean isBlackout) {
        if (isBlackout) {
            mShadeView.setVisibility(View.VISIBLE);
        } else {
            mShadeView.setVisibility(View.GONE);
        }
    }

    private class DisplayImageTask extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            File file = FilePathHelper.getGrimaceFile(mGrimace.getUrl());
            Bitmap bitmap = null;
            if (file.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                bitmap = BitmapFactory.decodeFile(file.getPath(), options);
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                mImageView.setImageBitmap(bitmap);
            } else {
                String image = mGrimace.getUrl();
                if (!image.equals("0")) {
                    if (!image.contains("http")) {
                        image = "file://" + image;
                    }
                    ImageLoader.getInstance().displayImage(image, mImageView);
                }
            }
        }
    }
}
