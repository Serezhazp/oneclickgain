package com.clickclap.widgets.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.RelativeLayout;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.model.UserInfo;

/**
 * Created by michael on 25.11.14.
 */
public class ActivateWidgetDialogFragment extends DialogFragment implements View.OnClickListener {


    public static ActivateWidgetDialogFragment newInstance() {
        ActivateWidgetDialogFragment fragment = new ActivateWidgetDialogFragment();
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        View view = View.inflate(getActivity(), R.layout.fragment_activate_widget, new RelativeLayout(getActivity()));
        View nextButton = view.findViewById(R.id.btn_nav);
        nextButton.setOnClickListener(this);
        return new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(view)
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nav:
                UserInfo user = AppUser.getInstance().get();
                user.setWidget(true);
                AppUser.getInstance().set(user);
                ClickclapApp.startWidget(getActivity());
                ClickclapApp.getSpiceManager().execute(new EditUserInfoRequest(EditUserInfoRequest.userInfoToArgsMap(user)), new SimpleRequestListener<>());
                dismiss();
                break;
        }
    }
}
