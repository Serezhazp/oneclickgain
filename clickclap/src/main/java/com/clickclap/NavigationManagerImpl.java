package com.clickclap;

import android.support.v4.app.Fragment;

import com.clickclap.activities.AgreementActivity;
import com.clickclap.activities.CaptureActivity;
import com.clickclap.activities.CommentsActivity;
import com.clickclap.activities.ContactsActivity;
import com.clickclap.activities.EditProfileActivity;
import com.clickclap.activities.InputActivity;
import com.clickclap.activities.PlayFlowActivity;
import com.clickclap.activities.SearchActivity;
import com.clickclap.activities.TokenActivity;
import com.clickclap.fragments.CCProfileFragment;
import com.clickclap.service.AppService;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.activities.PushActivity;
import com.humanet.humanetcore.fragments.capture.VideoDescriptionFragment;
import com.humanet.humanetcore.fragments.profile.BaseViewProfileFragment;
import com.octo.android.robospice.SpiceService;

/**
 * Created by ovi on 24.05.2016.
 */
public class NavigationManagerImpl implements NavigationManager.Delegate {
    @Override
    public Class getPlayFlowActivityClass() {
        return PlayFlowActivity.class;
    }

    @Override
    public Class getCommentsActivityClass() {
        return CommentsActivity.class;
    }

    @Override
    public Class getAgreementActivityClass() {
        return AgreementActivity.class;
    }

    @Override
    public Class getSearchActivityClass() {
        return SearchActivity.class;
    }

    @Override
    public Class getInputActivityClass() {
        return InputActivity.class;
    }

    @Override
    public Class getTokenActivityClass() {
        return TokenActivity.class;
    }

    @Override
    public Class getContactsActivityClass() {
        return ContactsActivity.class;
    }

    @Override
    public Class getEditProfileActivityClass() {
        return EditProfileActivity.class;
    }

    @Override
    public Class getCaptureActivityClass() {
        return CaptureActivity.class;
    }

    @Override
    public Class getPushActivityClass() {
        return PushActivity.class;
    }

    @Override
    public Class<? extends SpiceService> getSpiceServiceClass() {
        return AppService.class;
    }

    @Override
    public Class<? extends BaseViewProfileFragment> getViewProfileFragmentClass() {
        return CCProfileFragment.class;
    }

    @Override
    public Class<? extends Fragment> getVideoDescriptionFragmentClass() {
        return VideoDescriptionFragment.class;
    }
}
