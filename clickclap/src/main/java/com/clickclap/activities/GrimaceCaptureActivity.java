package com.clickclap.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.clickclap.fragments.grimaces.GrimaceCapturedPreviewFragment;
import com.clickclap.fragments.video.VideoRecordFragment;
import com.clickclap.job.SaveGrimaceToUploadJob;
import com.humanet.filters.videofilter.BaseFilter;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.fragments.capture.FilterFragment;
import com.humanet.humanetcore.fragments.capture.FramePreviewFragment;
import com.humanet.humanetcore.jobs.SaveVideoToUploadJob;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.utils.ToolbarHelper;

/**
 * Created by ovi on 23.05.2016.
 */
public class GrimaceCaptureActivity extends BaseActivity implements VideoRecordFragment.OnVideoRecordCompleteListener, GrimaceCapturedPreviewFragment.OnGrimaceVideoCommandSelected {


    public static void startNewInstance(Activity activity, int requestCode, int smileId) {
        Intent intent = new Intent(activity, GrimaceCaptureActivity.class);
        intent.putExtra("smileId", smileId);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            NewVideoInfo.init();
        } else {
            VideoInfo video = (VideoInfo) savedInstanceState.get("video");
            NewVideoInfo.set(video);
        }

        NewVideoInfo.get().setSmileId(getIntent().getIntExtra("smileId", 0));

        setContentView(com.humanet.humanetcore.R.layout.activity_base_fragment_container);

        toolbar = ToolbarHelper.createToolbar(this);
        toolbar.setVisibility(View.GONE);

        startFragment(VideoRecordFragment.newInstance(NewVideoInfo.get().getSmileId()), false);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("video", NewVideoInfo.get());
    }

    @Override
    public void onVideoRecordComplete(String videoPath, int cameraId) {
        NewVideoInfo.get().setAudioPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        NewVideoInfo.get().setFilter(new BaseFilter());
        NewVideoInfo.get().setAudioApplied(NewVideoInfo.get().getAudioPath());
        NewVideoInfo.get().setImageFilterApplied(null);
        NewVideoInfo.get().setVideoFilterApplied(null);
        NewVideoInfo.get().setCameraId(cameraId);

        int framesCount = FilePathHelper.getVideoFrameFolder().listFiles().length;
        NewVideoInfo.get().setOriginalImagePath(FilePathHelper.getVideoFrameImagePath(Math.max(framesCount / 2 - 1, 0)));
        NewVideoInfo.get().setOriginalVideoPath(videoPath);

        GrimaceCapturedPreviewFragment.VideoProcessTask.getInstance().mPreviewBitmap = null;

        NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());
        NewVideoInfo.grabFramesPack();

        startFragment(new GrimaceCapturedPreviewFragment(), true);


    }


    @Override
    public void onOpenFramePreview() {
        startFragment(new GrimaceFramePreviewFragment(), true);
    }

    @Override
    public void onOpenFilter() {
        startFragment(new GrimaceFilterFragment(), true);
    }

    @Override
    public boolean onComplete(int action) {
        SaveGrimaceToUploadJob.run(NewVideoInfo.get());
        SaveVideoToUploadJob saveVideoToUploadJob = new SaveVideoToUploadJob(NewVideoInfo.get());
        saveVideoToUploadJob.start();
        finish();
        return false;
    }

    public static class GrimaceFramePreviewFragment extends FramePreviewFragment {
        @Override
        public boolean isCanPlayVideo() {
            return false;
        }
    }

    public static class GrimaceFilterFragment extends FilterFragment {
        @Override
        public boolean isCanPlayVideo() {
            return false;
        }
    }
}
