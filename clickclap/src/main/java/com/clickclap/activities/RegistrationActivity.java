package com.clickclap.activities;

import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.clickclap.fragments.OkFragment;
import com.clickclap.fragments.registration.ProfilePrivateInfoFragment;
import com.clickclap.fragments.registration.VerificationFragment;
import com.clickclap.fragments.video.SelectEmoticonFragment;
import com.clickclap.fragments.video.VideoRecordFragment;
import com.humanet.audio.MoveFilesJob;
import com.humanet.filters.videofilter.BaseFilter;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.activities.BaseRegistrationActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.requests.database.GetCountryRequest;
import com.humanet.humanetcore.api.requests.user.AuthRequest;
import com.humanet.humanetcore.api.requests.user.EditUserInfoRequest;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.api.response.user.AuthResponse;
import com.humanet.humanetcore.fragments.capture.AudioFragment;
import com.humanet.humanetcore.fragments.capture.FilterFragment;
import com.humanet.humanetcore.fragments.capture.FramePreviewFragment;
import com.humanet.humanetcore.fragments.capture.VideoPreviewFragment;
import com.humanet.humanetcore.fragments.profile.BaseProfileFragment;
import com.humanet.humanetcore.jobs.SaveVideoToUploadJob;
import com.humanet.humanetcore.model.Country;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.utils.LocationWatcher;
import com.humanet.humanetcore.utils.PrefHelper;
import com.humanet.humanetcore.views.dialogs.MissedPermissionDialog;

import java.io.File;

/**
 * Created by ovi on 20.05.2016.
 * <p>
 * registration order:"
 * 1) select emoticon
 * 2) record video
 * 3) open preview
 * 3.1) select frame
 * 3.2) select track
 * 3.3) select filter
 * 4) upload and show ok screen
 * 4.1) share and show share description
 * 5)verify phone number
 * 6) fill profile
 * 7) show ok screen
 * 8) redirect to main activity
 */
public class RegistrationActivity extends BaseRegistrationActivity implements
        LocationWatcher.GetLocationListener,
        SelectEmoticonFragment.OnSelectSmileListener,
        VideoRecordFragment.OnVideoRecordCompleteListener,
        OkFragment.OnCongratulationListener,
        VideoPreviewFragment.OnVideoCommandSelected,
        ProfilePrivateInfoFragment.OnFillMainProfileListener,
        BaseProfileFragment.OnProfileFillListener {

    private UserInfo mUserInfo = null;

    private LocationWatcher mLocationWatcher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (PrefHelper.getStringPref(Constants.PREF.TOKEN) == null) {
            getSpiceManager().execute(new AuthRequest(), new SimpleRequestListener<AuthResponse>() {
                @Override
                public void onRequestSuccess(final AuthResponse authResponse) {
                    getSpiceManager().execute(new GetCountryRequest(), new SimpleRequestListener<Country[]>());
                }
            });
            MoveFilesJob.run(this, FilePathHelper.getAudioDirectory());
        }

        NewVideoInfo.init();

        mUserInfo = new UserInfo();

        setContentView(com.humanet.humanetcore.R.layout.activity_base_fragment_no_toolbar_container);

        // 1
        startFragment(new SelectEmoticonFragment(), false);

        if (CaptureActivity.isAndroidEmulator()) {
            onOpenVerification();
        }
    }

    // 1
    @Override
    public void onSmileSelected(int smileId) {
        NewVideoInfo.get().setSmileId(smileId);

        // 2
        checkPermissions();
    }

    void checkPermissions() {
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED

                ) {

            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO
                    },
                    101);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                onOpenRecorder(0);
            }, 100);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    checkPermissions();

                } else {
                    MissedPermissionDialog.show(this, ((dialogInterface, i) -> finish()));
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public final void onGotLocation() {
        mLocationWatcher = null;
    }

    // 2
    @Override
    public void onVideoRecordComplete(String videoPath, int cameraId) {
        NewVideoInfo.get().setAudioPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        NewVideoInfo.get().setFilter(new BaseFilter());
        NewVideoInfo.get().setAudioApplied(NewVideoInfo.get().getAudioPath());
        NewVideoInfo.get().setImageFilterApplied(null);
        NewVideoInfo.get().setVideoFilterApplied(null);
        NewVideoInfo.get().setCameraId(cameraId);

        int framesCount = FilePathHelper.getVideoFrameFolder().listFiles().length;
        NewVideoInfo.get().setOriginalImagePath(FilePathHelper.getVideoFrameImagePath(Math.max(framesCount / 2 - 1, 0)));
        NewVideoInfo.get().setOriginalVideoPath(videoPath);
        VideoPreviewFragment.VideoProcessTask.getInstance().mPreviewBitmap = null;

        NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());
        NewVideoInfo.grabFramesPack();

        // 3
        openPreview();
    }

    // 3
    protected void openPreview() {
        startFragment(VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_CREATE), true);
    }

    // 3.1
    @Override
    public void onOpenFramePreview() {
        startFragment(FramePreviewFragment.newInstance(), true);
    }

    // 3.2
    @Override
    public void onOpenTrack() {
        startFragment(AudioFragment.newInstance(), true);
    }

    // 3.3
    @Override
    public void onOpenFilter() {
        startFragment(FilterFragment.newInstance(), true);
    }

    // 3
    @Override
    public void onOpenRecorder(int step) {
        getFragmentManager().popBackStack(AudioFragment.class.getSimpleName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().popBackStack(FramePreviewFragment.class.getSimpleName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().popBackStack(FilterFragment.class.getSimpleName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().popBackStack(VideoPreviewFragment.class.getSimpleName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

        startFragment(VideoRecordFragment.newInstance(NewVideoInfo.get().getSmileId()), true);
    }

    // 4
    @Override
    public boolean onComplete(int action) {
        Uri avatarUri = Uri.fromFile(new File(NewVideoInfo.get().getImagePath()));
        mUserInfo.setAvatar(avatarUri.toString());

        new SaveVideoToUploadJob(NewVideoInfo.get()).start();

        // run OkFragment. OkFragment will wait while video is uploading
        startFragment(OkFragment.newInstance(OkFragment.EMOTICON_OK), true);

        return false;
    }

    // 4.1
    @Override
    public void onVideoShared() {
        startFragment(OkFragment.newInstance(OkFragment.VIDEO_OK), true);
    }

    // 5
    @Override
    public void onOpenVerification() {
        startFragment(new VerificationFragment(), true);
    }

    // 5
    @Override
    public void onVerify(boolean isNewUser, int countryId) {
        // 6
        if (isNewUser) {
            startFragment(new ProfilePrivateInfoFragment(), true);
        } else {
            openMainActivity();
        }
    }

    // 6
    @Override
    public void onOpenFillProfile() {
        startFragment(new ProfilePrivateInfoFragment(), true);
    }

    @Override
    public void onRegistrationProcedureComplete() {
        // 7
        openMainActivity();
    }

    @Override
    public void onFillProfile(UserInfo userInfo) {
        // empty
    }

    @Override
    public UserInfo getUser() {
        return mUserInfo;
    }

    @Override
    public void onFillMainProfileInfo(UserInfo userInfo) {
        AnalyticsHelper.trackEvent(this, AnalyticsHelper.FILL_PROFILE_BASE);

        mUserInfo = userInfo;
        startFragment(OkFragment.newInstance(Constants.CongratulationsType.PROFILE_OK), true);
        getSpiceManager().execute(new EditUserInfoRequest(EditUserInfoRequest.userInfoToArgsMap(userInfo)), new SimpleRequestListener<BaseResponse>());
    }

    // 7
    private void openMainActivity() {
        PrefHelper.setBooleanPref(Constants.PREF.REG, true);
        finish();

        AppUser.getInstance().clear();

        MainActivity.startNewInstance(this);
    }


}
