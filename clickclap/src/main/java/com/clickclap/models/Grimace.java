package com.clickclap.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.humanet.humanetcore.db.ContentDescriptor;

import java.io.Serializable;

/**
 * Created by Denis on 31.03.2015.
 */
public class Grimace implements Serializable, Comparable<Grimace> {
    public static final int TYPE_COLLECTION = 1;
    public static final int TYPE_USER = 2;
    public static final int TYPE_MARKET = 3;

    private int mLocalId;

    @SerializedName("id")
    private int mId;

    @SerializedName("url")
    private String mUrl;

    @SerializedName("id_author")
    private int mAuthorId;

    @SerializedName("id_smile")
    private int mSmileId;

    @SerializedName("selling")
    private Boolean mSelling = false;

    @SerializedName("bought")
    private Boolean mBought = false;


    private int mEffect;

    public int useCount;

    private int mToUpload;

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(6);
        values.put(ContentDescriptor.Grimaces.Cols.ID, mId);
        values.put(ContentDescriptor.Grimaces.Cols.URL, mUrl);
        values.put(ContentDescriptor.Grimaces.Cols.ID_AUTHOR, mAuthorId);
        values.put(ContentDescriptor.Grimaces.Cols.ID_SMILE, mSmileId <= 0 ? 1 : mSmileId);
        values.put(ContentDescriptor.Grimaces.Cols.SELLING, mSelling ? 1 : 0);
        values.put(ContentDescriptor.Grimaces.Cols.BOUGHT, mBought ? 1 : 0);
        values.put(ContentDescriptor.Grimaces.Cols.EFFECT, mEffect);
        values.put(ContentDescriptor.Grimaces.Cols.TO_UPLOAD, mToUpload);
        return values;
    }

    public static Grimace fromCursor(Cursor cursor) {
        Grimace grimace = new Grimace();
        grimace.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.ID)));
        grimace.setUrl(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.URL)));
        grimace.setAuthorId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.ID_AUTHOR)));
        grimace.setSmileId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.ID_SMILE)));
        grimace.setBought(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.BOUGHT)) == 1);
        grimace.setSelling(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.SELLING)) == 1);
        grimace.setToUpload(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.TO_UPLOAD)));
        grimace.setEffect(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Grimaces.Cols.EFFECT)));
        if (cursor.getColumnIndex(ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT) > -1) {
            grimace.useCount = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.GrimaceStatistic.Cols.USE_COUNT));
        }
        return grimace;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(int authorId) {
        mAuthorId = authorId;
    }

    public int getSmileId() {
        return mSmileId;
    }

    public void setSmileId(int smileId) {
        mSmileId = smileId;
    }

    public boolean isSelling() {
        return mSelling;
    }

    public void setSelling(boolean selling) {
        mSelling = selling;
    }

    public boolean isBought() {
        return mBought;
    }

    public void setBought(boolean bought) {
        mBought = bought;
    }

    @Override
    public int compareTo(Grimace another) {
        return 0;
    }

    public int getLocalId() {
        return mLocalId;
    }

    public void setLocalId(int localId) {
        mLocalId = localId;
    }

    public int getToUpload() {
        return mToUpload;
    }

    public void setToUpload(int toUpload) {
        mToUpload = toUpload;
    }

    public int getEffect() {
        return mEffect;
    }

    public void setEffect(int effect) {
        mEffect = effect;
    }
}
