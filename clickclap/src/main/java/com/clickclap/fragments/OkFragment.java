package com.clickclap.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clickclap.R;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.UploadingMedia;
import com.humanet.humanetcore.views.dialogs.ShareDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by ovi on 20.05.2016.
 */
public class OkFragment extends BaseTitledFragment implements View.OnClickListener {

    public static final int EMOTICON_OK = 1;
    public static final int VIDEO_OK = 2;
    public static final int VERIFY_OK = 3;
    public static final int PROFILE_OK = 4;


    private int mAction;

    @Override
    public String getTitle() {
        return null;
    }

    public static OkFragment newInstance(int action) {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.EXTRA_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }

    private OnCongratulationListener mOpenVerificationListener;

    public OkFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ok, container, false);

        mAction = getArguments().getInt(Constants.EXTRA_ACTION);

        Spanned info1;
        TextView infoView1 = (TextView) view.findViewById(R.id.info_text);
        TextView infoView2 = (TextView) view.findViewById(R.id.info_text_2);
        switch (mAction) {
            case EMOTICON_OK:
                infoView1.setText("");
                infoView2.setText(getString(R.string.greeting_1));
                view.findViewById(R.id.btn_next).setEnabled(false);

                getLoader().show();

                break;
            case VIDEO_OK:
                infoView1.setVisibility(View.GONE);
                infoView2.setText(R.string.greeting_2);
                break;
            case VERIFY_OK:
                infoView1.setVisibility(View.GONE);
                infoView2.setText(R.string.coin_info3);
                break;
            case PROFILE_OK:
                info1 = Html.fromHtml(getString(R.string.plus50));
                infoView1.setText(info1);
                infoView2.setText(R.string.coin_info4);
                break;
        }

        view.findViewById(R.id.btn_next).setOnClickListener(this);

        return view;
    }


    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UploadingMedia uploadingMedia) {
        if (getView() != null) {
            getView().findViewById(R.id.btn_next).setEnabled(true);

            getLoader().dismiss();

            new ShareDialog.Builder()
                    .setCanBeClosed(false)
                    .setLink(uploadingMedia.getShortUrl())
                    .setGifLink(ShareDialog.getGifUrl(uploadingMedia.getVideoId()))
                    .setVideoId(uploadingMedia.getVideoId())
                    .setShareType(ShareDialog.TYPE_EMOTICON)
                    .build()
                    .show(getFragmentManager(), ShareDialog.TAG);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (mAction) {
            case EMOTICON_OK:
                mOpenVerificationListener.onVideoShared();
                break;
            case VIDEO_OK:
                mOpenVerificationListener.onOpenVerification();
                break;
            case VERIFY_OK:
                mOpenVerificationListener.onOpenFillProfile();
                break;
            case PROFILE_OK:
                mOpenVerificationListener.onRegistrationProcedureComplete();
                break;
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOpenVerificationListener = (OnCongratulationListener) activity;
    }

    public interface OnCongratulationListener {
        void onVideoShared();

        void onOpenVerification();

        void onOpenFillProfile();

        void onRegistrationProcedureComplete();
    }
}
