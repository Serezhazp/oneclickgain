package com.clickclap.fragments.grimaces;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.clickclap.R;
import com.humanet.humanetcore.fragments.capture.BaseVideoCreationFragment;
import com.humanet.humanetcore.model.NewVideoInfo;
import com.humanet.humanetcore.model.VideoInfo;
import com.humanet.humanetcore.views.widgets.CircleView;

/**
 * Created by ovi on 23.05.2016.
 */
public class GrimaceCapturedPreviewFragment extends BaseVideoCreationFragment implements View.OnClickListener {


    private OnGrimaceVideoCommandSelected mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mListener = (OnGrimaceVideoCommandSelected) getActivity();
    }

    @Override
    public boolean isCanPlayVideo() {
        return false;
    }

    private View mFilterButton;

    private boolean mFrameSelected = false;
    private boolean mFilterSelected = false;

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grimace_capture_preview, container, false);

        if (NewVideoInfo.get().isAvatar() == VideoInfo.AVATAR) {
            ((TextView) view.findViewById(com.humanet.humanetcore.R.id.header)).setText(com.humanet.humanetcore.R.string.video_recording_title);
        }

        CircleView circleView = (CircleView) view.findViewById(com.humanet.humanetcore.R.id.video_view);

        mImageView = circleView.getPreviewView();
        mProgressBar = circleView.getProgressBar();
        mStartVideoButton = circleView.getPlayButton();
        mStartVideoButton.setVisibility(View.GONE);
        mSurfaceView = circleView.getSurfaceView();

        circleView.setPlayButtonClickListener(this);

        mImage = VideoProcessTask.getInstance().getPreview();
        if (mImage == null) {
            VideoProcessTask.getInstance().applyVideoFilter();
            onVideoProcessStatus(Status.DONE);
        }

        mNextButton = (ImageButton) view.findViewById(com.humanet.humanetcore.R.id.video_displaying_button_next);
        mNextButton.setOnClickListener(this);

        view.findViewById(com.humanet.humanetcore.R.id.select_preview).setOnClickListener(this);

        mFilterButton = view.findViewById(com.humanet.humanetcore.R.id.select_filter);
        mFilterButton.setOnClickListener(this);
        mFilterButton.setEnabled(mFrameSelected);

        view.findViewById(com.humanet.humanetcore.R.id.video_displaying_make_new).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == com.humanet.humanetcore.R.id.select_preview) {
            mListener.onOpenFramePreview();

            mFilterButton.setEnabled(true);

            mFrameSelected = true;
            validateNextButton();

        } else if (i == com.humanet.humanetcore.R.id.select_filter) {
            mListener.onOpenFilter();
            mFilterSelected = true;
            validateNextButton();

        } else if (i == com.humanet.humanetcore.R.id.video_displaying_button_next) {
            if (mListener.onComplete(0)) {
                mNextButton.setEnabled(false);
            }

        } else if (i == com.humanet.humanetcore.R.id.video_displaying_make_new) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onVideoProcessStatus(final Status status) {
        View view = getView();
        if (view == null) return;

        view.post(new Runnable() {
            @Override
            public void run() {
                mImage = VideoProcessTask.getInstance().getPreview();
                mImageView.setImageBitmap(mImage);
            }
        });
        if (status.equals(Status.DONE) || status.equals(Status.PREVIEW_COMPLETE)) {

            view.post(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mImageView.setVisibility(View.VISIBLE);
                }
            });
        }

        if (mNextButton != null) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    mNextButton.setEnabled(status.equals(Status.DONE) && mFilterSelected && mFrameSelected);
                }
            });
        }
    }

    private void validateNextButton() {
        mNextButton.setEnabled(mFilterSelected && mFrameSelected);
    }

    public interface OnGrimaceVideoCommandSelected {
        void onOpenFramePreview();

        void onOpenFilter();

        boolean onComplete(int action);
    }
}
