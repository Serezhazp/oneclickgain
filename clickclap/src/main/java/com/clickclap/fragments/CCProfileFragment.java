package com.clickclap.fragments;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.clickclap.R;
import com.clickclap.api.requests.GetGrimacesRequest;
import com.clickclap.api.responses.GrimacesListResponse;
import com.clickclap.models.Grimace;
import com.clickclap.models.enums.Emotion;
import com.clickclap.widgets.grimaces.GrimacesPagerAdapter;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.events.SimpleLoaderCallbacks;
import com.humanet.humanetcore.fragments.profile.ProfileFragment;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by ovi on 23.05.2016.
 */
public class CCProfileFragment extends ProfileFragment {
    private final static int LOADER_ID = 13234;

    public static ProfileFragment newInstance(int userId) {
        CCProfileFragment fragment = new CCProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.PARAMS.USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public int[] getTabNames() {
        return new int[]{
                R.string.menu_grimaces,
                com.humanet.humanetcore.R.string.view_profile_tab_questionary,
                com.humanet.humanetcore.R.string.view_profile_tab_statistic,
        };
    }

    private ViewPager mViewPager;
    private LinearLayout mEmotionsContainer;
    private static final int COUNT_PER_PAGE = 3;
    private int mCellSize;
    private GrimacesPagerAdapter mPagerAdapter;

    @Override
    protected View getVideoListView() {
        View grimacesGrid = getActivity().getLayoutInflater().inflate(R.layout.grimaces_grid, (ViewGroup) getView(), false);

        mViewPager = (ViewPager) grimacesGrid.findViewById(R.id.grimaces_pager);
        mEmotionsContainer = (LinearLayout) grimacesGrid.findViewById(R.id.emotions_container);

        mCellSize = (int) ((float) App.WIDTH / (COUNT_PER_PAGE + 1));
        mPagerAdapter = new GrimacesPagerAdapter(mCellSize);
        mViewPager.setAdapter(mPagerAdapter);

        mEmotionsContainer.getLayoutParams().width = mCellSize;

        getActivity().getSupportLoaderManager().restartLoader(LOADER_ID, null, mCursorLoaderCallbacks);

        loadGrimaces();
        return grimacesGrid;
    }

    @Override
    protected void showTokenButton() {
        Button toolbarButton = (Button) ((BaseActivity) getActivity()).getToolbar().findViewById(com.humanet.humanetcore.R.id.token);
        toolbarButton.setVisibility(View.VISIBLE);
        toolbarButton.setTag(getUserId());
        toolbarButton.setOnClickListener(this);

        if (getUserId() == AppUser.getInstance().getUid()) {
            toolbarButton.setBackgroundResource(R.drawable.ic_token);
            toolbarButton.setText(String.valueOf(AppUser.getInstance().get().getTokens()));
            toolbarButton.setTextColor(Color.BLACK);
        } else {
            toolbarButton.setBackgroundResource(com.humanet.humanetcore.R.drawable.ic_phone);
            toolbarButton.setText("");
        }
    }

    @Override
    protected void loadInitialFlowList() {
        // leave it blank for
    }

    private void loadGrimaces() {
        getSpiceManager().execute(
                new GetGrimacesRequest(Grimace.TYPE_USER, getArguments().getInt(Constants.PARAMS.USER_ID)),
                new SimpleRequestListener<GrimacesListResponse>());
    }

    private LoaderManager.LoaderCallbacks<Cursor> mCursorLoaderCallbacks = new SimpleLoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(),
                    ContentDescriptor.Grimaces.URI,
                    null,
                    ContentDescriptor.Grimaces.Cols.TYPE + " = " + Grimace.TYPE_USER + " AND " + ContentDescriptor.Grimaces.Cols.ID_AUTHOR + " = " + getUserId(),
                    null,
                    ContentDescriptor.Grimaces.Cols.ID_SMILE + " DESC");
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            super.onLoadFinished(loader, data);
            loadCollection(data);
        }
    };

    private void loadCollection(Cursor data) {
        final ArrayList<LinkedHashMap<Integer, Grimace[]>> pages = new ArrayList<>();
        mEmotionsContainer.removeAllViews();
        final ArrayList<Integer> emotions = new ArrayList<>();
        final ArrayList<Grimace> list = new ArrayList<>();
        if (data != null && data.moveToFirst()) {
            do {
                Grimace grimace = Grimace.fromCursor(data);
                list.add(grimace);
                int smileId = grimace.getSmileId();
                if (!emotions.contains(smileId)) {
                    emotions.add(smileId);
                    addEmotionToContainer(smileId);
                }
            } while (data.moveToNext());
        }

        mEmotionsContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (mEmotionsContainer.getHeight() > 0) {
                    mViewPager.getLayoutParams().height = mEmotionsContainer.getHeight();

                    int positionOnCurPage = 0;
                    int prevSmileId = 0;
                    int pageNum = 0;
                    for (Grimace grimace : list) {
                        int curSmileId = grimace.getSmileId();
                        if (curSmileId != prevSmileId) {
                            pageNum = 0;
                            positionOnCurPage = 0;
                        } else if (positionOnCurPage >= COUNT_PER_PAGE) {
                            pageNum++;
                            positionOnCurPage = 0;
                        }

                        if (pageNum > pages.size() - 1) {
                            LinkedHashMap<Integer, Grimace[]> page = new LinkedHashMap<>();
                            for (int emotion : emotions) {
                                page.put(emotion, new Grimace[COUNT_PER_PAGE]);
                            }
                            positionOnCurPage = 0;
                            pages.add(page);
                        }

                        LinkedHashMap<Integer, Grimace[]> page = pages.get(pageNum);

                        Grimace[] grimaces = page.get(curSmileId);
                        grimaces[positionOnCurPage] = grimace;

                        prevSmileId = curSmileId;
                        positionOnCurPage++;
                    }

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        mEmotionsContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mEmotionsContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }

                    if (!mPagerAdapter.isNewDataIdentical(pages)) {
                        int type;
                        if (AppUser.getInstance().getUid() == getUserId()) {
                            type = Grimace.TYPE_COLLECTION;
                        } else {
                            type = Grimace.TYPE_MARKET;
                        }
                        mPagerAdapter.setData(pages, type);
                    }
                }
            }
        });
    }

    private void addEmotionToContainer(int emotionId) {
        Context context = getActivity();
        if (context != null) {
            int margin = (int) ConverterUtil.dpToPix(getActivity(), 5);
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mCellSize - (margin * 2), mCellSize - (margin * 2));
            params.setMargins(margin, margin, margin, margin);
            imageView.setLayoutParams(params);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            ImageLoader.getInstance().displayImage(Emotion.getByIdNonNull(emotionId).getDrawable(), imageView);
            mEmotionsContainer.addView(imageView);
        }
    }

}
