package com.clickclap.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clickclap.R;

/**
 * Created by ovi on 24.05.2016.
 */
public class MonCoinAboutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_moncoins_about, container, false);
        TextView textView = (TextView) view.findViewById(R.id.about_moncoin);
        textView.setText(Html.fromHtml(getString(R.string.about_moncoin)));
        return view;
    }
}
