package com.clickclap.fragments.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by ovi on 21.05.2016.
 */
public class ProfilePrivateInfoFragment extends com.humanet.humanetcore.fragments.profile.ProfilePrivateInfoFragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        boolean isEdit = getArguments() != null && getArguments().containsKey("edit") && getArguments().getBoolean("edit");
        if (!isEdit)
            view.findViewById(com.humanet.humanetcore.R.id.avatar).setOnClickListener(null);
    }

}
