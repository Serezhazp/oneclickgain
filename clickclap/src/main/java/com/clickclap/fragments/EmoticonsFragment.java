package com.clickclap.fragments;

import com.clickclap.R;
import com.clickclap.activities.CaptureActivity;
import com.humanet.humanetcore.fragments.base.BaseVideoCategoryPickerFragment;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovi on 22.05.2016.
 */
public class EmoticonsFragment extends BaseVideoCategoryPickerFragment {
    @Override
    protected VideoType[] getVideoTypes() {
        return new VideoType[]{
                VideoType.EMOTICON_ALL,
                VideoType.EMOTICON_BEST,
                VideoType.EMOTICON_MY_CHOICE,
        };
    }

    @Override
    protected String[] getTabNames() {
        return new String[]{
                getString(R.string.emoticons_all),
                getString(R.string.emoticons_best),
                getString(R.string.emoticons_my_choice),
        };
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_emoticons);
    }

    @Override
    protected void addNewVideo() {
        CaptureActivity.startNewEmoticonInstance(getActivity(), 100);
    }
}
