package com.clickclap.fragments;

import android.view.View;

import com.humanet.humanetcore.R;
import com.humanet.humanetcore.fragments.info.FeedbackFragment;

/**
 * Created by Denis on 03.03.2015.
 */
public class CCFeedbackFragment extends FeedbackFragment  {

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.btn_add) {
            openFeedback(0);
        }
    }


}
