package com.clickclap.fragments.video;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.api.requests.GuessVideoSmileRequest;
import com.clickclap.models.enums.Emotion;
import com.humanet.humanetcore.api.listeners.SimpleRequestListener;
import com.humanet.humanetcore.api.response.BaseResponse;
import com.humanet.humanetcore.db.ContentDescriptor;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.modules.media.IMediaEventListener;
import com.humanet.humanetcore.modules.media.MediaFacade;
import com.humanet.humanetcore.presenters.video.VideoCacherPresenter;
import com.humanet.humanetcore.utils.FilePathHelper;
import com.humanet.humanetcore.views.IVideoCacheView;
import com.humanet.humanetcore.views.dialogs.DialogBuilder;
import com.humanet.humanetcore.views.widgets.CircleImageView;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;
import com.humanet.humanetcore.views.widgets.YellowProgressBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Владимир on 25.11.2014.
 */
public class GuessSmileFragment extends BaseTitledFragment implements
        CircleLayout.OnItemSelectedListener, View.OnClickListener,
        IVideoCacheView,
        IMediaEventListener {

    private View mNextButton;

    private SmileListAdapter mAdapter;

    private ImageButton mStartVideoButton;

    ImageView mPreviewImage;
    CircleLayout mCircleLayout;

    private Video mVideo;
    private Emotion mSelectedSmile;

    private YellowProgressBar mYellowProgressBar;

    private VideoCacherPresenter videoCacherPresenter;

    private MediaFacade mMediaFacade;

    public static GuessSmileFragment newInstance(Video video) {
        GuessSmileFragment fragment = new GuessSmileFragment();
        Bundle args = new Bundle();
        args.putSerializable("video", video);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_guess_smile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        videoCacherPresenter = new VideoCacherPresenter();
        videoCacherPresenter.setSpiceManager(getSpiceManager());
        videoCacherPresenter.setVideoCacheView(this);

        mVideo = (Video) getArguments().getSerializable("video");

        CircleView circleView = (CircleView) view.findViewById(R.id.video_view);
        mYellowProgressBar = circleView.getProgressBar();
        mPreviewImage = circleView.getPreviewView();
        mStartVideoButton = circleView.getPlayButton();

        circleView.setPlayButtonClickListener(this);


        mCircleLayout = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mCircleLayout.setOnItemSelectedListener(this);

        mNextButton = view.findViewById(R.id.btn_next);
        mNextButton.setOnClickListener(this);

        mMediaFacade = circleView.getFlowMediaFacade();
        mMediaFacade.setMediaEventListener(this);

        //init smiles mVoteList
        mAdapter = new SmileListAdapter(getActivity(), createSmileArray());
        mCircleLayout.setAdapter(mAdapter);
        mCircleLayout.setSelectedItem(2);

        ImageLoader.getInstance().displayImage(mVideo.getThumbUrl(), mPreviewImage);

        videoCacherPresenter.runCacher(mVideo.getUrl());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        videoCacherPresenter.stopCache();
    }

    @Override
    public void publishCachingProgress(float progress) {
        mYellowProgressBar.setVisibility(View.VISIBLE);
        mYellowProgressBar.setProgress((int) progress);
    }

    @Override
    public void onVideoPlayingStarted(MediaFacade mediaFacade, MediaPlayer mediaPlayer) {
        mStartVideoButton.setVisibility(View.INVISIBLE);
        mPreviewImage.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onVideoPlayingCompleted(MediaFacade mediaFacade, MediaPlayer mediaPlayer) {
        mStartVideoButton.setVisibility(View.VISIBLE);
        mPreviewImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void publishCachingComplete(boolean success) {
        mYellowProgressBar.setVisibility(View.INVISIBLE);
        mStartVideoButton.setVisibility(View.VISIBLE);
        mCircleLayout.setVisibility(View.VISIBLE);

        mMediaFacade.play(new File(FilePathHelper.getVideoCacheDirectory(), FilenameUtils.getName(mVideo.getUrl())).getAbsolutePath(), false);
    }

    @Override
    public void onStop() {
        mMediaFacade.closeMediaPlayer();
        super.onStop();
    }

    @Override
    public void onItemSelected(Object data) {
        mSelectedSmile = (Emotion) data;
    }

    /**
     * Create smiles and init them.
     */
    private ArrayList<Emotion> createSmileArray() {
        Emotion videoEmotion = null;
        Emotion[] emotions = Emotion.values();

        ArrayList<Emotion> emotionList = new ArrayList<>(emotions.length);
        Collections.addAll(emotionList, emotions);

        for (int i = emotionList.size() - 1; i >= 0; i--) {
            Emotion emotion = emotionList.get(i);
            if (emotion.getId() == mVideo.getSmileId()) {
                videoEmotion = emotion;
                emotionList.remove(i);
            }
        }

        Collections.shuffle(emotionList);
        while (emotionList.size() > 4) {
            emotionList.remove(3);
        }
        emotionList.add(videoEmotion);
        Collections.shuffle(emotionList);

        return emotionList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                checkChosenSmile();
                break;

            case R.id.play_button:
                mMediaFacade.play(new File(FilePathHelper.getVideoCacheDirectory(), FilenameUtils.getName(mVideo.getUrl())).getAbsolutePath());
                break;

        }

        Log.e("asd", v.toString());
    }

    @Override
    public void onPause() {
        super.onPause();
        mMediaFacade.closeMediaPlayer();
    }


    private void checkChosenSmile() {
        Emotion smile = mSelectedSmile;
        if (smile.getId() == mVideo.getSmileId()) {
            showMessageAboutWin();
        } else {
            showMessageAboutFail();
        }

        getSpiceManager().execute(
                new GuessVideoSmileRequest(mVideo.getVideoId(), mSelectedSmile.getId()),
                new SimpleRequestListener<BaseResponse>()
        );
    }

    private void showMessageAboutWin() {
        ContentValues values = new ContentValues(1);
        values.put(ContentDescriptor.Videos.Cols.GUESSED, 1);
        getActivity().getContentResolver().update(ContentDescriptor.Videos.URI,
                values,
                ContentDescriptor.Videos.Cols.ID + " = " + mVideo.getVideoId(),
                null);

        new DialogBuilder(getActivity())
                .setMessage(R.string.guess_smile_win)
                .setPositiveButton(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().onBackPressed();
                    }
                })
                .create()
                .show();
    }

    private void showMessageAboutFail() {
        new DialogBuilder(getActivity())
                .setMessage(R.string.guess_smile_fail)
                .setNegativeButton(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().onBackPressed();
                    }
                })
                .create()
                .show();
    }

    @Override
    public String getTitle() {
        return getActivity().getString(R.string.guess_emotion);
    }

    private static class SmileListAdapter extends BaseAdapter {

        private Context mContext;
        private ArrayList<Emotion> mData;

        public SmileListAdapter(Context context, ArrayList<Emotion> list) {
            mContext = context;
            mData = list;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Emotion getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SmileItemView view = (SmileItemView) convertView;
            if (view == null) {
                view = new SmileItemView(mContext);
            }

            Emotion smile = getItem(position);

            view.setData(smile);

            return view;
        }
    }

    private static class SmileItemView extends CircleLayout.ItemWrapper {

        private CircleImageView mImageView;
        private TextView mTextView;
        private Emotion mData;

        public SmileItemView(Context context) {
            super(context);
            View.inflate(context, com.humanet.humanetcore.R.layout.item_circle, this);
            setOrientation(LinearLayout.VERTICAL);


            mImageView = (CircleImageView) findViewById(com.humanet.humanetcore.R.id.item_image);
            mImageView.setBorderSize(getResources().getDimensionPixelOffset(com.humanet.humanetcore.R.dimen.stream_item_small_border));

            mTextView = (TextView) findViewById(com.humanet.humanetcore.R.id.item_title);
        }


        public void setData(Emotion data) {
            if(data == null)
                return;
            mData = data;
            ImageLoader.getInstance().displayImage(mData.getDrawable(), mImageView, DisplayImageOptions.createSimple());
            mTextView.setText(mData.getTitle());
        }
    }
}
