package com.clickclap.fragments.video;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.clickclap.ClickclapApp;
import com.clickclap.R;
import com.clickclap.models.enums.Emotion;
import com.humanet.humanetcore.views.utils.ConverterUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by ovi on 20.05.2016.
 */
public class VideoRecordFragment extends com.humanet.humanetcore.fragments.capture.VideoRecordFragment {

    private static final String EXTRA_SMILE = "EXTRA_SMILE";

    //TODO this method can be removed because NewVideo contains smileId that can be used instead fragment argument
    public static VideoRecordFragment newInstance(int smileId) {
        Bundle args;
        VideoRecordFragment f = new VideoRecordFragment();
        args = new Bundle();
        args.putInt(EXTRA_SMILE, smileId);
        f.setArguments(args);
        return f;
    }

    //private ImageView mSmileImageView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments().getInt(EXTRA_SMILE) == 0) {
            return super.onCreateView(inflater, container, savedInstanceState);
        } else {
            FrameLayout frameLayout = new FrameLayout(container.getContext());
            View view = inflater.inflate(com.humanet.humanetcore.R.layout.fragment_video_record, frameLayout, false);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            frameLayout.addView(view);
            return frameLayout;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Emotion emotion;
        if ((emotion = Emotion.getById(getArguments().getInt(EXTRA_SMILE))) != null) {
            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    View circleView = view.findViewById(R.id.surface_view);

                    if (circleView.getHeight() != 0) {
                        ImageView mSmileImageView = new ImageView(getActivity());

                        int smileSize = (int) ConverterUtil.dpToPix(getActivity(), 70);

                        mSmileImageView.setLayoutParams(new ViewGroup.LayoutParams(smileSize, smileSize));

                        int smileTop = circleView.getTop() + circleView.getHeight() - smileSize / 2;

                        mSmileImageView.setX(ClickclapApp.WIDTH / 2 - smileSize / 2);
                        mSmileImageView.setY(smileTop);

                        ImageLoader.getInstance().displayImage(emotion.getDrawable(), mSmileImageView);

                        ((ViewGroup) view).addView(mSmileImageView);

                        view.removeOnLayoutChangeListener(this);
                    }
                }
            });

        }
    }
}
