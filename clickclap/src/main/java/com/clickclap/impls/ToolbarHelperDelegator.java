package com.clickclap.impls;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.clickclap.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.utils.ToolbarHelper;

/**
 * Created by ovi on 23.05.2016.
 */
public class ToolbarHelperDelegator implements ToolbarHelper.Delegator {
    @Override
    public Toolbar createToolbar(BaseActivity activity) {
        Toolbar toolbar = (Toolbar) activity.findViewById(com.humanet.humanetcore.R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back);
        }
        return toolbar;
    }
}
