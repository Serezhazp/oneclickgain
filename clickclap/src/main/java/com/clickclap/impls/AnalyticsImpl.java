package com.clickclap.impls;

import android.content.Context;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.UserInfo;
import com.humanet.humanetcore.utils.AnalyticsHelper;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ovi on 2/2/16.
 */
public class AnalyticsImpl implements AnalyticsHelper.Analytics {

    public void init(Context context){
        Mixpanel.init(context);
        AppsFlyerLib.setAppsFlyerKey("uy8XpfD8rwR9CNWeuzZAKc");
        AppsFlyerLib.sendTracking(context);
    }

    @Override
    public void identifyUser(UserInfo userInfo) {
        Mixpanel.identifyUser(userInfo);
        AppsFlyerLib.setCustomerUserId(String.valueOf(userInfo.getId()));
    }

    @Override
    public void trackEvent(Context context, @AnalyticsHelper.TrackEventTitle String event) {
        Mixpanel.trackEvent(event);
        //AppsFlyer.trackEvent(context, event);
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.DATE_A, new Date());
        AppsFlyerLib.trackEvent(context, event, eventValue);
    }

    public static class Mixpanel {
        static MixpanelAPI mMixpanel;
        static final String TOKEN = "f6ad06c615aa6a04a8e902c9d1a71e35";

        public static void init(Context context) {
            mMixpanel = MixpanelAPI.getInstance(context, TOKEN);
        }

        public static void identifyUser(UserInfo userInfo) {
            MixpanelAPI  mixpanelAPI = getInstance();
            mixpanelAPI.identify(String.valueOf(userInfo.getId()));
            mixpanelAPI.getPeople().identify(String.valueOf(userInfo.getId()));
            mixpanelAPI.getPeople().set("$first_name", userInfo.getFName());
            mixpanelAPI.getPeople().set("$last_name", userInfo.getLName());
            mixpanelAPI.getPeople().set("App ID", userInfo.getId());
        }

        private static synchronized MixpanelAPI getInstance() {
            if (mMixpanel == null) {
                init(App.getInstance());
            }
            return mMixpanel;
        }

        public static void trackEvent(@AnalyticsHelper.TrackEventTitle String event) {
            getInstance().track(event);
        }
    }

}
