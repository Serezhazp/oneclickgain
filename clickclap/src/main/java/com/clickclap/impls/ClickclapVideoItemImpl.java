package com.clickclap.impls;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.clickclap.R;
import com.clickclap.fragments.video.GuessSmileFragment;
import com.clickclap.models.enums.Emotion;
import com.humanet.humanetcore.AppUser;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.Video;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.widgets.items.VideoItemView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by ovi on 23.05.2016.
 */
public class ClickclapVideoItemImpl implements VideoItemView.ClickclapVideoItemDelegator {

    @Override
    public void displayEmoticon(VideoItemView view, VideoType videoType, Category category, Video videoData) {
        if (videoType != null && (category.isEmoticon() || videoType.isEmoticon())) {
            ImageView image = (ImageView) view.findViewById(R.id.smileIcon);
            image.setScaleType(ImageView.ScaleType.FIT_XY);
            /*
            Emoticons from vistory have NEWS video type. We should give an opportunity to guess emotion only in this case
             */
            if (videoData.isSmileGuessed() || videoData.getAuthorId() == AppUser.getInstance().getUid() || videoType != VideoType.NEWS) {
                ImageLoader.getInstance().displayImage(
                        Emotion.getByIdNonNull(videoData.getSmileId()).getDrawable(),
                        image,
                        DisplayImageOptions.createSimple()
                );
            } else {
                image.setImageResource(R.drawable.question);
                image.setOnClickListener(view);
            }
            image.setVisibility(View.VISIBLE);
        }

        TextView textView = (TextView) view.findViewById(R.id.stream_item_likes);
        textView.setText(String.valueOf(videoData.getLikes()));
        textView.setCompoundDrawablesWithIntrinsicBounds(
                videoData.getMyMark() == 1 ? R.drawable.like_filled : R.drawable.like_empty, //left
                0, 0, 0);

        textView = (TextView) view.findViewById(R.id.stream_item_answers);
        textView.setText(String.valueOf(videoData.getReplays()));
    }

    @Override
    public void emoticonClicked(View view, Context context,Video data) {
        if (view.getId() == R.id.smileIcon) {
            BaseActivity activity = (BaseActivity) context;
            activity.startFragment(GuessSmileFragment.newInstance(data), true);
        }
    }
}
