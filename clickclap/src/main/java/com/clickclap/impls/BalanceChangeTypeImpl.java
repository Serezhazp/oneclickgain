package com.clickclap.impls;

import com.clickclap.R;
import com.humanet.humanetcore.App;
import com.humanet.humanetcore.model.enums.BalanceChangeType;
import com.humanet.humanetcore.utils.BalanceHelper;

/**
 * Created by ovi on 2/5/16.
 */
public class BalanceChangeTypeImpl implements BalanceHelper.BalanceTypeArray {

    enum Type implements BalanceChangeType {

        REGISTRATION {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_registration);
            }

            @Override
            public int getId() {
                return 100;
            }
        },
        FIRST_VIDEO {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_first_video);
            }

            @Override
            public int getId() {
                return 101;
            }
        },
        FIRST_SHARE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_first_share);
            }

            @Override
            public int getId() {
                return 102;
            }
        },
        FILL_EXTENDED_PROFILE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_fill_profile);
            }

            @Override
            public int getId() {
                return 103;
            }
        },
        INVITE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_invite_friend);
            }

            @Override
            public int getId() {
                return 104;
            }
        },
        ADD_VIDEO {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_add_video);
            }

            @Override
            public int getId() {
                return 105;
            }
        },
        VIDEO_SHARE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_share_video);
            }

            @Override
            public int getId() {
                return 106;
            }
        },
        CREATE_VLOG {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_create_vlog);
            }

            @Override
            public int getId() {
                return 107;
            }
        },
        FIRST_100 {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_first_100);
            }

            @Override
            public int getId() {
                return 108;
            }
        },
        LEFT_LIKE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_left_like);
            }

            @Override
            public int getId() {
                return 109;
            }
        },
        RECEIVE_LIKE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_receive_like);
            }

            @Override
            public int getId() {
                return 110;
            }
        },
        TRY_GUESS_SMILE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_try_guess_smile);
            }

            @Override
            public int getId() {
                return 111;
            }
        },
        GUESS_SMILE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_guess_smile);
            }

            @Override
            public int getId() {
                return 112;
            }
        },
        BUY_WIDGET {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_buy_widget);
            }

            @Override
            public int getId() {
                return 113;
            }
        },
        BUY_GRIMACE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_buy_grimace);
            }

            @Override
            public int getId() {
                return 114;
            }
        },
        ADD_GRIMACE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_add_grimace);
            }

            @Override
            public int getId() {
                return 115;
            }
        },
        SHARE_GRIMACE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_share_grimace);
            }

            @Override
            public int getId() {
                return 116;
            }
        },
        SELL_GRIMACE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_sell_grimace);
            }

            @Override
            public int getId() {
                return 117;
            }
        },
        FILL_PORTFOLIO {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_fill_portfolio);
            }

            @Override
            public int getId() {
                return 118;
            }
        },
        SUBSCRIBE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_subscribe);
            }

            @Override
            public int getId() {
                return 119;
            }
        },
        NEW_SUBSCRIBER {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_new_subscriber);
            }

            @Override
            public int getId() {
                return 120;
            }
        },
        VOTE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_vote);
            }

            @Override
            public int getId() {
                return 121;
            }
        },
        CREATE_POLL {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_create_poll);
            }

            @Override
            public int getId() {
                return 122;
            }
        },
        CROWD_RECEIVE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_crowd_receive);
            }

            @Override
            public int getId() {
                return 124;
            }
        },
        CROWD_GIVE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_crowd_give);
            }

            @Override
            public int getId() {
                return 125;
            }
        },
        CROWD_GIFT_GIVEN {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_crowd_gift_give);
            }

            @Override
            public int getId() {
                return 130;
            }
        },
        CROWD_GIFT_RECEIVED {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_crowd_gift_receive);
            }

            @Override
            public int getId() {
                return 131;
            }
        },
        ALIEN_LOOK_CREATE_TASK {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_alien_look_create_task);
            }

            @Override
            public int getId() {
                return 126;
            }
        },
        ALIEN_LOOK_PERFORM_TASK {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_alien_look_perform_task);
            }

            @Override
            public int getId() {
                return 127;
            }
        },
        ALIEN_LOOK_CREATE_CHALLENGE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_alien_look_create_challenge);
            }

            @Override
            public int getId() {
                return 128;
            }
        },
        ALIEN_LOOK_MEET_CHALLENGE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_alien_look_meet_challenge);
            }

            @Override
            public int getId() {
                return 129;
            }
        },
        RESPONSE {
            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.moncoins_history_response);
            }

            @Override
            public int getId() {
                return 132;
            }
        };

        @Override
        public String getDrawable() {
            return null;
        }
    }


    public BalanceChangeType getById(int id) {
        for (Type type : Type.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }


}
