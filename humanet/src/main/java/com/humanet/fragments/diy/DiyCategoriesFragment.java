package com.humanet.fragments.diy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.R;
import com.humanet.humanetcore.activities.CaptureActivity;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.VideoListFragment;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.model.Category;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.modules.CategoriesDataLoader;
import com.humanet.humanetcore.views.adapters.CategoryListAdapter;
import com.humanet.humanetcore.views.behaviour.CategoriesListView;
import com.humanet.humanetcore.views.widgets.CircleLayout;
import com.humanet.humanetcore.views.widgets.CircleView;

import java.util.List;

/**
 * Created by ovi on 2/3/16.
 */
public class DiyCategoriesFragment extends BaseTitledFragment implements IMenuBindFragment,
        CategoriesListView, CircleLayout.OnItemSelectedListener, View.OnClickListener {

    public static DiyCategoriesFragment newInstance(VideoType videoType, String title) {
        DiyCategoriesFragment fragment = new DiyCategoriesFragment();
        Bundle bundle = new Bundle(1);
        bundle.putSerializable("videoType", videoType);
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getTitle() {
        return getArguments().getString("title");
    }

    private CategoryListAdapter mCategoriesAdapter;
    private CircleLayout mCircleLayout;

    private CircleView mCircleView;

    private CategoriesDataLoader mCategoriesDataLoader;

    private Category mSelectedCategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diy_categories, container, false);

        mCategoriesDataLoader = new CategoriesDataLoader(getSpiceManager(), getLoaderManager(), this, false);
        VideoType videoType = (VideoType) getArguments().getSerializable("videoType");
        mCategoriesDataLoader.loadCategories(videoType);

        view.findViewById(com.humanet.humanetcore.R.id.new_video_shot).setOnClickListener(this);
        view.findViewById(com.humanet.humanetcore.R.id.btn_nav).setOnClickListener(this);


        mCircleView = (CircleView) view.findViewById(com.humanet.humanetcore.R.id.video_preview);

        mCategoriesAdapter = new CategoryListAdapter(getActivity());

        mCircleLayout = (CircleLayout) view.findViewById(com.humanet.humanetcore.R.id.horizontal_list_view);
        mCircleLayout.setOnItemSelectedListener(this);
        mCircleLayout.setAdapter(mCategoriesAdapter);


        return view;
    }

    @Override
    public void setCategories(List<Category> categoryList) {
        if (!isVisible()) {
            return;
        }

        if (categoryList == null || categoryList.size() == 0)
            return;
        //  categoryList = Category.reorder(categoryList);

        if (!mCategoriesAdapter.isNewDataIdentical(categoryList)) {
            mCategoriesAdapter.setData(categoryList);
            mCircleLayout.setAdapter(mCategoriesAdapter);
            mCircleLayout.setSelectedItem(mCategoriesAdapter.getInitialSelectionPosition());
            mCircleLayout.invalidateAll();
        }
    }


    @Override
    public void onItemSelected(Object data) {
        mSelectedCategory = (Category) data;
        mCircleView.setImage(mSelectedCategory.getImage());
    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == com.humanet.humanetcore.R.id.new_video_shot) {
            addNewVideo();

        } else if (viewId == com.humanet.humanetcore.R.id.btn_nav) {
            loadFlowList();

        }
    }

    private void addNewVideo() {
        Intent intent = CaptureActivity.createNewInstance(getActivity(), (VideoType) getArguments().getSerializable("videoType"));
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            loadFlowList();
        }
    }

    private void loadFlowList() {
        if (mSelectedCategory == null)
            return;

        VideoType videoType = (VideoType) getArguments().getSerializable("videoType");
        String title = getTitle();

        VideoListFragment.Builder builder = new VideoListFragment.Builder(videoType);
        builder.setCategoryType(mSelectedCategory);
        builder.setTitle(title);
        ((BaseActivity) getActivity()).startFragment(builder.build(), true);
    }


    @Override
    public int getSelectedMenuItem() {
        return R.id.do_it;
    }
}
