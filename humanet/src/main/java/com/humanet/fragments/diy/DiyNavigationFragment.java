package com.humanet.fragments.diy;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.R;
import com.humanet.humanetcore.activities.base.BaseActivity;
import com.humanet.humanetcore.events.IMenuBindFragment;
import com.humanet.humanetcore.fragments.base.BaseTitledFragment;
import com.humanet.humanetcore.interfaces.CirclePickerItem;
import com.humanet.humanetcore.model.enums.VideoType;
import com.humanet.humanetcore.views.widgets.CirclePickerView;

import java.util.ArrayList;

/**
 * Created by ovi on 2/3/16.
 */
public class DiyNavigationFragment extends BaseTitledFragment implements
        IMenuBindFragment,
        View.OnClickListener,
        CirclePickerView.OnPickListener {

    private static final int SKILL_ID = 1;
    private static final int ITEM_ID = 2;

    private int mSelectedPickerItem = -1;

    private TabLayout mTabLayout;

    @Override
    public String getTitle() {
        return getString(R.string.diy_title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diy, container, false);

        view.findViewById(R.id.btn_next).setOnClickListener(this);

        mTabLayout = ((TabLayout) view.findViewById(R.id.tabhost));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.diy_action_ask));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.diy_action_share));

        ArrayList<Item> items = new ArrayList<>(2);
        items.add(new Item(R.drawable.ic_diy_item, ITEM_ID, getString(R.string.diy_target_item)));
        items.add(new Item(R.drawable.ic_diy_skill, SKILL_ID, getString(R.string.diy_target_skill)));

        CirclePickerView circlePickerView = (CirclePickerView) view.findViewById(R.id.picker);
        circlePickerView.fill(items, 30, 120, -1, this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                VideoType videoType = getSelectedVideoType();
                String title = getSelectedTitle();
                if (videoType != null)
                    ((BaseActivity) getActivity()).startFragment(DiyCategoriesFragment.newInstance(videoType, title), true);

                break;
        }
    }


    private VideoType getSelectedVideoType() {
        int selectedTab = mTabLayout.getSelectedTabPosition();

        if (mSelectedPickerItem == -1)
            return null;

        if (selectedTab == 0) {
            if (mSelectedPickerItem == SKILL_ID)
                return VideoType.DIY_SHARE_SKILL;
            else
                return VideoType.DIY_SHARE_ITEM;
        } else {
            if (mSelectedPickerItem == SKILL_ID)
                return VideoType.DIY_ASK_SKILL;
            else
                return VideoType.DIY_ASK_ITEM;
        }
    }

    private String getSelectedTitle() {
        TabLayout.Tab tab = mTabLayout.getTabAt(mTabLayout.getSelectedTabPosition());
        if (tab == null || tab.getText() == null)
            throw new NullPointerException("tab is null or has no name");

        String title = tab.getText().toString() + ": ";
        if (mSelectedPickerItem == SKILL_ID)
            title += getString(R.string.diy_target_skill);
        else
            title += getString(R.string.diy_target_item);

        return title;
    }


    @Override
    public void onPick(View view, CirclePickerItem element) {
        mSelectedPickerItem = element.getId();
    }

    //--------------
    @Override
    public int getSelectedMenuItem() {
        return R.id.do_it;
    }

    //--------------
    private class Item implements CirclePickerItem {
        private String mDrawable;
        private int mId;
        private String mTitle;


        public Item(int drawable, int id, String titile) {
            mDrawable = "drawable://" + drawable;
            mId = id;
            mTitle = titile;
        }

        @Override
        public String getDrawable() {
            return mDrawable;
        }

        @Override
        public int getId() {
            return mId;
        }

        public String getTitle() {
            return mTitle;
        }
    }
}
