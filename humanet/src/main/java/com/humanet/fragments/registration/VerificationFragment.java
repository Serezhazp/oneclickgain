package com.humanet.fragments.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.humanet.R;
import com.humanet.humanetcore.fragments.registration.BaseVerificationFragment;

/**
 * Created by ovi on 2/2/16.
 */
public class VerificationFragment extends BaseVerificationFragment {

    public static VerificationFragment newInstance() {
        return new VerificationFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verification, container, false);
        return view;
    }
}
