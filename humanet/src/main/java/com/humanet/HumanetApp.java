package com.humanet;

import com.humanet.humanetcore.App;
import com.humanet.humanetcore.Constants;
import com.humanet.humanetcore.NavigationManager;
import com.humanet.humanetcore.fragments.balance.CurrencyFragment;
import com.humanet.humanetcore.fragments.balance.RatingsTabFragment;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.utils.BalanceHelper;
import com.humanet.humanetcore.utils.GsonHelper;
import com.humanet.impls.BalanceChangeTypeImpl;
import com.humanet.impls.CurrencyTabViewImpl;
import com.humanet.impls.RatingImpl;
import com.humanet.impls.VideoStatisticsImpl;

/**
 * Created by ovi on 1/26/16.
 */
public class HumanetApp extends App {

    static {
        API_APP_NAME = "humanet";
        DEBUG = BuildConfig.DEBUG;

        GsonHelper.BALANCE_SERIALIZATION_KEY = "wow";

        COINS_CODE = "TL";
    }

    @Override
    public void onCreate() {
        super.onCreate();

        BalanceHelper.setsImpl(new BalanceChangeTypeImpl());

        VideoStatistic.setVideoStatisticDelegator(new VideoStatisticsImpl());

        RatingsTabFragment.setImpl(new RatingImpl(this));

        CurrencyFragment.setImpl(new CurrencyTabViewImpl(this));

        NavigationManager.init(new NavigationManagerImpl());
    }

    public String getServerEndpoint() {
        return Constants.API;
    }
}
