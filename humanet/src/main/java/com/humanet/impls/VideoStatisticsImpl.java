package com.humanet.impls;

import com.humanet.R;
import com.humanet.humanetcore.model.VideoStatistic;
import com.humanet.humanetcore.model.enums.VideoType;

/**
 * Created by ovi on 2/23/16.
 */
public class VideoStatisticsImpl implements VideoStatistic.VideoStatisticDelegator {

    @Override
    public VideoStatistic create(VideoStatistic videoStatistic, VideoType videoType, int duration) {
        switch (videoType) {
            case NEWS:
                videoStatistic.setTitleResId(com.humanet.humanetcore.R.string.online_tab_news);
                videoStatistic.setImageResId("assets://statistics/news.png");
                break;
            case VLOG:
                videoStatistic.setTitleResId(com.humanet.humanetcore.R.string.online_tab_vlog);
                videoStatistic.setImageResId("assets://statistics/vlog.png");
                break;
            case MARKET_SELL:
            case MARKET_SHARE:
                videoStatistic.setImageResId("assets://statistics/market.png");
                videoStatistic.setTitleResId(com.humanet.humanetcore.R.string.market);
                break;

            case DIY_SHARE_ITEM:
            case DIY_SHARE_SKILL:
            case DIY_ASK_ITEM:
            case DIY_ASK_SKILL:
                videoStatistic.setImageResId("assets://statistics/diy.png");
                videoStatistic.setTitleResId(R.string.diy_title);
                break;
        }

        return videoStatistic;
    }
}
